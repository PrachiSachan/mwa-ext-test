package com.sony.pro.mwa.activity.utility.recoder.enumeration;

import com.sony.pro.mwa.rc.GroupId;
import com.sony.pro.mwa.rc.IMWARC;
import com.sony.pro.mwa.rc.MWARCUtil;

public enum RecoderRC implements IMWARC {
	INVALID_INPUT_PORT		("1001"),
	FILE_ALREADY_EXISTS		("1002"),
	DISK_FULL    			("1003"),
	FILE_WRITE_ERROR    	("1004"),
	OUTPUT_PORT    			("1005"),
	PORT_USING    			("1006"),
    ;
	
	/**
	 * TODO RECORDERが実装されたら変更すること
	 */
	private final static String GROUPID = GroupId.RECORDER.id();
	private String localId;
	
	private RecoderRC(String localId) {
		this.localId = localId;
	}
	
	public String code() {
		return MWARCUtil.genMWARC(GROUPID, this.localId);
	}
	
	public boolean isSuccess() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public static String getGroupId() { return GROUPID; }
	
	public static IMWARC valueOfByCode(String code) {
		IMWARC result = null;
		for (IMWARC value : RecoderRC.values()) {
			if (value.code().equals(code)) {
				result = value;
				break;
			}
		}
		return result;
	}
	public static RecoderRC valueOfByName(String name) {
		return RecoderRC.valueOf(name);
	}

}
