package com.sony.pro.mwa.activity.scheme.utils;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

public class ApplicationXml implements IHTTPRequestHeaderProvider {

	private static Header[] headers = {
			new BasicHeader("Content-type", "application/xml; charset=UTF-8"),
			new BasicHeader("Accept", "application/xml"),
			new BasicHeader("Accept-Charset", "UTF-8")
	};

	@Override
	public Header[] getHeaders() {
		return headers;
	}
}
