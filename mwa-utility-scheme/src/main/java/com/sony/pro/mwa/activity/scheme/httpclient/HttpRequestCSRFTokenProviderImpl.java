package com.sony.pro.mwa.activity.scheme.httpclient;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

/***
 * NVXにてCSRF対応が必要なHTTPリクエストに対するヘッダ情報を取得する
 */
public class HttpRequestCSRFTokenProviderImpl extends HttpRequestConfigProviderImpl {
	private Header csrfHeader = new BasicHeader("X-XSRF-TOKEN","WlUN+HLwt6iDYRJPnnyo5E9LkM+26ilq67zanyGvvfY=.1487828081876.Gsl+K+XBxhJ4Kd6ppaB3eUofGITOvhmulfb5gFlnGQI=");

	/***
	 * CSRFトークン付のHTTPリクエストヘッダを取得します。
	 */
	@Override
	public Header[] getRequestHeaders() {
		List<Header> buffer = new ArrayList<Header>();
		for (Header h : super.getRequestHeaders()) {
			buffer.add(h);
		}
		buffer.add(this.csrfHeader);
		Header[] response = buffer.toArray(new Header[buffer.size()]);
		return response;
	}
	
	public HttpRequestCSRFTokenProviderImpl(Header[] header) {
		super(header);
	}
	
	public HttpRequestCSRFTokenProviderImpl(Header[] header, int retryRequestCount) {
		super(header, retryRequestCount);
	}
}
