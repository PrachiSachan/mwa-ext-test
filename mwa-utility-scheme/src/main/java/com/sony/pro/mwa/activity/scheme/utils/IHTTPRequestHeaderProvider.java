package com.sony.pro.mwa.activity.scheme.utils;

import org.apache.http.Header;

public interface IHTTPRequestHeaderProvider {

	public Header[] getHeaders();
}
