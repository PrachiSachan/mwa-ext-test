package com.sony.pro.mwa.activity.scheme.httpclient;

public interface IHttpClientWrapper {

	/**
	 * HTTPリクエスト「GET」
	 * @param urlWithQuery リクエストURL
	 * @param provider リクエストに必要なHTTPヘッダ情報を払い出すクラスのインスタンス
	 * @return リクエスト結果
	 * @throws Exception
	 */
	HttpResponseResult get(String urlWithQuery, IHttpRequestConfigProvider provider) throws Exception;

	/**
	 * HTTPリクエスト「POST」
	 * @param urlWithQuery リクエストURL
	 * @param requestBody リクエスト情報
	 * @param provider HTTPヘッダ情報を提供するクラス
	 * @return リクエスト結果
	 * @throws Exception
	 */
	HttpResponseResult post(String urlWithQuery, String requestBody, IHttpRequestConfigProvider provider) throws Exception;

	/**
	 * HTTPリクエスト「PUT」
	 * @param urlWithQuery リクエストURL
	 * @param requestBody リクエスト情報
	 * @param provider HTTPヘッダ情報を提供するクラス
	 * @return リクエスト結果
	 * @throws Exception
	 */
	HttpResponseResult put(String urlWithQuery, String requestBody, IHttpRequestConfigProvider provider) throws Exception;

	/***
	 * HTTPリクエスト「delete」
	 * @param urlWithQuery リクエストURL
	 * @param requestBody リクエスト情報
	 * @param provider HTTPヘッダ情報を提供するクラス
	 * @return リクエスト結果
	 * @throws Exception
	 */
	HttpResponseResult delete(String urlWithQuery, String requestBody, IHttpRequestConfigProvider provider) throws Exception;

	/***
	 * HTTPリクエスト「patch」
	 * @param urlWithQuery リクエストURL
	 * @param requestBody リクエスト情報
	 * @param provider HTTPヘッダ情報を提供するクラス
	 * @return リクエスト結果
	 * @throws Exception
	 */
	HttpResponseResult patch(String urlWithQuery, String requestBody, IHttpRequestConfigProvider provider) throws Exception;
}
