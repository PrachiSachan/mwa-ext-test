package com.sony.pro.mwa.activity.scheme.httpclient;

import org.apache.http.cookie.Cookie;


/**
 * HTTP リクエストの結果を格納する Bean クラスを提供します。
 */
public class HttpResponseResult {
	private int statusCode;
	private String responseBody;
	private Cookie[] cookies;

	/**
	 * コンストラクタ
	 */
	public HttpResponseResult() {}

	/***
	 * コンストラクタ
	 * @param statusCode ステータスコード
	 * @param responseBody 応答データ
	 * @param cookies Cookie情報
	 */
	public HttpResponseResult(int statusCode, String responseBody, Cookie[] cookies) {
		this.statusCode = statusCode;
		this.responseBody = responseBody;
		this.cookies = cookies;
	}

	/**
	 * ステータスコードを取得する
	 * @return ステータスコード
	 */
	public int getStatusCode() {
		return this.statusCode;
	}
	/**
	 * 応答データを取得する
	 * @return リクエストに対する応答データ
	 */
	public String getResponseBody() {
		return this.responseBody;
	}
	/**
	 * Cookie情報を取得する
	 * @return リクエストのCookie情報
	 */
	public Cookie[] getCookies() {
		return this.cookies;
	}

	/**
	 * ステータスコードをセットする
	 * @param statusCode
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * 応答データをセットする
	 * @param responseBody
	 */
	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}

	/**
	 * Cookie情報をセットする
	 * @param cookies
	 */
	public void setCookies(Cookie[] cookies) {
		this.cookies = cookies;
	}
}
