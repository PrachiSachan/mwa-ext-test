package com.sony.pro.mwa.activity.scheme.utils;

public enum HTTPRequestHeaderSet {
	ApplicationJSON,
	ApplicationXml,
	ApplicationXmlWithURLEncoded
}
