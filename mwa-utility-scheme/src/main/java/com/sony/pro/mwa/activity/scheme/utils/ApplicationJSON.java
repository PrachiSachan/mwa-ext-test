package com.sony.pro.mwa.activity.scheme.utils;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

public class ApplicationJSON implements IHTTPRequestHeaderProvider {

	/**
	 * defaultのHTTPヘッダの設定
	 */
	private final static Header[] headers = {
			new BasicHeader("Content-type", "application/json; charset=UTF-8"),
			new BasicHeader("Accept", "application/json"),
			new BasicHeader("Accept-Charset", "UTF-8") };

	@Override
	public Header[] getHeaders() {
		return headers;
	}

}
