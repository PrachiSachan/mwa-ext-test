package com.sony.pro.mwa.activity.scheme.utils;

import java.util.HashMap;
import java.util.Map;

public class HTTPRequestHeaderProviderFactory {

	/*
	private static IHTTPRequestHeaderProvider applicationJson;
	private static IHTTPRequestHeaderProvider applicationXmlWithURLEncoded;
	*/
	private static Map<HTTPRequestHeaderSet, IHTTPRequestHeaderProvider> providerMap = new HashMap<HTTPRequestHeaderSet, IHTTPRequestHeaderProvider>();

	static {
		providerMap.put(HTTPRequestHeaderSet.ApplicationJSON, new ApplicationJSON());
		providerMap.put(HTTPRequestHeaderSet.ApplicationXml, new ApplicationXml());
		providerMap.put(HTTPRequestHeaderSet.ApplicationXmlWithURLEncoded, new ApplicationXmlWithURLEncoded());
	}

	public static IHTTPRequestHeaderProvider get(HTTPRequestHeaderSet httpRequestHeaderSet) {
		return providerMap.get(httpRequestHeaderSet);
	}
}
