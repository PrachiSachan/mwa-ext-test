package com.sony.pro.mwa.activity.scheme.httpclient;

import org.apache.http.Header;

/**
 * HTTP リクエストに関わるコンフィグ情報を提供するインタフェース。
 */
public interface IHttpRequestConfigProvider {
	/**
	 * リクエストに必要なHTTPヘッダ情報を取得する。
	 * @return リクエストにセットするHTTPヘッダ情報
	 */
	Header[] getRequestHeaders();
	
	/**
	 * リクエストに失敗した場合におけるリトライ回数を取得する。
	 * @return 最大リトライ回数
	 */
	int getRetryRequestCount();
}
