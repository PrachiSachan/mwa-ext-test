package com.sony.pro.mwa.activity.scheme.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.GenericPropertyModel;
import com.sony.pro.mwa.model.provider.ActivityProviderCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.model.provider.ServiceEndpointModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;

/**
 * エンドポイント生成ユーティリティ
 * @author Sony
 *
 */
public class EndpointProvider {

	private final static MwaLogger logger = MwaLogger.getLogger(EndpointProvider.class);

	/**
	 * MWAプロバイダ情報を取得するキー
	 */
	private static final String MWA_PROVIDER_NAME = "MWA_LOCAL";
	/**
	 * NVXプロバイダ情報を取得する際のキー
	 */
	private static final String NVX_PROVIDER_NAME = "NVX_LOCAL";
	/**
	 * ODS Clientプロバイダ情報を取得するキー
	 */
	private static final String ODS_CLIENT_PROVIDER_NAME = "ODA Client Application";

	private static final String DEFALUT_SCHEME = "https";
	private static final String DEFALUT_HOST = "localhost";
	private static final int DEFALUT_PORT = 443;

	public static final String MAP_KEY_SCHEME = "scheme";
	public static final String MAP_KEY_HOST = "host";
	public static final String MAP_KEY_PORT = "port";

	/**
	 * 【NVX用】生成したエンドポイントとapiを結合しURLを生成する。
	 * @param coreModule
	 * @param api
	 * @return
	 */
	public static String getURLAsNvxProvider(ICoreModules coreModule, String api) {
		return String.format("%s/%s", getNvxProviderEndpoint(coreModule), api);
	}

	/**
	 * 【MWA用】生成したエンドポイントとapiを結合しURLを生成する。
	 * @param coreModule
	 * @param api
	 * @return
	 */
	public static String getURLAsMwaProvider(ICoreModules coreModule, String api) {
		return String.format("%s/%s", getMwaProviderEndpoint(coreModule), api);
	}

	/**
	 * ProviderIDから生成したエンドポイントとapiを結合しURLを生成する。
	 * @param coreModule
	 * @param api
	 * @param providerId
	 * @return
	 */
	public static String getURLFromProviderId(ICoreModules coreModule, String api, String providerId) {
		return String.format("%s/%s", getProviderIdEndpoint(coreModule, providerId), api);
	}

	/**
	 * 【ODS Client用】生成したエンドポイントとapiを結合しURLを生成する。
	 * @param coreModule
	 * @param api
	 * @return
	 */
	public static List<String> getURLsAsODSClientProvider(ICoreModules coreModule, String api) {
		List<String> urls = new ArrayList<>();
		List<String> endpoints = EndpointProvider.getODSClientProviderEndpoints(coreModule);

		for (String endpoint: endpoints) {
			urls.add(String.format("%s/%s", endpoint, api));
		}

		return urls;
	}

	/**
	 * 【NVX用】CoreModuleの登録情報をもとに、エンドポイントを生成する。
	 * @param coreModule
	 * @return
	 */
	public static String getNvxProviderEndpoint(ICoreModules coreModule) {
		return EndpointProvider.getNvxEndpoint(coreModule, NVX_PROVIDER_NAME, DEFALUT_HOST, DEFALUT_PORT);
	}

	/**
	 * 【MWA用】CoreModuleの登録情報をもとに、エンドポイントを生成する。
	 * @param coreModule
	 * @return
	 */
	public static String getMwaProviderEndpoint(ICoreModules coreModule) {
		return EndpointProvider.getMwaEndpoint(coreModule, MWA_PROVIDER_NAME, DEFALUT_HOST, DEFALUT_PORT);
	}

	/**
	 * 【ODS Client用】CoreModuleの登録情報をもとに、エンドポイントを生成する。
	 * @param coreModule
	 * @return
	 */
	public static List<String> getODSClientProviderEndpoints(ICoreModules coreModule) {
		List<String> endpoints = EndpointProvider.getODSClientEndpoints(coreModule, ODS_CLIENT_PROVIDER_NAME, DEFALUT_HOST, DEFALUT_PORT);
		return endpoints;
	}

	/**
	 * CoreModule、ProviderIDの情報をもとに、エンドポイントを生成する。
	 * @param coreModule
	 * @param providerId
	 * @return
	 */
	public static String getProviderIdEndpoint(ICoreModules coreModule, String providerId) {

		Map<String, Object> resultMap = getEndpointInfoFromProviderId(coreModule, providerId, DEFALUT_HOST, DEFALUT_PORT);
		String scheme = resultMap.get(MAP_KEY_SCHEME).toString();
		String host = resultMap.get(MAP_KEY_HOST).toString();
		int port = Integer.parseInt(resultMap.get(MAP_KEY_PORT).toString());

		logger.debug("getEndpoint() : " + String.format("%s://%s:%d", scheme, host, port));
		return String.format("%s://%s:%d", scheme, host, port);
	}

	/**
	 * 【NVX用】指定されたプロバイダキーでCoreModuleからエンドポイント情報を取得し、エンドポイントを生成する
	 * @param coreModule
	 * @param providerName
	 * @return
	 */
	public static String getNvxEndpoint(ICoreModules coreModule, String providerName, String defaultHost, int defaultPort) {

		String scheme = getNvxScheme(coreModule);
		String host = getNvxHost(coreModule);
		int port = getNvxPort(coreModule);

		logger.debug("getEndpoint() : " + String.format("%s://%s:%d", scheme, host, port));
		return String.format("%s://%s:%d", scheme, host, port);
	}

	/**
	 * 【MWA用】指定されたプロバイダキーでCoreModuleからエンドポイント情報を取得し、エンドポイントを生成する
	 * @param coreModule
	 * @param providerName
	 * @return
	 */
	public static String getMwaEndpoint(ICoreModules coreModule, String providerName, String defaultHost, int defaultPort) {

		String scheme = getMwaScheme(coreModule);
		String host = getMwaHost(coreModule);
		int port = getMwaPort(coreModule);

		logger.debug("getEndpoint() : " + String.format("%s://%s:%d", scheme, host, port));
		return String.format("%s://%s:%d", scheme, host, port);
	}

	/**
	 * 【ODS Client用】指定されたプロバイダキーでCoreModuleからエンドポイント情報を取得し、エンドポイントを生成する
	 * @param coreModule
	 * @param providerName
	 * @return
	 */
	public static List<String> getODSClientEndpoints(ICoreModules coreModule, String providerName, String defaultHost, int defaultPort) {

		List<String> endpoints = new ArrayList<>();
		List<Map<String, Object>> endpointInfos = EndpointProvider.getEndpointInfoList(coreModule, providerName, defaultHost, defaultPort);

		if (!endpointInfos.isEmpty()) {
			for (Map<String, Object> endpointInfo : endpointInfos) {

				String scheme = endpointInfo.get(MAP_KEY_SCHEME).toString();
				String host = endpointInfo.get(MAP_KEY_HOST).toString();
				int port = Integer.parseInt(endpointInfo.get(MAP_KEY_PORT).toString());

				logger.debug("getEndpoint() : " + String.format("%s://%s:%d", scheme, host, port));

				endpoints.add(String.format("%s://%s:%d", scheme, host, port));
			}
		}

		return endpoints;
	}

	/**
	 * 【Nvx用】CoreModuleの登録情報をもとに、Schemeを取得する。
	 * @param coreModule
	 * @return
	 */
	public static String getNvxScheme(ICoreModules coreModule) {
		return getEndpointInfo(coreModule, NVX_PROVIDER_NAME, DEFALUT_HOST, DEFALUT_PORT).get(MAP_KEY_SCHEME).toString();
	}

	/**
	 * 【Mwa用】CoreModuleの登録情報をもとに、Schemeを取得する。
	 * @param coreModule
	 * @return
	 */
	public static String getMwaScheme(ICoreModules coreModule) {
		return getEndpointInfo(coreModule, MWA_PROVIDER_NAME, DEFALUT_HOST, DEFALUT_PORT).get(MAP_KEY_SCHEME).toString();
	}

	/**
	 * 【NVX用】CoreModuleの登録情報をもとに、Hostを取得する。
	 * @param coreModule
	 * @return
	 */
	public static String getNvxHost(ICoreModules coreModule) {
		return getEndpointInfo(coreModule, NVX_PROVIDER_NAME, DEFALUT_HOST, DEFALUT_PORT).get(MAP_KEY_HOST).toString();
	}

	/**
	 * 【MWA用】CoreModuleの登録情報をもとに、Hostを取得する。
	 * @param coreModule
	 * @return
	 */
	public static String getMwaHost(ICoreModules coreModule) {
		return getEndpointInfo(coreModule, MWA_PROVIDER_NAME, DEFALUT_HOST, DEFALUT_PORT).get(MAP_KEY_HOST).toString();
	}

	/**
	 * 【NVX用】CoreModuleの登録情報をもとに、Portを取得する。
	 * @param coreModule
	 * @return
	 */
	public static int getNvxPort(ICoreModules coreModule) {
		return Integer.parseInt(getEndpointInfo(coreModule, NVX_PROVIDER_NAME, DEFALUT_HOST, DEFALUT_PORT).get(MAP_KEY_PORT).toString());
	}

	/**
	 * 【Mwa用】CoreModuleの登録情報をもとに、Portを取得する。
	 * @param coreModule
	 * @return
	 */
	public static int getMwaPort(ICoreModules coreModule) {
		return Integer.parseInt(getEndpointInfo(coreModule, MWA_PROVIDER_NAME, DEFALUT_HOST, DEFALUT_PORT).get(MAP_KEY_PORT).toString());
	}

	/**
	 * providerIdをもとに、エンドポイント情報を取得する。
	 * @param coreModule
	 * @param providerId
	 * @return
	 */
	public static Map<String, Object> getEndpointInfoFromProviderId(ICoreModules coreModule, String providerId) {
		return getEndpointInfoFromProviderId(coreModule, providerId, DEFALUT_HOST, DEFALUT_PORT);
	}

	/**
	 * 指定されたプロバイダキーでCoreModuleからエンドポイント情報を取得する。
	 * @param coreModule
	 * @param providerName
	 * @param defaultHost
	 * @param defaultPort
	 * @return
	 */
	private static Map<String, Object> getEndpointInfo(ICoreModules coreModule, String providerName, String defaultHost, int defaultPort) {
		assert coreModule != null : "CoreModules is null";
		assert !providerName.isEmpty() : "ProviderName is empty";

		String scheme = "";
		String host = "";
		int port = -1;

		ActivityProviderCollection col = coreModule.getActivityProviderManager().getProviders(
				null, Arrays.asList("name" + FilterOperatorEnum.EQUAL.toSymbol() + providerName), null,
				null);

		if (col.getCount() != 1) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PROVIDER_NOT_FOUND, null, null);
		}

		ActivityProviderModel model = col.first();

		if (model.getEndpointList().size() != 1) {
			host = defaultHost;
			port = defaultPort;
			scheme = DEFALUT_SCHEME;
		} else {
			ServiceEndpointModel serviceEndpointModel = model.getEndpointList().get(0);
			List<GenericPropertyModel> propertyList = model.getPropertyList();

			host = serviceEndpointModel.getHost();
			port = serviceEndpointModel.getPort();

			for (GenericPropertyModel propertyModel : propertyList) {
				if ("PROTOCOL".equals(propertyModel.getPropertyName())) {
					scheme = propertyModel.getPropertyValue();
				}
			}

			if (scheme == null || scheme.isEmpty()) {
				// もし存在していない場合には http スキーマとする。
				scheme = DEFALUT_SCHEME;
			}
		}

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put(MAP_KEY_SCHEME, scheme);
		resultMap.put(MAP_KEY_HOST, host);
		resultMap.put(MAP_KEY_PORT, port);

		return resultMap;
	}

	/**
	 * 指定されたプロバイダキーでCoreModuleからエンドポイント情報を取得する。
	 * @param coreModule
	 * @param providerName
	 * @param defaultHost
	 * @param defaultPort
	 * @return
	 */
	private static List<Map<String, Object>> getEndpointInfoList(ICoreModules coreModule, String providerName, String defaultHost, int defaultPort) {
		assert coreModule != null : "CoreModules is null";
		assert !providerName.isEmpty() : "ProviderName is empty";

		String scheme = "";
		String host = "";
		int port = -1;

		ActivityProviderCollection col = coreModule.getActivityProviderManager().getProviders(
				null, Arrays.asList("name" + FilterOperatorEnum.EQUAL.toSymbol() + providerName), null,
				null);

		if (col.getCount() != 1) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PROVIDER_NOT_FOUND, null, null);
		}

		List<ActivityProviderModel> models = col.getModels();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> resultList = new ArrayList<>();

		if (models.isEmpty()) {
			// models が空の場合はレスポンスを空のリストのまま返す
		} else {

			for (ActivityProviderModel model : models) {
				ServiceEndpointModel serviceEndpointModel = model.getEndpointList().get(0);
				List<GenericPropertyModel> propertyList = model.getPropertyList();

				host = serviceEndpointModel.getHost();
				port = serviceEndpointModel.getPort();

				for (GenericPropertyModel propertyModel : propertyList) {
					if ("PROTOCOL".equals(propertyModel.getPropertyName())) {
						scheme = propertyModel.getPropertyValue();
					}
				}

				if (scheme == null || scheme.isEmpty()) {
					// もし存在していない場合には http スキーマとする。
					scheme = DEFALUT_SCHEME;
				}

				resultMap.put(MAP_KEY_SCHEME, scheme);
				resultMap.put(MAP_KEY_HOST, host);
				resultMap.put(MAP_KEY_PORT, port);

				resultList.add(resultMap);
			}
		}

		return resultList;
	}

	/**
	 * 指定されたプロバイダキーでCoreModuleからエンドポイント情報を取得する。
	 * @param coreModule
	 * @param providerName
	 * @param defaultHost
	 * @param defaultPort
	 * @return
	 */
	private static Map<String, Object> getEndpointInfoFromProviderId(ICoreModules coreModule, String providerId, String defaultHost, int defaultPort) {
		assert coreModule != null : "CoreModules is null";
		assert !providerId.isEmpty() : "ProviderId is empty";

		String scheme = "";
		String host = "";
		int port = -1;

		ActivityProviderCollection col = coreModule.getActivityProviderManager().getProviders(
				null, Arrays.asList("id" + FilterOperatorEnum.EQUAL.toSymbol() + providerId), null,
				null);

		if (col.getCount() != 1) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PROVIDER_NOT_FOUND, null, null);
		}

		ActivityProviderModel model = col.first();

		if (model.getEndpointList().size() != 1) {
			host = defaultHost;
			port = defaultPort;
			scheme = DEFALUT_SCHEME;
		} else {
			ServiceEndpointModel serviceEndpointModel = model.getEndpointList().get(0);
			List<GenericPropertyModel> propertyList = model.getPropertyList();

			host = serviceEndpointModel.getHost();
			port = serviceEndpointModel.getPort();

			for (GenericPropertyModel propertyModel : propertyList) {
				if ("ENDPOINT_PROTOCOL".equals(propertyModel.getPropertyName()) || "PROTOCOL".equals(propertyModel.getPropertyName())) {
					scheme = propertyModel.getPropertyValue();
				}
			}

			if (scheme == null || scheme.isEmpty()) {
				// もし存在していない場合には http スキーマとする。
				scheme = DEFALUT_SCHEME;
			}
		}

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put(MAP_KEY_SCHEME, scheme);
		resultMap.put(MAP_KEY_HOST, host);
		resultMap.put(MAP_KEY_PORT, port);

		return resultMap;
	}

}
