package com.sony.pro.mwa.activity.scheme.httpclient;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

import com.sony.pro.mwa.activity.scheme.utils.EndpointProvider;
import com.sony.pro.mwa.common.log.MwaLogger;

/**
 * HTTP リクエスト処理の汎用クラス
 */
public class HttpClientWrapper implements IHttpClientWrapper {
	private final static MwaLogger logger = MwaLogger.getLogger(HttpClientWrapper.class);
	
	private class AlwaysTrustSelfSignedStrategy implements TrustStrategy {
		public AlwaysTrustSelfSignedStrategy() {
		}
		public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			return true;
		}
	}
	
	private class HttpRequestExceptionRetryHandler implements HttpRequestRetryHandler {
		private int maxExecutionCount;
		public HttpRequestExceptionRetryHandler(int maxExecutionCount) {
			this.maxExecutionCount = maxExecutionCount;
		}
		@Override
		public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
			logger.warn("Call http request retry handling.", exception);
			if (executionCount > this.maxExecutionCount) {
				logger.warn("Maximum tries reached for client http pool. max=" + this.maxExecutionCount);
				return false;
			}
			// 2017/12/05 (Discussion with Matsui-san)
			// org.apache.http.NoHttpResponseException とは限らず例外が発生するケースを救ってあげるために、
			// 上限までリトライ処理を実行する
			return true;
		}
	}

	/**
	 * HttpClientのインスタンスを生成する。
	 *
	 * @param requestConfigProvider ヘッダー情報を払い出すクラス
	 * @param cookieStore Cookieストア
	 * @return HttpClientのインスタンス
	 */
	private CloseableHttpClient createClient(HttpRequestBase method, IHttpRequestConfigProvider requestConfigProvider, CookieStore cookieStore)
			throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		CloseableHttpClient httpClient = null;
		if ("https".equals(method.getURI().getScheme())) {
			httpClient = HttpClientBuilder.create()
					.setSslcontext(new SSLContextBuilder()
							.loadTrustMaterial(new AlwaysTrustSelfSignedStrategy())
							.build())
					.setSSLHostnameVerifier(new NoopHostnameVerifier())
					.setRetryHandler(new HttpRequestExceptionRetryHandler(requestConfigProvider.getRetryRequestCount()))
					.setDefaultCookieStore(cookieStore)
					.setDefaultHeaders(Arrays.asList(requestConfigProvider.getRequestHeaders())).build();
		} else {
			httpClient = HttpClientBuilder.create()
					.setRetryHandler(new HttpRequestExceptionRetryHandler(requestConfigProvider.getRetryRequestCount()))
					.setDefaultCookieStore(cookieStore)
					.setDefaultHeaders(Arrays.asList(requestConfigProvider.getRequestHeaders())).build();
		}
		return httpClient;
	}


	/***
	 * HTTPメソッドを実行する
	 * @param method HTTPメソッド
	 * @param requestConfigProvider HTTPヘッダー情報払い出しクラス
	 * @return メソッド実行結果
	 * @throws Exception
	 */
	private HttpResponseResult doMethod(HttpRequestBase method, IHttpRequestConfigProvider requestConfigProvider) throws Exception {
		CookieStore cookieStore = new BasicCookieStore();
		try (CloseableHttpClient httpClient = this.createClient(method, requestConfigProvider, cookieStore);
			 CloseableHttpResponse httpResponse = httpClient.execute(method)) {

			int statusCode = httpResponse.getStatusLine().getStatusCode();
			HttpEntity entity = httpResponse.getEntity();
			List<Cookie> cookieList = cookieStore.getCookies();

			return new HttpResponseResult(
					statusCode,
					EntityUtils.toString(entity, StandardCharsets.UTF_8),
					cookieList.toArray(new Cookie[cookieList.size()]));
		} catch (IOException | NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
			throw e;
		}
	}

	/**
	 * HTTPメソッド「GET」
	 * @throws Exception
	 */
	@Override
	public HttpResponseResult get(String urlWithQuery, IHttpRequestConfigProvider requestConfigProvider) throws Exception {
		HttpGet method = new HttpGet(urlWithQuery);

		return doMethod(method, requestConfigProvider);
	}

	/***
	 * HTTPメソッド「POST」
	 * @throws Exception
	 */
	@Override
	public HttpResponseResult post(String urlWithQuery, String requestBody, IHttpRequestConfigProvider requestConfigProvider) throws Exception {
		HttpPost method = new HttpPost(urlWithQuery);
		method.setEntity(new StringEntity(requestBody, StandardCharsets.UTF_8));

		return doMethod(method, requestConfigProvider);
	}

	/***
	 * HTTPメソッド「PUT」
	 * @throws Exception
	 */
	@Override
	public HttpResponseResult put(String urlWithQuery, String requestBody, IHttpRequestConfigProvider requestConfigProvider) throws Exception {
		HttpPut method = new HttpPut(urlWithQuery);
		method.setEntity(new StringEntity(requestBody, StandardCharsets.UTF_8));

		return doMethod(method, requestConfigProvider);
	}

	/**
	 * HTTPメソッド「DELETE」
	 * @throws Exception
	 */
	@Override
	public HttpResponseResult delete(String urlWithQuery, String requestBody, IHttpRequestConfigProvider requestConfigProvider) throws Exception {
		// requestBodyがnullかを確認してnullではない場合はpostで送る
		if (requestBody != null) {
			HttpDeleteWithEntity method = new HttpDeleteWithEntity(urlWithQuery);
			method.setHeaders(requestConfigProvider.getRequestHeaders());
			method.setEntity(new StringEntity(requestBody, StandardCharsets.UTF_8));
			return doMethod(method, requestConfigProvider);
		}
		HttpDelete method = new HttpDelete(urlWithQuery);
		method.setHeaders(requestConfigProvider.getRequestHeaders());
		return doMethod(method, requestConfigProvider);
	}

	/***
	 * HTTPメソッド「PATCH」
	 * @throws Exception
	 */
	@Override
	public HttpResponseResult patch(String urlWithQuery, String requestBody, IHttpRequestConfigProvider requestConfigProvider) throws Exception {
		HttpPatch method = new HttpPatch(urlWithQuery);
		method.setEntity(new StringEntity(requestBody, StandardCharsets.UTF_8));

		return doMethod(method, requestConfigProvider);
	}

}
