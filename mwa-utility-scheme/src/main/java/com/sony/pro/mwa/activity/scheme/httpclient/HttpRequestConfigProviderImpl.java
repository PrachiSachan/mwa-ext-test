package com.sony.pro.mwa.activity.scheme.httpclient;

import org.apache.http.Header;

public class HttpRequestConfigProviderImpl implements IHttpRequestConfigProvider {
	private Header[] header;
	private int retryRequestCount = 3;     // デフォルトリトライ回数
	
	@Override
	public Header[] getRequestHeaders() {
		return header;
	}
	
	@Override
	public int getRetryRequestCount() {
		return retryRequestCount;
	}

	public HttpRequestConfigProviderImpl(Header[] header) {
		this.header = header;
	}
	
	public HttpRequestConfigProviderImpl(Header[] header, int retryRequestCount) {
		this.header = header;
		this.retryRequestCount = retryRequestCount;
	}
}
