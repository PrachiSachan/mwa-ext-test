package com.sony.pro.mwa.activity.scheme.httpclient;

import org.apache.http.client.methods.HttpPost;

public class HttpDeleteWithEntity extends HttpPost {

	public HttpDeleteWithEntity(String url) {
		super(url);
	}

	@Override
	public String getMethod() {
		return "DELETE";
	}

}
