package com.sony.pro.mwa.activity.type.xda;

import net.sony.xmlns.pro.mbb.xda.XDAExchange;

import com.sony.pro.mwa.parameter.type.ObjectParameterType;

/**
 * Singleton parameter types for various objects used in the GAM interface.
 *
 */
public class XdaExchangeTypes {
    public static final ObjectParameterType<XDAExchange> XdaExchangeType = new ObjectParameterType<>(XDAExchange.class);
}
