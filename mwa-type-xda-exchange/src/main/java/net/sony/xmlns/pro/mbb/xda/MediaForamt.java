//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.09.18 at 04:18:16 PM JST 
//


package net.sony.xmlns.pro.mbb.xda;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MediaForamt complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MediaForamt">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GUID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="FileFormatID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="MediaTypeID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ImageWidth" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="ImageHeight" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="BitsPerPixel" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="PixelFormat" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="FrameRate" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Compression" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="DataRate" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="ScanMode" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="GopSize" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="BFrameCount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="PFrameCount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="Channels" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="SamplesRate" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="BitsPerSample" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="FrameCount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="FileType" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="CompressionType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Format" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FileLength" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="AudioStreamNum" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="FOURCC" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="YuvSampeMode" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="VideoBitrate" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="GOPIPLength" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="CompressFormatName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HasAudioStream" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="HasVideoStream" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IsHighQuality" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AudioDuration" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="AudioAvgBytesPerSec" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="AudioFormatTag" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="VideoStreamNum" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="VideoFormatInfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AudioFormatInfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MediaForamt", propOrder = {
    "guid",
    "fileFormatID",
    "mediaTypeID",
    "imageWidth",
    "imageHeight",
    "bitsPerPixel",
    "pixelFormat",
    "frameRate",
    "compression",
    "dataRate",
    "scanMode",
    "gopSize",
    "bFrameCount",
    "pFrameCount",
    "channels",
    "samplesRate",
    "bitsPerSample",
    "frameCount",
    "fileType",
    "compressionType",
    "format",
    "fileLength",
    "audioStreamNum",
    "fourcc",
    "yuvSampeMode",
    "videoBitrate",
    "gopipLength",
    "compressFormatName",
    "hasAudioStream",
    "hasVideoStream",
    "isHighQuality",
    "audioDuration",
    "audioAvgBytesPerSec",
    "audioFormatTag",
    "videoStreamNum",
    "videoFormatInfo",
    "audioFormatInfo"
})
public class MediaForamt {

    @XmlElement(name = "GUID")
    protected long guid;
    @XmlElement(name = "FileFormatID")
    protected long fileFormatID;
    @XmlElement(name = "MediaTypeID")
    protected long mediaTypeID;
    @XmlElement(name = "ImageWidth")
    protected long imageWidth;
    @XmlElement(name = "ImageHeight")
    protected long imageHeight;
    @XmlElement(name = "BitsPerPixel")
    protected long bitsPerPixel;
    @XmlElement(name = "PixelFormat")
    protected long pixelFormat;
    @XmlElement(name = "FrameRate")
    protected double frameRate;
    @XmlElement(name = "Compression")
    protected long compression;
    @XmlElement(name = "DataRate")
    protected Long dataRate;
    @XmlElement(name = "ScanMode")
    protected Long scanMode;
    @XmlElement(name = "GopSize")
    protected Long gopSize;
    @XmlElement(name = "BFrameCount")
    protected Long bFrameCount;
    @XmlElement(name = "PFrameCount")
    protected Long pFrameCount;
    @XmlElement(name = "Channels")
    protected long channels;
    @XmlElement(name = "SamplesRate")
    protected long samplesRate;
    @XmlElement(name = "BitsPerSample")
    protected long bitsPerSample;
    @XmlElement(name = "FrameCount")
    protected long frameCount;
    @XmlElement(name = "FileType")
    protected long fileType;
    @XmlElement(name = "CompressionType")
    protected int compressionType;
    @XmlElement(name = "Format", required = true)
    protected String format;
    @XmlElement(name = "FileLength")
    protected long fileLength;
    @XmlElement(name = "AudioStreamNum")
    protected long audioStreamNum;
    @XmlElement(name = "FOURCC")
    protected long fourcc;
    @XmlElement(name = "YuvSampeMode")
    protected long yuvSampeMode;
    @XmlElement(name = "VideoBitrate")
    protected long videoBitrate;
    @XmlElement(name = "GOPIPLength")
    protected Long gopipLength;
    @XmlElement(name = "CompressFormatName", required = true)
    protected String compressFormatName;
    @XmlElement(name = "HasAudioStream")
    protected int hasAudioStream;
    @XmlElement(name = "HasVideoStream")
    protected int hasVideoStream;
    @XmlElement(name = "IsHighQuality")
    protected int isHighQuality;
    @XmlElement(name = "AudioDuration")
    protected double audioDuration;
    @XmlElement(name = "AudioAvgBytesPerSec")
    protected long audioAvgBytesPerSec;
    @XmlElement(name = "AudioFormatTag")
    protected int audioFormatTag;
    @XmlElement(name = "VideoStreamNum")
    protected long videoStreamNum;
    @XmlElement(name = "VideoFormatInfo", required = true)
    protected String videoFormatInfo;
    @XmlElement(name = "AudioFormatInfo", required = true)
    protected String audioFormatInfo;

    /**
     * Gets the value of the guid property.
     * 
     */
    public long getGUID() {
        return guid;
    }

    /**
     * Sets the value of the guid property.
     * 
     */
    public void setGUID(long value) {
        this.guid = value;
    }

    /**
     * Gets the value of the fileFormatID property.
     * 
     */
    public long getFileFormatID() {
        return fileFormatID;
    }

    /**
     * Sets the value of the fileFormatID property.
     * 
     */
    public void setFileFormatID(long value) {
        this.fileFormatID = value;
    }

    /**
     * Gets the value of the mediaTypeID property.
     * 
     */
    public long getMediaTypeID() {
        return mediaTypeID;
    }

    /**
     * Sets the value of the mediaTypeID property.
     * 
     */
    public void setMediaTypeID(long value) {
        this.mediaTypeID = value;
    }

    /**
     * Gets the value of the imageWidth property.
     * 
     */
    public long getImageWidth() {
        return imageWidth;
    }

    /**
     * Sets the value of the imageWidth property.
     * 
     */
    public void setImageWidth(long value) {
        this.imageWidth = value;
    }

    /**
     * Gets the value of the imageHeight property.
     * 
     */
    public long getImageHeight() {
        return imageHeight;
    }

    /**
     * Sets the value of the imageHeight property.
     * 
     */
    public void setImageHeight(long value) {
        this.imageHeight = value;
    }

    /**
     * Gets the value of the bitsPerPixel property.
     * 
     */
    public long getBitsPerPixel() {
        return bitsPerPixel;
    }

    /**
     * Sets the value of the bitsPerPixel property.
     * 
     */
    public void setBitsPerPixel(long value) {
        this.bitsPerPixel = value;
    }

    /**
     * Gets the value of the pixelFormat property.
     * 
     */
    public long getPixelFormat() {
        return pixelFormat;
    }

    /**
     * Sets the value of the pixelFormat property.
     * 
     */
    public void setPixelFormat(long value) {
        this.pixelFormat = value;
    }

    /**
     * Gets the value of the frameRate property.
     * 
     */
    public double getFrameRate() {
        return frameRate;
    }

    /**
     * Sets the value of the frameRate property.
     * 
     */
    public void setFrameRate(double value) {
        this.frameRate = value;
    }

    /**
     * Gets the value of the compression property.
     * 
     */
    public long getCompression() {
        return compression;
    }

    /**
     * Sets the value of the compression property.
     * 
     */
    public void setCompression(long value) {
        this.compression = value;
    }

    /**
     * Gets the value of the dataRate property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDataRate() {
        return dataRate;
    }

    /**
     * Sets the value of the dataRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDataRate(Long value) {
        this.dataRate = value;
    }

    /**
     * Gets the value of the scanMode property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getScanMode() {
        return scanMode;
    }

    /**
     * Sets the value of the scanMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setScanMode(Long value) {
        this.scanMode = value;
    }

    /**
     * Gets the value of the gopSize property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getGopSize() {
        return gopSize;
    }

    /**
     * Sets the value of the gopSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setGopSize(Long value) {
        this.gopSize = value;
    }

    /**
     * Gets the value of the bFrameCount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBFrameCount() {
        return bFrameCount;
    }

    /**
     * Sets the value of the bFrameCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBFrameCount(Long value) {
        this.bFrameCount = value;
    }

    /**
     * Gets the value of the pFrameCount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPFrameCount() {
        return pFrameCount;
    }

    /**
     * Sets the value of the pFrameCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPFrameCount(Long value) {
        this.pFrameCount = value;
    }

    /**
     * Gets the value of the channels property.
     * 
     */
    public long getChannels() {
        return channels;
    }

    /**
     * Sets the value of the channels property.
     * 
     */
    public void setChannels(long value) {
        this.channels = value;
    }

    /**
     * Gets the value of the samplesRate property.
     * 
     */
    public long getSamplesRate() {
        return samplesRate;
    }

    /**
     * Sets the value of the samplesRate property.
     * 
     */
    public void setSamplesRate(long value) {
        this.samplesRate = value;
    }

    /**
     * Gets the value of the bitsPerSample property.
     * 
     */
    public long getBitsPerSample() {
        return bitsPerSample;
    }

    /**
     * Sets the value of the bitsPerSample property.
     * 
     */
    public void setBitsPerSample(long value) {
        this.bitsPerSample = value;
    }

    /**
     * Gets the value of the frameCount property.
     * 
     */
    public long getFrameCount() {
        return frameCount;
    }

    /**
     * Sets the value of the frameCount property.
     * 
     */
    public void setFrameCount(long value) {
        this.frameCount = value;
    }

    /**
     * Gets the value of the fileType property.
     * 
     */
    public long getFileType() {
        return fileType;
    }

    /**
     * Sets the value of the fileType property.
     * 
     */
    public void setFileType(long value) {
        this.fileType = value;
    }

    /**
     * Gets the value of the compressionType property.
     * 
     */
    public int getCompressionType() {
        return compressionType;
    }

    /**
     * Sets the value of the compressionType property.
     * 
     */
    public void setCompressionType(int value) {
        this.compressionType = value;
    }

    /**
     * Gets the value of the format property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormat() {
        return format;
    }

    /**
     * Sets the value of the format property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormat(String value) {
        this.format = value;
    }

    /**
     * Gets the value of the fileLength property.
     * 
     */
    public long getFileLength() {
        return fileLength;
    }

    /**
     * Sets the value of the fileLength property.
     * 
     */
    public void setFileLength(long value) {
        this.fileLength = value;
    }

    /**
     * Gets the value of the audioStreamNum property.
     * 
     */
    public long getAudioStreamNum() {
        return audioStreamNum;
    }

    /**
     * Sets the value of the audioStreamNum property.
     * 
     */
    public void setAudioStreamNum(long value) {
        this.audioStreamNum = value;
    }

    /**
     * Gets the value of the fourcc property.
     * 
     */
    public long getFOURCC() {
        return fourcc;
    }

    /**
     * Sets the value of the fourcc property.
     * 
     */
    public void setFOURCC(long value) {
        this.fourcc = value;
    }

    /**
     * Gets the value of the yuvSampeMode property.
     * 
     */
    public long getYuvSampeMode() {
        return yuvSampeMode;
    }

    /**
     * Sets the value of the yuvSampeMode property.
     * 
     */
    public void setYuvSampeMode(long value) {
        this.yuvSampeMode = value;
    }

    /**
     * Gets the value of the videoBitrate property.
     * 
     */
    public long getVideoBitrate() {
        return videoBitrate;
    }

    /**
     * Sets the value of the videoBitrate property.
     * 
     */
    public void setVideoBitrate(long value) {
        this.videoBitrate = value;
    }

    /**
     * Gets the value of the gopipLength property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getGOPIPLength() {
        return gopipLength;
    }

    /**
     * Sets the value of the gopipLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setGOPIPLength(Long value) {
        this.gopipLength = value;
    }

    /**
     * Gets the value of the compressFormatName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompressFormatName() {
        return compressFormatName;
    }

    /**
     * Sets the value of the compressFormatName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompressFormatName(String value) {
        this.compressFormatName = value;
    }

    /**
     * Gets the value of the hasAudioStream property.
     * 
     */
    public int getHasAudioStream() {
        return hasAudioStream;
    }

    /**
     * Sets the value of the hasAudioStream property.
     * 
     */
    public void setHasAudioStream(int value) {
        this.hasAudioStream = value;
    }

    /**
     * Gets the value of the hasVideoStream property.
     * 
     */
    public int getHasVideoStream() {
        return hasVideoStream;
    }

    /**
     * Sets the value of the hasVideoStream property.
     * 
     */
    public void setHasVideoStream(int value) {
        this.hasVideoStream = value;
    }

    /**
     * Gets the value of the isHighQuality property.
     * 
     */
    public int getIsHighQuality() {
        return isHighQuality;
    }

    /**
     * Sets the value of the isHighQuality property.
     * 
     */
    public void setIsHighQuality(int value) {
        this.isHighQuality = value;
    }

    /**
     * Gets the value of the audioDuration property.
     * 
     */
    public double getAudioDuration() {
        return audioDuration;
    }

    /**
     * Sets the value of the audioDuration property.
     * 
     */
    public void setAudioDuration(double value) {
        this.audioDuration = value;
    }

    /**
     * Gets the value of the audioAvgBytesPerSec property.
     * 
     */
    public long getAudioAvgBytesPerSec() {
        return audioAvgBytesPerSec;
    }

    /**
     * Sets the value of the audioAvgBytesPerSec property.
     * 
     */
    public void setAudioAvgBytesPerSec(long value) {
        this.audioAvgBytesPerSec = value;
    }

    /**
     * Gets the value of the audioFormatTag property.
     * 
     */
    public int getAudioFormatTag() {
        return audioFormatTag;
    }

    /**
     * Sets the value of the audioFormatTag property.
     * 
     */
    public void setAudioFormatTag(int value) {
        this.audioFormatTag = value;
    }

    /**
     * Gets the value of the videoStreamNum property.
     * 
     */
    public long getVideoStreamNum() {
        return videoStreamNum;
    }

    /**
     * Sets the value of the videoStreamNum property.
     * 
     */
    public void setVideoStreamNum(long value) {
        this.videoStreamNum = value;
    }

    /**
     * Gets the value of the videoFormatInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVideoFormatInfo() {
        return videoFormatInfo;
    }

    /**
     * Sets the value of the videoFormatInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVideoFormatInfo(String value) {
        this.videoFormatInfo = value;
    }

    /**
     * Gets the value of the audioFormatInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAudioFormatInfo() {
        return audioFormatInfo;
    }

    /**
     * Sets the value of the audioFormatInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAudioFormatInfo(String value) {
        this.audioFormatInfo = value;
    }

}
