package com.sony.pro.mwa.activity.type.gam;

import com.sony.mbc.gam.AssetInfo;
import com.sony.mbc.gam.Context;
import com.sony.mbc.gam.Result;
import com.sony.mbc.gam.SearchForAssetsResult;
import com.sony.pro.mwa.parameter.type.ObjectParameterType;

/**
 * Singleton parameter types for various objects used in the GAM interface.
 *
 */
public class GamTypes {
    public static final ObjectParameterType<AssetInfo> AssetInfoType = new ObjectParameterType<>(AssetInfo.class);
    public static final ObjectParameterType<Result> ResultType = new ObjectParameterType<>(Result.class);
    public static final ObjectParameterType<Context> ContextType = new ObjectParameterType<>(Context.class);
    public static final ObjectParameterType<SearchForAssetsResult> SearchForAssetsResultType = new ObjectParameterType<>(SearchForAssetsResult.class);
}
