package com.sony.pro.mwa.activity.fm;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.enumeration.StandardPerspective;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.storage.NativeLocationModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.provider.IActivityProvider;
import com.sony.pro.mwa.service.storage.IPerspective;
import com.sony.pro.mwa.service.storage.IUriResolver;
import com.sony.pro.mwa.utils.UriUtils;

public class GenericTransferUtils {

	/**
	 * パスがmwaパスの場合、nativeパスに変換します。 nativeパスをparseし、パス情報を返却します。
	 *
	 * @param path
	 *            ファイルパス
	 * @param uriResolver
	 * @param activityProvider
	 * @return PathInfoBean パス情報
	 */
	public static PathInfoBean getPathInfo(String path, IUriResolver uriResolver, IActivityProvider activityProvider) {
		PathInfoBean info = new PathInfoBean();
		info.setOriginalPath(path);

		// 入力されたPerspectiveのパターンを判定
		IPerspective perspective = UriUtils.checkPerspective(path);

		if (perspective == null) {
			String errMsg = String.format("perspective is null. path -> %s ", path);
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PERSPECTIVE, null, errMsg);
		}

		if (StandardPerspective.MWA.equals(perspective)) {

			String providerId = null;
			String defaultPerspective = null;
			if (activityProvider != null) {
				providerId = activityProvider.getId();
				defaultPerspective = activityProvider.getDefaultPerspective();
			}

			// MWA-URI変換
			String[] perspectives = ArrayUtils.addAll(new String[] {defaultPerspective}, StandardPerspective.getDefaultNativePathPerspectiveArray());
			if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
				perspectives = ArrayUtils.addAll(perspectives, new String[] {StandardPerspective.WINDOWS_FORWARD_SLASH.name(), StandardPerspective.UNC_FORWARD_SLASH.name()});
			}
			perspectives = ArrayUtils.addAll(perspectives, new String[] {StandardPerspective.FTP.name()});
			NativeLocationModel nlm = uriResolver.asNativePathViaProvider(path, providerId, perspectives);
			info.setUser(nlm.getUser());
			info.setPassword(nlm.getPassword());
			path = nlm.getFirstPath();
			// MWA-URI変換後のperspectiveを取得
			perspective = UriUtils.checkPerspective(nlm.getFirstPath());
		}

		info.setNativePerspective(perspective);
		info.setNativePath(path);

		if (!perspective.isUri()) {
			info.setPath(path);
			info.setSchema("file");
			info.setPort(-1);
		} else {

			if (-1 != path.indexOf("?")) {
				info.setPath(path.substring(0, path.indexOf("?")));
			} else {
				info.setPath(path);
			}

			URI uri = null;
			try {
				uri = new URI(path); // デコード済みのpath、UserInfoを取得できる
			} catch (URISyntaxException e) {
				throw new MwaInstanceError(MWARC.SYSTEM_ERROR, MWARC.SYSTEM_ERROR.code(), "Convert-URI is failed. path -> " + path);
			}

			// infoにパス情報を詰める
			info.setSchema(uri.getScheme());
			info.setQueryMap(getQueryMap(uri.getQuery()));
			info.setHost(uri.getHost());
			info.setPort(uri.getPort());

			info.setPath(uri.getPath());

			// MWAURI変換時にユーザ情報を取得してなく、変換後のパスにユーザ情報が存在した場合
			if (StringUtils.isEmpty(info.getUser()) && StringUtils.isNotEmpty(uri.getUserInfo())) {
				String[] userInfos = uri.getUserInfo().split(":");
				if (StringUtils.isNotEmpty(userInfos[0])) {
					// ユーザ名をセット
					info.setUser(userInfos[0]);
				}
				if (StringUtils.isNotEmpty(userInfos[1])) {
					// パスワードをセット
					info.setPassword(userInfos[1]);
				}
			}
		}

		// パスがディレクトリ指定の場合
		if (checkDirPath(info.getPath())) {
			// パスをディレクトリパスとしてセット
			info.setDirPath(info.getPath());
		} else {
			// ファイル名とディレクトリパスをセット
			info.setFilename(parseFileName(info.getPath()));
			info.setDirPath(parseWorkDirPath(info.getPath(), info.getFilename()));
		}

		return info;
	}

	/**
	 * queryをnameとvalueに分解し、mapに詰めます。 queryがnull・空文字の場合、空のmapを返却します。
	 *
	 * @param query
	 * @return queryMap
	 */
	public static Map<String, String> getQueryMap(String query) {
		Map<String, String> map = new HashMap<String, String>();
		if (StringUtils.isNotEmpty(query)) {
			String[] queryParams = query.split("&");
			for (String qp : queryParams) {
				String[] params = qp.split("=");

				if (params.length == 1) {
					map.put(params[0], null);
				} else {
					map.put(params[0], params[1]);
				}
			}
		}
		return map;
	}

	/**
	 * ディレクトリ/ファイル名からファイル名を抽出します。
	 *
	 * @param str
	 * @return　fileName
	 */
	public static String parseFileName(final String str) {
		int startIndex = str.lastIndexOf('/');
		if (startIndex == -1) {
			startIndex = str.lastIndexOf('\\');
		}
		return str.substring(startIndex + 1, str.length());
	}

	/**
	 * ワークディレクトリのパスを抽出します。
	 *
	 * @param str
	 * @return　dirPath
	 */
	public static String parseWorkDirPath(final String str, final String fileName) {
		final int endIndex = str.length() - fileName.length();
		return str.substring(0, endIndex);
	}

	/**
	 * パスの最後にセパレータ(/ または \)がある場合、trueを返します。
	 *
	 * @param path
	 * @return ディレクトリか判定結果
	 */
	public static boolean checkDirPath(String path) {
		// パスの最後の文字を取得
		char lastCharacter = path.charAt(path.length() - 1);
		if (lastCharacter == '/' || lastCharacter == '\\') {
			return true;
		}
		return false;
	}
}
