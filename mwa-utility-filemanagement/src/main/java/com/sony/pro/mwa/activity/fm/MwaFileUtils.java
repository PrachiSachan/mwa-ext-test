package com.sony.pro.mwa.activity.fm;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import com.sony.pro.mwa.enumeration.StandardPerspective;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.provider.IActivityProvider;
import com.sony.pro.mwa.service.storage.IUriResolver;

public class MwaFileUtils {

	final static String ENCODE_UTF_8 = "UTF-8";
	final static String SEPARETER_FTP = "/";
	final static String READ_PERMISSION = "r";
	final static String WRITE_PERMISSION = "w";
	final static String EXECUTE_PERMISSION = "x";
	static final List<StandardPerspective> fileSystemTypeList = Arrays.asList(StandardPerspective.FILE, StandardPerspective.UNIX, StandardPerspective.UNC, StandardPerspective.WINDOWS_FORWARD_SLASH,
			StandardPerspective.UNC_FORWARD_SLASH, StandardPerspective.WINDOWS);

	/**
	 * ファイルパスからfileInfoBeanをつくるメソッド
	 * 
	 * @param path
	 * @param uriResolver
	 * @param provider
	 * @return
	 */
	public static FileInfoBean getFileInfo(String path, IUriResolver uriResolver, IActivityProvider provider) {
		PathInfoBean pathInfoBean = GenericTransferUtils.getPathInfo(path, uriResolver, provider);
		return getFileInfo(pathInfoBean);
	}

	/**
	 * pathInfoBeanからFileInfoBeanを作成するメソッド
	 * 
	 * @param pathInfoBean
	 * @return
	 */
	public static FileInfoBean getFileInfo(PathInfoBean pathInfoBean) {

		if (fileSystemTypeList.contains(pathInfoBean.getNativePerspective())) {
			return getFileInfoAtFileSystem(pathInfoBean);
		} else if (pathInfoBean.getNativePerspective().equals(StandardPerspective.FTP)) {
			// TODO 実装途中のためFTPの場合はスローする
			throw new MwaInstanceError(MWARC.INVALID_INPUT_LOCATION, null, "perspective is invalid");
			// return getFileInfoAtFTP(pathInfoBean);
		} else {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_LOCATION, null, "perspective is invalid");
		}
	}

	/**
	 * PathInfoBeanからFileInfoBeanをつくるメソッド
	 * 
	 * @param bean
	 * @return
	 */
	protected static FileInfoBean getFileInfoAtFileSystem(PathInfoBean pathInfobean) {

		FileInfoBean fileInfoBean = new FileInfoBean(pathInfobean);
		File file = new File(pathInfobean.getPath());
		boolean isExist = file.exists();
		if (!isExist) {
			fileInfoBean.setExists(false);
			return fileInfoBean;
		}

		// ファイルが存在するかどうか確認
		fileInfoBean.setExists(isExist);
		// pathInfoBeanの情報を取得
		fileInfoBean.setPathInfoBean(pathInfobean);

		// パスがファイルの時のみ取得
		if (file.isFile()) {
			// 拡張子を取得
			fileInfoBean.setExtention(FilenameUtils.getExtension(pathInfobean.getFilename()));
			// ファイルのサイズを取得
			fileInfoBean.setSize(FileUtils.sizeOf(file));
		}

		// パスがディレクトリかどうか確認
		fileInfoBean.setDir(file.isDirectory());
		// ディレクトリを取得
		fileInfoBean.setParentPath(pathInfobean.getDirPath());
		// ファイルの名前を取得
		fileInfoBean.setName(pathInfobean.getFilename());
		// 作成時間を取得
		fileInfoBean.setCreatedTime(getCreatedTime(file));
		// 最終更新時間を取得
		fileInfoBean.setLastModifiedTime(file.lastModified());
		// 書き込み、読み込み、実行可を取得
		fileInfoBean.setWritable(file.canWrite());
		fileInfoBean.setReadable(file.canRead());
		fileInfoBean.setExecutable(file.canExecute());

		return fileInfoBean;
	}

	/**
	 * ファイルの作成してから現在までのミリ秒を取得する
	 * 
	 * @param file
	 * @return
	 */
	private static long getCreatedTime(File file) {
		try {
			// 作成時間を取得
			BasicFileAttributeView attributeView = Files.getFileAttributeView(file.toPath(), BasicFileAttributeView.class);
			BasicFileAttributes attribute = attributeView.readAttributes();

			return attribute.creationTime().toMillis();
		} catch (IOException e) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, null, "fileAttributeView ioexception error");
		}
	}

	/**
	 * PathInfoBeanからFileInfoBeanをつくるメソッド
	 * 
	 * @param fileName
	 * @return
	 */
	protected static FileInfoBean getFileInfoAtFTP(PathInfoBean bean) {

		FTPClient ftpClient = new FTPClient();
		FileInfoBean fileInfoBean = new FileInfoBean(bean);
		try {
			init(ftpClient, bean);
			// ftpClientにPASVモードに設定
			ftpClient.enterLocalPassiveMode();
			printFtpReply(ftpClient, "ftp_client", "change passiveMode");
			FTPFile ftpFile = null;

			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			// TODO ここ修正
			// ファイルを取得する
			try {
				ftpClient.changeWorkingDirectory(bean.getPath());
				printFtpReply(ftpClient, "ftp_client", "changeWorkingDirectry targetFile");

				// 成功
				// cwdでひとつ前戻る
				String previous = getPreviousAddress(bean.getPath(), SEPARETER_FTP);
				ftpClient.changeWorkingDirectory(previous);
				printFtpReply(ftpClient, "ftp_client", "changeWorkingDirectry parentDir");
				// listfileする
				FTPFile[] ftpFiles = ftpClient.listFiles(bean.getFilename());
				// 配列から名前が同じftpFileを取得する
				ftpFile = ftpFiles[0];

				fileInfoBean.setExists(true);

			} catch (MwaInstanceError e) {
				// CWDに失敗したらパスがないか、CWD先がファイル
				FTPFile[] ftpFiles = ftpClient.listFiles(bean.getPath());

				// 対象のファイルがないときはreturn
				if (ftpFiles.length == 0) {
					fileInfoBean.setExists(false);
					return fileInfoBean;
				}
				// CWDしようとした先のファイル
				ftpFile = ftpFiles[0];
			}
			fileInfoBean.setExists(true);

			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			// ファイルなら取得する
			if (ftpFile.isFile()) {
				// 拡張子取得
				String fileName = bean.getFilename();
				fileInfoBean.setExtention(fileName.substring(fileName.indexOf(".")));
				// ファイルサイズ取得
				fileInfoBean.setSize(ftpFile.getSize());
				// ファイル名を取得
				fileInfoBean.setName(ftpFile.getName());
			} else {
				fileInfoBean.setName(bean.getFilename());
			}
			// ディレクトリかどうか取得
			fileInfoBean.setDir(ftpFile.isDirectory());
			// ディレクトリパス取得
			fileInfoBean.setParentPath(bean.getSchema() + "://" + bean.getHost() + bean.getDirPath());
			// 書き込み、読み込み、実行可か取得
			fileInfoBean.setReadable(isExistPermission(ftpFile.toFormattedString().substring(1)));
			fileInfoBean.setWritable(isExistPermission(ftpFile.toFormattedString().substring(2)));
			fileInfoBean.setExecutable(isExistPermission(ftpFile.toFormattedString().substring(3)));
			// 最新更新日時取得
			fileInfoBean.setLastModifiedTime(ftpFile.getTimestamp().getTimeInMillis());

		} catch (IOException e) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, null, "ftpClient.listFiles occur IOException ");
		} finally {
			finalize(ftpClient);
		}
		return fileInfoBean;
	}

	private static String getPreviousAddress(String fullPath, String separeter) {

		String[] hairetu = fullPath.split(separeter);
		List<String> list = Arrays.asList(hairetu);
		StringBuilder resultBuilder = new StringBuilder();
		for (String hako : list) {

			if (hako.equals(hairetu[hairetu.length - 1])) {
				break;
			}

			resultBuilder.append(hako);
			resultBuilder.append(separeter);
		}
		return resultBuilder.toString();
	}

	/**
	 * 処理権限があるかどうかをチェックします。
	 * 
	 * @param word
	 * @return
	 */
	private static boolean isExistPermission(String letter) {
		// wordがrのときは読み込み権限、wのときは書き込み権限、xのときは実行権限
		if (letter.equals(READ_PERMISSION) || letter.equals(WRITE_PERMISSION) || letter.equals(EXECUTE_PERMISSION)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * FTPserverに接続・ログインする
	 * 
	 * @param ftpClient
	 * @param bean
	 */
	private static void init(FTPClient ftpClient, PathInfoBean bean) {

		ftpClient.setControlEncoding(ENCODE_UTF_8);

		try {
			if (bean.getPort() == -1) {
				// ポートのデフォルトは21
				ftpClient.connect(bean.getHost());
			} else {
				ftpClient.connect(bean.getHost(), bean.getPort());
			}
			printFtpReply(ftpClient, "ftp_client", "connect");
		} catch (UnknownHostException e) {
			throw new MwaInstanceError(MWARC.CONNECTION_HOST_NOT_FOUND, null, "The host[" + bean.getHost() + "] is unknown.");
		} catch (SocketException e) {
			throw new MwaInstanceError(MWARC.CONNECTION_TIMEOUT, null, "Socket-connection is timeout.");
		} catch (IOException e) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "FtpConnect process failed.");
		}

		String user = bean.getUser();
		String password = bean.getPassword();

		try {
			if (ftpClient.login(user, password) == false) {
				throw new MwaInstanceError(MWARC.AUTHENTICATION_FAILED, null, "Invalid user/password. user=" + user + ",password=" + password);
			}
			printFtpReply(ftpClient, "ftp_client", "login");
		} catch (IOException e) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "FtpLogin process failed.");
		}
	}

	/**
	 * FTPserverをログアウト・切断する
	 * 
	 * @param ftpClient
	 */
	private static void finalize(FTPClient ftpClient) {
		// ファイル転送先ホストからログアウトして切断する
		try {
			if ((ftpClient != null) && (ftpClient.isConnected())) {
				ftpClient.logout();
				ftpClient.disconnect();
				printFtpReply(ftpClient, "ftp_client", "logout");
			}
		} catch (IOException e) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "logoutError");
		}
	}

	/**
	 * replyCodeをチェックするメソッド
	 * 
	 * @param ftpClient
	 * @param ftpClientName
	 * @param cmd
	 */
	private static void printFtpReply(final FTPClient ftpClient, final String ftpClientName, final String cmd) {
		final int replyCode = ftpClient.getReplyCode();
		if (!FTPReply.isPositiveCompletion(replyCode)) {
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, String.format("ReplyCode of the Command[%s] is not positive.", cmd));
		}
	}
}
