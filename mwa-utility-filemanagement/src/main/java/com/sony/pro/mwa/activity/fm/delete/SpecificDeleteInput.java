package com.sony.pro.mwa.activity.fm.delete;

import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.utils.ParameterUtils;

public class SpecificDeleteInput implements GenericDeleteConstants {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	private String ProviderId;
	private String target;
	private String actionIfNotExists = ACTION_IF_NOT_EXISTS_FAIL;
	private String deleteEmptyFolders = DELETE_EMPTY_FOLDERS_FALSE;

	public SpecificDeleteInput() {

	}

	public SpecificDeleteInput(List<? extends IParameterDefinition> inputs, Map<String, Object> params) {

		// 値の格納
		if (params != null) {

			Map<String, Object> inputMap = ParameterUtils.FROM_STRING.convert(inputs, params);

			// targets
			if ((inputMap.get(DELETE_INPUT_KEY_TARGET) != null) && (inputMap.get(DELETE_INPUT_KEY_TARGET) instanceof String)) {
				this.setTarget((String) inputMap.get(DELETE_INPUT_KEY_TARGET));
			}

			// actionIfNotExists
			if ((inputMap.get(DELETE_INPUT_KEY_ACTION_IF_NOT_EXISTS) != null) && (inputMap.get(DELETE_INPUT_KEY_ACTION_IF_NOT_EXISTS) instanceof String)) {
				this.setActionIfNotExists((String) inputMap.get(DELETE_INPUT_KEY_ACTION_IF_NOT_EXISTS));
			}

			// deleteEmptyFolders
			if ((inputMap.get(DELETE_INPUT_KEY_DELETE_EMPTY_FOLDERS) != null) && (inputMap.get(DELETE_INPUT_KEY_DELETE_EMPTY_FOLDERS) instanceof String)) {
				this.deleteEmptyFolders = (String) inputMap.get(DELETE_INPUT_KEY_DELETE_EMPTY_FOLDERS);
			}

		} else {
			throw new MwaInstanceError(MWARC.INVALID_INPUT);
		}
	}

	public String getDeleteEmptyFolders() {
		return deleteEmptyFolders;
	}

	public void setDeleteEmptyFolders(String deleteEmptyFolders) {
		this.deleteEmptyFolders = deleteEmptyFolders;
	}

	public String getActionIfNotExists() {
		return actionIfNotExists;
	}

	public void setActionIfNotExists(String actionIfNotExists) {
		this.actionIfNotExists = actionIfNotExists;
	}

	public String getProviderId() {
		return ProviderId;
	}

	public void setProviderId(String providerId) {
		ProviderId = providerId;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

}
