package com.sony.pro.mwa.activity.fm;

import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.type.TypeListString;
import com.sony.pro.mwa.parameter.type.TypeString;

public enum GenericTransferInputPrameter implements IParameterDefinition
{
	ParentInstanceId(TypeString.instance(), false),
	ProviderId(TypeString.instance(), false),
	mode(TypeString.instance(), true),
	sources(TypeListString.instance(), true),
	destinations(TypeListString.instance(), false),
	actionIfExists(TypeString.instance(), false),
	deleteEmptyFolders(TypeString.instance(), false),
	rollback(TypeString.instance(), false),
	username(TypeString.instance(), false),
	;

	private GenericTransferInputPrameter(IParameterType type, boolean required) {
		this.type = type;
		this.required = required;
	}

	public boolean getRequired()
	{
		return required;
	}

	public String getKey()
	{
		return this.name();
	}

	public IParameterType getType()
	{
		return type;
	}

	public String getTypeName()
	{
		return type.name();
	}

	IParameterType type;
	boolean required;
}
