package com.sony.pro.mwa.activity.fm.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.utils.UriUtils;

public class PathCoverterUtils {

	/**
	 * 利用可能なCartridgeInfoを返します。 条件： 1.他の転送が行われていない 2.Transfer可能か
	 * 
	 * @param cartridgeInfoBeanList
	 * @param sourceFileSize
	 * @return
	 */
	public static List<CartridgeInfoBean> getValidCartridgeId(List<CartridgeInfoBean> cartridgeInfoBeanList, long sourceFileSize) {

		List<CartridgeInfoBean> result = new ArrayList<CartridgeInfoBean>();

		for (CartridgeInfoBean bean : cartridgeInfoBeanList) {
			if ((bean.isValidTransfer()) && (bean.getFreeSpace() > sourceFileSize)) {
				result.add(bean);
			}
		}

		return result;
	}

	/**
	 * Uriのクエリ部分からCartridgeSerialIdを取得します。
	 * 
	 * @param mwaUri
	 * @return
	 */
	public static String getCartidgeSerialIdFromUri(String mwaUri) {

		String cartridgeSerialId = null;

		if (mwaUri.contains("cartridgeId=")) {
			URI uriPath = URI.create(mwaUri);
			cartridgeSerialId = uriPath.getQuery();
			cartridgeSerialId = StringUtils.difference("cartridgeId=", cartridgeSerialId);
			try {
				cartridgeSerialId = UriUtils.percentDecode(cartridgeSerialId);
			} catch (UnsupportedEncodingException e) {
				String msg = "It falied to decode";
				throw new MwaInstanceError(MWARC.SYSTEM_ERROR, "", msg);
			}
		} else {
			return null;
		}

		return cartridgeSerialId;
	}

	/**
	 * ファイルパスから合計ファイルサイズを取得します。
	 * 
	 * @param filePath
	 *            WINDOWS or UNC
	 * @return
	 */
	public static long getSumFileSize(List<String> filePathList) {

		long result = 0;

		for (String filePath : filePathList) {
			Path path = Paths.get(filePath);
			File file = new File(path.toString());
			result += FileUtils.sizeOf(file);
		}

		return result;
	}
}
