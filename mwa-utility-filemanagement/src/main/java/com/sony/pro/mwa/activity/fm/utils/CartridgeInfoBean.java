package com.sony.pro.mwa.activity.fm.utils;

public class CartridgeInfoBean {
	private String cartridgeSerialId;
	private String cartridgeId;
	private String DriveLetter;
	private double freeSpace;
	private boolean validTransfer;
	
	public String getCartridgeSerialId()
	{
		return cartridgeSerialId;
	}

	public void setCartridgeSerialId(String cartridgeSerialId)
	{
		this.cartridgeSerialId = cartridgeSerialId;
	}

	public String getCartridgeId()
	{
		return cartridgeId;
	}

	public void setCartridgeId(String cartridgeId)
	{
		this.cartridgeId = cartridgeId;
	}

	public String getDriveLetter()
	{
		return DriveLetter;
	}

	public void setDriveLetter(String driveLetter)
	{
		DriveLetter = driveLetter;
	}

	public double getFreeSpace()
	{
		return freeSpace;
	}

	public void setFreeSpace(double freeSpace)
	{
		this.freeSpace = freeSpace;
	}
	
	public boolean isValidTransfer() {
		return validTransfer;
	}

	public void setValidTransfer(boolean validTransfer) {
		this.validTransfer = validTransfer;
	}

}
