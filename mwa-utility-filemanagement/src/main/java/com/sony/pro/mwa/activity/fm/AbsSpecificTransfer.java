package com.sony.pro.mwa.activity.fm;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.sony.pro.mwa.activity.framework.AbsMwaPollingActivity;
import com.sony.pro.mwa.activity.framework.stm.IStateMachine;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.service.activity.IState;

public abstract class AbsSpecificTransfer extends AbsMwaPollingActivity implements GenericTransferConstants {

		public AbsSpecificTransfer(IState state, IStateMachine stm) {
			super(state, stm);
		}

		/**
		 * GenericTransfer用requestSubmitイベントに対する実処理
		 * GenericTransfer用requestSubmitは必須のイベント処理なので継承先の実装を強制している
		 * 
		 * @param inputParams GenericTransferIFの入力値情報
		 * @return Activity実装部がサポートするEventのリスト
		 */
		protected abstract OperationResult requestSubmitImpl(GenericTransferInput inputParams);

		/**
		 * requestSubmitイベントに対する実処理
		 */
		@Override
		protected OperationResult requestSubmitImpl(Map<String, Object> params)
		{

			logger.debug("requestSubmitImpl called.");

			//GenericTransfer用入力チェックオブジェクトの生成
			SpecificTransferInputValidate specificTransferInputValidate = new SpecificTransferInputValidate();
			
			//入力値の妥当性チェックおよびデフォルト値の設定
			SpecificTransferInput preInputParams = null;
			try {
				preInputParams = specificTransferInputValidate.validateInputParam(getTemplate().getInputs(),params);
			} catch (MwaInstanceError e) {
				//下回りの処理でエラーコードは詰めている。
				throw e;
			}
			

			//SepcificIF情報をGenericIFのリスト1件情報に置換
			GenericTransferInput inputParams = new GenericTransferInput();
			try {
				BeanUtils.copyProperties(inputParams, preInputParams);
			} catch (IllegalAccessException e) {
				//発生しないはず　発生する場合は実装バグ
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				//発生しないはず　発生する場合は実装バグ
				e.printStackTrace();
			}
			
			//dest1件のセット
			List<String> destinationURIs = new ArrayList<String>();
			destinationURIs.add(preInputParams.getDestination());
			inputParams.setDestinations(destinationURIs);
			
			//ｓｒｃ1件のセット
			List<String> sourceURIs = new ArrayList<String>();
			sourceURIs.add(preInputParams.getSource());
			inputParams.setSources(sourceURIs);
			
			// GenericTransfer用requestSubmitイベントに対する実処理
			return requestSubmitImpl(inputParams);
		}
		

}
