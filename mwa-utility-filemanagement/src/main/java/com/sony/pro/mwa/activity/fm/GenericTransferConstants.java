package com.sony.pro.mwa.activity.fm;

public interface GenericTransferConstants {

	public static final String OPERATION_COPY = "copy";
	public static final String OPERATION_MOVE = "move";
	public static final String OPERATION_DELETE = "delete";
	public static final String OPERATION_LINK = "link";
	
	public static final String ACTION_IF_EXISTS_VERIFY = "verify";
	public static final String ACTION_IF_EXISTS_RENAME = "rename";
	public static final String ACTION_IF_EXISTS_RESUME = "resume";
	public static final String ACTION_IF_EXISTS_REPLACE = "replace";
	public static final String ACTION_IF_EXISTS_FAIL = "fail";
	public static final String ACTION_IF_EXISTS_SKIP = "skip";
	
	public static final String DELETE_EMPTY_FOLDERS_FALSE = "false";
	public static final String DELETE_EMPTY_FOLDERS_TRUE = "true";

	public static final String ROLLBACK_FALSE = "false";
	public static final String ROLLBACK_TRUE = "true";
	
	public static final String GFM_INPUT_KYE_TARGET = "target";
	
	public static final String INPUT_KEY_USER_NAME = "username";
	public static final String INPUT_KEY_ROLLBACK = "rollback";
	public static final String INPUT_KEY_DELETE_EMPTY_FOLDERS = "deleteEmptyFolders";
	public static final String INPUT_KEY_ACTION_IF_EXISTS = "actionIfExists";
	public static final String INPUT_KEY_DESTINATION_URIS = "destinations";
	public static final String INPUT_KEY_MODE = "mode";
	public static final String INPUT_KEY_SOURCE_URIS = "sources";
	
	public static final String INPUT_KEY_DESTINATION_URI = "destination";
	public static final String INPUT_KEY_SOURCE_URI = "source";
	
	public static final String FILE_SCHEMA_TRIPLE_SLASH = "file:///";
	public static final String FILE_SCHEMA_DOUBLE_SLASH = "file://";
	
}
