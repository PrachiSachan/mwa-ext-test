package com.sony.pro.mwa.activity.fm.delete;

import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.type.TypeListString;

public enum GenericDeleteOutputPrameter implements IParameterDefinition {
	deletedPaths(TypeListString.instance(), false),
	notDeletedPaths(TypeListString.instance(), false),
	notExistedPaths(TypeListString.instance(), false);

	private GenericDeleteOutputPrameter(IParameterType type, boolean required) {
		this.type = type;
		this.required = required;
	}

	public boolean getRequired() {
		return required;
	}

	public String getKey() {
		return this.name();
	}

	public IParameterType getType() {
		return type;
	}

	public String getTypeName() {
		return type.name();
	}

	IParameterType type;
	boolean required;
}
