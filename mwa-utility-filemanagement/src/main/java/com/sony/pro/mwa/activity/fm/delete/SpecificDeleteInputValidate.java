package com.sony.pro.mwa.activity.fm.delete;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.rc.MWARC;

public class SpecificDeleteInputValidate implements GenericDeleteConstants {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	/**
	 * 入力値の妥当性チェックおよびデフォルト値の設定
	 * 
	 * @param params
	 */
	public SpecificDeleteInput validateInputParam(List<? extends IParameterDefinition> inputs, Map<String, Object> params) {
		// Type指定に沿って、paramsのvalueをpurseして詰替えする。
		SpecificDeleteInput inputParams = new SpecificDeleteInput(inputs, params);

		// ActionIfNotExistsの入力値妥当性チェック
		if (inputParams.getActionIfNotExists() != null) {

			switch (inputParams.getActionIfNotExists()) {
			case ACTION_IF_NOT_EXISTS_SKIP:
			case ACTION_IF_NOT_EXISTS_FAIL:
				break;
			default:
				logger.error("ActionIfExists parameter is invalid input!");
				throw new MwaInstanceError(MWARC.INVALID_INPUT);
			}
		} else {
			inputParams.setActionIfNotExists(ACTION_IF_NOT_EXISTS_FAIL);
		}

		// targetの妥当性チェック
		if (StringUtils.isBlank(inputParams.getTarget())) {
			logger.error("Target parameter is blank!");
			throw new MwaInstanceError(MWARC.INVALID_INPUT);
		}

		// DeleteEmptyFoldersの入力値妥当性チェック
		if (inputParams.getDeleteEmptyFolders() != null) {
			switch (inputParams.getDeleteEmptyFolders()) {
			case DELETE_EMPTY_FOLDERS_TRUE:
			case DELETE_EMPTY_FOLDERS_FALSE:
				break;
			default:
				logger.error("DeleteEmptyFolders parameter is invalid input!");
				throw new MwaInstanceError(MWARC.INVALID_INPUT);
			}
		} else {
			inputParams.setDeleteEmptyFolders(DELETE_EMPTY_FOLDERS_FALSE);
		}

		return inputParams;
	}
}
