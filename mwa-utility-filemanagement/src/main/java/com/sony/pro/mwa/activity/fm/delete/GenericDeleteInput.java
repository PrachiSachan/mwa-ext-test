package com.sony.pro.mwa.activity.fm.delete;

import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.utils.ParameterUtils;

public class GenericDeleteInput implements GenericDeleteConstants {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	private String ProviderId;
	private List<String> targets;
	private String actionIfNotExists = ACTION_IF_NOT_EXISTS_FAIL;
	private String deleteEmptyFolders = DELETE_EMPTY_FOLDERS_FALSE;

	public GenericDeleteInput() {

	}

	@SuppressWarnings("unchecked")
	public GenericDeleteInput(List<? extends IParameterDefinition> inputs, Map<String, Object> params) {

		// 値の格納
		if (params != null) {

			Map<String, Object> inputMap = ParameterUtils.FROM_STRING.convert(inputs, params);

			// targets
			if ((inputMap.get(DELETE_INPUT_KEY_TARGETS) != null) && (inputMap.get(DELETE_INPUT_KEY_TARGETS) instanceof List)) {
				this.setTargets((List<String>) inputMap.get(DELETE_INPUT_KEY_TARGETS));
			}

			// actionIfExists [skip,fail,replace,resume,rename,verify
			// ]ではない場合はデフォルトにする？
			if ((inputMap.get(DELETE_INPUT_KEY_ACTION_IF_NOT_EXISTS) != null) && (inputMap.get(DELETE_INPUT_KEY_ACTION_IF_NOT_EXISTS) instanceof String)) {
				this.setActionIfNotExists((String) inputMap.get(DELETE_INPUT_KEY_ACTION_IF_NOT_EXISTS));
			}

			// deleteEmptyFolders
			if ((inputMap.get(DELETE_INPUT_KEY_DELETE_EMPTY_FOLDERS) != null) && (inputMap.get(DELETE_INPUT_KEY_DELETE_EMPTY_FOLDERS) instanceof String)) {
				this.deleteEmptyFolders = (String) inputMap.get(DELETE_INPUT_KEY_DELETE_EMPTY_FOLDERS);
			}

		} else {
			throw new MwaInstanceError(MWARC.INVALID_INPUT);
		}
	}

	public String getDeleteEmptyFolders() {
		return deleteEmptyFolders;
	}

	public void setDeleteEmptyFolders(String deleteEmptyFolders) {
		this.deleteEmptyFolders = deleteEmptyFolders;
	}

	public String getActionIfNotExists() {
		return actionIfNotExists;
	}

	public void setActionIfNotExists(String actionIfNotExists) {
		this.actionIfNotExists = actionIfNotExists;
	}

	public List<String> getTargets() {
		return targets;
	}

	public void setTargets(List<String> targets) {
		this.targets = targets;
	}

	public String getProviderId() {
		return ProviderId;
	}

	public void setProviderId(String providerId) {
		ProviderId = providerId;
	}

}
