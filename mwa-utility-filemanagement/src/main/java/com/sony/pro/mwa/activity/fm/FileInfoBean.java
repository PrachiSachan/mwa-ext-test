package com.sony.pro.mwa.activity.fm;

public class FileInfoBean {

	/** ファイルパスが存在するか */
	private boolean exists;
	/** ファイルパスがディレクトリかどうか */
	private boolean isDir;
	/** ディレクトリまでのパス */
	private String parentPath;
	/** ファイル、もしくはディレクトリの名前 */
	private String name;
	/** ファイルの拡張子 */
	private String extention;
	/** ファイル、もしくはディレクトリのサイズ */
	private long size;
	/** ファイルの作成した時間 */
	private long createdTime;
	/** 最終更新時間 */
	private long lastModifiedTime;
	/** 書き換え権限の有無 */
	private boolean isWritable;
	/** 読み込み権限の有無 */
	private boolean isReadable;
	/** 実行権限の有無 */
	private boolean isExecutable;
	/** FileInfoBeanの元となるbean */
	private PathInfoBean pathInfoBean;

	public FileInfoBean() {

	}

	public FileInfoBean(PathInfoBean pathInfoBean) {
		this.pathInfoBean = pathInfoBean;
	}

	public boolean isExists() {
		return exists;
	}

	public void setExists(boolean exists) {
		this.exists = exists;
	}

	public boolean isDir() {
		return isDir;
	}

	public void setDir(boolean isDir) {
		this.isDir = isDir;
	}

	public String getParentPath() {
		return parentPath;
	}

	public void setParentPath(String parentPath) {
		this.parentPath = parentPath;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExtention() {
		return extention;
	}

	public void setExtention(String extention) {
		this.extention = extention;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	public long getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(long lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public boolean isWritable() {
		return isWritable;
	}

	public void setWritable(boolean isWritable) {
		this.isWritable = isWritable;
	}

	public boolean isReadable() {
		return isReadable;
	}

	public void setReadable(boolean isReadable) {
		this.isReadable = isReadable;
	}

	public boolean isExecutable() {
		return isExecutable;
	}

	public void setExecutable(boolean isExecutable) {
		this.isExecutable = isExecutable;
	}

	public PathInfoBean getPathInfoBean() {
		return pathInfoBean;
	}

	public void setPathInfoBean(PathInfoBean pathInfoBean) {
		this.pathInfoBean = pathInfoBean;
	}

}
