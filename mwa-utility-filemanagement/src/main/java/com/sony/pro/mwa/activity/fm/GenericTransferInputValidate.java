package com.sony.pro.mwa.activity.fm;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.rc.MWARC;

public class GenericTransferInputValidate implements GenericTransferConstants {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());

	/**
	 * 入力値の妥当性チェックおよびデフォルト値の設定
	 * 
	 * @param params
	 */
	public GenericTransferInput validateInputParam(List<? extends IParameterDefinition> inputs, Map<String, Object> params) throws MwaInstanceError
	{
		// Type指定に沿って、paramsのvalueをpurseして詰替えする。
		GenericTransferInput inputParams = new GenericTransferInput(inputs, params);

		// validate　check
		{
			// Operationの入力値妥当性チェック
			switch (inputParams.getMode()) {
			case OPERATION_COPY:
			case OPERATION_MOVE:
				validDest(inputParams);
				break;
			case OPERATION_DELETE:
			case OPERATION_LINK:
				break;
			default:
				logger.error("Operation parameter is invalid input!");
				throw new MwaInstanceError(MWARC.INVALID_INPUT);
			}

			// ActionIfExistsの入力値妥当性チェック
			if (inputParams.getActionIfExists() != null) {

				switch (inputParams.getActionIfExists()) {
				case ACTION_IF_EXISTS_SKIP:
				case ACTION_IF_EXISTS_FAIL:
				case ACTION_IF_EXISTS_REPLACE:
				case ACTION_IF_EXISTS_RESUME:
				case ACTION_IF_EXISTS_RENAME:
				case ACTION_IF_EXISTS_VERIFY:
					break;
				default:
					logger.error("ActionIfExists parameter is invalid input!");
					throw new MwaInstanceError(MWARC.INVALID_INPUT);
				}

			} else {
				inputParams.setActionIfExists(ACTION_IF_EXISTS_FAIL);
			}

			//sourcesの妥当性チェック
			for(String source : inputParams.getSources()){
				
				if(StringUtils.isBlank(source)){
					logger.error("source parameter is blank!");
					throw new MwaInstanceError(MWARC.INVALID_INPUT);
				}
			}
			
			// DeleteEmptyFoldersの入力値妥当性チェック
			if (inputParams.getDeleteEmptyFolders() != null) {

				switch (inputParams.getDeleteEmptyFolders()) {
				case DELETE_EMPTY_FOLDERS_TRUE:
				case DELETE_EMPTY_FOLDERS_FALSE:
					break;
				default:
					logger.error("DeleteEmptyFolders parameter is invalid input!");
					throw new MwaInstanceError(MWARC.INVALID_INPUT);
				}
			} else {
				inputParams.setDeleteEmptyFolders(DELETE_EMPTY_FOLDERS_FALSE);
			}

			// rollback の入力値妥当性チェック
			if (inputParams.getRollback() != null) {

				switch (inputParams.getRollback()) {
				case ROLLBACK_TRUE:
				case ROLLBACK_FALSE:
					break;
				default:
					logger.error("DeleteEmptyFolders parameter is invalid input!");
					throw new MwaInstanceError(MWARC.INVALID_INPUT);
				}
			} else {
				inputParams.setRollback(ROLLBACK_FALSE);
			}

		}

		return inputParams;
	}

	private void validDest(GenericTransferInput inputParams){
		
		if(inputParams.getDestinations() == null ){
			logger.error("destinations parameter is invalid input required param not found!");
			throw new MwaInstanceError(MWARC.INVALID_INPUT);
		}
		
		//destinationsの妥当性チェック
		for(String dest : inputParams.getDestinations()){
			
			if(StringUtils.isBlank(dest)){
				logger.error("destination parameter is blank!");
				throw new MwaInstanceError(MWARC.INVALID_INPUT);
			}
		}
	}
	
}
