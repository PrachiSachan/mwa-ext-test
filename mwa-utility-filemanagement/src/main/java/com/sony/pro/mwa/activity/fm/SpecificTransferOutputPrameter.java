package com.sony.pro.mwa.activity.fm;

import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.type.TypeString;

public enum SpecificTransferOutputPrameter implements IParameterDefinition
{
	Result(TypeString.instance(), false), 
	destination(TypeString.instance(), false), ;

	private SpecificTransferOutputPrameter(IParameterType type, boolean required) {
		this.type = type;
		this.required = required;
	}

	public boolean getRequired()
	{
		return required;
	}

	public String getKey()
	{
		return this.name();
	}

	public IParameterType getType()
	{
		return type;
	}

	public String getTypeName()
	{
		return type.name();
	}

	IParameterType type;
	boolean required;
}
