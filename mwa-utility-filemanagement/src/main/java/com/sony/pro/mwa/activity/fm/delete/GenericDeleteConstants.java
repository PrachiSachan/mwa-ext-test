package com.sony.pro.mwa.activity.fm.delete;

public interface GenericDeleteConstants {

	public static final String DELETE_INPUT_KEY_DELETE_EMPTY_FOLDERS = "deleteEmptyFolders";
	public static final String DELETE_INPUT_KEY_ACTION_IF_NOT_EXISTS = "actionIfNotExists";
	public static final String DELETE_INPUT_KEY_TARGETS = "targets";
	public static final String DELETE_INPUT_KEY_TARGET = "target";
	
	public static final String ACTION_IF_NOT_EXISTS_FAIL = "fail";
	public static final String ACTION_IF_NOT_EXISTS_SKIP = "skip";
	
	public static final String DELETE_EMPTY_FOLDERS_FALSE = "false";
	public static final String DELETE_EMPTY_FOLDERS_TRUE = "true";
	
	public static final String GENERIC_DELETE_TASK = "Generic";
	public static final String SPECIFIC_DELETE_TASK = "Specific";
	
}
