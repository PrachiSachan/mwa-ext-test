package com.sony.pro.mwa.activity.fm.delete;

import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.type.TypeString;

public enum SpecificDeleteInputPrameter implements IParameterDefinition {
	ParentInstanceId(TypeString.instance(), false),
	ProviderId(TypeString.instance(), false),
	target(TypeString.instance(), true),
	actionIfNotExists(TypeString.instance(), false),
	deleteEmptyFolders(TypeString.instance(), false), ;

	private SpecificDeleteInputPrameter(IParameterType type, boolean required) {
		this.type = type;
		this.required = required;
	}

	public boolean getRequired() {
		return required;
	}

	public String getKey() {
		return this.name();
	}

	public IParameterType getType() {
		return type;
	}

	public String getTypeName() {
		return type.name();
	}

	IParameterType type;
	boolean required;
}
