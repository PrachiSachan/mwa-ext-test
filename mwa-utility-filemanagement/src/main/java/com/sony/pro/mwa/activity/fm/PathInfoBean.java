package com.sony.pro.mwa.activity.fm;

import java.util.Map;

import com.sony.pro.mwa.service.storage.IPerspective;

/**
 * パスの情報を格納するbeanクラスです。
 */
public class PathInfoBean {

	private String originalPath;
	private String nativePath;
	private String path;
	private String dirPath;
	private String filename;
	private String schema;
	private String host;
	private int port;
	private String user;
	private String password;
	private Map<String, String> queryMap;
	private IPerspective nativePerspective;

	public String getOriginalPath() {
		return originalPath;
	}

	public void setOriginalPath(String originalPath) {
		this.originalPath = originalPath;
	}

	public String getNativePath() {
		return nativePath;
	}

	public void setNativePath(String nativePath) {
		this.nativePath = nativePath;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Map<String, String> getQueryMap() {
		return queryMap;
	}

	public void setQueryMap(Map<String, String> queryMap) {
		this.queryMap = queryMap;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getDirPath() {
		return dirPath;
	}

	public void setDirPath(String dirPath) {
		this.dirPath = dirPath;
	}

	public IPerspective getNativePerspective() {
		return nativePerspective;
	}

	public void setNativePerspective(IPerspective nativePerspective) {
		this.nativePerspective = nativePerspective;
	}
}
