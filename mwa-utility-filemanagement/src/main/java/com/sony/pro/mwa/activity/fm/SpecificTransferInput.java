package com.sony.pro.mwa.activity.fm;

import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.utils.ParameterUtils;

public class SpecificTransferInput implements GenericTransferConstants {

	protected MwaLogger logger = MwaLogger.getLogger(this.getClass());
	
	// member
	private String mode; 
	private String source;
	private String destination;
	private String actionIfExists = "fail";
	private String deleteEmptyFolders;
	private String rollback;
	private String username;
	
	public SpecificTransferInput(List<? extends IParameterDefinition> inputs, Map<String, Object> params) {
		
		//値の格納
		if (params != null) {
			
			Map<String, Object> inputMap = ParameterUtils.FROM_STRING.convert(inputs, params);
			
			//OPERATION
			if((inputMap.get(INPUT_KEY_MODE) != null)
					&& (inputMap.get(INPUT_KEY_MODE) instanceof String)){
				this.mode = (String) inputMap.get(INPUT_KEY_MODE);
			}
			
			//sourceURIs
			if((inputMap.get(INPUT_KEY_SOURCE_URI) != null)
					&& (inputMap.get(INPUT_KEY_SOURCE_URI) instanceof String)){
				this.source = (String) inputMap.get(INPUT_KEY_SOURCE_URI);
			}
			
			//destinationURIs Patternにより必須項目（必要なパターンで必須チェックを実装する）
			if((inputMap.get(INPUT_KEY_DESTINATION_URI) != null)
					&& (inputMap.get(INPUT_KEY_DESTINATION_URI) instanceof String)){
				this.destination = (String) inputMap.get(INPUT_KEY_DESTINATION_URI);
			}
			
			//actionIfExists [skip,fail,replace,resume,rename,verify ]ではない場合はデフォルトにする？
			if((inputMap.get(INPUT_KEY_ACTION_IF_EXISTS) != null)
					&& (inputMap.get(INPUT_KEY_ACTION_IF_EXISTS) instanceof String)){
				this.actionIfExists = (String) inputMap.get(INPUT_KEY_ACTION_IF_EXISTS);
			}

			//deleteEmptyFolders
			if((inputMap.get(INPUT_KEY_DELETE_EMPTY_FOLDERS) != null)
					&& (inputMap.get(INPUT_KEY_DELETE_EMPTY_FOLDERS) instanceof String)){
				this.deleteEmptyFolders = (String) inputMap.get(INPUT_KEY_DELETE_EMPTY_FOLDERS);
			}

			//rollback 
			if((inputMap.get(INPUT_KEY_ROLLBACK) != null)
					&& (inputMap.get(INPUT_KEY_ROLLBACK) instanceof String)){
				this.rollback = (String) inputMap.get(INPUT_KEY_ROLLBACK);
			}
			
			//username
			if((inputMap.get(INPUT_KEY_USER_NAME) != null)
					&& (inputMap.get(INPUT_KEY_USER_NAME) instanceof String)){
				this.username = (String) inputMap.get(INPUT_KEY_USER_NAME);
			}
			
			//username
			if((inputMap.get(INPUT_KEY_USER_NAME) != null)
					&& (inputMap.get(INPUT_KEY_USER_NAME) instanceof String)){
				this.username = (String) inputMap.get(INPUT_KEY_USER_NAME);
			}
			
			
		} else {
			throw new MwaInstanceError(MWARC.INVALID_INPUT);
		}
	}


	public String getMode()
	{
		return mode;
	}

	public void setMode(String mode)
	{
		this.mode = mode;
	}

	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public String getDestination()
	{
		return destination;
	}

	public void setDestination(String destination)
	{
		this.destination = destination;
	}

	public String getActionIfExists()
	{
		return actionIfExists;
	}

	public void setActionIfExists(String actionIfExists)
	{
		this.actionIfExists = actionIfExists;
	}

	public String getDeleteEmptyFolders()
	{
		return deleteEmptyFolders;
	}

	public void setDeleteEmptyFolders(String deleteEmptyFolders)
	{
		this.deleteEmptyFolders = deleteEmptyFolders;
	}

	public String getRollback()
	{
		return rollback;
	}

	public void setRollback(String rollback)
	{
		this.rollback = rollback;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	
}
