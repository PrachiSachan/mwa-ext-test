package com.sony.pro.mwa.activity.fm;

import java.util.Map;

import com.sony.pro.mwa.activity.framework.AbsMwaPollingActivity;
import com.sony.pro.mwa.activity.framework.stm.IStateMachine;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.service.activity.IState;

public abstract class AbsGenericTransfer extends AbsMwaPollingActivity implements GenericTransferConstants {

	public AbsGenericTransfer(IState state, IStateMachine stm) {
		super(state, stm);
	}

	/**
	 * GenericTransfer用requestSubmitイベントに対する実処理
	 * GenericTransfer用requestSubmitは必須のイベント処理なので継承先の実装を強制している
	 *
	 * @param inputParams
	 *            GenericTransferIFの入力値情報
	 * @return Activity実装部がサポートするEventのリスト
	 */
	protected abstract OperationResult requestSubmitImpl(GenericTransferInput inputParams);

	/**
	 * requestSubmitイベントに対する実処理
	 */
	@Override
	protected OperationResult requestSubmitImpl(Map<String, Object> params)
	{

		logger.debug("requestSubmitImpl called.");

		// GenericTransfer用入力チェックオブジェクトの生成
		GenericTransferInputValidate genericTransferInputValidate = new GenericTransferInputValidate();

		// 入力値の妥当性チェックおよびデフォルト値の設定
		GenericTransferInput inputParams = null;
		try {
			inputParams = genericTransferInputValidate.validateInputParam(getTemplate().getInputs(), params);
		} catch (MwaInstanceError e) {
			// 下回りの処理でエラーコードは詰めている。
			throw e;
		}

		// GenericTransfer用requestSubmitイベントに対する実処理
		return requestSubmitImpl(inputParams);
	}

}
