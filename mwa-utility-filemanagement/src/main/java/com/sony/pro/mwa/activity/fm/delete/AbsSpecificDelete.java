package com.sony.pro.mwa.activity.fm.delete;

import java.util.Map;

import com.sony.pro.mwa.activity.framework.AbsMwaPollingActivity;
import com.sony.pro.mwa.activity.framework.stm.IStateMachine;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.service.activity.IState;

public abstract class AbsSpecificDelete extends AbsMwaPollingActivity implements GenericDeleteConstants {

	public AbsSpecificDelete(IState state, IStateMachine stm) {
		super(state, stm);
	}

	/**
	 * GenericDelete用requestSubmitイベントに対する実処理
	 * GenericDelete用requestSubmitは必須のイベント処理なので継承先の実装を強制している
	 * 
	 * @param inputParams
	 *            GenericDeleteIFの入力値情報
	 * @return Activity実装部がサポートするEventのリスト
	 */
	protected abstract OperationResult requestSubmitImpl(SpecificDeleteInput inputParams);

	/**
	 * requestSubmitイベントに対する実処理
	 */
	@Override
	protected OperationResult requestSubmitImpl(Map<String, Object> params) {

		// GenericDelete用入力チェックオブジェクトの生成
		SpecificDeleteInputValidate specificDeleteInputValidate = new SpecificDeleteInputValidate();

		// 入力値の妥当性チェックおよびデフォルト値の設定
		SpecificDeleteInput inputParams = specificDeleteInputValidate.validateInputParam(getTemplate().getInputs(), params);

		// GenericDelete用requestSubmitイベントに対する実処理
		return requestSubmitImpl(inputParams);
	}
}
