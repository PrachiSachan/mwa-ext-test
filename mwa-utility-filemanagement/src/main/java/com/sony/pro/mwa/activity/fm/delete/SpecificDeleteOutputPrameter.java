package com.sony.pro.mwa.activity.fm.delete;

import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.type.TypeListString;

public enum SpecificDeleteOutputPrameter implements IParameterDefinition {
	deletedPath(TypeListString.instance(), false),
	notDeletedPath(TypeListString.instance(), false),
	notExistedPath(TypeListString.instance(), false);

	private SpecificDeleteOutputPrameter(IParameterType type, boolean required) {
		this.type = type;
		this.required = required;
	}

	public boolean getRequired() {
		return required;
	}

	public String getKey() {
		return this.name();
	}

	public IParameterType getType() {
		return type;
	}

	public String getTypeName() {
		return type.name();
	}

	IParameterType type;
	boolean required;
}
