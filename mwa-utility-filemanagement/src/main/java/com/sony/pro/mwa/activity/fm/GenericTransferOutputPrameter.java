package com.sony.pro.mwa.activity.fm;

import com.sony.pro.mwa.parameter.IParameterDefinition;
import com.sony.pro.mwa.parameter.IParameterType;
import com.sony.pro.mwa.parameter.type.TypeListString;
import com.sony.pro.mwa.parameter.type.TypeString;

public enum GenericTransferOutputPrameter implements IParameterDefinition
{
	Result(TypeString.instance(), false), 
	destinations(TypeListString.instance(), false),;

	private GenericTransferOutputPrameter(IParameterType type, boolean required) {
		this.type = type;
		this.required = required;
	}

	public boolean getRequired()
	{
		return required;
	}

	public String getKey()
	{
		return this.name();
	}

	public IParameterType getType()
	{
		return type;
	}

	public String getTypeName()
	{
		return type.name();
	}

	IParameterType type;
	boolean required;
}
