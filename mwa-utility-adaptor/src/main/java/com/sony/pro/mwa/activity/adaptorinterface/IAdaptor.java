package com.sony.pro.mwa.activity.adaptorinterface;

import java.util.Map;

import com.sony.pro.mwa.activity.adaptor.common.Job;
import com.sony.pro.mwa.activity.adaptor.common.JobResponse;
import com.sony.pro.mwa.activity.adaptor.common.JobStatusResponse;
import com.sony.pro.mwa.activity.adaptor.exception.DeviceException;
import com.sony.pro.mwa.activity.adaptor.exception.ServiceException;




/**
 *
 */
public interface IAdaptor {

	/**
	 *
	 * @param job
	 * @return
	 * @throws ServiceException
	 * @throws DeviceException
	 */
	public JobResponse submitJob(Job job) throws ServiceException,DeviceException;

	/**
	 *
	 * @param deviceJobId
	 * @return
	 * @throws ServiceException
	 * @throws DeviceException
	 */
	public JobResponse cancelJob(String deviceJobId)  throws ServiceException,DeviceException;

	/**
	 *
	 * @param deviceJobId
	 * @param properties
	 * @return
	 * @throws ServiceException
	 * @throws DeviceException
	 */
	public JobStatusResponse getJobStatus(String deviceJobId,Map<String,String> properties)  throws ServiceException,DeviceException;

	/**
	 *
	 * @param deviceJobId
	 * @return
	 * @throws ServiceException
	 * @throws DeviceException
	 */
	public JobResponse pauseJob(String deviceJobId)  throws ServiceException,DeviceException;

	/**
	 *
	 * @param deviceJobId
	 * @return
	 * @throws ServiceException
	 * @throws DeviceException
	 */
	public JobResponse resumeJob(String deviceJobId)  throws ServiceException,DeviceException;

}
