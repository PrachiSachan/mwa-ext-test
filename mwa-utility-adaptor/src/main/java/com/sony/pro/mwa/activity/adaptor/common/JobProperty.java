package com.sony.pro.mwa.activity.adaptor.common;


import java.util.Arrays;

/**
 *
 */
public class JobProperty  {

	private String name;
	private String value;
	private String [] valueList;
	private String description;



	public JobProperty() {

	}

	/**
	 *  Constructor
	 * @param name
	 * @param value
	 * @param valueList
	 * @param description
	 */
	public JobProperty(String name, String value, String[] valueList,
			String description) {
		super();
		this.name = name;
		this.value = value;
		this.valueList = valueList;
		this.description = description;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * @return the valueList
	 */
	public String[] getValueList() {
		return valueList;
	}
	/**
	 * @param valueList the valueList to set
	 */
	public void setValueList(String[] valueList) {
		this.valueList = valueList;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result + Arrays.hashCode(valueList);
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobProperty other = (JobProperty) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		if (!Arrays.equals(valueList, other.valueList))
			return false;
		return true;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "JobProperty [name=" + name + ", value=" + value
				+ ", valueList=" + Arrays.toString(valueList)
				+ ", description=" + description + "]";
	}

}
