package com.sony.pro.mwa.activity.adaptor.exception;

/**
 *
 */
public class ServiceException extends Exception {


	private static final long serialVersionUID = -2244195316329273637L;

	public ServiceException(Exception ex) {
		super(ex);
	}


	public ServiceException(String errorDetail) {
		super(errorDetail);
	}

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

}
