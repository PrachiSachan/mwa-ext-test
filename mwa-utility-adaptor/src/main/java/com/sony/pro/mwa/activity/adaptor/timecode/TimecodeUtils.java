package com.sony.pro.mwa.activity.adaptor.timecode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author gbputchs
 *
 */
public class TimecodeUtils {

	private static final Pattern TIME_IN_MILLI_SECS_PATTERN;
	static 	{
		TIME_IN_MILLI_SECS_PATTERN = Pattern.compile("(\\d\\d)[:;](\\d\\d)[:;](\\d\\d)([.:;])(\\d\\d\\d)"); //$NON-NLS-1$
	}

	/**
	 * 
	 * @param timecodeStr
	 * @param fpsStr
	 * @param dropframeStr
	 * @return
	 */
	public static int getFrameCount(String timecodeStr, String fpsStr, String dropframeStr) throws IllegalArgumentException{

		Boolean dropFrame = null;
		Matcher matcher = TIME_IN_MILLI_SECS_PATTERN.matcher(timecodeStr);

		if (matcher.matches()) {
			return new TimeInMilliSeconds(timecodeStr, 1000).getTotalMillis();
		}

		if (fpsStr == null) {
			throw new IllegalArgumentException("fps cannot be null");
		}

		double fps = Double.parseDouble(fpsStr);					
		if (dropframeStr != null) {
			dropFrame = Boolean.parseBoolean(dropframeStr);
		} 		
		return new Timecode(timecodeStr, fps, dropFrame).getTotalFrames();		
	}	


	/**
	 *  
	 * @param somTimecodeStr
	 * @param durationTimecodeStr
	 * @param fpsStr
	 * @param dropframeStr
	 * @return eom time code
	 */
	public  static  String getEom(String somTimecodeStr,String durationTimecodeStr, String fpsStr, String dropframeStr) {		
		double fps = Double.parseDouble(fpsStr);
		Boolean dropFrame = null;
		if(dropframeStr!=null) {
			dropFrame= Boolean.parseBoolean(dropframeStr);
		}
		//Construct start of media time code
		Timecode somTimecode=new Timecode(somTimecodeStr, fps,dropFrame);

		//Constructor duration time code
		Timecode durationTimecode=new Timecode(durationTimecodeStr, fps, dropFrame);

		//Get the end of media time code
		return somTimecode.addDuration(durationTimecode).toString();
	}


	/**
	 * 
	 * @param somTimecode
	 * @param eomTimecode
	 * @param fpsStr
	 * @param dropframeStr
	 * @return
	 */
	public static String getDuration(String somTimecodeStr,String eomTimecodeStr, String fpsStr, String dropframeStr) {

		double fps = Double.parseDouble(fpsStr);
		Boolean dropFrame = null;

		if(dropframeStr!=null) {
			dropFrame = Boolean.parseBoolean(dropframeStr);
		}		
		//Construct start of media time code
		Timecode somTimecode=new Timecode(somTimecodeStr, fps,dropFrame);

		//Constructor eom time code
		Timecode eomTimecode=new Timecode(eomTimecodeStr, fps, dropFrame);

		//Get the duration		
		return somTimecode.calculateDuration(eomTimecode).toString();
	}



	/**
	 * 
	 * @param timecode_hh_mm_ss_ff
	 * @param fpsStr
	 * @return
	 */
	public static String convertTimecodeFramesToMillis(String timecode_hh_mm_ss_ff, String fpsStr) {

		double fps = Double.parseDouble(fpsStr);		
		Timecode timecode = new Timecode(timecode_hh_mm_ss_ff, fps);

		int hh = timecode.getHh();
		int mm = timecode.getMm();
		int ss = timecode.getSs();
		int ff = timecode.getFf();
		int millis = (int) (1000 * ff / fps);

		return String.format("%02d:%02d:%02d.%03d", hh, mm, ss, millis);

	}

	/**
	 *  
	 * @param timecode_hh_mm_ss_msec
	 * @param fpsStr
	 * @return
	 */
	public static String convertMillisecondsToTimecode(String timecode_hh_mm_ss_msec,String fpsStr) {

		double fps = Double.parseDouble(fpsStr);				
		TimeInMilliSeconds timeInMilliSeconds = new TimeInMilliSeconds(timecode_hh_mm_ss_msec, fps);

		int hh = timeInMilliSeconds.getHh();
		int mm = timeInMilliSeconds.getMm();
		int ss = timeInMilliSeconds.getSs();
		int sss = timeInMilliSeconds.getSss();
		int frames = (int) (sss*fps/1000);
		
		return String.format("%02d:%02d:%02d:%02d", hh, mm, ss, frames);
	}	
	
	/**
	 * 
	 * @param frameCount
	 * @param fps
	 * @param dropframe
	 * @return
	 */
	public  static String getTimecodeStr(int frameCount,double fps, boolean dropframe) {		
		return new Timecode(frameCount, fps, dropframe).toString();
	}	
	
	public  static String getTimecodeStr(String frameCount,double fps, boolean dropframe) {		
		return new Timecode(Integer.parseInt(frameCount), fps, dropframe).toString();
	}	
	
}

