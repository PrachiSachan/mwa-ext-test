package com.sony.pro.mwa.activity.adaptor.transcode;

 
/**
 * 
 * @author gbputchs
 *
 */
public enum TranscodeTypeEnum {
	 AUDIO_VIDEO("AUDIO_VIDEO") , 
	 VIDEO("VIDEO"), 
	 AUDIO("AUDIO") ,
	 SEQUENCE("SEQUENCE"), 
	 LUT("LUT") ;
	 
	private String type;

	 TranscodeTypeEnum(String type) {
		this.type=type;
	}

	public String getType() {
		return this.type;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public static TranscodeTypeEnum getEnum(String typeStr) {			
		for(TranscodeTypeEnum transcodeTypeEnum : TranscodeTypeEnum.values()) {
			if(typeStr.equalsIgnoreCase(transcodeTypeEnum.type)) {
				return transcodeTypeEnum;
			}
		}			
		return null;
	}	 
};
