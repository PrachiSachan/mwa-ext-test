package com.sony.pro.mwa.activity.adaptor.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import com.sony.pro.mwa.activity.adaptor.exception.ServiceException;

public class FileUtils {


	/**
	 *
	 * @param content
	 * @param filePath
	 * @throws ServiceException
	 */
	public static void  stringToFile(String data, String charset, String filePath) throws ServiceException {

		FileOutputStream fileOutputStream = null;
		OutputStreamWriter streamWriter=null;
		Writer bufferedWriter=null;

		try	{

			File file = new File(filePath);
			file.createNewFile();
			if (file.exists()) {
				fileOutputStream =new FileOutputStream(file);
				streamWriter = new OutputStreamWriter(fileOutputStream,charset!=null?charset:"UTF-8");
				bufferedWriter = new BufferedWriter(streamWriter);
				bufferedWriter.write(data);
				bufferedWriter.close();
			}
			else {
				throw new ServiceException("The system cannot find the file specified: " + filePath);
			}
		}
		catch( FileNotFoundException ex) {
			throw new ServiceException(ex);
		}
		catch ( IOException ex ) {
			throw new ServiceException(ex);
		}
		finally	{
			IOUtil.closeQuitely(fileOutputStream);
			IOUtil.closeQuitely(streamWriter);
			IOUtil.closeQuitely(bufferedWriter);
		}
	}
}
