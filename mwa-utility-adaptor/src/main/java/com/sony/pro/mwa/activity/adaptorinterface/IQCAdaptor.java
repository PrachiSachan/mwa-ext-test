package com.sony.pro.mwa.activity.adaptorinterface;

import com.sony.pro.mwa.activity.adaptor.exception.DeviceException;
import com.sony.pro.mwa.activity.adaptor.exception.ServiceException;
import com.sony.pro.mwa.activity.adaptor.qc.ReportInput;
import com.sony.pro.mwa.activity.adaptor.qc.ReportOutput;

public interface IQCAdaptor extends IAdaptor{

	/**
	 * This will generate the
	 * @param reportInput
	 * @return
	 * @throws ServiceException
	 * @throws DeviceException
	 */
	public ReportOutput generateReport(ReportInput reportInput) throws ServiceException,DeviceException;

}
