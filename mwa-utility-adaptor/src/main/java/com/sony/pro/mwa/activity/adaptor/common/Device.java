package com.sony.pro.mwa.activity.adaptor.common;



/**
 *
 */

public class Device {

	private String hostName;
	private String wsdlUrl;
	private String userName;
	private String password;
	private int    port;
	private Profile profile;


	/**
	 *
	 * @param wsdlUrl
	 * @param hostName
	 * @param userName
	 * @param password
	 * @param port
	 * @param profile
	 */
	public Device(String wsdlUrl,String hostName, String userName, String password,int port,Profile profile) {
		super();
		this.wsdlUrl=wsdlUrl;
		this.hostName = hostName;
		this.userName = userName;
		this.password = password;
		this.port = port;
		this.profile=profile;
	}



	public Device() {
		this.port = 0;
		this.profile = new Profile();
	}
	/**
	 * @return the hostName
	 */
	public String getHostName() {
		return hostName;
	}
	/**
	 * @param hostName the hostName to set
	 */
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}
	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}
	/**
	 * @return the endPoint
	 */

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}





	/**
	 * @return the wsdlUrl
	 */
	public String getWsdlUrl() {
		return wsdlUrl;
	}

	/**
	 * @param wsdlUrl the wsdlUrl to set
	 */
	public void setWsdlUrl(String wsdlUrl) {
		this.wsdlUrl = wsdlUrl;
	}

	/**
	 * @return the profile
	 */
	public Profile getProfile() {
		return profile;
	}

	/**
	 * @param profile the profile to set
	 */
	public void setProfile(Profile profile) {
		this.profile = profile;
	}



	/**
	 *
	 * @param key
	 * @return
	 */
	public String getDevProfilePropertyValue(String key) {
		if(this.profile!=null) {
			return this.profile.getProperties().get(key);
		}
		return null;
	}

	/**
	 *
	 * @param key
	 * @return
	 */
	public String getDevProfilePropertyValue(String key,String defaultValue) {
		if(this.profile!=null) {
			String _value = this.profile.getProperties().get(key);
			return _value == null ? defaultValue : _value;
		}
		return null;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((hostName == null) ? 0 : hostName.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result + port;
		result = prime * result + ((profile == null) ? 0 : profile.hashCode());
		result = prime * result
				+ ((userName == null) ? 0 : userName.hashCode());
		result = prime * result + ((wsdlUrl == null) ? 0 : wsdlUrl.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Device other = (Device) obj;
		if (hostName == null) {
			if (other.hostName != null)
				return false;
		} else if (!hostName.equals(other.hostName))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (port != other.port)
			return false;
		if (profile == null) {
			if (other.profile != null)
				return false;
		} else if (!profile.equals(other.profile))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		if (wsdlUrl == null) {
			if (other.wsdlUrl != null)
				return false;
		} else if (!wsdlUrl.equals(other.wsdlUrl))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Device [hostName=" + hostName + ", wsdlUrl=" + wsdlUrl
				+ ", userName=" + userName	+ ", password=" + password +
				", port=" + port + ", profile="	+ profile
				+  "]";
	}



}
