package com.sony.pro.mwa.activity.adaptor.common;


/**
 *
 */
public class FrameRate{


	private int numerator;
	private int denominator;


	  /**
	   * Constructor
	   * @param numerator
	   * @param deonominator
	   */
	  public FrameRate(int numerator,int deonominator) {
		  this.numerator =numerator;
		  this.denominator=deonominator;
	  }


	/**
	 * @return the numerator
	 */
	public int getNumerator() {
		return numerator;
	}


	/**
	 * @param numerator the numerator to set
	 */
	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}


	/**
	 * @return the denominator
	 */
	public int getDenominator() {
		return denominator;
	}


	/**
	 * @param denominator the denominator to set
	 */
	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (denominator ^ (denominator >>> 32));
		result = prime * result + (int) (numerator ^ (numerator >>> 32));
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FrameRate other = (FrameRate) obj;
		if (denominator != other.denominator)
			return false;
		if (numerator != other.numerator)
			return false;
		return true;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FrameRate [numerator=" + numerator + ", denominator="
				+ denominator + "]";
	}
}
