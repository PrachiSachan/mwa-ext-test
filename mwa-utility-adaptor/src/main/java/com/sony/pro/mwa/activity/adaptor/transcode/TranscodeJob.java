package com.sony.pro.mwa.activity.adaptor.transcode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.activity.adaptor.common.Job;
import com.sony.pro.mwa.activity.adaptor.common.JobProperty;
import com.sony.pro.mwa.activity.adaptor.common.Path;
import com.sony.pro.mwa.activity.adaptor.common.PriorityEnum;


/**
 *
 *
 * @author gbputchs
 *
 */
public class TranscodeJob extends Job {

	private String jobProfileName;
	private Path destinationPath;
	private Path destMetadataFilePath;
	private List<Source> source;


	public TranscodeJob() {
		super();
		this.source = new ArrayList<Source>();
	}


	/**
	 * Constructor
	 * @param deviceJobId
	 * @param dueDate
	 * @param scheduledDate
	 * @param operation
	 * @param priority
	 * @param actionType
	 * @param transcodeProfileName
	 */
	public TranscodeJob(String jobName,String jobId, Date dueDate, Date scheduledDate,PriorityEnum priority,
			Map<String,JobProperty> jobExtensionAttributes,List<Source> source,	String userId) {

		super(jobName,jobId, dueDate, scheduledDate,priority,jobExtensionAttributes,userId);
		this.source=source;
	}



	/**
	 * @return the source
	 */
	public List<Source> getSource() {
		return source;
	}


	/**
	 * @param source the source to set
	 */
	public void setSource(List<Source> source) {
		this.source = source;
	}




	/**
	 * @return the destinationPath
	 */
	public Path getDestinationPath() {
		return destinationPath;
	}


	/**
	 * @param destinationPath the destinationPath to set
	 */
	public void setDestinationPath(Path destinationPath) {
		this.destinationPath = destinationPath;
	}


	/**
	 * @return the destMetadataFolderPath
	 */
	public Path getDestMetadataFilePath() {
		return destMetadataFilePath;
	}


	/**
	 * @param destMetadataFolderPath the destMetadataFolderPath to set
	 */
	public void setDestMetadataFilePath(Path destMetadataFilePath) {
		this.destMetadataFilePath = destMetadataFilePath;
	}





	/**
	 * @return the jobProfileName
	 */
	public String getJobProfileName() {
		return jobProfileName;
	}


	/**
	 * @param jobProfileName the jobProfileName to set
	 */
	public void setJobProfileName(String jobProfileName) {
		this.jobProfileName = jobProfileName;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((destMetadataFilePath == null) ? 0 : destMetadataFilePath
						.hashCode());
		result = prime * result
				+ ((destinationPath == null) ? 0 : destinationPath.hashCode());
		result = prime * result
				+ ((jobProfileName == null) ? 0 : jobProfileName.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TranscodeJob other = (TranscodeJob) obj;
		if (destMetadataFilePath == null) {
			if (other.destMetadataFilePath != null)
				return false;
		} else if (!destMetadataFilePath.equals(other.destMetadataFilePath))
			return false;
		if (destinationPath == null) {
			if (other.destinationPath != null)
				return false;
		} else if (!destinationPath.equals(other.destinationPath))
			return false;
		if (jobProfileName == null) {
			if (other.jobProfileName != null)
				return false;
		} else if (!jobProfileName.equals(other.jobProfileName))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		return true;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TranscodeJob [jobProfileName=" + jobProfileName
				+ "destinationPath="+ destinationPath +
				", destMetadataFilePath=" + destMetadataFilePath + ", source=" + source
				+ ", jobExtensionAttributes=" + jobExtensionAttributes + "]";
	}






}
