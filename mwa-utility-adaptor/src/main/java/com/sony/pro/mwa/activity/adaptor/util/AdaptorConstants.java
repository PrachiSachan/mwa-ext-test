package com.sony.pro.mwa.activity.adaptor.util;

public class AdaptorConstants {
	//WebService Related
	public static final String HTTP_PROTOCOL="http://";

	//com.sun.xml.ws.connect.timeout
	//com.sun.xml.ws.request.timeout
	
	public static final String WS_CONNECT_TIME_OUT_KEY = "com.sun.xml.internal.ws.connect.timeout";
	public static final String WS_REQUEST_TIME_OUT_KEY = "com.sun.xml.internal.ws.request.timeout";
	public static final int    DEFAULT_TIME_OUT = 10*1000 ; // 10 seconds	
	

	public static final String WS_REQUEST_TIMEOUT_KEY = "wsRequestTimeout";
	
	//Retry mechanism keys
	public static final String RETRYABLE_EXCEPTIONS_KEY = "ws.retryOn.exceptions";	
	public static final String RETRY_INTERVAL_KEY = "ws.retry.interval.millSecs";
	public static final String RETRY_MECHANISM_ENABLE_KEY = "ws.retry.enable";
	public static final long   DEFAULT_RETRY_INTERVAL = 1000;
	public static final String RETRYABLE_DEVICE_EXCEPTIONS_KEY = "retry.deviceExceptions";
	public static final String RETRY_DEVICE_EXCEPTION_INTERVAL_KEY = "retry.deviceException.interval.millSecs";
	public static final String RETRY_CONFIG_FILE_PATH_KEY = "retry.config.propertyfilePath";
	public static final String RETRY_DEVICE_EXCEPTION_KEY = "retry.deviceException.enable";
	public static final String ALLOWABLE_RETRIES_KEY = "retry.deviceException.allowableRetries";
	
	public static final String FALSE_UPPER_STR="FALSE";
	public static final String FALSE_LOWER_STR = "false";	
	public static final String TRUE_LOWER_STR = "true";
	public static final String EMPTY_STR = "";
	
	
	//MBC/MWA Status
	
	
	public static final String ERROR_STATUS = "ERROR";
	public static final String INPROGRESS_STATUS = "IN-PROGRESS";
	public static final String QUEUED_STATUS = "QUEUED";
	public static final String CANCELLED_STATUS = "CANCELLED";
	public static final String COMPLETED_STATUS = "COMPLETED";
	public static final String UNKNOWN_STATUS = "UNKNOWN";
	public static final String STARTED_STATUS="STARTED";
	public static final String SUCCESS_STATUS="SUCCESS";
	
	//Error Messages
	public static final String RESUME_NOT_SUPPORTED="Resume Operation is not Supported";
	public static final String PAUSE_NOT_SUPPORED="Pause Operation is not Supported";	
	public static final String CANCEL_NOT_SUPPORED="Cancel Operation is not Supported";
	
	//For QC Url Generation
	public static final String FILE_PATH_NULL_ERROR_MSG = "[00633406] File path is null";
	public static final String LOCATION_PATH_NULL_ERROR_MSG ="[00633406] Location path is null";
	public static final String LOCATION_HOST_NULL_ERROR_MSG="[00633406] Location host is null";
	public static final String LOCATION_HOST_EMPTY_ERROR_MSG="[00633406] Location host is empty";
	public static final String USER_INFO_NULL_ERROR_MSG = "[00633406] User info is null";
	public static final String USER_INFO_EMPTY_ERROR_MSG = "[00633406] User info is empty";
	public static final String USER_NAME_NULL_ERROR_MSG = "[00633406] User name is null";	
	
	public static final String DEFAULT_UUID = "00000000-0000-0000-0000-000000000000";
	
	/** JDK's JAX-WS implementation class */
	public final static String JDK_JAXWS_SERVICE_PROVIDER_IMPL ="com.sun.xml.internal.ws.spi.ProviderImpl";
}
