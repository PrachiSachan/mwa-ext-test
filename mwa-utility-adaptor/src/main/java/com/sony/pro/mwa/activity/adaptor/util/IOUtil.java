package com.sony.pro.mwa.activity.adaptor.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;


/**
 * 
 * @author gbputchs
 *
 */
public final class IOUtil {
	
	
	//Private Constructor
	private IOUtil() {}
	
	/**
	 * Closes InputStream
	 * @param inputStream
	 */
	public static void closeQuitely(InputStream inputStream) {		
		try
		{
			if(inputStream!=null) {
				inputStream.close();
			}
		}
		catch(IOException ex) {			
			
		}
	}
	
	
	/**
	 * Closes InputStream
	 * @param inputStream
	 */
	public static void closeQuitely(OutputStream outputStream) {		
		try
		{
			if(outputStream!=null) {
				outputStream.close();
			}
		}
		catch(IOException ex) {			
			
		}
	}	
	
	
	/**
	 * Closes InputStream
	 * @param inputStream
	 */
	public static void closeQuitely(Writer bufferedWriter) {		
		try
		{
			if(bufferedWriter!=null) {
				bufferedWriter.close();
			}
		}
		catch(IOException ex) {			
			
		}
	}			 
}
