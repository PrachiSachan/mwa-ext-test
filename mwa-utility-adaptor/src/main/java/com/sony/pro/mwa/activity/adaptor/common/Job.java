package com.sony.pro.mwa.activity.adaptor.common;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 *
 */
public class Job {

	private String name;
	private String jobId;
	private Date dueDate;
	private Date scheduledDate;
	private PriorityEnum priority;
	protected Map<String,JobProperty>  jobExtensionAttributes;
	private String userId;


	/**
	 *
	 * @param name
	 * @param jobId
	 * @param deviceJobId
	 * @param dueDate
	 * @param scheduledDate
	 * @param operation
	 * @param priority
	 * @param jobExtensionAttributes
	 * @param userId
	 */
	public Job(String name,String jobId,Date dueDate, Date scheduledDate,
			PriorityEnum priority,Map<String,JobProperty>  jobExtensionAttributes,String userId) {
		super();
		this.name = name;
		this.jobId = jobId;
		this.dueDate = dueDate;
		this.scheduledDate = scheduledDate;
		this.priority = priority;
		this.jobExtensionAttributes=jobExtensionAttributes;
		this.userId = userId;
	}


	public Job() {
		this.jobExtensionAttributes = new HashMap<String,JobProperty>();
	}



	/**
	 * @return the dueDate
	 */
	public Date getDueDate() {
		return dueDate;
	}


	/**
	 * @param dueDate the dueDate to set
	 */
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}


	/**
	 * @return the scheduledDate
	 */
	public Date getScheduledDate() {
		return scheduledDate;
	}


	/**
	 * @param scheduledDate the scheduledDate to set
	 */
	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}


	/**
	 * @return the priority
	 */
	public PriorityEnum getPriority() {
		return priority;
	}


	/**
	 * @param priority the priority to set
	 */
	public void setPriority(PriorityEnum priority) {
		this.priority = priority;
	}



	/**
	 * @return the jobExtensionAttributes
	 */
	public Map<String, JobProperty> getJobExtensionAttributes() {
		return jobExtensionAttributes;
	}


	/**
	 * @param jobExtensionAttributes the jobExtensionAttributes to set
	 */
	public void setJobExtensionAttributes(Map<String, JobProperty> jobExtensionAttributes) {
		this.jobExtensionAttributes = jobExtensionAttributes;
	}






	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}




	/**
	 * @return the jobId
	 */
	public String getJobId() {
		return jobId;
	}


	/**
	 * @param jobId the jobId to set
	 */
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}




     /* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime
				* result
				+ ((jobExtensionAttributes == null) ? 0
						: jobExtensionAttributes.hashCode());
		result = prime * result + ((jobId == null) ? 0 : jobId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((priority == null) ? 0 : priority.hashCode());
		result = prime * result
				+ ((scheduledDate == null) ? 0 : scheduledDate.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Job other = (Job) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		if (dueDate == null) {
			if (other.dueDate != null)
				return false;
		} else if (!dueDate.equals(other.dueDate))
			return false;
		if (jobExtensionAttributes == null) {
			if (other.jobExtensionAttributes != null)
				return false;
		} else if (!jobExtensionAttributes.equals(other.jobExtensionAttributes))
			return false;
		if (jobId == null) {
			if (other.jobId != null)
				return false;
		} else if (!jobId.equals(other.jobId))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (priority != other.priority)
			return false;
		if (scheduledDate == null) {
			if (other.scheduledDate != null)
				return false;
		} else if (!scheduledDate.equals(other.scheduledDate))
			return false;
		return true;
	}





	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Job [name=" + name + ", jobId=" + jobId + ", dueDate="
				+ dueDate + ", scheduledDate=" + scheduledDate + ", priority="
				+ priority + ", jobExtensionAttributes="
				+ jobExtensionAttributes + ", userId=" + userId + "]";
	}


	/**
	 *
	 * @return KeySet for Job Extension Attributes
	 */
	public Set<String> getAttributesKeySet() {
		if(this.jobExtensionAttributes!=null) {
			return this.jobExtensionAttributes.keySet();
		}
		return null;
	}


	/**
	 *
	 * @param key
	 * @return Job Extension Attribute key value
	 */
	public String getValue(String key) {
		if(this.jobExtensionAttributes!=null) {
			return this.jobExtensionAttributes.get(key).getValue();
		}
		return null;
	}


	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}


	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}


}
