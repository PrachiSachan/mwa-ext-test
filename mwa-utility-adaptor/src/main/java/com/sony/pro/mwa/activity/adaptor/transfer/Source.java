package com.sony.pro.mwa.activity.adaptor.transfer;

import com.sony.pro.mwa.activity.adaptor.common.Path;

/**
 *
 * @author gbputchs
 *
 */

public class Source {


	private String systemName;
	private String scheme;
	private String ipAddress;
	private int port;
	private String username;
	private String password;
	private boolean isDirectory;
	private String newName;
	private Path sourcePath;


	/**
	 * Default Constructor
	 */
	public Source() {

	}


	/**
	 *
	 * @param systemName
	 * @param scheme
	 * @param ipAddress
	 * @param port
	 * @param username
	 * @param password
	 * @param isDirectory
	 * @param newName
	 * @param sourcePath
	 */
	public Source(String systemName, String scheme, String ipAddress,
			int port, String username, String password, boolean isDirectory,
			String newName, Path sourcePath) {
		super();
		this.systemName = systemName;
		this.scheme = scheme;
		this.ipAddress = ipAddress;
		this.port = port;
		this.username = username;
		this.password = password;
		this.isDirectory = isDirectory;
		this.newName = newName;
		this.sourcePath = sourcePath;
	}


	/**
	 * @return the systemName
	 */
	public String getSystemName() {
		return systemName;
	}
	/**
	 * @param systemName the systemName to set
	 */
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	/**
	 * @return the scheme
	 */
	public String getScheme() {
		return scheme;
	}
	/**
	 * @param scheme the scheme to set
	 */
	public void setScheme(String scheme) {
		this.scheme = scheme;
	}
	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}
	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}
	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the isDirectory
	 */
	public boolean isDirectory() {
		return isDirectory;
	}
	/**
	 * @param isDirectory the isDirectory to set
	 */
	public void setDirectory(boolean isDirectory) {
		this.isDirectory = isDirectory;
	}
	/**
	 * @return the newName
	 */
	public String getNewName() {
		return newName;
	}
	/**
	 * @param newName the newName to set
	 */
	public void setNewName(String newName) {
		this.newName = newName;
	}
	/**
	 * @return the sourcePath
	 */
	public Path getSourcePath() {
		return sourcePath;
	}
	/**
	 * @param sourcePath the sourcePath to set
	 */
	public void setSourcePath(Path sourcePath) {
		this.sourcePath = sourcePath;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((ipAddress == null) ? 0 : ipAddress.hashCode());
		result = prime * result + (isDirectory ? 1231 : 1237);
		result = prime * result + ((newName == null) ? 0 : newName.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result + port;
		result = prime * result + ((scheme == null) ? 0 : scheme.hashCode());
		result = prime * result
				+ ((sourcePath == null) ? 0 : sourcePath.hashCode());
		result = prime * result
				+ ((systemName == null) ? 0 : systemName.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Source other = (Source) obj;
		if (ipAddress == null) {
			if (other.ipAddress != null)
				return false;
		} else if (!ipAddress.equals(other.ipAddress))
			return false;
		if (isDirectory != other.isDirectory)
			return false;
		if (newName == null) {
			if (other.newName != null)
				return false;
		} else if (!newName.equals(other.newName))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (port != other.port)
			return false;
		if (scheme == null) {
			if (other.scheme != null)
				return false;
		} else if (!scheme.equals(other.scheme))
			return false;
		if (sourcePath == null) {
			if (other.sourcePath != null)
				return false;
		} else if (!sourcePath.equals(other.sourcePath))
			return false;
		if (systemName == null) {
			if (other.systemName != null)
				return false;
		} else if (!systemName.equals(other.systemName))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */


	@Override
	public String toString() {
		return "Source [systemName=" + systemName + ", scheme=" + scheme
				+ ", ipAddress=" + ipAddress + ", port=" + port + ", username="
				+ username + ", password=" + password + ", isDirectory="
				+ isDirectory + ", newName=" + newName + ", sourcePath="
				+ sourcePath + "]";
	}

}
