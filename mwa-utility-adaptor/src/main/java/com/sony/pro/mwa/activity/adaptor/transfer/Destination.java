package com.sony.pro.mwa.activity.adaptor.transfer;

import com.sony.pro.mwa.activity.adaptor.common.Path;

/**
 *
 * @author gbputchs
 *
 */
public class Destination {

	private String systemName;
	private String scheme;
	private String ipAddress;
	private String username;
	private String password;
	private int port;
	private Path location;

	/**
	 * Default Constructor
	 */
	public Destination() {

	}


	/**
	 * Copy Constructor
	 * @param dest
	 */
	public Destination(Destination destination) {
		this.systemName = destination.systemName;
		this.scheme = destination.scheme;
		this.ipAddress = destination.ipAddress;
		this.username = destination.username;
		this.password = destination.password;
		this.location = new Path();
		this.location.setFullPath(destination.getLocation().getFullPath());
		this.port = destination.port;

	}


	/**
	 *
	 * @param systemName
	 * @param scheme
	 * @param ipAddress
	 * @param username
	 * @param password
	 * @param location
	 * @param port
	 */
	public Destination(String systemName, String scheme, String ipAddress,
			String username, String password, Path location, int port) {
		super();
		this.systemName = systemName;
		this.scheme = scheme;
		this.ipAddress = ipAddress;
		this.username = username;
		this.password = password;
		this.location = location;
		this.port = port;
	}

	/**
	 * @return the systemName
	 */
	public String getSystemName() {
		return systemName;
	}

	/**
	 * @param systemName the systemName to set
	 */
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	/**
	 * @return the scheme
	 */
	public String getScheme() {
		return scheme;
	}

	/**
	 * @param scheme the scheme to set
	 */
	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the location
	 */
	public Path getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(Path location) {
		this.location = location;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((ipAddress == null) ? 0 : ipAddress.hashCode());
		result = prime * result
				+ ((location == null) ? 0 : location.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result + port;
		result = prime * result + ((scheme == null) ? 0 : scheme.hashCode());
		result = prime * result
				+ ((systemName == null) ? 0 : systemName.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Destination other = (Destination) obj;
		if (ipAddress == null) {
			if (other.ipAddress != null)
				return false;
		} else if (!ipAddress.equals(other.ipAddress))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (port != other.port)
			return false;
		if (scheme == null) {
			if (other.scheme != null)
				return false;
		} else if (!scheme.equals(other.scheme))
			return false;
		if (systemName == null) {
			if (other.systemName != null)
				return false;
		} else if (!systemName.equals(other.systemName))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Destination [systemName=" + systemName + ", scheme=" + scheme
				+ ", ipAddress=" + ipAddress + ", username=" + username
				+ ", password=" + password + ", location=" + location
				+ ", port=" + port + "]";
	}









}
