package com.sony.pro.mwa.activity.adaptor.common;



/**
 *
 */
public class JobResponse {


	/**
	 * Status field is used to tell the status of the operation
	 * for e.g., cancel/pause/submit
	 */
	private String status;

	// Details will give more information
	private String statusDetails;

	//This field will get populated, when the submitJob() successfully
	//submits the job to the device
	private String deviceJobId;

	/**
	 * Constructor
	 * @param status
	 * @param statusDetails
	 * @param deviceJobId
	 */
	public JobResponse(String status, String statusDetails, String deviceJobId) {
		super();
		this.status = status;
		this.statusDetails = statusDetails;
		this.deviceJobId = deviceJobId;
	}



	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}



	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}



	/**
	 * @return the statusDetails
	 */
	public String getStatusDetails() {
		return statusDetails;
	}



	/**
	 * @param statusDetails the statusDetails to set
	 */
	public void setStatusDetails(String statusDetails) {
		this.statusDetails = statusDetails;
	}



	/**
	 * @return the deviceJobId
	 */
	public String getDeviceJobId() {
		return deviceJobId;
	}



	/**
	 * @param deviceJobId the deviceJobId to set
	 */
	public void setDeviceJobId(String deviceJobId) {
		this.deviceJobId = deviceJobId;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((deviceJobId == null) ? 0 : deviceJobId.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result
				+ ((statusDetails == null) ? 0 : statusDetails.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobResponse other = (JobResponse) obj;
		if (deviceJobId == null) {
			if (other.deviceJobId != null)
				return false;
		} else if (!deviceJobId.equals(other.deviceJobId))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (statusDetails == null) {
			if (other.statusDetails != null)
				return false;
		} else if (!statusDetails.equals(other.statusDetails))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "JobResponse [status=" + status + ", statusDetails="
				+ statusDetails + ", deviceJobId=" + deviceJobId + "]";
	}

}
