package com.sony.pro.mwa.activity.adaptor.util;


import static com.sony.pro.mwa.activity.adaptor.util.AdaptorConstants.*;

import java.net.URL;

import com.sony.pro.mwa.activity.adaptor.exception.ServiceException;
import com.sony.pro.mwa.activity.adaptor.qc.Location;

public class QualityControlUrlUtils {


	private enum PathType  { FTP_PATH,UNC_PATH,UNIX_PATH,WINDOWS_PATH };


	/**
	 * Validate the Provided Source Location
	 * and returns the Full location URL for
	 * @param source
	 * @return
	 */
	public static String getContentURL(Location source) throws ServiceException{

		String url="";
		try	{
			String path = source.getPath().getFullPath();
			switch(determineLocationType(path))	{

				//FTP Location?
				case FTP_PATH:

					URL url_1 = new URL(path);
					validateSource(source); //Validate Source
					validateIPAddress(url_1,source.getIpAddress()); //Validate IP-Address
					validateUserInfo(url_1,source.getUserName()); //Validate User Info
					if ((url_1.getFile()).equals("")) {
						throw new ServiceException(FILE_PATH_NULL_ERROR_MSG);
					}
					// create url
					url = "ftp://" + source.getUserName() + "@" + source.getIpAddress() + url_1.getFile();
					break;

				//UNC,UNIX and WINDOWS ?
				case UNC_PATH:
				case UNIX_PATH:
				case WINDOWS_PATH:
					url=path;
					break;
				default:
					throw new ServiceException("Unable to determine the location Type "+path);
			}
		}
		catch(ServiceException ex) {
			throw ex;
		}
		catch (Exception e)	{
			throw new ServiceException(e);
		}
		return url;
	}


	/**
	 *
	 * @param source
	 * @throws ServiceException
	 */
	private static void validateSource(Location source) throws ServiceException {
		//Validate Source Path
		if(source.getPath() == null) {
			throw new ServiceException(LOCATION_PATH_NULL_ERROR_MSG);
		}
		else if(source.getPath().getFullPath() == null ) {
			throw new ServiceException(LOCATION_PATH_NULL_ERROR_MSG);
		}

	}

	/**
	 *
	 * @param url_1
	 * @param ipAddress
	 * @throws ServiceException
	 */
	private static void validateIPAddress(URL url,String ipAddress) throws ServiceException {
		// check address
		if (ipAddress == null || ipAddress.isEmpty()) {
			if (url.getHost() == null){
				throw new ServiceException(LOCATION_HOST_NULL_ERROR_MSG);
			}else if (url.getHost().isEmpty()) {
				throw new ServiceException(LOCATION_HOST_EMPTY_ERROR_MSG);
			}else {
				ipAddress = url.getHost();
			}
		}
	}


	/**
	 *
	 * @param url
	 * @param userName
	 * @throws ServiceException
	 */
	private static void validateUserInfo(URL url,String userName) throws ServiceException {
		// check username
		if (userName == null || userName.isEmpty()) {
			String userInfo = url.getUserInfo();
			if (userInfo == null) { throw new ServiceException(USER_INFO_NULL_ERROR_MSG); }
			else if (userInfo.isEmpty()) { throw new ServiceException(USER_INFO_EMPTY_ERROR_MSG); }
			else
			{
				if (userInfo.indexOf(":") == -1) {
					userName = userInfo;
				}
				else {
					userName = (userInfo).substring(0, (userInfo).indexOf(":"));
					if (userName.equals("")) { throw new ServiceException(USER_NAME_NULL_ERROR_MSG); }
				}
			}
		}
	}


	/**
	 * Determine the Location Type
	 * @param path
	 * @return
	 */
	private static PathType  determineLocationType(String path) {
		if (path.startsWith("ftp://")) {
			return PathType.FTP_PATH;
		}
		else if ((path.substring(1, 2)).equals("\\")) {
			return PathType.UNC_PATH; // unc
		}
		else if ((path.substring(1, 2)).equals(":")) {
			return PathType.WINDOWS_PATH; // windows
		}
		else {
			return PathType.UNIX_PATH;		// unix
		}

	}
}
