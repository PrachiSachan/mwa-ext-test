package com.sony.pro.mwa.activity.adaptor.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.sony.pro.mwa.activity.adaptor.exception.ServiceException;
import com.sony.pro.mwa.activity.adaptor.qc.ReportInput;
import com.sony.pro.mwa.activity.adaptor.qc.ReportOutput;
import com.sony.pro.mwa.activity.adaptor.qc.report.Report;


public final class QCReportUtils {

	private QCReportUtils() {

	}

	/**
	 * Returns the Device specific Report FilePath
	 * @param input
	 * @return
	 */
	public static String getReportFilePath(ReportInput input,String suffix) {

		String uuid = UUID.randomUUID().toString();
		if(input.getJobId()!=null) {
			uuid = input.getJobId();
		}
		return new File(input.getReportPath() + File.separator +uuid +suffix).getAbsolutePath();
	}


	/**
	 * Generates the DeviceSpecific Report
	 * and saves to FileSystem
	 * @param taskInformation
	 * @param input
	 * @param ouput
	 * @throws ServiceException
	 * @throws JAXBException
	 */
	public static void generateDeviceReportXml(Object jaxbReportObj,ReportInput input,ReportOutput output,String suffix) throws ServiceException, JAXBException {
		/** Generate Device Report XML from taskInformation instance */
		String deviceReportXml = JAXBUtils.convertObjectToXML(jaxbReportObj);
		output.setDeviceReportXmlStr(deviceReportXml);

		if(input.isGenerateiDeviceReport()) {
			// Prepare the Device Report File Path
			String deviceReportXmlPath = getReportFilePath(input,suffix);

			//Save the Report to a File
			FileUtils.stringToFile(deviceReportXml,null, deviceReportXmlPath);

			//Set the Report XML to the ReportOutput instance
			output.setDeviceReportXmlPath(deviceReportXmlPath);
		}
	}



	/**
	 * Generates the DeviceSpecific Report
	 * and saves to FileSystem
	 * @param taskInformation
	 * @param input
	 * @param ouput
	 * @throws ServiceException
	 * @throws JAXBException
	 */
	public static void generateDeviceReportXml(String deviceReportXml,ReportInput input,ReportOutput output,String suffix) throws ServiceException, JAXBException {

		output.setDeviceReportXmlStr(deviceReportXml);
		if(input.isGenerateiDeviceReport()) {
			// Prepare the Device Report File Path
			String deviceReportXmlPath = getReportFilePath(input,suffix);

			//Save the Report to a File
			FileUtils.stringToFile(deviceReportXml,null, deviceReportXmlPath);

			//Set the Report XML to the ReportOutput instance
			output.setDeviceReportXmlPath(deviceReportXmlPath);
		}
	}


	/**
	 * Generates the Generic Report XML , will be stored
	 * on FS ( this is only for MWA), but MBC expects the
	 * generic report as a string
	 * @param genericReport
	 * @param input
	 * @param output
	 * @throws ServiceException
	 */
	public static void generateGenericReportXml(Report genericReport,ReportInput input,ReportOutput output) throws ServiceException {
		try
		{

			/** Generate the Generic Report XML from Report Instance */
			String genericReportXml = JAXBUtils.convertObjectToXML(genericReport);
			output.setMbcReportXmlStr(genericReportXml);

			if(input.isGenerateGenericReport()) {

				/** Prepare Generic XML Report File Path */
				String genericReportXmlPath =  QCReportUtils.getReportFilePath(input, "_1_mwa.xml");

				/** Write String to File */
				FileUtils.stringToFile(genericReportXml, null, genericReportXmlPath);

				/** Set the File Path to ReportOutput */
				output.setGenericReportXmlPath(genericReportXmlPath);
			}
		}
		catch(ServiceException ex) {
			throw ex;
		}
		catch(Exception ex) {
			throw new ServiceException(ex);
		}
	}



	/**
	 * Returns the Current Date in XMLGregorianCalendar
	 * @return
	 * @throws ServiceException
	 */
	public static XMLGregorianCalendar getCurrentDateStr() throws ServiceException {
		try {
			/** SimpleDateFormat static instances are not thread safe */
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			String date = sdf.format(new Date());
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
		} catch (Exception ex) {
			throw new ServiceException(ex);
		}
	}
}
