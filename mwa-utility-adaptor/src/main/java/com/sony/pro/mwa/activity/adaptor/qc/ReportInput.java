package com.sony.pro.mwa.activity.adaptor.qc;


/**
 * 
 * @author gbputchs
 *
 */
public class ReportInput {

	/** Path Location for keeping the Devicespecific report and
	 Generic Report */	
	 private String reportPath;
	 	 	 
	 /** deviceJobId  */
	 private String deviceJobId;
	 
	 
	 private boolean generateGenericReport;	 	 
	 private boolean generateiDeviceReport;
	 
	 
	 /** Job Id in MBC.  Report Filename will use this jobid for constructing thefilename
	  *  If it's not provided , a uuid will be used 
	  */
	 private String jobId;
	 
	 private boolean generatePdfReport;

	 
	 public ReportInput() {
		 
	 }
	 
	 
 
	/**
	 *  
	 * @param reportPath
	 * @param deviceJobId
	 * @param generateGenericReport
	 * @param generateiDeviceReport
	 * @param jobId
	 */
	public ReportInput(String reportPath, String deviceJobId,boolean generateGenericReport, 
			boolean generateiDeviceReport,
			String jobId) {

		this.reportPath = reportPath;
		this.deviceJobId = deviceJobId;
		this.generateGenericReport = generateGenericReport;
		this.generateiDeviceReport = generateiDeviceReport;
		this.jobId = jobId;
		this.generatePdfReport = false;
	}

	/**
	 *  
	 * @param reportPath
	 * @param deviceJobId
	 * @param generateGenericReport
	 * @param generateiDeviceReport
	 * @param jobId
	 * @param generatePdfReport
	 */
	public ReportInput(String reportPath, String deviceJobId,boolean generateGenericReport, 
			boolean generateiDeviceReport,
			String jobId,boolean generatePdfReport) {

		this.reportPath = reportPath;
		this.deviceJobId = deviceJobId;
		this.generateGenericReport = generateGenericReport;
		this.generateiDeviceReport = generateiDeviceReport;
		this.jobId = jobId;
		this.generatePdfReport = generatePdfReport;
	}



	/**
	 * @return the reportPath
	 */
	public String getReportPath() {
		return reportPath;
	}

	/**
	 * @param reportPath the reportPath to set
	 */
	public void setReportPath(String reportPath) {
		this.reportPath = reportPath;
	}

 

	/**
	 * @return the deviceJobId
	 */
	public String getDeviceJobId() {
		return deviceJobId;
	}

	/**
	 * @param deviceJobId the deviceJobId to set
	 */
	public void setDeviceJobId(String deviceJobId) {
		this.deviceJobId = deviceJobId;
	}


	/**
	 * @return the generateGenericReport
	 */
	public boolean isGenerateGenericReport() {
		return generateGenericReport;
	}


	/**
	 * @param generateGenericReport the generateGenericReport to set
	 */
	public void setGenerateGenericReport(boolean generateGenericReport) {
		this.generateGenericReport = generateGenericReport;
	}

	
	
	

	/**
	 * @return the jobId
	 */
	public String getJobId() {
		return jobId;
	}


	/**
	 * @param jobId the jobId to set
	 */
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}



	/**
	 * @return the generateiDeviceReport
	 */
	public boolean isGenerateiDeviceReport() {
		return generateiDeviceReport;
	}



	/**
	 * @param generateiDeviceReport the generateiDeviceReport to set
	 */
	public void setGenerateiDeviceReport(boolean generateiDeviceReport) {
		this.generateiDeviceReport = generateiDeviceReport;
	}


	/**
	 * @return generatePdfReport
	 */
	public boolean isGeneratePdfReport() {
		return generatePdfReport;
	}


	/**
	 * @param generatePdfReport set  generatePdfReport
	 */
	public void setGeneratePdfReport(boolean generatePdfReport) {
		this.generatePdfReport = generatePdfReport;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((deviceJobId == null) ? 0 : deviceJobId.hashCode());
		result = prime * result + (generateGenericReport ? 1231 : 1237);
		result = prime * result + (generatePdfReport ? 1231 : 1237);
		result = prime * result + (generateiDeviceReport ? 1231 : 1237);
		result = prime * result + ((jobId == null) ? 0 : jobId.hashCode());
		result = prime * result
				+ ((reportPath == null) ? 0 : reportPath.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReportInput other = (ReportInput) obj;
		if (deviceJobId == null) {
			if (other.deviceJobId != null)
				return false;
		} else if (!deviceJobId.equals(other.deviceJobId))
			return false;
		if (generateGenericReport != other.generateGenericReport)
			return false;
		if (generatePdfReport != other.generatePdfReport)
			return false;
		if (generateiDeviceReport != other.generateiDeviceReport)
			return false;
		if (jobId == null) {
			if (other.jobId != null)
				return false;
		} else if (!jobId.equals(other.jobId))
			return false;
		if (reportPath == null) {
			if (other.reportPath != null)
				return false;
		} else if (!reportPath.equals(other.reportPath))
			return false;
		return true;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ReportInput [reportPath=" + reportPath + ", deviceJobId="
				+ deviceJobId + ", generateGenericReport="
				+ generateGenericReport + ", generateiDeviceReport="
				+ generateiDeviceReport + ", jobId=" + jobId
				+ ", generatePdfReport=" + generatePdfReport + "]";
	}



	

	
	
}
