package com.sony.pro.mwa.activity.adaptor.util;

import static com.sony.pro.mwa.activity.adaptor.util.AdaptorConstants.*;

import java.lang.reflect.Field;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.spi.Provider;
import javax.xml.ws.spi.ServiceDelegate;

import com.sony.pro.mwa.activity.adaptor.exception.ServiceException;


/**
 * Service Utilities
 *
 * @author gbputchs
 *
 */
public final class ServiceUtil {

	private static final String DELEGATE_FIELD="delegate";
	private static final String AXIS_IMPLEMENTATION="axis";

	private ServiceUtil() {

	}


	/**
	 * Following delegation is required because
	 * 1. webMethods 8.2 & 9.5 does not pick the JDK's implementation, it's
	 *    picking the Axi2 implementation, which has serious performance issues.
	 * @param service
	 * @param wsdlURL
	 * @param serviceName
	 * @throws ServiceException
	 */
	public static void delegateService(Service service,URL wsdlURL,QName serviceName) throws ServiceException {
		try
		{
			Field delegateField = Service.class.getDeclaredField(DELEGATE_FIELD);
			delegateField.setAccessible(true);
			ServiceDelegate previousDelegate = (ServiceDelegate) delegateField.get(service);
			/** Check if it's an Axis implementation */
			if (previousDelegate.getClass().getName().contains(AXIS_IMPLEMENTATION)) {
				ServiceDelegate serviceDelegate = ((Provider) Class.forName(JDK_JAXWS_SERVICE_PROVIDER_IMPL).newInstance())
							.createServiceDelegate(wsdlURL, serviceName, service.getClass());
				delegateField.set(service, serviceDelegate);
			}
		}
		catch(Exception ex) {
			throw new ServiceException("Unable to delegate the Service ",ex);
		}
	}

	/**
	 * Sets WebService Connect , Request TimeOut
	 * and EndPoint URL dynamically
	 * @param provider
	 * @param endPoint
	 */
	public static void setEndPointAndTimeOut(BindingProvider provider,String endPoint,int timeOut) {
		provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,endPoint);
		provider.getRequestContext().put(WS_CONNECT_TIME_OUT_KEY, timeOut);
		provider.getRequestContext().put(WS_REQUEST_TIME_OUT_KEY, timeOut);
	}

}
