package com.sony.pro.mwa.activity.adaptor.common;



/**
 *
 */
public enum PriorityEnum
{
	NONE("none"),
	LOW("low"),
	MEDIUM("medium"),
	HIGH("high"),
	URGENT("urgent");

	private String priority;

	/**
	 *
	 * @param priority
	 */
	PriorityEnum(String priority) {
		this.priority = priority;
	}

	/**
	 *
	 * @return
	 */
	public String getPriority() {
		return this.priority;
	}
	/**
	 *
	 * @param priority
	 * @return
	 */
 	public static PriorityEnum getEnum(String priorityStr) {
		for(PriorityEnum priorityEnum : PriorityEnum.values()) {
			if(priorityStr.equalsIgnoreCase(priorityEnum.priority)) {
				return priorityEnum;
			}
		}
		return null;
	}
}
