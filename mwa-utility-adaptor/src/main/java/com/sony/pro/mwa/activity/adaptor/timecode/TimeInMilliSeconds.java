package com.sony.pro.mwa.activity.adaptor.timecode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 
 * @author gbputchs
 *
 */
public class TimeInMilliSeconds {
 
	private static final Pattern TIME_IN_MILLI_SECS_PATTERN;
	private int hh;
	private int mm;
	private int ss;
	private int sss;
	

	static {
		TIME_IN_MILLI_SECS_PATTERN= Pattern.compile("(\\d\\d)[:;](\\d\\d)[:;](\\d\\d)([.:;])(\\d\\d\\d)");	
	}

	/**
	 * Constructor
	 * @param timecodeStr
	 * @param fps
	 */
	public TimeInMilliSeconds(String timecodeStr, double fps) {
		
		//Validate frames per second
		if (fps <=0) {
			throw new IllegalArgumentException("Invalid Frame Rate: " + fps);
		}
		 
		//Milliseconds Regex pattern match
		Matcher matcher = TIME_IN_MILLI_SECS_PATTERN.matcher(timecodeStr);
		if (!matcher.matches()) {
			throw new IllegalArgumentException("Invalid Time Code: " + timecodeStr);
		}
		
		//Hour
		this.hh = new Integer(matcher.group(1));
		
		//Minutes
		this.mm = new Integer(matcher.group(2));
		
		//Seconds
		this.ss = new Integer(matcher.group(3));
		
		//Milliseconds
		this.sss = new Integer(matcher.group(5));
	}

 
	/**
	 * 
	 * @return
	 */
	public String getTimecodeStr() {
		return String.format("%02d:%02d:%02d:%02d", hh, mm, ss, sss); //$NON-NLS-1$
	}

	/**
	 * 
	 * @return
	 */
	public int getTotalMillis() {
		return ((getHh() * 60 + getMm()) * 60 + getSs()) * 1000 + getSss(); 
	}

	/**
	 * @return the hh
	 */
	public int getHh() {
		return hh;
	}

	/**
	 * @param hh the hh to set
	 */
	public void setHh(int hh) {
		this.hh = hh;
	}

	/**
	 * @return the mm
	 */
	public int getMm() {
		return mm;
	}

	/**
	 * @param mm the mm to set
	 */
	public void setMm(int mm) {
		this.mm = mm;
	}

	/**
	 * @return the ss
	 */
	public int getSs() {
		return ss;
	}

	/**
	 * @param ss the ss to set
	 */
	public void setSs(int ss) {
		this.ss = ss;
	}

	/**
	 * @return the sss
	 */
	public int getSss() {
		return sss;
	}

	/**
	 * @param sss the sss to set
	 */
	public void setSss(int sss) {
		this.sss = sss;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TimeInMilliSeconds [hh=" + hh + ", mm=" + mm + ", ss=" + ss
				+ ", sss=" + sss + "]";
	}
}
