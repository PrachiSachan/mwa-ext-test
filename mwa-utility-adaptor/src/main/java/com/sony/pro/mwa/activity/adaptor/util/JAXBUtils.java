package com.sony.pro.mwa.activity.adaptor.util;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.DataBindingException;
import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBException;

/**
 * 
 * JAXB Utilities for Marshalling & Unmarshalling
 * the JAXB Beans to and from the XML. This class
 * will use the javax.xml.bind.JAXB Utility class
 * 
 * @author gbputchs
 *
 */
public final class JAXBUtils {

	/**
	 * Constructor 
	 */
	private JAXBUtils() {

	}

	/**
	 * Convert XML to object 
	 * @param className
	 * @param xmlPath  : XML file location
	 * @return
	 */		 
	public static  <T> T  convertXMLToObject(Class<T> className, String xmlPath) throws DataBindingException{
		return  JAXB.unmarshal(new File(xmlPath),className);
	}	


	/**
	 * Convert XML to java Bean
	 * @param xml
	 * @param className
	 * @return
	 * @throws JAXBException
	 */
	public static <T> T  convertXMLToObject(String xml, Class<T> className) throws DataBindingException{
		return (T) JAXB.unmarshal(new StringReader(xml), className);
	}		


	/**
	 * Converts Java Bean to XML
	 * @param <T>
	 * @param jaxbObject
	 * @return
	 * @throws JAXBException 
	 */
	public static <T> String convertObjectToXML(T jaxbObject) throws DataBindingException {	 
		StringWriter stringWriter = new StringWriter();
		JAXB.marshal(jaxbObject, stringWriter);		
		return stringWriter.toString();
	}	
	
}
