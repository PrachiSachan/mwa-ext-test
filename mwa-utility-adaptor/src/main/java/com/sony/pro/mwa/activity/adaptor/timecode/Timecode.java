package com.sony.pro.mwa.activity.adaptor.timecode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author gbputchs
 *
 */
public class Timecode {

	private int hh;
	private int mm;
	private int ss;
	private int ff;
	private double fps;
	private boolean df;

	private double framerate;
	private int roundedFrameRate;
	private long dropFrames;
	private int framesPerSecond; 
	private long framesPerMinute;
	private long framesPer10Minutes;	
	private static final Pattern TIME_PATTERN;


	static {
		TIME_PATTERN = Pattern.compile("(\\d\\d)[:;](\\d\\d)[:;](\\d\\d)([:;])(\\d\\d)"); //$NON-NLS-1$
	}

	/**
	 * 
	 * @param timecodeStr
	 * @param fps
	 * @param df
	 */
	public Timecode(String timecodeStr, double fps, Boolean df) {
		this(timecodeStr, fps);
		
		if( df!=null ) {
			this.df=df;
		}
	}

	/**
	 * 
	 * @param timecodeStr
	 * @param fps
	 */
	public Timecode(String timecodeStr, double fps) {
		checkFps(fps);

		Matcher matcher = TIME_PATTERN.matcher(timecodeStr);
		if (!matcher.matches()) {
			throw new IllegalArgumentException("Invalid Time Code: " + timecodeStr);
		}

		this.hh = new Integer(matcher.group(1)); 
		this.mm = new Integer(matcher.group(2));
		this.ss = new Integer(matcher.group(3));
		this.ff = new Integer(matcher.group(5)); 
		this.df = ";".equals(matcher.group(4));
		this.fps = fps;
	}


	/**
	 * 
	 * @param totalFrames
	 * @param fps
	 * @param df
	 */
	public Timecode(long totalFrames, double fps, boolean df) {
		checkFps(fps);
		this.df = df;

		if (df) {
			processDropFrameRates(fps);
			long tenMinuteUnits = totalFrames / framesPer10Minutes;
			long framesMod10min = totalFrames % framesPer10Minutes;
			long singleMinutes = (framesMod10min - dropFrames) / framesPerMinute;
			long framesMod01min = (framesMod10min - dropFrames) % framesPerMinute + dropFrames;

			this.fps = (int) framesPerSecond;
			this.ff = (int) framesMod01min % framesPerSecond;
			this.ss = (int) framesMod01min / framesPerSecond;
			this.mm = (int) (10 * (tenMinuteUnits % 6) + singleMinutes);
			this.hh = (int) tenMinuteUnits / 6;
		}

		else {
			int totalSecs = (int) (totalFrames / fps);
			int totalMins = totalSecs / 60;
			int totalHours = totalMins / 60;

			this.hh = totalHours % 24; 
			this.mm = totalMins % 60; 
			this.ss = totalSecs % 60; 
			this.ff = (int) (totalFrames % fps); 
			this.fps = fps;
		}
	}

	/**
	 * 
	 * @param hh
	 * @param mm
	 * @param ss
	 * @param ff
	 * @param fps
	 * @param df
	 */
	public Timecode(int hh, int mm, int ss, int ff, int fps, boolean df) {
		checkFps(fps);

		this.hh = hh;
		this.mm = mm;
		this.ss = ss;
		this.ff = ff;
		this.fps = fps;
		this.df = df;
	}

	/**
	 * 
	 * @param fps
	 * @return
	 */
	private double getDFFrameRate(double fps) {
		double framerate;
		if (fps == 24) {
			framerate = 23.976;
		} else if (fps ==29.97) {
			framerate = 29.97;
		}else if (fps == 30) {
			framerate = 29.97;
		}else if  (fps == 59.94){
			framerate = 59.94;
		}else if (fps == 60) {
			framerate = 59.94;
		} else {
			throw new IllegalArgumentException("Invalid frame rate for drop frame timecode: " + fps);
		}
		return framerate;
	}

	private void checkFps(double fps) {
		if (fps <=0) {
			throw new IllegalArgumentException("Invalid Frame Rate: " + fps);
		}
	}

	private String getTimecodeStr() {
		return String.format("%02d:%02d:%02d%S%02d", hh, mm, ss, df ? ";" : ":", ff); //$NON-NLS-1$
	}


	private void processDropFrameRates(double fps)
	{
		framerate = getDFFrameRate(fps);
		roundedFrameRate = (int)Math.ceil(framerate);

		dropFrames = 2 * (roundedFrameRate / 30);
		framesPerSecond = roundedFrameRate; 
		framesPerMinute = 60 * framesPerSecond - dropFrames;
		framesPer10Minutes = 10 * framesPerMinute + dropFrames;
	}	


	@Override
	public String toString() {
		return getTimecodeStr();
	}



	/**
	 * 
	 * @return
	 */
	public int getTotalFrames() {
		if (df) {
			processDropFrameRates(fps);
			int framesToNextValid;
			if (mm % 10 == 0 || ss != 0 || ff >= dropFrames) {
				framesToNextValid = 0;
			} else {
				framesToNextValid = (int) dropFrames - ff;
			}

			int framesInHHM0 = (int) framesPer10Minutes * (6 * hh + (mm / 10));
			int framesIn0M = (int) framesPerMinute * (mm % 10);
			int framesInSSFF = (int) framesPerSecond * ss + ff;

			return framesInHHM0 + framesIn0M + framesInSSFF + framesToNextValid;

		}

		return (int) (ff + fps * (ss + 60 * (mm + 60 * hh)));
	}

	/**
	 * 
	 * @param duration
	 * @return
	 */
	public Timecode addDuration(Timecode duration) {
		int somTotalFrames = this.getTotalFrames();
		int durTotalFrames = duration.getTotalFrames();
		if (duration.fps != this.fps) {
			durTotalFrames = (int) ((durTotalFrames * this.fps) / duration.fps);
		}

		long eomTotalFrames = (long) (somTotalFrames + durTotalFrames);

		return new Timecode(eomTotalFrames, fps, df);
	}

	/**
	 * 
	 * @param other
	 * @return
	 */
	public Timecode calculateDuration(Timecode other) {
		int frames1 = this.getTotalFrames();
		int frames2 = other.getTotalFrames();

		if (other.fps != this.fps) {
			frames2 = (int) ((frames2 * this.fps) / other.fps);
		}

		long somTotalFrames = Math.min(frames1, frames2);
		long eomTotalFrames = Math.max(frames1, frames2);
		long durTotalFrames = eomTotalFrames - somTotalFrames;

		return new Timecode(durTotalFrames, fps, df);
	}


	/**
	 * @return the hh
	 */
	public int getHh() {
		return hh;
	}

	/**
	 * @param hh the hh to set
	 */
	public void setHh(int hh) {
		this.hh = hh;
	}

	/**
	 * @return the mm
	 */
	public int getMm() {
		return mm;
	}

	/**
	 * @param mm the mm to set
	 */
	public void setMm(int mm) {
		this.mm = mm;
	}

	/**
	 * @return the ss
	 */
	public int getSs() {
		return ss;
	}

	/**
	 * @param ss the ss to set
	 */
	public void setSs(int ss) {
		this.ss = ss;
	}

	/**
	 * @return the ff
	 */
	public int getFf() {
		return ff;
	}

	/**
	 * @param ff the ff to set
	 */
	public void setFf(int ff) {
		this.ff = ff;
	}

	/**
	 * @return the fps
	 */
	public double getFps() {
		return fps;
	}

	/**
	 * @param fps the fps to set
	 */
	public void setFps(double fps) {
		this.fps = fps;
	}

	/**
	 * @return the df
	 */
	public boolean isDf() {
		return df;
	}

	/**
	 * @param df the df to set
	 */
	public void setDf(boolean df) {
		this.df = df;
	}

	/**
	 * @return the dropFrames
	 */
	public long getDropFrames() {
		return dropFrames;
	}
}
