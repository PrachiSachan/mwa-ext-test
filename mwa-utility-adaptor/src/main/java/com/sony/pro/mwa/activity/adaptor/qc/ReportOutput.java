package com.sony.pro.mwa.activity.adaptor.qc;


/**
 * 
 * @author gbputchs
 *
 */
public class ReportOutput {

		
	/** MBC/MWA Report XML String */
	private String mbcReportXmlStr;
	
	/** Device Report XML String */
	private String deviceReportXmlStr;
	
	
	/** Complete path of the Device Report XML File */
	private String deviceReportXmlPath;
	
	/** Complete path of the Generic Report XML File */
	private String genericReportXmlPath;
	
	/** Complete path of the pdf report File */
	private String pdfReportPath;
	
	
	/** Empty Constructor */
	public ReportOutput() {
		
	}


	
	/**
	 * 
	 * @param mbcReportXmlStr
	 * @param deviceReportXmlStr
	 * @param deviceReportXmlPath
	 * @param genericReportXmlPath
	 */
	public ReportOutput(String mbcReportXmlStr, String deviceReportXmlStr,String deviceReportXmlPath, 
			String genericReportXmlPath) {
		super();
		this.mbcReportXmlStr = mbcReportXmlStr;
		this.deviceReportXmlStr = deviceReportXmlStr;
		this.deviceReportXmlPath = deviceReportXmlPath;
		this.genericReportXmlPath = genericReportXmlPath;
	}

	/**
	 * 
	 * @param mbcReportXmlStr
	 * @param deviceReportXmlStr
	 * @param deviceReportXmlPath
	 * @param genericReportXmlPath
	 */
	public ReportOutput(String mbcReportXmlStr, String deviceReportXmlStr,String deviceReportXmlPath, 
			String genericReportXmlPath,String pdfReportPath) {
		super();
		this.mbcReportXmlStr = mbcReportXmlStr;
		this.deviceReportXmlStr = deviceReportXmlStr;
		this.deviceReportXmlPath = deviceReportXmlPath;
		this.genericReportXmlPath = genericReportXmlPath;
		this.pdfReportPath = pdfReportPath;
	}





	/**
	 * @return the mbcReportXmlStr
	 */
	public String getMbcReportXmlStr() {
		return mbcReportXmlStr;
	}


	/**
	 * @param mbcReportXmlStr the mbcReportXmlStr to set
	 */
	public void setMbcReportXmlStr(String mbcReportXmlStr) {
		this.mbcReportXmlStr = mbcReportXmlStr;
	}


	/**
	 * @return the deviceReportXmlPath
	 */
	public String getDeviceReportXmlPath() {
		return deviceReportXmlPath;
	}


	/**
	 * @param deviceReportXmlPath the deviceReportXmlPath to set
	 */
	public void setDeviceReportXmlPath(String deviceReportXmlPath) {
		this.deviceReportXmlPath = deviceReportXmlPath;
	}


	/**
	 * @return the genericReportXmlPath
	 */
	public String getGenericReportXmlPath() {
		return genericReportXmlPath;
	}


	/**
	 * @param genericReportXmlPath the genericReportXmlPath to set
	 */
	public void setGenericReportXmlPath(String genericReportXmlPath) {
		this.genericReportXmlPath = genericReportXmlPath;
	}

	
	
	
	

	/**
	 * @return the deviceReportXmlStr
	 */
	public String getDeviceReportXmlStr() {
		return deviceReportXmlStr;
	}



	/**
	 * @param deviceReportXmlStr the deviceReportXmlStr to set
	 */
	public void setDeviceReportXmlStr(String deviceReportXmlStr) {
		this.deviceReportXmlStr = deviceReportXmlStr;
	}

	/**
	 * @return pdfReportPath
	 */
	public String getPdfReportPath() {
		return pdfReportPath;
	}

	/**
	 * @param pdfReportPath , pdfReportPath to set
	 */
	public void setPdfReportPath(String pdfReportPath) {
		this.pdfReportPath = pdfReportPath;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((deviceReportXmlPath == null) ? 0 : deviceReportXmlPath
						.hashCode());
		result = prime
				* result
				+ ((deviceReportXmlStr == null) ? 0 : deviceReportXmlStr
						.hashCode());
		result = prime
				* result
				+ ((genericReportXmlPath == null) ? 0 : genericReportXmlPath
						.hashCode());
		
		result = prime * result
				+ ((mbcReportXmlStr == null) ? 0 : mbcReportXmlStr.hashCode());
		result = prime
				* result
				+ ((pdfReportPath == null) ? 0 : pdfReportPath
						.hashCode());

		return result;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReportOutput other = (ReportOutput) obj;
		
		if (deviceReportXmlPath == null) {
			if (other.deviceReportXmlPath != null){
				return false;
			}
		} else if (!deviceReportXmlPath.equals(other.deviceReportXmlPath)){
			return false;
		}
		
		if (deviceReportXmlStr == null) {
			if (other.deviceReportXmlStr != null)
				return false;
		} else if (!deviceReportXmlStr.equals(other.deviceReportXmlStr)){
			return false;
		}
		
		if (genericReportXmlPath == null) {
			if (other.genericReportXmlPath != null)
				return false;
		} else if (!genericReportXmlPath.equals(other.genericReportXmlPath)){
			return false;
		}
		
		if (mbcReportXmlStr == null) {
			if (other.mbcReportXmlStr != null)
				return false;
		} else if (!mbcReportXmlStr.equals(other.mbcReportXmlStr)){
			return false;
		}
		
		if (pdfReportPath == null) {
			if (other.pdfReportPath != null)
				return false;
		} else if (!pdfReportPath.equals(other.pdfReportPath)){
			return false;
		}
		return true;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ReportOutput [mbcReportXmlStr=" + mbcReportXmlStr
				+ ", deviceReportXmlStr=" + deviceReportXmlStr
				+ ", deviceReportXmlPath=" + deviceReportXmlPath
				+ ", genericReportXmlPath=" + genericReportXmlPath
				+ ", pdfReportPath=" + pdfReportPath + "]";
	}

}
