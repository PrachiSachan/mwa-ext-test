package com.sony.pro.mwa.activity.adaptor.common;

import java.util.HashMap;
import java.util.Map;


/**
 *
 */
public class Profile{

	//Profile Name
	private String profileName;
	//Profile properties
	private Map<String,String> properties;


	/**
	 *
	 */
	public Profile() {
		this.profileName = null;
		this.properties = new HashMap<String,String>();
	}


	/**
	 *
	 * @param profileName
	 * @param properties
	 */
	public Profile(String profileName, Map<String, String> properties) {
		super();
		this.profileName = profileName;
		this.properties = properties;
	}

	/**
	 * @return the profileName
	 */
	public String getProfileName() {
		return profileName;
	}

	/**
	 * @param profileName the profileName to set
	 */
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	/**
	 * @return the properties
	 */
	public Map<String, String> getProperties() {
		return properties;
	}

	/**
	 * @param properties the properties to set
	 */
	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((profileName == null) ? 0 : profileName.hashCode());
		result = prime * result
				+ ((properties == null) ? 0 : properties.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Profile other = (Profile) obj;
		if (profileName == null) {
			if (other.profileName != null)
				return false;
		} else if (!profileName.equals(other.profileName))
			return false;
		if (properties == null) {
			if (other.properties != null)
				return false;
		} else if (!properties.equals(other.properties))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Profile [profileName=" + profileName + ", properties="
				+ properties + "]";
	}






}
