package com.sony.pro.mwa.activity.adaptor.exception;


/**
 *
 */
public class DeviceException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = -1567117880615461502L;


	public DeviceException(Exception ex) {
		super(ex);
	}

	public DeviceException(String errorDetails) {
		super(errorDetails);
	}


    public DeviceException(String message, Throwable cause) {
        super(message, cause);
    }


    public DeviceException(Throwable cause) {
        super(cause);
    }
}
