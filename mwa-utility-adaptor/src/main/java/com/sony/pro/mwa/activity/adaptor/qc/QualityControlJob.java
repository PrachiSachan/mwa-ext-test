package com.sony.pro.mwa.activity.adaptor.qc;

import java.util.Date;
import java.util.Map;

import com.sony.pro.mwa.activity.adaptor.common.Job;
import com.sony.pro.mwa.activity.adaptor.common.JobProperty;
import com.sony.pro.mwa.activity.adaptor.common.PriorityEnum;



/**
 *
 * @author gbputchs
 *
 */
public class QualityControlJob extends Job {

	private Location source;
	private Location destination;
	private String jobProfileName;
	private PartialVerification partialVerification;


	/**
	 * Empty Constructor
	 */
	public QualityControlJob() {
		super();
	}


	/**
	 * Constructor
	 * @param name
	 * @param jobId
	 * @param dueDate
	 * @param scheduledDate
	 * @param priority
	 * @param jobExtensionAttributes
	 * @param userId
	 * @param source
	 * @param destination
	 * @param jobProfileName
	 * @param partialVerification
	 */
	public QualityControlJob(String name, String jobId, Date dueDate,Date scheduledDate, PriorityEnum priority,
			Map<String, JobProperty> jobExtensionAttributes, String userId,Location source, Location destination,
			String jobProfileName,PartialVerification partialVerification) {
		super(name, jobId, dueDate, scheduledDate, priority,jobExtensionAttributes, userId);
		this.source = source;
		this.destination = destination;
		this.jobProfileName = jobProfileName;
		this.partialVerification = partialVerification;
	}


	/**
	 * @return the source
	 */
	public Location getSource() {
		return source;
	}


	/**
	 * @param source the source to set
	 */
	public void setSource(Location source) {
		this.source = source;
	}


	/**
	 * @return the destination
	 */
	public Location getDestination() {
		return destination;
	}


	/**
	 * @param destination the destination to set
	 */
	public void setDestination(Location destination) {
		this.destination = destination;
	}


	/**
	 * @return the jobProfileName
	 */
	public String getJobProfileName() {
		return jobProfileName;
	}


	/**
	 * @param jobProfileName the jobProfileName to set
	 */
	public void setJobProfileName(String jobProfileName) {
		this.jobProfileName = jobProfileName;
	}


	/**
	 * @return the partialVerification
	 */
	public PartialVerification getPartialVerification() {
		return partialVerification;
	}


	/**
	 * @param partialVerification the partialVerification to set
	 */
	public void setPartialVerification(PartialVerification partialVerification) {
		this.partialVerification = partialVerification;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((destination == null) ? 0 : destination.hashCode());
		result = prime * result
				+ ((jobProfileName == null) ? 0 : jobProfileName.hashCode());
		result = prime
				* result
				+ ((partialVerification == null) ? 0 : partialVerification
						.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		QualityControlJob other = (QualityControlJob) obj;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (jobProfileName == null) {
			if (other.jobProfileName != null)
				return false;
		} else if (!jobProfileName.equals(other.jobProfileName))
			return false;
		if (partialVerification == null) {
			if (other.partialVerification != null)
				return false;
		} else if (!partialVerification.equals(other.partialVerification))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		return true;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return "QualityControlJob [source=" + source + ", destination="
				+ destination + ", jobProfileName=" + jobProfileName
				+ ", partialVerification=" + partialVerification + "]";
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */


}
