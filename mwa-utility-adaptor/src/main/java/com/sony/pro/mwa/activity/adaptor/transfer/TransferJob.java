package com.sony.pro.mwa.activity.adaptor.transfer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.activity.adaptor.common.Job;
import com.sony.pro.mwa.activity.adaptor.common.JobProperty;
import com.sony.pro.mwa.activity.adaptor.common.PriorityEnum;


/**
 *
 * @author gbputchs
 *
 */
public class TransferJob  extends Job {

	private String transferType;
	private List<Source> sourceList;
	private List<Destination> destinationList ;



	/**
	 *
	 */
	public TransferJob() {
		super();
		this.sourceList = new ArrayList<Source>();
		this.destinationList = new ArrayList<Destination>();
	}

	/**
	 *
	 * @param jobName
	 * @param jobId
	 * @param dueDate
	 * @param scheduledDate
	 * @param priority
	 * @param jobExtensionAttributes
	 * @param userId
	 * @param sourceList
	 * @param destinationList
	 */
	public TransferJob(String jobName,String jobId, Date dueDate, Date scheduledDate,PriorityEnum priority,
			Map<String,JobProperty> jobExtensionAttributes,	String userId,
			List<Source> sourceList,List<Destination>   destinationList,String transferType) {

		this(jobName,jobId, dueDate, scheduledDate,priority,jobExtensionAttributes,userId);
		this.sourceList = sourceList;
		this.destinationList = destinationList;
		this.transferType=transferType;
	}


	/**
	 *
	 * @param jobName
	 * @param jobId
	 * @param dueDate
	 * @param scheduledDate
	 * @param priority
	 * @param jobExtensionAttributes
	 * @param userId
	 */
	public TransferJob(String jobName,String jobId, Date dueDate, Date scheduledDate,PriorityEnum priority,
			Map<String,JobProperty> jobExtensionAttributes,	String userId) {

		super(jobName,jobId, dueDate, scheduledDate,priority,jobExtensionAttributes,userId);
		sourceList = new ArrayList<Source>();
		destinationList = new ArrayList<Destination>();
	}

	/**
	 * @return the sourceList
	 */
	public List<Source> getSourceList() {
		return sourceList;
	}

	/**
	 * @param sourceList the sourceList to set
	 */
	public void setSourceList(List<Source> sourceList) {
		this.sourceList = sourceList;
	}

	/**
	 * @return the destinationList
	 */
	public List<Destination> getDestinationList() {
		return destinationList;
	}

	/**
	 * @param destinationList the destinationList to set
	 */
	public void setDestinationList(List<Destination> destinationList) {
		this.destinationList = destinationList;
	}




	/**
	 * @return the transferType
	 */
	public String getTransferType() {
		return transferType;
	}

	/**
	 * @param transferType the transferType to set
	 */
	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((destinationList == null) ? 0 : destinationList.hashCode());
		result = prime * result
				+ ((sourceList == null) ? 0 : sourceList.hashCode());
		result = prime * result
				+ ((transferType == null) ? 0 : transferType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransferJob other = (TransferJob) obj;
		if (destinationList == null) {
			if (other.destinationList != null)
				return false;
		} else if (!destinationList.equals(other.destinationList))
			return false;
		if (sourceList == null) {
			if (other.sourceList != null)
				return false;
		} else if (!sourceList.equals(other.sourceList))
			return false;
		if (transferType == null) {
			if (other.transferType != null)
				return false;
		} else if (!transferType.equals(other.transferType))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransferJob [transferType=" + transferType + ", sourceList="
				+ sourceList + ", destinationList=" + destinationList + "]";
	}




}
