package com.sony.pro.mwa.activity.adaptor.transcode;

import com.sony.pro.mwa.activity.adaptor.common.FrameRate;
import com.sony.pro.mwa.activity.adaptor.common.Path;

/**
 *
 * @author gbputchs
 *
 */
public class Source  {

	private Path sourcePath;
	private String sourceTrack;
	private String destinationTrack;
	private String inPoint;
	private String outPoint;
	private TranscodeTypeEnum transcodeType;
	private FrameRate frameRate;


	public Source() {}



	/**
	 *
	 * @param sourcePath
	 * @param sourceTrack
	 * @param destinationTrack
	 * @param inPoint
	 * @param outPoint
	 * @param transcodeType
	 * @param frameRate
	 */
	public Source(Path sourcePath, String sourceTrack, String destinationTrack,	String inPoint,
			String outPoint, TranscodeTypeEnum transcodeType,FrameRate frameRate) {

		super();
		this.sourcePath = sourcePath;
		this.sourceTrack = sourceTrack;
		this.destinationTrack = destinationTrack;
		this.inPoint = inPoint;
		this.outPoint = outPoint;
		this.transcodeType = transcodeType;
		this.frameRate = frameRate;
	}



	/**
	 * @return the sourcePath
	 */
	public Path getSourcePath() {
		return sourcePath;
	}

	/**
	 * @param sourcePath the sourcePath to set
	 */
	public void setSourcePath(Path sourcePath) {
		this.sourcePath = sourcePath;
	}

	/**
	 * @return the sourceTrack
	 */
	public String getSourceTrack() {
		return sourceTrack;
	}

	/**
	 * @param sourceTrack the sourceTrack to set
	 */
	public void setSourceTrack(String sourceTrack) {
		this.sourceTrack = sourceTrack;
	}

	/**
	 * @return the destinationTrack
	 */
	public String getDestinationTrack() {
		return destinationTrack;
	}

	/**
	 * @param destinationTrack the destinationTrack to set
	 */
	public void setDestinationTrack(String destinationTrack) {
		this.destinationTrack = destinationTrack;
	}

	/**
	 * @return the inPoint
	 */
	public String getInPoint() {
		return inPoint;
	}

	/**
	 * @param inPoint the inPoint to set
	 */
	public void setInPoint(String inPoint) {
		this.inPoint = inPoint;
	}

	/**
	 * @return the outPoint
	 */
	public String getOutPoint() {
		return outPoint;
	}

	/**
	 * @param outPoint the outPoint to set
	 */
	public void setOutPoint(String outPoint) {
		this.outPoint = outPoint;
	}

	/**
	 * @return the transcodeType
	 */
	public TranscodeTypeEnum getTranscodeType() {
		return transcodeType;
	}

	/**
	 * @param transcodeType the transcodeType to set
	 */
	public void setTranscodeType(TranscodeTypeEnum transcodeType) {
		this.transcodeType = transcodeType;
	}


	/**
	 * @return the frameRate
	 */
	public FrameRate getFrameRate() {
		return frameRate;
	}

	/**
	 * @param frameRate the frameRate to set
	 */
	public void setFrameRate(FrameRate frameRate) {
		this.frameRate = frameRate;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((destinationTrack == null) ? 0 : destinationTrack.hashCode());
		result = prime * result
				+ ((frameRate == null) ? 0 : frameRate.hashCode());
		result = prime * result + ((inPoint == null) ? 0 : inPoint.hashCode());
		result = prime * result
				+ ((outPoint == null) ? 0 : outPoint.hashCode());
		result = prime * result
				+ ((sourcePath == null) ? 0 : sourcePath.hashCode());
		result = prime * result
				+ ((sourceTrack == null) ? 0 : sourceTrack.hashCode());
		result = prime * result
				+ ((transcodeType == null) ? 0 : transcodeType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Source other = (Source) obj;
		if (destinationTrack == null) {
			if (other.destinationTrack != null)
				return false;
		} else if (!destinationTrack.equals(other.destinationTrack))
			return false;
		if (frameRate == null) {
			if (other.frameRate != null)
				return false;
		} else if (!frameRate.equals(other.frameRate))
			return false;
		if (inPoint == null) {
			if (other.inPoint != null)
				return false;
		} else if (!inPoint.equals(other.inPoint))
			return false;
		if (outPoint == null) {
			if (other.outPoint != null)
				return false;
		} else if (!outPoint.equals(other.outPoint))
			return false;
		if (sourcePath == null) {
			if (other.sourcePath != null)
				return false;
		} else if (!sourcePath.equals(other.sourcePath))
			return false;
		if (sourceTrack == null) {
			if (other.sourceTrack != null)
				return false;
		} else if (!sourceTrack.equals(other.sourceTrack))
			return false;
		if (transcodeType != other.transcodeType)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Source [sourcePath=" + sourcePath + ", sourceTrack="
				+ sourceTrack + ", destinationTrack=" + destinationTrack
				+ ", inPoint=" + inPoint + ", outPoint=" + outPoint
				+ ", transcodeType=" + transcodeType + ", frameRate="
				+ frameRate + "]";
	}

}
