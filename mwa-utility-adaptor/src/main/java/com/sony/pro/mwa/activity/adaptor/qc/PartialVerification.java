package com.sony.pro.mwa.activity.adaptor.qc;

public class PartialVerification {

	private String startTC;
	private String endTC;
	private String fps;
	
	
	/**
	 * Empty Constructor
	 */
	public PartialVerification() {
		
	}


	/**
	 * Constructor
	 * @param startTC
	 * @param endTC
	 * @param fps
	 */
	public PartialVerification(String startTC, String endTC, String fps) {
		super();
		this.startTC = startTC;
		this.endTC = endTC;
		this.fps = fps;
	}


	/**
	 * @return the startTC
	 */
	public String getStartTC() {
		return startTC;
	}


	/**
	 * @param startTC the startTC to set
	 */
	public void setStartTC(String startTC) {
		this.startTC = startTC;
	}


	/**
	 * @return the endTC
	 */
	public String getEndTC() {
		return endTC;
	}


	/**
	 * @param endTC the endTC to set
	 */
	public void setEndTC(String endTC) {
		this.endTC = endTC;
	}


	/**
	 * @return the fps
	 */
	public String getFps() {
		return fps;
	}


	/**
	 * @param fps the fps to set
	 */
	public void setFps(String fps) {
		this.fps = fps;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endTC == null) ? 0 : endTC.hashCode());
		result = prime * result + ((fps == null) ? 0 : fps.hashCode());
		result = prime * result + ((startTC == null) ? 0 : startTC.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PartialVerification other = (PartialVerification) obj;
		if (endTC == null) {
			if (other.endTC != null)
				return false;
		} else if (!endTC.equals(other.endTC))
			return false;
		if (fps == null) {
			if (other.fps != null)
				return false;
		} else if (!fps.equals(other.fps))
			return false;
		if (startTC == null) {
			if (other.startTC != null)
				return false;
		} else if (!startTC.equals(other.startTC))
			return false;
		return true;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PartialVerification [startTC=" + startTC + ", endTC=" + endTC
				+ ", fps=" + fps + "]";
	}

	
}
