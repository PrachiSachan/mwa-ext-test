package com.sony.pro.mwa.activity.adaptor.qc;

import com.sony.pro.mwa.activity.adaptor.common.Path;

/**
 *
 * @author gbputchs
 *
 */
public class Location {

	private String ipAddress;
	private String port;
	private String userName;
	private String password;
	private String systemName;
	private Path  path;


	/**
	 * Empty Constructor
	 */
	public Location() {

	}


	/**
	 * Constructor
	 * @param ipAddress
	 * @param port
	 * @param userName
	 * @param password
	 * @param systemName
	 * @param path
	 */
	public Location(String ipAddress, String port, String userName,String password, String systemName, Path path) {
		super();
		this.ipAddress = ipAddress;
		this.port = port;
		this.userName = userName;
		this.password = password;
		this.systemName = systemName;
		this.path = path;
	}





	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}


	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}


	/**
	 * @return the port
	 */
	public String getPort() {
		return port;
	}


	/**
	 * @param port the port to set
	 */
	public void setPort(String port) {
		this.port = port;
	}


	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}


	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}


	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}


	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	/**
	 * @return the path
	 */
	public Path getPath() {
		return path;
	}


	/**
	 * @param path the path to set
	 */
	public void setPath(Path path) {
		this.path = path;
	}


	/**
	 * @return the systemName
	 */
	public String getSystemName() {
		return systemName;
	}


	/**
	 * @param systemName the systemName to set
	 */
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((ipAddress == null) ? 0 : ipAddress.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + ((port == null) ? 0 : port.hashCode());
		result = prime * result
				+ ((systemName == null) ? 0 : systemName.hashCode());
		result = prime * result
				+ ((userName == null) ? 0 : userName.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (ipAddress == null) {
			if (other.ipAddress != null)
				return false;
		} else if (!ipAddress.equals(other.ipAddress))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		if (port == null) {
			if (other.port != null)
				return false;
		} else if (!port.equals(other.port))
			return false;
		if (systemName == null) {
			if (other.systemName != null)
				return false;
		} else if (!systemName.equals(other.systemName))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Location [ipAddress=" + ipAddress + ", port=" + port
				+ ", userName=" + userName + ", password=" + password
				+ ", systemName=" + systemName + ", path=" + path + "]";
	}








}
