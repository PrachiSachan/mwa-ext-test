package com.sony.pro.mwa.activity.type.fm;

import com.sony.ods.BasePath;
import com.sony.ods.BasePathList;
import com.sony.ods.Cartridge;
import com.sony.ods.CartridgeList;
import com.sony.ods.Catalog;
import com.sony.ods.CatalogAddRequest;
import com.sony.ods.CatalogDetails;
import com.sony.ods.CatalogList;
import com.sony.ods.Drive;
import com.sony.ods.DriveList;
import com.sony.ods.DriveRequest;
import com.sony.ods.FileAddRequest;
import com.sony.ods.FileList;
import com.sony.ods.Job;
import com.sony.ods.JobGroup;
import com.sony.ods.JobGroupList;
import com.sony.ods.JobList;
import com.sony.ods.Property;
import com.sony.ods.PropertyList;
import com.sony.ods.SystemList;
import com.sony.ods.SystemRequest;
import com.sony.ods.Tray;
import com.sony.ods.TrayRequest;
import com.sony.pro.mwa.parameter.type.ObjectParameterType;

/**
 * Singleton parameter types for various objects used in the GAM interface.
 * 
 */
public class OdaFmTypes {
	public static final ObjectParameterType<BasePath> BasePathType = new ObjectParameterType<>(
			BasePath.class);
	public static final ObjectParameterType<BasePathList> BasePathListType = new ObjectParameterType<>(
			BasePathList.class);
	public static final ObjectParameterType<Cartridge> CartridgeType = new ObjectParameterType<>(
			Cartridge.class);
	public static final ObjectParameterType<CartridgeList> CartridgeListType = new ObjectParameterType<>(
			CartridgeList.class);
	public static final ObjectParameterType<Catalog> CatalogType = new ObjectParameterType<>(
			Catalog.class);
	public static final ObjectParameterType<CatalogAddRequest> CatalogAddRequestType = new ObjectParameterType<>(
			CatalogAddRequest.class);
	public static final ObjectParameterType<CatalogDetails> CatalogDetailsType = new ObjectParameterType<>(
			CatalogDetails.class);
	public static final ObjectParameterType<CatalogList> CatalogListType = new ObjectParameterType<>(
			CatalogList.class);
	public static final ObjectParameterType<Drive> DriveType = new ObjectParameterType<>(
			Drive.class);
	public static final ObjectParameterType<DriveList> DriveListType = new ObjectParameterType<>(
			DriveList.class);
	public static final ObjectParameterType<DriveRequest> DriveRequestType = new ObjectParameterType<>(
			DriveRequest.class);
	public static final ObjectParameterType<FileAddRequest> FileAddRequestType = new ObjectParameterType<>(
			FileAddRequest.class);
	public static final ObjectParameterType<FileList> FileListType = new ObjectParameterType<>(
			FileList.class);
	public static final ObjectParameterType<Job> JobType = new ObjectParameterType<>(
			Job.class);
	public static final ObjectParameterType<JobGroup> JobGroupType = new ObjectParameterType<>(
			JobGroup.class);
	public static final ObjectParameterType<JobGroupList> JobGroupListType = new ObjectParameterType<>(
			JobGroupList.class);
	public static final ObjectParameterType<JobList> JobListType = new ObjectParameterType<>(
			JobList.class);
	public static final ObjectParameterType<Property> PropertyType = new ObjectParameterType<>(
			Property.class);
	public static final ObjectParameterType<PropertyList> PropertyListType = new ObjectParameterType<>(
			PropertyList.class);
	public static final ObjectParameterType<System> SystemType = new ObjectParameterType<>(
			System.class);
	public static final ObjectParameterType<SystemList> SystemListType = new ObjectParameterType<>(
			SystemList.class);
	public static final ObjectParameterType<SystemRequest> SystemRequestType = new ObjectParameterType<>(
			SystemRequest.class);
	public static final ObjectParameterType<Tray> TrayType = new ObjectParameterType<>(
			Tray.class);
	public static final ObjectParameterType<TrayRequest> TrayRequestType = new ObjectParameterType<>(
			TrayRequest.class);

}
