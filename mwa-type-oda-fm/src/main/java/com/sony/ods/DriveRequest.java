package com.sony.ods;


import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="driveRequest")
public class DriveRequest {
	private String useFlag;
	
	public DriveRequest(String useFlag) {
		this.useFlag = useFlag;
	}
	
	public DriveRequest() {}
	
	public String getUseFlag() {
		return useFlag;
	}

	public void setUseFlag(String useFlag) {
		this.useFlag = useFlag;
	}
}

