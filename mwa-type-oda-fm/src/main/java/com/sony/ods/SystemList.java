package com.sony.ods;


import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="systems")
public class SystemList {
	private long totalCount;
	private List<System> systems;

	public SystemList() {}
		
	public SystemList(List<System> systems) {
		this.systems = systems;
		this.totalCount = systems.size();
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
		
	@XmlElement(name="system")
	public List<System> getSystems() {
		return systems;
	}

	public void setSystems(List<System> systems) {
		this.systems = systems;
	}
}

