package com.sony.ods;

import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.*;

@XmlRootElement(name="basePath")
public class BasePath {
	@Pattern(regexp="(?i)(true|false)")
	private String isFileToBeDeleted;
	@Size(min=1, max=128)
	@Pattern(regexp="^/{1}([^\\\\\\*\\?\"/:<>\\|]+)")
	private String virtualPath;
	@Size(min=1, max=1024)
	private String physicalPath;

	/**
	 * 
	 */
	public BasePath() {}

	/**
	 * @param virtualPath
	 * @param physicalPath
	 */
	public BasePath(String virtualPath, String physicalPath, String isFileToBeDeleted) {
		this.virtualPath = virtualPath;
		this.physicalPath = physicalPath;
		this.isFileToBeDeleted = isFileToBeDeleted;
	}

	/**
	 * @return the virtualPath
	 */
	public String getVirtualPath() {
		return virtualPath;
	}

	/**
	 * @param virtualPath the virtualPath to set
	 */
	public void setVirtualPath(String virtualPath) {
		this.virtualPath = virtualPath;
	}

	/**
	 * @return the physicalPath
	 */
	public String getPhysicalPath() {
		return physicalPath;
	}

	/**
	 * @param physicalPath the physicalPath to set
	 */
	public void setPhysicalPath(String physicalPath) {
		this.physicalPath = physicalPath;
	}


	/**
	 * @return the isFileToBeDeleted
	 */
	public String getIsFileToBeDeleted() {
		return isFileToBeDeleted;
	}


	/**
	 * @param isFileToBeDeleted the isFileToBeDeleted to set
	 */
	public void setIsFileToBeDeleted(String isFileToBeDeleted) {
		this.isFileToBeDeleted = isFileToBeDeleted;
	}
}

