package com.sony.ods;

import java.lang.reflect.InvocationTargetException;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.beanutils.BeanUtils;

@XmlRootElement(name="catalog")
public class Catalog {

	private String id;
	private String cartridgeId;
	private String cartridgeTitle;
	private String cartridgeLocation;
	private String name;
	private String fullName;
	private String parentId;
	private String archiveRequestTime; 
	private String archiveStartTime; 
	private String archiveEndTime;
	private String archiveFileUpdateTime;
	private String originalFileName;
	private String originalFileType;
	private String originalFileUpdateTime;
	private String status;
	private String retrieveCounter;
	private String lastRetrieveTime;
	private String groupsId;
	private String isDir;
	private String fileSize;
	private String startDiskNumber;
	private String endDiskNumber;
	private String hasMetadata;
	private String jobId;

	public Catalog() {}
	
	public Catalog(Catalog catalog, String id) {

		try {
			BeanUtils.copyProperties(this, catalog);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new InternalError(e.getMessage());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new InternalError(e.getMessage());
		}

		this.id = id;
	}
	public Catalog(String cartridgeId, String parentId, String name, String fullName, String originalFilePath, String originalFileType, String originalFileUpdateTime, String groupsId, String isDir, String fileSize) {
		this.cartridgeId = cartridgeId;
		this.parentId = parentId;
		this.name = name;
		this.fullName = fullName;
		this.originalFileName = originalFilePath;
		this.originalFileType = originalFileType;
		this.originalFileUpdateTime = originalFileUpdateTime;
		this.groupsId = groupsId;
		this.isDir = isDir;
		this.fileSize = fileSize;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the cartridgeId
	 */
	public String getCartridgeId() {
		return cartridgeId;
	}

	/**
	 * @param cartridgeId the cartridgeId to set
	 */
	public void setCartridgeId(String cartridgeId) {
		this.cartridgeId = cartridgeId;
	}

	/**
	 * @return the cartridgeTitle
	 */
	public String getCartridgeTitle() {
		return cartridgeTitle;
	}

	/**
	 * @param cartridgeTitle the cartridgeTitle to set
	 */
	public void setCartridgeTitle(String cartridgeTitle) {
		this.cartridgeTitle = cartridgeTitle;
	}

	/**
	 * @return the cartridgeLocation
	 */
	public String getCartridgeLocation() {
		return cartridgeLocation;
	}

	/**
	 * @param cartridgeLocation the cartridgeLocation to set
	 */
	public void setCartridgeLocation(String cartridgeLocation) {
		this.cartridgeLocation = cartridgeLocation;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the parentId
	 */
	public String getParentId() {
		return parentId;
	}
	
	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the archiveRequestTime
	 */
	public String getArchiveRequestTime() {
		return archiveRequestTime;
	}

	/**
	 * @param archiveRequestTime the archiveRequestTime to set
	 */
	public void setArchiveRequestTime(String archiveRequestTime) {
		this.archiveRequestTime = archiveRequestTime;
	}

	/**
	 * @return the archiveStartTime
	 */
	public String getArchiveStartTime() {
		return archiveStartTime;
	}

	/**
	 * @param archiveStartTime the archiveStartTime to set
	 */
	public void setArchiveStartTime(String archiveStartTime) {
		this.archiveStartTime = archiveStartTime;
	}

	/**
	 * @return the archiveEndTime
	 */
	public String getArchiveEndTime() {
		return archiveEndTime;
	}

	/**
	 * @param archiveEndTime the archiveEndTime to set
	 */
	public void setArchiveEndTime(String archiveEndTime) {
		this.archiveEndTime = archiveEndTime;
	}

	/**
	 * @return the archiveFileUpdateTime
	 */
	public String getArchiveFileUpdateTime() {
		return archiveFileUpdateTime;
	}

	/**
	 * @param archiveFileUpdateTime the archiveFileUpdateTime to set
	 */
	public void setArchiveFileUpdateTime(String archiveFileUpdateTime) {
		this.archiveFileUpdateTime = archiveFileUpdateTime;
	}

	/**
	 * @return the originalFileName
	 */
	public String getOriginalFileName() {
		return originalFileName;
	}

	/**
	 * @param originalFileName the originalFileName to set
	 */
	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	/**
	 * @return the originalFileType
	 */
	public String getOriginalFileType() {
		return originalFileType;
	}

	/**
	 * @param originalFileType the originalFileType to set
	 */
	public void setOriginalFileType(String originalFileType) {
		this.originalFileType = originalFileType;
	}

	/**
	 * @return the originalFileUpdateTime
	 */
	public String getOriginalFileUpdateTime() {
		return originalFileUpdateTime;
	}

	/**
	 * @param originalFileUpdateTime the originalFileUpdateTime to set
	 */
	public void setOriginalFileUpdateTime(String originalFileUpdateTime) {
		this.originalFileUpdateTime = originalFileUpdateTime;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the retrieveCounter
	 */
	public String getRetrieveCounter() {
		return retrieveCounter;
	}

	/**
	 * @param retrieveCounter the retrieveCounter to set
	 */
	public void setRetrieveCounter(String retrieveCounter) {
		this.retrieveCounter = retrieveCounter;
	}

	/**
	 * @return the lastRetrieveTime
	 */
	public String getLastRetrieveTime() {
		return lastRetrieveTime;
	}

	/**
	 * @param lastRetrieveTime the lastRetrieveTime to set
	 */
	public void setLastRetrieveTime(String lastRetrieveTime) {
		this.lastRetrieveTime = lastRetrieveTime;
	}

	/**
	 * @return the owner
	 */
	public String getGroupsId() {
		return groupsId;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setGroupsId(String groupsId) {
		this.groupsId = groupsId;
	}

	/**
	 * @return the isDir
	 */
	public String getIsDir() {
		return isDir;
	}

	/**
	 * @param isDir the isDir to set
	 */
	public void setIsDir(String isDir) {
		this.isDir = isDir;
	}

	/**
	 * @return the fileSize
	 */
	public String getFileSize() {
		return fileSize;
	}

	/**
	 * @param fileSize the fileSize to set
	 */
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * @return the startDiskNumber
	 */
	public String getStartDiskNumber() {
		return startDiskNumber;
	}

	/**
	 * @param startDiskNumber the startDiskNumber to set
	 */
	public void setStartDiskNumber(String startDiskNumber) {
		this.startDiskNumber = startDiskNumber;
	}

	/**
	 * @return the endDiskNumber
	 */
	public String getEndDiskNumber() {
		return endDiskNumber;
	}

	/**
	 * @param endDiskNumber the endDiskNumber to set
	 */
	public void setEndDiskNumber(String endDiskNumber) {
		this.endDiskNumber = endDiskNumber;
	}

	/**
	 * @return the hasMetadata
	 */
	public String getHasMetadata() {
		return hasMetadata;
	}

	/**
	 * @param hasMetadata the hasMetadata to set
	 */
	public void setHasMetadata(String hasMetadata) {
		this.hasMetadata = hasMetadata;
	}

	/**
	 * @return the jobId
	 */
	public String getJobId() {
		return jobId;
	}

	/**
	 * @param jobId the jobId to set
	 */
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public int compareTo(Catalog target) {
		return this.fullName.toUpperCase().compareTo(target.fullName.toUpperCase());
	}
	

}

