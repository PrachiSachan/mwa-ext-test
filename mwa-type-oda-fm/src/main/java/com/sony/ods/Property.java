package com.sony.ods;

import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.*;

@XmlRootElement(name="property")
public class Property {
	@Size(max=256)
	private String key;
	@Size(max=256)
	private String value;

	public Property() {}

	public Property(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

