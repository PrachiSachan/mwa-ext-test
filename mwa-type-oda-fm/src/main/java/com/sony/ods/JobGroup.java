package com.sony.ods;

import java.lang.reflect.InvocationTargetException;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.beanutils.BeanUtils;

@XmlRootElement(name="jobGroup")
public class JobGroup {
	private String id;
	private String type;
	private String createTime;
	private String startTime;
	private String endTime;
	private String usersId;
	private String status;
	private String progress;
	private String cancelable;
	private String cancelRequired;
	private String jobCount;

	public JobGroup() {}

	/**
	 * @param jobGroup
	 * @param id
	 */
	public JobGroup(JobGroup jobGroup, String id) {

		try {
			BeanUtils.copyProperties(this, jobGroup);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new InternalError(e.getMessage());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new InternalError(e.getMessage());
		}
		
		this.id = id;
	}

	/**
	 * @param type
	 * @param usersId
	 */
	public JobGroup(String type, String usersId) {
		this.type = type;
		this.usersId = usersId;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the createTime
	 */
	public String getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return the usersId
	 */
	public String getUsersId() {
		return usersId;
	}

	/**
	 * @param usersId the usersId to set
	 */
	public void setUsersId(String usersId) {
		this.usersId = usersId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the progress
	 */
	public String getProgress() {
		return progress;
	}

	/**
	 * @param progress the progress to set
	 */
	public void setProgress(String progress) {
		this.progress = progress;
	}

	/**
	 * @return the cancelable
	 */
	public String getCancelable() {
		return cancelable;
	}

	/**
	 * @param cancelable the cancelable to set
	 */
	public void setCancelable(String cancelable) {
		this.cancelable = cancelable;
	}

	/**
	 * @return the cancelRequired
	 */
	public String getCancelRequired() {
		return cancelRequired;
	}

	/**
	 * @param cancelRequired the cancelRequired to set
	 */
	public void setCancelRequired(String cancelRequired) {
		this.cancelRequired = cancelRequired;
	}

	/**
	 * @return the jobCount
	 */
	public String getJobCount() {
		return jobCount;
	}

	/**
	 * @param jobCount the jobCount to set
	 */
	public void setJobCount(String jobCount) {
		this.jobCount = jobCount;
	}
}

