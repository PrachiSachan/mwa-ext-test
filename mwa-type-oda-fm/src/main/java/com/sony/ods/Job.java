package com.sony.ods;

import java.lang.reflect.InvocationTargetException;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.beanutils.BeanUtils;

@XmlRootElement(name="job")
public class Job {

	private String id;
	private String groupId;
	private String type;
	private String createTime;
	private String startTime;
	private String endTime;
	private String usersId;
	private String status;
	private String cancelable;
	private String cancelRequired;
	private String cartridgeId;
	private String cartridgeTitle;
	private String driveId;
	private String fromFile;
	private String fromFileAsPhysicalPath;
	private String toFile;
	private String toFileAsPhysicalPath;
	private String catalogId;
	private String catalogName;
	private String isFileToBeDeleted;
	private String progress;
	private String jobOrder;
	private String systemId;
	private String errorKey;
	private String errorMessage;
	
	public Job() {}
	
	/**
	 * @param job
	 * @param id
	 */
	public Job(Job job, String id) {

		try {
			BeanUtils.copyProperties(this, job);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new InternalError(e.getMessage());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new InternalError(e.getMessage());
		}
		
		this.id = id;
	}
	
	/**
	 * @param groupId
	 * @param type
	 * @param usersId
	 * @param cartridgeId
	 * @param catalogId
	 * @param fromFile
	 * @param fromFileAsPhysicalPath
	 * @param toFile
	 * @param toFileAsPhysicalPath
	 * @param isFileToBeDeleted
	 */
	public Job(String groupId, String type, String usersId, String cartridgeId, String catalogId, String fromFile, String fromFileAsPhysicalPath, String toFile, String toFileAsPhysicalPath, String isFileToBeDeleted) {
		this.groupId = groupId;
		this.type = type;
		this.usersId = usersId;
		this.cancelable = Boolean.TRUE.toString();
		this.cartridgeId = cartridgeId;
		this.catalogId = catalogId;
		this.fromFile = fromFile;
		this.fromFileAsPhysicalPath = fromFileAsPhysicalPath;
		this.toFile = toFile;
		this.toFileAsPhysicalPath = toFileAsPhysicalPath;
		this.isFileToBeDeleted = isFileToBeDeleted;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the groupId
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the createTime
	 */
	public String getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return the usersId
	 */
	public String getUsersId() {
		return usersId;
	}

	/**
	 * @param usersId the usersId to set
	 */
	public void setUsersId(String usersId) {
		this.usersId = usersId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the cancelable
	 */
	public String getCancelable() {
		return cancelable;
	}

	/**
	 * @param cancelable the cancelable to set
	 */
	public void setCancelable(String cancelable) {
		this.cancelable = cancelable;
	}

	/**
	 * @return the cancelRequired
	 */
	public String getCancelRequired() {
		return cancelRequired;
	}

	/**
	 * @param cancelRequired the cancelRequired to set
	 */
	public void setCancelRequired(String cancelRequired) {
		this.cancelRequired = cancelRequired;
	}

	/**
	 * @return the cartridgeId
	 */
	public String getCartridgeId() {
		return cartridgeId;
	}

	/**
	 * @param cartridgeId the cartridgeId to set
	 */
	public void setCartridgeId(String cartridgeId) {
		this.cartridgeId = cartridgeId;
	}

	/**
	 * @return the cartridgeTitle
	 */
	public String getCartridgeTitle() {
		return cartridgeTitle;
	}

	/**
	 * @param cartridgeTitle the cartridgeTitle to set
	 */
	public void setCartridgeTitle(String cartridgeTitle) {
		this.cartridgeTitle = cartridgeTitle;
	}

	/**
	 * @return the driveId
	 */
	public String getDriveId() {
		return driveId;
	}

	/**
	 * @param driveId the driveId to set
	 */
	public void setDriveId(String driveId) {
		this.driveId = driveId;
	}

	/**
	 * @return the fromFile
	 */
	public String getFromFile() {
		return fromFile;
	}

	/**
	 * @param fromFile the fromFile to set
	 */
	public void setFromFile(String fromFile) {
		this.fromFile = fromFile;
	}

	/**
	 * @return the fromFileAsPhysicalPath
	 */
	public String getFromFileAsPhysicalPath() {
		return fromFileAsPhysicalPath;
	}

	/**
	 * @param fromFileAsPhysicalPath the fromFileAsPhysicalPath to set
	 */
	public void setFromFileAsPhysicalPath(String fromFileAsPhysicalPath) {
		this.fromFileAsPhysicalPath = fromFileAsPhysicalPath;
	}

	/**
	 * @return the toFile
	 */
	public String getToFile() {
		return toFile;
	}

	/**
	 * @param toFile the toFile to set
	 */
	public void setToFile(String toFile) {
		this.toFile = toFile;
	}

	/**
	 * @return the toFileAsPhysicalPath
	 */
	public String getToFileAsPhysicalPath() {
		return toFileAsPhysicalPath;
	}

	/**
	 * @param toFileAsPhysicalPath the toFileAsPhysicalPath to set
	 */
	public void setToFileAsPhysicalPath(String toFileAsPhysicalPath) {
		this.toFileAsPhysicalPath = toFileAsPhysicalPath;
	}

	/**
	 * @return the catalogId
	 */
	public String getCatalogId() {
		return catalogId;
	}

	/**
	 * @param catalogId the catalogId to set
	 */
	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	/**
	 * @return the catalogName
	 */
	public String getCatalogName() {
		return catalogName;
	}

	/**
	 * @param catalogName the catalogName to set
	 */
	public void setCatalogName(String catalogName) {
		this.catalogName = catalogName;
	}

	/**
	 * @return the isFileToBeDeleted
	 */
	public String getIsFileToBeDeleted() {
		return isFileToBeDeleted;
	}

	/**
	 * @param isFileToBeDeleted the isFileToBeDeleted to set
	 */
	public void setIsFileToBeDeleted(String isFileToBeDeleted) {
		this.isFileToBeDeleted = isFileToBeDeleted;
	}

	/**
	 * @return the progress
	 */
	public String getProgress() {
		return progress;
	}

	/**
	 * @param progress the progress to set
	 */
	public void setProgress(String progress) {
		this.progress = progress;
	}

	/**
	 * @return the jobOrder
	 */
	public String getJobOrder() {
		return jobOrder;
	}

	/**
	 * @return jobOrder the jobOrder to set
	 */
	public void setJobOrder(String jobOrder) {
		this.jobOrder = jobOrder;
	}

	/**
	 * @return the systemId
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @param systemId the systemId to set
	 */
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	/**
	 * @return the errorKey
	 */
	public String getErrorKey() {
		return errorKey;
	}

	/**
	 * @param errorKey the errorKey to set
	 */
	public void setErrorKey(String errorKey) {
		this.errorKey = errorKey;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}

