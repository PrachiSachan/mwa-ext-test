package com.sony.ods;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="system")
public class System {

	private String systemId;
	private String systemName;
	private String systemKind;
	private String systemStatus;
	private String numOfDrives;
	private String numOfIESlots;
	private String numOfStorageSlots;
	private String errorMessage;
	private String host;
	private String shutdownRequest;
	private String wakeupRequest;
	private String shutdownAndStartStatus;
	private String shutdownAndStartEnable;	// "enable-terminate"/"disable-terminate"/"enable-start"/"disable-start"
	private String cancelEnableForShutdown;	// "enable"/"disable", if canceling of shutdown is acceptable, set to "enable"
	private String totalRunningJobs;
	private String totalStandbyAndProcessingJobs;

	/**
	 * @return the shutdownAndStartEnable
	 */
	public String getShutdownAndStartEnable() {
		return shutdownAndStartEnable;
	}

	/**
	 * @param shutdownAndStartEnable the shutdownAndStartEnable to set
	 */
	public void setShutdownAndStartEnable(String shutdownAndStartEnable) {
		this.shutdownAndStartEnable = shutdownAndStartEnable;
	}

	/**
	 * @return the shutdownAndStartStatus
	 */
	public String getShutdownAndStartStatus() {
		return shutdownAndStartStatus;
	}

	/**
	 * @param shutdownAndStartStatus the shutdownAndStartStatus to set
	 */
	public void setShutdownAndStartStatus(String shutdownAndStartStatus) {
		this.shutdownAndStartStatus = shutdownAndStartStatus;
	}

	/**
	 * @return the wakeup_request
	 */
	public String getWakeup_request() {
		return wakeupRequest;
	}

	/**
	 * @param wakeup_request the wakeup_request to set
	 */
	public void setWakeup_request(String wakeupRequest) {
		this.wakeupRequest = wakeupRequest;
	}

	/**
	 * @return the shutdown_request
	 */
	public String getShutdown_request() {
		return shutdownRequest;
	}

	/**
	 * @param shutdown_request the shutdown_request to set
	 */
	public void setShutdown_request(String shutdownRequest) {
		this.shutdownRequest = shutdownRequest;
	}

	/**
	 * @return the totalRunningJobs
	 */
	public String getTotalRunningJobs() {
		return totalRunningJobs;
	}

	/**
	 * @param totalRunningJobs the totalRunningJobs to set
	 */
	public void setTotalRunningJobs(String totalRunningJobs) {
		this.totalRunningJobs = totalRunningJobs;
	}

	/**
	 * @return the totalStandbyAndProcessingJobs
	 */
	public String getTotalStandbyAndProcessingJobs() {
		return totalStandbyAndProcessingJobs;
	}

	/**
	 * @param totalStandbyAndProcessingJobs the totalStandbyAndProcessingJobs to set
	 */
	public void setTotalStandbyAndProcessingJobs(
			String totalStandbyAndProcessingJobs) {
		this.totalStandbyAndProcessingJobs = totalStandbyAndProcessingJobs;
	}


	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	public System() {}

	/**
	 * @return the systemId
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @param systemId the systemId to set
	 */
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	/**
	 * @return the systemName
	 */
	public String getSystemName() {
		return systemName;
	}

	/**
	 * @param systemName the systemName to set
	 */
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	/**
	 * @return the systemKind
	 */
	public String getSystemKind() {
		return systemKind;
	}

	/**
	 * @param systemKind the systemKind to set
	 */
	public void setSystemKind(String systemKind) {
		this.systemKind = systemKind;
	}

	/**
	 * @return the systemStatus
	 */
	public String getSystemStatus() {
		return systemStatus;
	}

	/**
	 * @param systemStatus the systemStatus to set
	 */
	public void setSystemStatus(String systemStatus) {
		this.systemStatus = systemStatus;
	}

	/**
	 * @return the numOfDrives
	 */
	public String getNumOfDrives() {
		return numOfDrives;
	}

	/**
	 * @param numOfDrives the numOfDrives to set
	 */
	public void setNumOfDrives(String numOfDrives) {
		this.numOfDrives = numOfDrives;
	}

	/**
	 * @return the numOfIESlots
	 */
	public String getNumOfIESlots() {
		return numOfIESlots;
	}

	/**
	 * @param numOfIESlots the numOfIESlots to set
	 */
	public void setNumOfIESlots(String numOfIESlots) {
		this.numOfIESlots = numOfIESlots;
	}

	/**
	 * @return the numOfStorageSlots
	 */
	public String getNumOfStorageSlots() {
		return numOfStorageSlots;
	}

	/**
	 * @param numOfStorageSlots the numOfStorageSlots to set
	 */
	public void setNumOfStorageSlots(String numOfStorageSlots) {
		this.numOfStorageSlots = numOfStorageSlots;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the cancelEnableForShutdown
	 */
	public String getCancelEnableForShutdown() {
		return cancelEnableForShutdown;
	}

	/**
	 * @param cancelEnableForShutdown the cancelEnableForShutdown to set
	 */
	public void setCancelEnableForShutdown(String cancelEnableForShutdown) {
		this.cancelEnableForShutdown = cancelEnableForShutdown;
	}
}

