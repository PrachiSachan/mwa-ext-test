package com.sony.ods;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="tray")
public class Tray {
	private String jobId;
	private String lockStatus;
	public Tray() {}
	
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getLockStatus() {
		return lockStatus;
	}
	public void setLockStatus(String lockStatus) {
		this.lockStatus = lockStatus;
	}
}

