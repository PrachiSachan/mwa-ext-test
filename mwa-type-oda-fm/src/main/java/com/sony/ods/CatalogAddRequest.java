package com.sony.ods;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;
import com.sony.ods.Property;

@XmlRootElement(name="catalogAddRequest")
public class CatalogAddRequest {
	private String cartridgeId;
	private List<String> filePaths;
	private String catalogId;
	private List<Property> properties;
	public CatalogAddRequest() {}
	public CatalogAddRequest(String cartridgeId, List<String> filePaths, String catalogId, List<Property> properties) {
		this.cartridgeId = cartridgeId;
		this.filePaths = filePaths;
		this.catalogId = catalogId;
		this.properties = properties;
	}

	public String getCartridgeId() {
		return cartridgeId;
	}

	public void setCartridgeId(String cartridgeId) {
		this.cartridgeId = cartridgeId;
	}
	@XmlElementWrapper(name = "filePaths")
	@XmlElement(name = "filePath")
	public List<String> getFilePaths() {
		return filePaths;
	}
	public void setFilePaths(List<String> filePaths) {
		this.filePaths = filePaths;
	}
	
	public String getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}
	
	@XmlElementWrapper(name = "properties")
	@XmlElement(name = "property")
	public List<Property> getProperties() {
		return properties;
	}

	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}
}

