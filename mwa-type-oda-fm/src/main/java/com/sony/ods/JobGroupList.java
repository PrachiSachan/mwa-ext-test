package com.sony.ods;


import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="jobGroups")
public class JobGroupList {
	private String totalCount;
	private String count;
	private List<JobGroup> jobGroups;

	public JobGroupList() {}

	/**
	 * @param jobGroups
	 */
	public JobGroupList(List<JobGroup> jobGroups) {
		this.jobGroups = jobGroups;
	}

	/**
	 * @return the totalCount
	 */
	public String getTotalCount() {
		return totalCount;
	}

	/**
	 * @param totalCount the totalCount to set
	 */
	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * @return the count
	 */
	public String getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(String count) {
		this.count = count;
	}

	/**
	 * @return the jobGroups
	 */
	@XmlElement(name="jobGroup")
	public List<JobGroup> getJobGroups() {
		return jobGroups;
	}

	/**
	 * @param jobGroups the jobGroups to set
	 */
	public void setJobGroups(List<JobGroup> jobGroups) {
		this.jobGroups = jobGroups;
	}
	
	
}

