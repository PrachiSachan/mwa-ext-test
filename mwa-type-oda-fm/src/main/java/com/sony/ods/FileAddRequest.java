package com.sony.ods;


import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElement;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

@XmlRootElement(name="fileAddRequest")
public class FileAddRequest {
	private String cartridgeId;
	private List<String> catalogIds;
	@NotNull
	private String filePath;

	public FileAddRequest() {}

	/**
	 * @param cartridgeId
	 * @param catalogIds
	 * @param filePath
	 */
	public FileAddRequest(String cartridgeId, List<String> catalogIds, String filePath) {
		this.cartridgeId = cartridgeId;
		this.catalogIds = catalogIds;
		this.filePath = filePath;
	}

	/**
	 * @param request
	 */
	public FileAddRequest(FileAddRequest request) {
		try {
			BeanUtils.copyProperties(this, request);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new InternalError();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new InternalError();
		}	
	}
	
	/**
	 * @return the cartridgeId
	 */
	public String getCartridgeId() {
		return cartridgeId;
	}
	/**
	 * @param cartridgeId the cartridgeId to set
	 */
	public void setCartridgeId(String cartridgeId) {
		this.cartridgeId = cartridgeId;
	}
	/**
	 * @return the catalogIds
	 */
	@XmlElementWrapper(name = "catalogIds")
	@XmlElement(name = "catalogId")
	public List<String> getCatalogIds() {
		return catalogIds;
	}
	/**
	 * @param catalogIds the catalogIds to set
	 */
	public void setCatalogIds(List<String> catalogIds) {
		this.catalogIds = catalogIds;
	}
	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return filePath;
	}
	/**
	 * @param filePath the filePath to set
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
}

