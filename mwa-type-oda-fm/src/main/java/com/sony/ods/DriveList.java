package com.sony.ods;


import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="drives")
public class DriveList {
	private long totalCount;
	private List<Drive> drives;

	public DriveList() {}
		
	public DriveList(List<Drive> drives) {
		this.drives = drives;
		this.totalCount = drives.size();
	}

	@XmlElement(name="totalCount")
	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
		
	@XmlElement(name="drive")
	public List<Drive> getDrives() {
		return drives;
	}

	public void setDrives(List<Drive> drives) {
		this.drives = drives;
	}
}

