package com.sony.ods;

import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="cartridge")
public class Cartridge {
	private String id;
	private String mediaId;
	private String barcode;
	@Size(min=1, max=128)
	private String title;
	private String kind;
	private String location;
	private String deviceId;
	private String trayId;
	@Size(max=256)
	private String shelfLocation;
	private String volumeModifyId;
	private String rfTag;
	private String free;
	private String capacity;
	private String ejectRequestStatus;
	private String lamp;
	private String status;
	private String isArchivePermitted;
	private String isRetrievePermitted;
	private String isDeletePermitted;
	private String isFormatPermitted;
	private String isFinalizePermitted;
	private String isWriteProtectOnPermitted;
	private String isWriteProtectOffPermitted;
	private String isEjectCartridgePermitted;
	private String isMetadataEditPermitted;
	private String errorKey;
	private String errorMessage;
	private String writeProtectReason;
	private String deleteRequired;
	private String dbIntegrity;
	private String parity;
	private String generation;
	private String reserveStatus;
	private String reserveMountPoint;
	private String vendor;
	private String jobId;
	private String productId;
	private String mediumTypeForLongForm;
	private String mediumTypeForShortForm;
	private String volumeLabel;

	public Cartridge() {}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the mediaId
	 */
	public String getMediaId() {
		return mediaId;
	}

	/**
	 * @param mediaId the mediaId to set
	 */
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	/**
	 * @return the barcode
	 */
	public String getBarcode() {
		return barcode;
	}

	/**
	 * @param barcode the barcode to set
	 */
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the kind
	 */
	public String getKind() {
		return kind;
	}

	/**
	 * @param kind the kind to set
	 */
	public void setKind(String kind) {
		this.kind = kind;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * @return the trayId
	 */
	public String getTrayId() {
		return trayId;
	}

	/**
	 * @param trayId the trayId to set
	 */
	public void setTrayId(String trayId) {
		this.trayId = trayId;
	}

	/**
	 * @return the shelfLocation
	 */
	public String getShelfLocation() {
		return shelfLocation;
	}

	/**
	 * @param shelfLocation the shelfLocation to set
	 */
	public void setShelfLocation(String shelfLocation) {
		this.shelfLocation = shelfLocation;
	}

	/**
	 * @return the volumeModifyId
	 */
	public String getVolumeModifyId() {
		return volumeModifyId;
	}

	/**
	 * @param volumeModifyId the volumeModifyId to set
	 */
	public void setVolumeModifyId(String volumeModifyId) {
		this.volumeModifyId = volumeModifyId;
	}

	/**
	 * @return the rfTag
	 */
	public String getRfTag() {
		return rfTag;
	}

	/**
	 * @param rfTag the rfTag to set
	 */
	public void setRfTag(String rfTag) {
		this.rfTag = rfTag;
	}

	/**
	 * @return the free
	 */
	public String getFree() {
		return free;
	}

	/**
	 * @param free the free to set
	 */
	public void setFree(String free) {
		this.free = free;
	}

	/**
	 * @return the capacity
	 */
	public String getCapacity() {
		return capacity;
	}

	/**
	 * @param capacity the capacity to set
	 */
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	/**
	 * @return the ejectRequestStatus
	 */
	public String getEjectRequestStatus() {
		return ejectRequestStatus;
	}

	/**
	 * @param ejectRequestStatus the ejectRequestStatus to set
	 */
	public void setEjectRequestStatus(String ejectRequestStatus) {
		this.ejectRequestStatus = ejectRequestStatus;
	}

	/**
	 * @return the lamp
	 */
	public String getLamp() {
		return lamp;
	}

	/**
	 * @param lamp the lamp to set
	 */
	public void setLamp(String lamp) {
		this.lamp = lamp;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the isArchivePermitted
	 */
	public String getIsArchivePermitted() {
		return isArchivePermitted;
	}

	/**
	 * @param isArchivePermitted the isArchivePermitted to set
	 */
	public void setIsArchivePermitted(String isArchivePermitted) {
		this.isArchivePermitted = isArchivePermitted;
	}

	/**
	 * @return the isRetrievePermitted
	 */
	public String getIsRetrievePermitted() {
		return isRetrievePermitted;
	}

	/**
	 * @param isRetrievePermitted the isRetrievePermitted to set
	 */
	public void setIsRetrievePermitted(String isRetrievePermitted) {
		this.isRetrievePermitted = isRetrievePermitted;
	}

	/**
	 * @return the isDeletePermitted
	 */
	public String getIsDeletePermitted() {
		return isDeletePermitted;
	}

	/**
	 * @param isDeletePermitted the isDeletePermitted to set
	 */
	public void setIsDeletePermitted(String isDeletePermitted) {
		this.isDeletePermitted = isDeletePermitted;
	}

	/**
	 * @return the isFormatPermitted
	 */
	public String getIsFormatPermitted() {
		return isFormatPermitted;
	}

	/**
	 * @param isFormatPermitted the isFormatPermitted to set
	 */
	public void setIsFormatPermitted(String isFormatPermitted) {
		this.isFormatPermitted = isFormatPermitted;
	}

	/**
	 * @return the isFinalizePermitted
	 */
	public String getIsFinalizePermitted() {
		return isFinalizePermitted;
	}

	/**
	 * @param isFinalizePermitted the isFinalizePermitted to set
	 */
	public void setIsFinalizePermitted(String isFinalizePermitted) {
		this.isFinalizePermitted = isFinalizePermitted;
	}

	/**
	 * @return the isWriteProtectOnPermitted
	 */
	public String getIsWriteProtectOnPermitted() {
		return isWriteProtectOnPermitted;
	}

	/**
	 * @param isWriteProtectOnPermitted the isWriteProtectOnPermitted to set
	 */
	public void setIsWriteProtectOnPermitted(String isWriteProtectOnPermitted) {
		this.isWriteProtectOnPermitted = isWriteProtectOnPermitted;
	}

	/**
	 * @return the isWriteProtectOffPermitted
	 */
	public String getIsWriteProtectOffPermitted() {
		return isWriteProtectOffPermitted;
	}

	/**
	 * @param isWriteProtectOffPermitted the isWriteProtectOffPermitted to set
	 */
	public void setIsWriteProtectOffPermitted(String isWriteProtectOffPermitted) {
		this.isWriteProtectOffPermitted = isWriteProtectOffPermitted;
	}

	/**
	 * @return the isEjectCartridgePermitted
	 */
	public String getIsEjectCartridgePermitted() {
		return isEjectCartridgePermitted;
	}

	/**
	 * @param isEjectCartridgePermitted the isEjectCartridgePermitted to set
	 */
	public void setIsEjectCartridgePermitted(String isEjectCartridgePermitted) {
		this.isEjectCartridgePermitted = isEjectCartridgePermitted;
	}

	/**
	 * @return the isMetadataEditPermitted
	 */
	public String getIsMetadataEditPermitted() {
		return isMetadataEditPermitted;
	}

	/**
	 * @param isMetadataEditPermitted the isMetadataEditPermitted to set
	 */
	public void setIsMetadataEditPermitted(String isMetadataEditPermitted) {
		this.isMetadataEditPermitted = isMetadataEditPermitted;
	}

	/**
	 * @return the errorKey
	 */
	public String getErrorKey() {
		return errorKey;
	}

	/**
	 * @param errorKey the errorKey to set
	 */
	public void setErrorKey(String errorKey) {
		this.errorKey = errorKey;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return writeProtectReason
	 */
	public String getWriteProtectReason() {
		return writeProtectReason;
	}

	/**
	 * @param writeProtectReason
	 */
	public void setWriteProtectReason(String writeProtectReason) {
		this.writeProtectReason = writeProtectReason;
	}

	/**
	 * @return deleteRequired
	 */
	public String getDeleteRequired() {
		return deleteRequired;
	}

	/**
	 * @param deleteRequired
	 */
	public void setDeleteRequired(String deleteRequired) {
		this.deleteRequired = deleteRequired;
	}

	/**
	 * @return dbIntegrity
	 */
	public String getDbIntegrity() {
		return dbIntegrity;
	}

	/**
	 * @param dbIntegrity
	 */
	public void setDbIntegrity(String dbIntegrity) {
		this.dbIntegrity = dbIntegrity;
	}

	/**
	 * @return parity
	 */
	public String getParity() {
		return parity;
	}

	/**
	 * @param parity
	 */
	public void setParity(String parity) {
		this.parity = parity;
	}

	/**
	 * @return generation
	 */
	public String getGeneration() {
		return generation;
	}

	/**
	 * @param generation
	 */
	public void setGeneration(String generation) {
		this.generation = generation;
	}

	/**
	 * @return reserveStatus
	 */
	public String getReserveStatus() {
		return reserveStatus;
	}

	/**
	 * @param reserveStatus
	 */
	public void setReserveStatus(String reserveStatus) {
		this.reserveStatus = reserveStatus;
	}

	/**
	 * @return reserveMountPoint
	 */
	public String getReserveMountPoint() {
		return reserveMountPoint;
	}

	/**
	 * @param reserveMountPoint
	 */
	public void setReserveMountPoint(String reserveMountPoint) {
		this.reserveMountPoint = reserveMountPoint;
	}

	/**
	 * @return vendor
	 */
	public String getVendor() {
		return vendor;
	}

	/**
	 * @param vendor
	 */
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	/**
	 * @return the jobId
	 */
	public String getJobId() {
		return jobId;
	}

	/**
	 * @param jobId the jobId to set
	 */
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return Medium Type For Long Form
	 */
	public String getMediumTypeForLongForm() {
		return mediumTypeForLongForm;
	}

	/**
	 * @param mediumTypeForLongForm Medium Type For Long Form to set
	 */
	public void setMediumTypeForLongForm(String mediumTypeForLongForm) {
		this.mediumTypeForLongForm = mediumTypeForLongForm;
	}

	/**
	 * @return Medium Type For short Form
	 */
	public String getMediumTypeForShortForm() {
		return mediumTypeForShortForm;
	}

	/**
	 * @param mediumTypeForShortForm Medium Type For short Form to set
	 */
	public void setMediumTypeForShortForm(String mediumTypeForShortForm) {
		this.mediumTypeForShortForm = mediumTypeForShortForm;
	}

	/**
	 * @return volumeLabel
	 */
	public String getVolumeLabel() {
		return volumeLabel;
	}

	/**
	 * @param volumeLabel
	 */
	public void setVolumeLabel(String volumeLabel) {
		this.volumeLabel = volumeLabel;
	}
}