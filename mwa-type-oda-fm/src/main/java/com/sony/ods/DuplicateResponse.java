package com.sony.ods;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="duplicateResponse")
public class DuplicateResponse {
	private String sourceCartridgeId;
	private String destinationCartridgeId;
	private Boolean cartridgeMemoryCopyFlag;
	private String writeErrorPolicy;
	private List<Cartridge> cartridges;

	@XmlElement(name = "sourceCartridgeId")
	public String getSourceCartridgeId() {
		return sourceCartridgeId;
	}

	public void setSourceCartridgeId(String sourceCartridgeId) {
		this.sourceCartridgeId = sourceCartridgeId;
	}

	@XmlElement(name = "destinationCartridgeId")
	public String getDestinationCartridgeId() {
		return destinationCartridgeId;
	}

	public void setDestinationCartridgeId(String destinationCartridgeId) {
		this.destinationCartridgeId = destinationCartridgeId;
	}

	@XmlElement(name = "cartridgeMemoryCopyFlag")
	public Boolean getCartridgeMemoryCopyFlag() {
		return cartridgeMemoryCopyFlag;
	}

	public void setCartridgeMemoryCopyFlag(Boolean cartridgeMemoryCopyFlag) {
		this.cartridgeMemoryCopyFlag = cartridgeMemoryCopyFlag;
	}

	@XmlElement(name = "writeErrorPolicy")
	public String getWriteErrorPolicy() {
		return writeErrorPolicy;
	}

	public void setWriteErrorPolicy(String writeErrorPolicy) {
		this.writeErrorPolicy = writeErrorPolicy;
	}

	@XmlElementWrapper(name = "cartridges")
	@XmlElement(name = "cartridge")
	public List<Cartridge> getCartridges() {
		return cartridges;
	}

	public void setCartridges(List<Cartridge> cartridges) {
		this.cartridges = cartridges;
	}
}