package com.sony.ods;


import java.util.List;

import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="properties")
public class PropertyList {
	@Size(max=20)
	private List<Property> properties;

	/**
	 * 
	 */
	public PropertyList() {}

	/**
	 * @param properties
	 */
	public PropertyList(List<Property> properties) {
		this.properties = properties;
	}

	/**
	 * @return the properties
	 */
	@XmlElement(name="property")
	public List<Property> getProperties() {
		return properties;
	}

	/**
	 * @param properties the properties to set
	 */
	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}
}

