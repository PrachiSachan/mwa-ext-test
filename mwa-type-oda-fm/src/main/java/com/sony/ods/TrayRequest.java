package com.sony.ods;


import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="trayRequest")
public class TrayRequest {
	private String command;
	
	public TrayRequest(String command) {
		this.command = command;
	}
	
	public TrayRequest() {}
	
	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}
}

