package com.sony.ods;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="drive")
public class Drive {

	private String driveId;
	private String systemId;
	private String number;		// drive number
	private String cartridgeId;
	private String mountPoint;
	private String useFlag;
	private String status;
	private String errorKey;
	private String errorMessage;
	private String unlocalizedMessage;	//used locally only

	/**
	 * @return the unlocalizedMessage
	 */
	public String getUnlocalizedMessage() {
		return unlocalizedMessage;
	}

	/**
	 * @param unlocalizedMessage the unlocalizedMessage to set
	 */
	public void setUnlocalizedMessage(String unlocalizedMessage) {
		this.unlocalizedMessage = unlocalizedMessage;
	}

	public Drive() {}

	/**
	 * @return the driveId
	 */
	public String getDriveId() {
		return driveId;
	}

	/**
	 * @param driveId the driveId to set
	 */
	public void setDriveId(String driveId) {
		this.driveId = driveId;
	}

	/**
	 * @return the systemId
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @param systemId the systemId to set
	 */
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the cartridgeId
	 */
	public String getCartridgeId() {
		return cartridgeId;
	}

	/**
	 * @param cartridgeId the cartridgeId to set
	 */
	public void setCartridgeId(String cartridgeId) {
		this.cartridgeId = cartridgeId;
	}

	/**
	 * @return the mountPoint
	 */
	public String getMountPoint() {
		return mountPoint;
	}

	/**
	 * @param mountPoint the mountPoint to set
	 */
	public void setMountPoint(String mountPoint) {
		this.mountPoint = mountPoint;
	}

	/**
	 * @return the useFlag
	 */
	public String getUseFlag() {
		return useFlag;
	}

	/**
	 * @param useFlag the useFlag to set
	 */
	public void setUseFlag(String useFlag) {
		this.useFlag = useFlag;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/*
	 * (non-Javadoc)
	 * @see com.sony.ods.library.bean.IErrorInfo#getErrorKey()
	 */
	public String getErrorKey() {
		return errorKey;
	}

	/*
	 * (non-Javadoc)
	 * @see com.sony.ods.library.bean.IErrorInfo#setErrorKey(java.lang.String)
	 */
	public void setErrorKey(String errorKey) {
		this.errorKey = errorKey;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.sony.ods.library.bean.IErrorInfo#getErrorMessage()
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/*
	 * (non-Javadoc)
	 * @see com.sony.ods.library.bean.IErrorInfo#setErrorMessage(java.lang.String)
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}

