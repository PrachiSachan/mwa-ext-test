package com.sony.ods;

import java.util.List;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;
import com.sony.ods.Property;
import com.sony.ods.Catalog;


@XmlRootElement(name="catalogDetails")
public class CatalogDetails extends Catalog {
	protected List<Property> properties;

	public CatalogDetails() {}
	
	public CatalogDetails(Catalog catalog, List<Property> properties) {
		super(catalog, catalog.getId());
		this.properties = properties;
	}
	
	/**
	 * @return the properties
	 */
	@XmlElementWrapper(name = "properties")
	@XmlElement(name = "property")
	public List<Property> getProperties() {
		return properties;
	}

	/**
	 * @param properties the properties to set
	 */
	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}
	
}

