package com.sony.ods;


import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="basePaths")
public class BasePathList {
	private List<BasePath> basePaths;

	
	/**
	 * 
	 */
	public BasePathList() {}

	/**
	 * @param basePaths
	 */
	public BasePathList(List<BasePath> basePaths) {
		this.basePaths = basePaths;
	}

	/**
	 * @return the basePaths
	 */
	@XmlElement(name="basePath")
	public List<BasePath> getBasePaths() {
		return basePaths;
	}

	/**
	 * @param basePaths the basePaths to set
	 */
	public void setBasePaths(List<BasePath> basePaths) {
		this.basePaths = basePaths;
	}
}

