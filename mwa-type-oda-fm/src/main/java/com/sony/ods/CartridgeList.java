package com.sony.ods;


import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="cartridges")
public class CartridgeList {
	private String totalCount;
	private String count;
	private List<Cartridge> cartridges;

	public CartridgeList() {}
		
	public CartridgeList(List<Cartridge> cartridges) {
		this.cartridges = cartridges;
	}

	/**
	 * @return the totalCount
	 */
	public String getTotalCount() {
		return totalCount;
	}

	/**
	 * @param totalCount the totalCount to set
	 */
	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * @return the count
	 */
	public String getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(String count) {
		this.count = count;
	}
		
	/**
	 * @return the cartridges
	 */
	@XmlElement(name="cartridge")
	public List<Cartridge> getCartridges() {
		return cartridges;
	}

	/**
	 * @param cartridges the cartridges to set
	 */
	public void setCartridges(List<Cartridge> cartridges) {
		this.cartridges = cartridges;
	}
}

