package com.sony.ods;


import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="systemRequest")
public class SystemRequest {
	private String command;

	/**
	 * @param command
	 */
	public SystemRequest(String command) {
		this.command = command;
	}
	
	public SystemRequest() {}
	
	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}
}

