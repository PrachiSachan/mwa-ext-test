package com.sony.ods;

import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.*;


@XmlRootElement(name="file")
public class File {
	@Size(min=1,max=127)
	private String name;
	private String path;
	private String type;
	private String size;
	private String updateTime;
	private String jobId;
	
	public File() {};
	
	public File(String name, String path, String type, String size, String updateTime) {
		this.name = name;
		this.path = path;
		this.type = type.toUpperCase();
		this.size = size;
		this.updateTime = updateTime;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getPath() {
		return this.path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getType() {
		return this.type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getSize() {
		return this.size;
	}
	
	public void setSize(String size) {
		this.size = size;
	}
	
	public String getUpdateTime() {
		return this.updateTime;
	}
	
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return the jobId
	 */
	public String getJobId() {
		return jobId;
	}

	/**
	 * @param jobId the jobId to set
	 */
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public int compareTo(File target) {
		return this.path.compareToIgnoreCase(target.path);
	}
}

