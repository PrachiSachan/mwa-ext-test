package com.sony.pro.mwa.util.exceptions;

import java.net.URISyntaxException;

/**
 * クラス「MwaUri」の中で発生する例外
 * 
 * このクラスは、通常のExceptionから派生しています。MwaUriのユーザーはこの例外をハンドリングし
 * 適宜MwaInstanceErrorを生成して例外を報告してください。
 * 
 * TODO：Supplierも兼ねている。。。よろしくない。。。。
 */
public class MwaUriException extends Exception {

	public static final String BASE_IS_EMPTY = "baseURI is null or empty.";
	public static final String INVALID_FORMAT = "baseURI %s format is invalid. baseURI's must be  mwa://{location}/{assetId}/{fileName} or mwa://{location}/{assetId}/{essenceId}/{fileName}";
	public static final String INVALID_PARAMETER = "The value %s specified for %s is invalid";
	public static final String SYNTAX_ERROR_OCCURED = "URI Syntax error occured. reason:%s. path:[%s]";

	public MwaUriException() {}
	public MwaUriException(String message) { super(message); }
	public MwaUriException(Exception e) { super(e); }
	public MwaUriException(String message, Exception e) { super(message, e); }

	/**
	 * baseURIが空またはnullの場合
	 * @throws Exception
	 */
	public static MwaUriException supplyBaseIsEmpty() {
		return new MwaUriException(BASE_IS_EMPTY);
	}
	/**
	 * フォーマット不正
	 * @param invalidUri
	 * @throws Exception
	 */
	public static MwaUriException supplyInvalidUriFormat(String invalidUri) {
		return new MwaUriException(String.format(INVALID_FORMAT, invalidUri));
	}
	/**
	 * パラメータ不正
	 * @param name
	 * @param value
	 * @return
	 */
	public static MwaUriException supplyInvalidParameter(String name, String value) {
		return new MwaUriException(String.format(INVALID_PARAMETER, value == null ? "null" : value, name));
	}
	/**
	 * URI構文エラーが発生した
	 */
	public static MwaUriException supplySyntaxErrorOccured(URISyntaxException uriExp, String uri) {
		return new MwaUriException(String.format(SYNTAX_ERROR_OCCURED, uriExp.getReason(), uri == null ? "null" : uri), uriExp);
	}
}
