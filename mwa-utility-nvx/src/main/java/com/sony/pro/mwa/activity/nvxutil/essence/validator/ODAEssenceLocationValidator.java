package com.sony.pro.mwa.activity.nvxutil.essence.validator;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.activity.nvxutil.EssenceLocationHelper;
import com.sony.pro.mwa.common.log.MwaLogger;

public class ODAEssenceLocationValidator extends AbstractEssenceLocationValidator {
	private static final MwaLogger logger = MwaLogger.getLogger(ODAEssenceLocationValidator.class);

	/**
	 * primary essence のカートリッジID が一致しているかどうかを返します。
	 * @param targetEssencePath
	 * @param primaryEssenceLocationPaths
	 * @return
	 */
	@Override
	protected boolean validPrimaryEssence(String targetEssencePath, List<String> primaryEssenceLocationPaths) {
		if (StringUtils.isNotEmpty(targetEssencePath)) {
			String cartridgeSerialId = EssenceLocationHelper.getCartridgeId(targetEssencePath, false);
			if (StringUtils.isNotEmpty(cartridgeSerialId)) {
				logger.info(String.format("cartridge serial id from mwapath : %s", cartridgeSerialId));
				for (String essenceLocation : primaryEssenceLocationPaths) {
					if (cartridgeSerialId.equals(EssenceLocationHelper.getCartridgeId(essenceLocation, false))) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * proxy essence のカートリッジID が一致しているかどうかを返します。
	 * @param targetEssencePath
	 * @param proxyEssenceLocationPaths
	 * @return
	 */
	@Override
	protected boolean validProxyEssence(String targetEssencePath, List<String> proxyEssenceLocationPaths) {
		// 現状はprimary とproxy で同一処理を実装しておく。
		// 今後、proxy で独自のvalidate が必要になった場合は、このメソッドを修正する必要があります。
		return this.validPrimaryEssence(targetEssencePath, proxyEssenceLocationPaths);
	}
}
