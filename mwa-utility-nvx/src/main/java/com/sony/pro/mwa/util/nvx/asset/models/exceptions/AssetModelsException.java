package com.sony.pro.mwa.util.nvx.asset.models.exceptions;

import com.fasterxml.jackson.databind.JsonMappingException;

/***
 * asset.models下の各オブジェクトで、データ不正等を検知した場合にスローする例外
 */
public class AssetModelsException extends Exception {

	public static String JSONDATA_PARSE_OR_MAPPING_INVALID = "Faild to parse or map JSON data. The failed data is as follows.\\r\\n%s";
	public static String JSONDATA_IS_IVALID = "JSON data is invalid. reason:[ %s ]";
	
	public AssetModelsException() { }
	public AssetModelsException(String message) { super(message); }
	public AssetModelsException(Exception exception) { super(exception); }
	public AssetModelsException(String message, Exception exception) { super(message, exception); }
	
	/**
	 * JsonMappingFailedException発生時に投げる例外
	 * @param failedData マッピングに失敗したJSONデータ
	 * @param exception 発生した例外
	 * @return
	 */
	public static AssetModelsException supplyJsonParseOrMappingFailed(String failedData, Exception exception) {
		return new AssetModelsException(
				String.format(JSONDATA_PARSE_OR_MAPPING_INVALID, failedData), exception);
	}
	
	/**
	 * JSONデータの処理中に、意図するノードが見つからなかった、意図したデータ型でない、などの場合にスローする例外。
	 * @param reason 不正と判断した理由
	 * @return
	 */
	public static AssetModelsException supplyJsonIsInvalid(String reason) {
		return new AssetModelsException(String.format(JSONDATA_IS_IVALID, reason));
	}
}
