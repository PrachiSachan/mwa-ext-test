package com.sony.pro.mwa.activity.nvxutil;

import org.apache.commons.lang.StringUtils;

import com.sony.pro.mwa.activity.nvxutil.EssenceLocationHelper.PrimaryVolumeResponse;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.StandardPerspective;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.storage.NativeLocationModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;
import com.sony.pro.mwa.service.storage.IPerspective;
import com.sony.pro.mwa.service.storage.IUriResolver;
import com.sony.pro.mwa.utils.UriUtils;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * NavigatorX における Essence に関連したユーティリティー機能を提供します。
 */
public class EssenceUtils {
	private static MwaLogger logger = MwaLogger.getLogger(EssenceUtils.class);

	/**
	 * SRT の vtt のロケーションパス(MWAパス）を取得します。(*_convert.vtt 用)
	 * マスターボリュームに該当するロケーション名を確認する必要があるので、NavigatorX と通信することが前提です。
	 *
	 * @param srtMwaPath Hi-res の高解像度の MXF のロケーションパス（MWAパス）
	 * @param essenceId エッセンス ID
	 * @param core MWA コアモジュール
	 * @return MP4 プロキシのロケーションパス(MWAパス）
	 */
	public static String getVTTMasterLocationPathFromSRT(String srtMwaPath, String essenceId, ICoreModules core) {
		PrimaryVolumeResponse volumeInfo = EssenceLocationHelper.getPrimaryVolumeInfo(core);

		String result = null;
		if (volumeInfo != null && !StringUtils.isEmpty(volumeInfo.masterVolumeName)) {
			result = EssenceLocationHelper.replaceLocationName(srtMwaPath, volumeInfo.masterVolumeName);
		} else {
			result = srtMwaPath;
			logger.warn("Failed to get location name. Set location name same with mxfMwaPath.");
		}

		result = EssenceLocationHelper.insertEssenceIdBetweenAssetIdAndFileName(srtMwaPath, essenceId);
		result = EssenceLocationHelper.changeFileNameToConvertVTT(result);

		logger.info(String.format("Get xx_convert.vtt master location path = %s", result));
		return result;
	}

	/**
	 * TimecodeVTT プロキシ のロケーションパス(MWAパス）を取得します。(timecode.vtt 用)
	 *
	 * マスターボリュームに該当するロケーション名を確認する必要があるので、
	 * NavigatorX と通信することが前提です。
	 *
	 * @param anyMwaPath 任意のロケーションパス。互換性を保持するために本パラメータのLocationを使用する（※念のため）
	 * @param assetId 指定するアセットID
	 * @param essenceId 指定するエッセンスID
	 * @return VTT プロキシのロケーションパス(MWAパス）
	 */
	public static String getVTTMasterLocationPath(String anyMwaPath, String assetId, String essenceId, ICoreModules core) {
		PrimaryVolumeResponse volumeInfo = EssenceLocationHelper.getPrimaryVolumeInfo(core);

		String locationName = null;
		if (volumeInfo != null && !StringUtils.isEmpty(volumeInfo.masterVolumeName)) {
			locationName = volumeInfo.masterVolumeName;
		} else {
			logger.warn("Failed to get location name. Set location name same with anyMwaPath.");
			locationName = EssenceLocationHelper.getLocationName(anyMwaPath);
		}

		String result = EssenceLocationHelper.formatTimecodeVTTMwaPathForMXF(locationName, assetId, essenceId);

		logger.info(String.format("Get timecode.vtt master location path = %s", result));
		return result;
	}

	/**
	 * MP4 プロキシのロケーションパス(MWAパス）を取得します。
	 *
	 * プロキシボリュームに該当するロケーション名を確認する必要があるので、
	 * NavigatorX と通信することが前提です。
	 *
	 * @param mxfMwaPath Hi-res の高解像度の MXF のロケーションパス（MWAパス）
	 * @param core MWA コアモジュール
	 * @return MP4 プロキシのロケーションパス(MWAパス）
	 */
	public static String getMP4ProxyLocationPath(String mxfMwaPath, ICoreModules core) {
		PrimaryVolumeResponse volumeInfo = EssenceLocationHelper.getPrimaryVolumeInfo(core);

		String result = null;
		if (volumeInfo != null && !StringUtils.isEmpty(volumeInfo.proxyVolumeName)) {
			result = EssenceLocationHelper.replaceLocationName(mxfMwaPath, volumeInfo.proxyVolumeName);
		} else {
			result = mxfMwaPath;
			logger.warn("Failed to get location name. Set location name same with mxfMwaPath.");
		}

		result = EssenceLocationHelper.changeFileNameToProxyMP4(result);
		logger.info(String.format("Get mp4 proxy location path = %s", result));
		return result;
	}

	/**
	 * MXF ファイルのロケーションパス(MWAパス）を取得します。
	 *
	 * マスターボリュームに該当するロケーション名を確認する必要があるので、
	 * NavigatorX と通信することが前提です。
	 *
	 * @param title アセットタイトル
	 * @param assetId 指定するアセットID
	 * @return MXF のロケーションパス(MWAパス）
	 */
	public static String getMXFMasterLocationPath(String title, String assetId, ICoreModules core) {
		PrimaryVolumeResponse volumeInfo = EssenceLocationHelper.getPrimaryVolumeInfo(core);

		String locationName = null;
		if (volumeInfo != null && !StringUtils.isEmpty(volumeInfo.masterVolumeName)) {
			locationName = volumeInfo.masterVolumeName;
		} else {
			String message = "Failed to get location name for Master: assetId=" + assetId;
			throw new MwaError(MWARC.OPERATION_FAILED, null, message);
		}

		String result = EssenceLocationHelper.formatMXFMwaPath(locationName, assetId, title);

		logger.info(String.format("Get mxf master location path = %s", result));
		return result;
	}

	/**
	 * Thumbnail ファイル(JPG)のロケーションパス(MWAパス）を取得します。
	 *
	 * マスターボリュームに該当するロケーション名を確認する必要があるので、
	 * NavigatorX と通信することが前提です。
	 *
	 * @param assetId 指定するアセットID
	 * @return Thumbnail ファイル(JPG)のロケーションパス(MWAパス）
	 */
	public static String getThumbnailMasterLocationPath(String assetId, String essenceId, ICoreModules core) {
		PrimaryVolumeResponse volumeInfo = EssenceLocationHelper.getPrimaryVolumeInfo(core);

		String locationName = null;
		if (volumeInfo != null && !StringUtils.isEmpty(volumeInfo.masterVolumeName)) {
			locationName = volumeInfo.masterVolumeName;
		} else {
			String message = "Failed to get location name for Thumbnail: assetId=" + assetId;
			throw new MwaError(MWARC.OPERATION_FAILED, null, message);
		}

		String result = EssenceLocationHelper.formatThumbnailMwaPathForMXF(locationName, assetId, essenceId);

		logger.info(String.format("Get thumbnail location path = %s", result));
		return result;
	}

	/**
	 * ファイル名/アセットIDを渡し、Master Volume に対するロケーションパス(MWAパス）を取得します。
	 *
	 * マスターボリュームに該当するロケーション名を確認する必要があるので、
	 * NavigatorX と通信することが前提です。
	 *
	 * @param fileName ファイル名
	 * @param assetId 指定するアセットID
	 * @return Master Volume に対するロケーションパス(MWAパス）
	 */
	public static String getAnyMasterLocationPath(String fileName, String assetId, ICoreModules core) {
		PrimaryVolumeResponse volumeInfo = EssenceLocationHelper.getPrimaryVolumeInfo(core);

		String locationName = null;
		if (volumeInfo != null && !StringUtils.isEmpty(volumeInfo.masterVolumeName)) {
			locationName = volumeInfo.masterVolumeName;
		} else {
			String message = "Failed to get location name for any Master: assetId=" + assetId;
			throw new MwaError(MWARC.OPERATION_FAILED, null, message);
		}

		String result = EssenceLocationHelper.formatMwaPathUnderAssetId(locationName, assetId, fileName);

		logger.info(String.format("Get master location path = %s", result));
		return result;
	}

	/**
	 * ファイル名/アセットIDを渡し、Proxy Volume に対するロケーションパス(MWAパス）を取得します。
	 *
	 * マスターボリュームに該当するロケーション名を確認する必要があるので、
	 * NavigatorX と通信することが前提です。
	 *
	 * @param fileName ファイル名
	 * @param assetId 指定するアセットID
	 * @return Proxy Volume に対するロケーションパス(MWAパス）
	 */
	public static String getAnyProxyLocationPath(String fileName, String assetId, ICoreModules core) {
		PrimaryVolumeResponse volumeInfo = EssenceLocationHelper.getPrimaryVolumeInfo(core);

		String locationName = null;
		if (volumeInfo != null && !StringUtils.isEmpty(volumeInfo.proxyVolumeName)) {
			locationName = volumeInfo.proxyVolumeName;
		} else {
			String message = "Failed to get location name for any Proxy: assetId=" + assetId;
			throw new MwaError(MWARC.OPERATION_FAILED, null, message);
		}

		String result = EssenceLocationHelper.formatMwaPathUnderAssetId(locationName, assetId, fileName);

		logger.info(String.format("Get proxy location path = %s", result));
		return result;
	}

	/**
	 * MWA パスを Native パスに変換する。
	 * その際に、Windows Perspective、UNC Perspective の順に変換を試みます。
	 *
	 * @param mwaPath MWA パス
	 * @param uriResolver 変換する Core インタフェース
	 * @exception MwaInstanceError Perspective が存在していない場合
	 * @return Native パス
	 */
	public static String convertNativePath(String mwaPath, IUriResolver uriResolver) throws MwaInstanceError {
		String nativePath = null;
		LogUtils.log("EssenceUtils_convertNativePath_mwaPath", mwaPath);

		IPerspective perspective = UriUtils.checkPerspective(mwaPath);
		if (StandardPerspective.MWA.equals(perspective)) {
			NativeLocationModel model = uriResolver.asNativePath(mwaPath, StandardPerspective.getDefaultNativePathPerspectiveArray());
			if ((model != null) && (!model.getProperties().containsKey("messageKey"))) {
				nativePath = model.getFirstPath();
			}
		}

		if (nativePath == null || nativePath.isEmpty()) {
			throw new MwaInstanceError(
					MWARC.INVALID_INPUT_PERSPECTIVE,
					null,
					String.format("Failed to convert Path. Not found location perspective. path = %s", mwaPath)
					);
		}

		LogUtils.log("EssenceUtils_convertNativePath_nativePath", nativePath);
		return nativePath;

	}

	/**
	 * MWA パスを Native パスに変換する。
	 * その際に、UNC Perspective の順に変換を試みます。
	 *
	 * @param mwaPath MWA パス
	 * @param uriResolver 変換する Core インタフェース
	 * @exception MwaInstanceError Perspective が存在していない場合
	 * @return Native パス
	 */
	public static String convertNativeUNCPath(String mwaPath, IUriResolver uriResolver) throws MwaInstanceError {
		LogUtils.log("EssenceUtils_convertNativeUNCPath_mwaPath", mwaPath);
		LogUtils.log("EssenceUtils_convertNativeUNCPath_result", convertNativePath(mwaPath, uriResolver, new String[] { "UNC" }));
		return convertNativePath(mwaPath, uriResolver, new String[] { "UNC" });
	}

	private static String convertNativePath(String mwaPath, IUriResolver uriResolver, String[] perspectiveList) throws MwaInstanceError {
		String nativePath = null;

		IPerspective perspective = UriUtils.checkPerspective(mwaPath);
		if (StandardPerspective.MWA.equals(perspective)) {
			NativeLocationModel model = uriResolver.asNativePath(mwaPath, perspectiveList);
			if ((model != null) && (!model.getProperties().containsKey("messageKey"))) {
				nativePath = model.getFirstPath();
			}
		}

		if (nativePath == null || nativePath.isEmpty()) {
			throw new MwaInstanceError(
					MWARC.INVALID_INPUT_PERSPECTIVE,
					null,
					String.format("Failed to convert Path. Not found location perspective. path = %s", mwaPath)
					);
		}

		return nativePath;
	}

	/**
	 * assetModel からvideoInfoModel を取り出します。
	 * @param assetModel
	 * @return
	 */
	public static JsonObject getVideoInfoModel(JsonObject assetModel) {
		JsonObject videoInfoModel = null;
		JsonArray essenceModels = assetModel.getJsonArray("essences");
		for (int index = 0; index < essenceModels.size(); index++) {
			if (essenceModels.getJsonObject(index).containsKey("videoInfo")) {
				videoInfoModel = essenceModels.getJsonObject(index).getJsonObject("videoInfo");
				break;
			}
		}
		return videoInfoModel;
	}

	public static String getAnyVttOrImageLocationPath(String location, String beforeAssetId, String assetId, ICoreModules core) {

		String dirStr = location.substring(location.indexOf(beforeAssetId) + beforeAssetId.length() +1, location.length());
		logger.info("dirStr = " + dirStr);
		String result = EssenceUtils.getAnyMasterLocationPath(dirStr, assetId, core);
		logger.info("result = " + result);
		return result;
	}
}
