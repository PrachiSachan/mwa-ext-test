package com.sony.pro.mwa.activity.nvxutil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sony.pro.mwa.util.enumeration.LocationNameEnum;
import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.model.storage.LocationCollection;
import com.sony.pro.mwa.model.storage.LocationModel;
import com.sony.pro.mwa.service.ICoreModules;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class PlaceTypeProvider extends CommandServiceBase {
	private static MwaLogger logger = MwaLogger.getLogger(PlaceTypeProvider.class);

	public static enum PlaceTypeEnum {
		ODS_DRIVE("ODS_DRIVE"),
		ODS_LIBRARY("ODS_LIBRARY"),
		ARCHIVE_STORAGE("ARCHIVE_STORAGE"),
		ONLINE_STORAGE("ONLINE_STORAGE"),
		PFD_DRIVE("PFD_DRIVE"),
		UNKNOWN(""),
		;

		private String name;

		private PlaceTypeEnum(String name) {
			this.name = name;
		}

		public static PlaceTypeEnum getPlaceType(String name) {
			PlaceTypeEnum[] placeTypeArray = PlaceTypeEnum.values();

	        for (PlaceTypeEnum placeType : placeTypeArray) {
	            if (placeType.name.toString().equals(name)) {
	                return placeType;
	            }
	        }
	        return PlaceTypeEnum.UNKNOWN;
	    }
	}

	public PlaceTypeProvider(ICoreModules coreModules) {
		super(coreModules);
	}

	/**
	 * odaMwapath の List から key: Serial 番号、value: 対応する PlaceType の Map を取得します
	 * @param odaMwaPaths
	 * @return
	 */
	public Map<String, String> getByOdaMwaPath(List<String> odaMwaPaths) {

		Map<String, String> serialPlaceTypeMap = new HashMap<>();

		// serial を取得
		List<String> serials = new ArrayList<>();
		for (String mwaPath : odaMwaPaths) {
			serials.add(EssenceLocationHelper.getCartridgeId(mwaPath, true));
		}

		// volume models を取得
		ArchiveVolumesCommandService command = new ArchiveVolumesCommandService(this.getCoreModules());
		JsonArray models = command.getArchiveVolumes().getJsonArray("models");

		// シリアル番号に一致するボリュームモデルを取得
		JsonObject volumeModel = new JsonObject();
		for (String serial : serials) {
			volumeModel = matchVolume(serial, models);
			String placeType = getPlaceTypeOrDefaultEmpty(volumeModel);
			serialPlaceTypeMap.put(serial, placeType);
		}
		return serialPlaceTypeMap;
	}

	public String getBySerial(String serial) {

		// volume models を取得
		ArchiveVolumesCommandService command = new ArchiveVolumesCommandService(this.getCoreModules());
		JsonArray models = command.getArchiveVolumes().getJsonArray("models");

		// シリアル番号に一致するボリュームモデルを取得
		JsonObject volumeModel = new JsonObject();
		volumeModel = matchVolume(serial, models);
		String placeType = getPlaceTypeOrDefaultEmpty(volumeModel);

		return placeType;
	}

	public String getByLocationName(String locationName) {
		LocationNameEnum locationNameEnum = LocationNameEnum.getLocationName(locationName);

		// LocationModel には ODA のモデルがないため、
		// locationName が ODA の場合は、最初に判定処理をして返す (ODA_LIBRARY,PFDも同様にする)
		switch (locationNameEnum) {
			case ODA:
				return PlaceTypeEnum.ODS_DRIVE.name();
			case ODA_LIBRARY:
				return PlaceTypeEnum.ODS_LIBRARY.name();
			case PFD:
				return PlaceTypeEnum.PFD_DRIVE.name();
			default:
				break;
		}

		LocationCollection collection = this.getCoreModules().getStorageManager().getLocations(null, null, null, null);// StorageManagerからすべてのロケーションを取得
		List<LocationModel> list = (List<LocationModel>) collection.getModels();// ロケーションからすべてのロケーションモデルを取得
		String locationId = null;
		for (LocationModel model : list) {
			if (locationName.equals(model.getName())) {
				locationId = model.getId();
			}
		}

		ArchivePlaceCommandService command = new ArchivePlaceCommandService(this.getCoreModules());
		JsonObject response = command.getArchivePlaces();
		JsonArray models = response.getJsonArray("models");
		List<JsonObject> placeModels = new ArrayList<JsonObject>();
		Iterator<Object> vitr = models.iterator();
		while (vitr.hasNext()) {
			placeModels.add((JsonObject) vitr.next());
		}

		String placeType = null;
		for (JsonObject model : placeModels) {
			if (locationId.equals(model.getString("locationId"))) {
				placeType = model.getString("type");
			}
		}
		return placeType;
	}

	private String getPlaceTypeOrDefaultEmpty(JsonObject volume) {
		String result = "";
		String placeType = volume.getString("placeType");
		if (!StringUtils.isEmpty(placeType)) {
			result = placeType;
		}
		return result;
	}

	private JsonObject matchVolume(String serial, JsonArray models) {
		JsonObject result = new JsonObject();

		if (models != null) {
			for (Object volumeObject : models) {
				serial = VolumeUtils.decodeSpace(serial);
				JsonObject volumeJson = (JsonObject) volumeObject;
				if (StringUtils.isEmpty(volumeJson.getString("serial"))) {
					continue;
				}
				String volumeSerial = VolumeUtils.decodeSpace(volumeJson.getString("serial"));
				if (serial.equals(volumeSerial)) {
					result = volumeJson;
				}
			}
		}
		return result;
	}
}