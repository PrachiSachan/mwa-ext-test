package com.sony.pro.mwa.activity.nvxutil.commandservice;

import com.sony.pro.mwa.activity.nvxutil.CommandServiceBase;
import com.sony.pro.mwa.activity.nvxutil.LogUtils;
import com.sony.pro.mwa.activity.scheme.httpclient.HttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.httpclient.HttpResponseResult;
import com.sony.pro.mwa.activity.scheme.httpclient.IHttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.utils.EndpointProvider;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;

/**
 * NavigatorX における Storyboard に関する API の HTTP リクエストに対するサービスクラスです。
 */
public class StoryboardCommandService extends CommandServiceBase {
	private static MwaLogger logger = MwaLogger.getLogger(StoryboardCommandService.class);
	// Base URI
	private final static String BASE_URI = "nvx/api/v1";

	// Storyboard URI
	private final static String STORYBOARD_URI = BASE_URI + "/storyboards";
	private final static String STORYBOARD_SPECIFIED_URI = STORYBOARD_URI + "/%s";

	/**
	 * 引数付きコンストラクタを提供します。
	 *
	 * @param coreModules
	 */
	public StoryboardCommandService(ICoreModules coreModules) {
		super(coreModules);
	}

	public String getStoryboard(String accessKey, String collectionId) {
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), STORYBOARD_SPECIFIED_URI);
		String urlWithQuery = String.format(url, collectionId);
		logger.info("AssetCommandService_getStoryboard_urlWithQuery: " + urlWithQuery);
		HttpResponseResult httpResponse = new HttpResponseResult();
		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.get(urlWithQuery, this.getHttpRequestProvider(accessKey));
			LogUtils.log("AssetCommandService_getStoryboard_responseBody", httpResponse.getResponseBody());
		} catch (Exception e) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to get storyboard. Connection error occurred.");
		}
		if (httpResponse.getStatusCode() != 200) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to get storyboard. Response is incorrect from NVX server.");
		}
		return httpResponse.getResponseBody();
	}

	public String patchStoryboard(String accessKey, String requestBody, String collectionId) {
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), STORYBOARD_SPECIFIED_URI);
		String urlWithQuery = String.format(url, collectionId);
		logger.info("AssetCommandService_getStoryboard_urlWithQuery: " + urlWithQuery);
		HttpResponseResult httpResponse = new HttpResponseResult();
		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			LogUtils.log("AssetCommandService_patchStoryboard_requestBody", requestBody);
			httpResponse = client.patch(urlWithQuery, requestBody, this.getHttpRequestProvider(accessKey));
			LogUtils.log("AssetCommandService_getStoryboard_responseBody", httpResponse.getResponseBody());
		} catch (Exception e) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to patch storyboard. Connection error occurred.");
		}
		if (httpResponse.getStatusCode() != 200) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to patch storyboard. Response is incorrect from NVX server.");
		}
		return httpResponse.getResponseBody();
	}
}