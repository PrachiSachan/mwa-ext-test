package com.sony.pro.mwa.util.nvx.asset.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.sony.pro.mwa.util.nvx.asset.models.exceptions.AssetModelsException;

/**
 * 配列形式のJsonNode を定義する基底クラス
 * 
 * @author Kousuke Hosaka
 *
 */
public class ArrayNodeBase {
	protected JsonNode jsonNode = null;
	protected ArrayNode arrayNode = null;
	
	/**
	 * Assetのように、idを指定してもmodels配列として来てしまうようなケースでは
	 * コンストラクタにJsonNodeを渡すだけでは処理がたりない。
	 * 
	 * 派生クラスの方で、基底コンストラクタを呼ばなくでもコンストラクタを記述することが出来
	 * 然るべきノードを検索してjsonNodeを差し替えられるようにしておきたいので、protectedでデフォルトコンストラクタを切ります。
	 */
	protected ArrayNodeBase() {}

	/**
	 * このクラスで処理するJsonNodeを差し替える
	 * @param node
	 */
	protected void setNode(JsonNode node) {
		this.jsonNode = node;
		this.arrayNode = (ArrayNode)node;
	}
	
	/**
	 * このクラスで処理するJsonNodeを差し替える
	 * @param jsonText
	 */
	protected void setNode(String jsonText) throws AssetModelsException {
		try {
			ObjectMapper mapper = new ObjectMapper();
			this.setNode(mapper.readValue(jsonText, JsonNode.class));
		} catch (Exception e) {
			throw AssetModelsException.supplyJsonParseOrMappingFailed(jsonText, e);
		}		
	}
	
	/**
	 * JSON文字列を指定するコンストラクタ
	 * @param jsonText Assetデータに相当するJSON文字列
	 * @throws AssetModelsException パースまたはマッピングエラーが発生した場合にスローされる
	 */
	public ArrayNodeBase(String jsonText) throws AssetModelsException {
		this.setNode(jsonText);
	}
	
	/**
	 * コンストラクタ
	 * 該当するJsonNode を渡して、
	 * このクラスで処理するJsonNode のインスタンスを生成します。
	 * @param jsonNode
	 * @throws AssetModelsException 
	 */
	public ArrayNodeBase(JsonNode jsonNode) throws AssetModelsException {
		if (!jsonNode.isArray()) {
			throw AssetModelsException.supplyJsonIsInvalid("jsonNode is not Array.");
		}
		this.jsonNode = jsonNode;
		this.arrayNode = (ArrayNode) this.jsonNode;
		
	}	
	
	/**
	 * 指定のKey からJsonNodeでValue を返却します。
	 * @param key
	 * @return
	 */
	public JsonNode getNodeValue(String key) {
		JsonNode nodeValue = this.jsonNode.get(key);
		// TODO : nullチェック
		// TODO : requiredチェック
		return nodeValue;
	}
	
	public void setObjectNode(JsonNode objectNode) {
		this.arrayNode.add(objectNode);
	}
}
