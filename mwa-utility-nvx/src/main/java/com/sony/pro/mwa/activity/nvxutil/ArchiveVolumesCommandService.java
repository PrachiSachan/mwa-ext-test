package com.sony.pro.mwa.activity.nvxutil;

import java.util.List;

import org.springframework.util.StringUtils;

import com.sony.pro.mwa.activity.scheme.httpclient.HttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.httpclient.HttpResponseResult;
import com.sony.pro.mwa.activity.scheme.httpclient.IHttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.utils.EndpointProvider;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;

import io.vertx.core.json.JsonObject;

/**
 * NavigatorX における Archive Volume API の HTTP リクエストに対するサービスクラスです。
 */
public class ArchiveVolumesCommandService extends CommandServiceBase {
	private static MwaLogger logger = MwaLogger.getLogger(ArchiveVolumesCommandService.class);
	private final static String BASE_URI = "nvx/api/v1/internal/archive-volumes";
	private final static String BASE_URI_WITHOUT_INTERNAL = "nvx/api/v1/archive-volumes";

	/**
	 * 引数付きコンストラクタを提供します。
	 *
	 * @param coreModules
	 */
	public ArchiveVolumesCommandService(ICoreModules coreModules) {
		super(coreModules);
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public JsonObject getArchiveVolumes() {
		return getArchiveVolumes(null, null, null, null, null);
	}
	
	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public JsonObject getODSCartridgeArchiveVolumes() {
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), BASE_URI);
		String urlWithQuery = url + "?filter=type==ODS_CARTRIDGE";

		HttpResponseResult httpResponse = new HttpResponseResult();

		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.get(urlWithQuery, this.getHttpRequestProvider());
			logger.debug(httpResponse.getResponseBody());
		} catch (Exception e) {
			String errorMessage = "Failed to get archive-volumes from navigatorx.";
			logger.error(errorMessage, e);
		}

		JsonObject result = null;

		// TODO: レスポンスボディから VolumeCollection にデシリアライズする。
		if (!StringUtils.isEmpty(httpResponse.getResponseBody())) {
			result = new JsonObject(httpResponse.getResponseBody());
		}

		return result;
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public JsonObject getArchiveVolumes(List<String> sort, String search, List<String> filter, String offset, String limit) {
		// TODO : 将来的には、BASE_URI_WITHOUT_INTERNALの方を使用する
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), BASE_URI);
		String urlWithQuery = url;

		HttpResponseResult httpResponse = new HttpResponseResult();

		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.get(urlWithQuery, this.getHttpRequestProvider());
			logger.debug(httpResponse.getResponseBody());
		} catch (Exception e) {
			String errorMessage = "Failed to get archive-volumes from navigatorx.";
			logger.error(errorMessage, e);
		}

		JsonObject result = null;

		// TODO: レスポンスボディから VolumeCollection にデシリアライズする。
		if (!StringUtils.isEmpty(httpResponse.getResponseBody())) {
			result = new JsonObject(httpResponse.getResponseBody());
		}

		return result;
	}

	public JsonObject patchArchiveVolumes(String updateVolumeId, String requestBody) {
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), BASE_URI);
		logger.info("url: " + url);
		String urlWithQuery = url + "/" + updateVolumeId;
		HttpResponseResult httpResponse = new HttpResponseResult();
		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.patch(urlWithQuery, requestBody, this.getHttpRequestProvider());
			logger.debug(httpResponse.getResponseBody());
		} catch (Exception e) {
			String errorMessage = "Failed to patch archive-volume from nabigatorx. volumeId = " + updateVolumeId;
			logger.error(errorMessage, e);
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, errorMessage);
		}
		if (!StringUtils.isEmpty(httpResponse.getResponseBody())) {
			return new JsonObject(httpResponse.getResponseBody());
		} return null;
	}

	public JsonObject patchArchiveVolumesNotInternal(String updateVolumeId, String requestBody) {
		// リファクタリング対象
		// プールID の更新が Internal が付いたベースパスだとできないため、
		// Internal が付いてないベースパスを用意した
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), BASE_URI_WITHOUT_INTERNAL);
		logger.info("url: " + url);
		String urlWithQuery = url + "/" + updateVolumeId;
		HttpResponseResult httpResponse = new HttpResponseResult();
		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.patch(urlWithQuery, requestBody, this.getHttpRequestProvider());
			logger.debug(httpResponse.getResponseBody());
		} catch (Exception e) {
			String errorMessage = "Failed to patch archive-volume from nabigatorx. volumeId = " + updateVolumeId;
			logger.error(errorMessage, e);
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, errorMessage);
		}
		if (!StringUtils.isEmpty(httpResponse.getResponseBody())) {
			return new JsonObject(httpResponse.getResponseBody());
		} return null;
	}
}