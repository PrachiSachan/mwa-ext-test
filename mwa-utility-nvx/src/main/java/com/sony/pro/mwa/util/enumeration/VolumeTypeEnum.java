package com.sony.pro.mwa.util.enumeration;

public enum VolumeTypeEnum {
	MASTER("MASTER"),
	PROXY("PROXY");

	private String name;

	private VolumeTypeEnum(String name) {
		this.name = name;
	}

	public static VolumeTypeEnum getLocationName(String name) {
		VolumeTypeEnum[] locationNameArray = VolumeTypeEnum.values();

        for (VolumeTypeEnum locationName : locationNameArray) {
            if (locationName.name.toString().equals(name)) {
                return locationName;
            }
        }
        return VolumeTypeEnum.MASTER;
    }
}
