package com.sony.pro.mwa.util.nvx.asset.models;

import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.util.nvx.asset.models.AssetInfo;
import com.sony.pro.mwa.util.nvx.asset.models.Constants;
import com.sony.pro.mwa.util.nvx.asset.models.exceptions.AssetModelsException;


/**
 * Define the properties of Asset.
 * 
 * @author Kousuke Hosaka
 *
 */
public class Asset extends ObjectNodeBase {

	/**
	 * デフォルトコンストラクタ
	 */
	public Asset() {}
	
	/**
	 * コンストラクタ
	 * @param model
	 */
	public Asset(JsonNode model) throws AssetModelsException {
		// idを指定しても、「models配列の要素数が１」という状態でJSONが取得されるケースが大半のため
		// modelsから先頭のオブジェクトノードを取り出して、それをルートとするよう、ここで処理します。
		JsonNode asset = this.findAsset(model);
		this.setNode(asset);
	}
	
	/**
	 * Assetデータ相当のJSON文字列を指定するコンストラクタ
	 * @param jsonText Assetデータ相当のJSON文字列
	 * @throws AssetModelsException
	 */
	public Asset(String jsonText) throws AssetModelsException {
		// idを指定しても、「models配列の要素数が１」という状態でJSONが取得されるケースが大半のため
		// modelsから先頭のオブジェクトノードを取り出して、それをルートとするよう、ここで処理します。
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode asset = this.findAsset(mapper.readValue(jsonText, JsonNode.class));
			this.setNode(asset);
		} catch (Exception e) {
			throw AssetModelsException.supplyJsonParseOrMappingFailed(jsonText, e);
		}	
	}
	
	/**
	 * AssetId を取得する
	 * @return
	 */
	public String getAssetId() {
		// TODO: Idはこのキーでよい。。。？_source下のIdを返すべき？
		return this.getNodeValue(Constants.Id).asText();
	}
	
	/**
	 * AssetId を書き換えます。
	 * @param assetId
	 */
	public void setAssetId(String assetId) {
		this.setNodeValue(Constants.Id, assetId);
	}
	
////////// 以下、本当に必要なパラメータか不明なため、途中まで記述 //////////
	/**
	 * RevisionId を取得します。
	 * @return
	 */
	public int getRevisionId() {
		return this.getNodeValue(Constants.Asset.RevisionId).asInt();
	}
	
	/**
	 * RevisionId を書き換えます。
	 * @param revisionId
	 */
	public void setRevisionId(int revisionId) {
		this.setNodeValue(Constants.Asset.RevisionId, revisionId);
	}
	
	/**
	 * RevisionNote を取得します。
	 * @return
	 */
	public String getRevisonNote() {
		return this.getNodeValue(Constants.Asset.RevisionNote).asText();
	}

	/**
	 * RevisionNote を書き換えます。
	 * @param revisionNote
	 */
	public void setRevisonNote(String revisionNote) {
		this.setNodeValue(Constants.Asset.RevisionNote, revisionNote);
	}
	
	/**
	 * Parents を取得します。
	 * @return
	 */
	public List<String> getParents() {
		return this.convertJsonNodeToListString(this.getNodeValue(Constants.Asset.Parents));
	}
	
	/**
	 * Parents を書き換えます。
	 * @param parents
	 * @throws AssetModelsException
	 */
	public void setParents(List<String> parents) throws AssetModelsException {
		this.setNodeValue(Constants.Asset.Parents, this.convertListToJsonNode(parents));
	}
	
	/**
	 * ShortcutParentIds を取得します。
	 * @return
	 */
	public List<Integer> getShortcutParentIds() {
		return this.convertJsonNodeToListInteger(this.getNodeValue(Constants.Asset.ShortcutParentIds));
	}
	
	/**
	 * ShortcutParentIds を書き換えます。
	 * @param shortcutParentIds
	 * @throws AssetModelsException
	 */
	public void setShortcutParentIds(List<Integer> shortcutParentIds) throws AssetModelsException {
		this.setNodeValue(Constants.Asset.ShortcutParentIds, this.convertListToJsonNode(shortcutParentIds));
	}
	
	/**
	 * ThumbnailEventId を取得します。
	 * @return
	 */
	public String getThumbnailEventId() {
		return this.getNodeValue(Constants.Asset.ThumbnailEventId).asText();
	}
	
	/**
	 * ThumbnailEventId を書き換えます。
	 * @param thumbnailEventId
	 */
	public void setThumbnailEventId(String thumbnailEventId) {
		this.setNodeValue(Constants.Asset.ThumbnailEventId, thumbnailEventId);
	}
	
	/**
	 * MetadataSchemaId を取得します。
	 * @return
	 */
	public String getMetadataSchemaId() {
		return this.getNodeValue(Constants.Asset.MetadataSchemaId).asText();
	}
	
	/**
	 * MetadataSchemaId を書き換えます。
	 * @param thumbnailEventId
	 */
	public void setMetadataSchemaId(String metadataSchemaId) {
		this.setNodeValue(Constants.Asset.MetadataSchemaId, metadataSchemaId);
	}
	
	/**
	 * Operator を取得します。
	 * @return
	 */
	public String getOperator() {
		return this.getNodeValue(Constants.Asset.Operator).asText();
	}
	
	/**
	 * Operator を書き換えます。
	 * @param operator
	 */
	public void setOperator(String operator) {
		this.setNodeValue(Constants.Asset.Operator, operator);
	}

////////// ここまで //////////
	
	
	/**
	 * AssetInfo を取得する
	 * @return
	 */
	public AssetInfo getAssetInfo() {
		return new AssetInfo(this.getNodeValue(Constants.Asset.AssetInfo));
	}
	
	/**
	 * AssetInfo を書き換える。
	 * @param assetInfo
	 */
	public void setAssetInfo(AssetInfo assetInfo) {
		try {
			this.setObjectNode(Constants.Asset.AssetInfo, assetInfo);
		} catch (AssetModelsException e) {
			e.printStackTrace();
		}
	}
	
	/***
	 * Essences を取得する
	 * @return Essences
	 */
	public Essences getEssences() throws AssetModelsException {
		JsonNode essences = this.find(Constants.Asset.Essences);
		if (essences == null || !essences.isArray()) {
			throw AssetModelsException.supplyJsonIsInvalid("essences property not found.");
		}
		return new Essences(essences);
	}
	
	/**
	 * Events を取得する
	 * @return
	 * @throws AssetModelsException 
	 */
	public Events getEvents() throws AssetModelsException {
		JsonNode events = this.find(Constants.Asset.Events);
		if (events == null || !events.isArray()) {
			throw AssetModelsException.supplyJsonIsInvalid("events property not found.");
		}
		return new Events(events);
	}
	
	public void setEvents(List<Event> eventList) throws AssetModelsException {
		// this.setNodeValue(Constants.Asset.Events, this.convertListToJsonNode(eventList));
		try {
			this.setObjectNode(Constants.Asset.Events, eventList);
		} catch (AssetModelsException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * root要素から_source要素を検索する。models配列が指定された場合は先頭要素から、modelsの要素オブジェクトが指定された場合は、そのオブジェクトから_sourceを検索する。
	 * @param root modelsを含むJsonNode
	 * @return _sourceノードの中身
	 */
	private JsonNode findAsset(JsonNode root) throws AssetModelsException {

		if (!root.isObject()) {
			throw new UnsupportedOperationException("class Asset supports only object-node.");
		}
		
		// rootノードが_sourceを保持している場合は、Assetのオブジェクトノードそのものが指定されたと
		// みなして以降の処理をしない。
		if (root.has(Constants.Asset._Source)) {
			return root.get(Constants.Asset._Source);
		}

		// 保証はないが、assetInfoとeventsとessenceを保持しているなら、rootが_sourceであるとみなす。
		if (root.has(Constants.Asset.Essences) && root.has(Constants.Asset.Events) && root.has(Constants.Asset.AssetInfo)) {
			return root;
		}
		
		
		if (!root.has(Constants.Models)) {
			throw AssetModelsException.supplyJsonIsInvalid("models property not found.");
		}
		
		// _sourceを保持していない場合は、models配列を保持する親オブジェクトが指定されたものとみなす。
		JsonNode models = root.findPath(Constants.Models);	
		Iterator<JsonNode> iter = models.iterator();
		
		if (!iter.hasNext()) {
			throw AssetModelsException.supplyJsonIsInvalid("asset object not found in models property.");
		}
		
		JsonNode firstNode = iter.next();
		
		return firstNode.get(Constants.Asset._Source);
	}
}
