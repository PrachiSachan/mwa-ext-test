package com.sony.pro.mwa.activity.nvxutil.essence.converter;

public class ODALibraryEssenceLocationPathConverter extends AbstractEssenceLocationPathConverter implements IEssenceLocationPathConverter{
    public String getLocationName() {
       return "ODA_LIBRARY";
    }
}