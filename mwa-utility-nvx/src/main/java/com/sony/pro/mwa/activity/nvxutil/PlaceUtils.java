package com.sony.pro.mwa.activity.nvxutil;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * NavigatorX における Place に関連したユーティリティー機能を提供します。
 */
public class PlaceUtils {
	private static MwaLogger logger = MwaLogger.getLogger(PlaceUtils.class);
	
	/**
	 *  OnlinePlaceIDからLocationIDとPlaceTypeの取得を行います
	 * @param placeId
	 * @return
	 */
	public static JsonObject getPlaceModel(String placeId, ICoreModules core) {
		JsonObject result = null;
		JsonArray placeModels = getPlaceModels(core);
		if (placeModels == null) {
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, null);
		}
		for (int i = 0; i < placeModels.size(); i++) {
			JsonObject place = placeModels.getJsonObject(i);
			if (place != null && place.containsKey("id") && placeId.equals(place.getString("id"))) {
				result = place;
				break;
			}
		}
		return result;		
	}
	
	/**
	 * PlaceCollectionの取得を行います
	 * @return
	 */
	public static JsonArray getPlaceModels(ICoreModules core) {
		ArchivePlaceCommandService archivePlaceCommandService = new ArchivePlaceCommandService(core);
		JsonObject placeModelCollection = new JsonObject();
		String errMsg = "Internal communication error was occured. Please contact the system administrator.";
		try {
			placeModelCollection = archivePlaceCommandService.getArchivePlaces();
			if (placeModelCollection == null || !placeModelCollection.containsKey("models")) {
				logger.error("Failed to get information on Place Models.");
				throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, errMsg);
			}
		} catch (Exception e) {
			logger.error("An error occurred. Failed to get information on Place Models.", e);
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, errMsg);
		}
		
		JsonArray placeModelsArray = new JsonArray();
		if (!placeModelCollection.isEmpty()) {
			placeModelsArray = placeModelCollection.getJsonArray("models");
		}		
		return placeModelsArray;
	}

}
