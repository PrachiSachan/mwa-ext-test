package com.sony.pro.mwa.activity.nvxutil.essence.filter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sony.pro.mwa.activity.nvxutil.PlaceTypeProvider;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import org.apache.commons.collections4.CollectionUtils;

import com.sony.pro.mwa.activity.nvxutil.CommandServiceBase;
import com.sony.pro.mwa.activity.nvxutil.EssenceLocationHelper;
import com.sony.pro.mwa.activity.nvxutil.PlaceTypeProvider.PlaceTypeEnum;
import com.sony.pro.mwa.activity.nvxutil.VolumeUtils;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.service.ICoreModules;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class EssenceLocationFilter extends CommandServiceBase {
	private EssenceLocationModel odsEssenceLocationModel = new EssenceLocationModel();
	private EssenceLocationModel odsLibraryEssenceLocationModel = new EssenceLocationModel();
	private EssenceLocationModel archiveStorageEssenceLocationModel = new EssenceLocationModel();
	private EssenceLocationModel onlineStorageEssenceLocationModel = new EssenceLocationModel();
	private EssenceLocationModel pfdEssenceLocationModel = new EssenceLocationModel();
	private EssenceLocationModel essenceLocationModel = new EssenceLocationModel();
	private static MwaLogger logger = MwaLogger.getLogger(VolumeUtils.class);

	// AssetModel は Json 形式
	public EssenceLocationFilter(JsonObject assetModel, ICoreModules coreModules) {
		super(coreModules);
		JsonArray essences = assetModel.getJsonArray("essences");
		List<JsonObject> essenceModels = new ArrayList<JsonObject>();
		Iterator<Object> vitr = essences.iterator();
		while (vitr.hasNext()) {
			essenceModels.add((JsonObject) vitr.next());
		}
		for (JsonObject model: essenceModels){
			intialize(model);
		}
	}

	// EssenceModel(mwa-activity-nvx) をどう持ってくるか
	public EssenceLocationFilter(List<JsonObject> essenceModels, ICoreModules coreModules) {
		super(coreModules);
		for (JsonObject model: essenceModels){
			intialize(model);
		}
	}

	// ODA, ODA_Library は Archive された時の状態で振り分けをする
	// 実行時の volume の状態ではないので注意のこと
	private void intialize(JsonObject model) {
		String location = model.getString("location");
		String placeType = "";
		PlaceTypeProvider placeTypeProvider = new PlaceTypeProvider(this.getCoreModules());
		String locationName = EssenceLocationHelper.getLocationName(location);
		placeType = placeTypeProvider.getByLocationName(locationName);
		PlaceTypeEnum placeTypeEnum = PlaceTypeEnum.getPlaceType(placeType);

		// placeType を区別せず、primaryEssenceLocation とproxyEssenceLocation に振り分ける
		this.addEssenceLocation(model, location, essenceLocationModel);

		switch (placeTypeEnum) {
			case ODS_DRIVE:
				this.addEssenceLocation(model, location, odsEssenceLocationModel);
				break;

			case ODS_LIBRARY:
				this.addEssenceLocation(model, location, odsLibraryEssenceLocationModel);
				break;

			case ARCHIVE_STORAGE:
				this.addEssenceLocation(model, location, archiveStorageEssenceLocationModel);
				break;

			case ONLINE_STORAGE:
				this.addEssenceLocation(model, location, onlineStorageEssenceLocationModel);
				break;

			case PFD_DRIVE:
				this.addEssenceLocation(model, location, pfdEssenceLocationModel);
				break;

			default:
				throw new MwaInstanceError(MWARC.INVALID_INPUT, null, "Failed to resolved location path. Please check location and place settings.");
		}
	}

	/**
	 * onlineStrageのprimary一覧を取得
	 * @return
	 */
	public List<String> getPrimaryEssenceForOnlineStrage() {
		List<String> result = new ArrayList<>();
		if(onlineStorageEssenceLocationModel.primaryEssenceLocation != null &&
				!onlineStorageEssenceLocationModel.primaryEssenceLocation.isEmpty()) {
			result.addAll(onlineStorageEssenceLocationModel.primaryEssenceLocation);
		}

		return result;
	}


	/**
	 * ods関連（ODA Cartridge/Library）のprimary、proxyEssence一覧取得
	 * @return
	 */
	public List<String> getODAArchived() {
		List<String> result = new ArrayList<>();
		if(odsEssenceLocationModel.primaryEssenceLocation != null &&
				!odsEssenceLocationModel.primaryEssenceLocation.isEmpty()) {
			result.addAll(odsEssenceLocationModel.primaryEssenceLocation);
		}
		if(odsEssenceLocationModel.proxyEssenceLocation != null &&
				!odsEssenceLocationModel.proxyEssenceLocation.isEmpty()) {
			result.addAll(odsEssenceLocationModel.proxyEssenceLocation);
		}
		if(odsLibraryEssenceLocationModel.primaryEssenceLocation != null &&
				!odsLibraryEssenceLocationModel.primaryEssenceLocation.isEmpty()) {
			result.addAll(odsLibraryEssenceLocationModel.primaryEssenceLocation);
		}
		if(odsLibraryEssenceLocationModel.proxyEssenceLocation != null &&
				!odsLibraryEssenceLocationModel.proxyEssenceLocation.isEmpty()) {
			result.addAll(odsLibraryEssenceLocationModel.proxyEssenceLocation);
		}
		return result;
	}

	/**
	 * ods関連（ODA Cartridge/Library）のprimary一覧を取得
	 * @return
	 */
	public List<String> getPrimaryEssenceArchivedToODA() {
		List<String> result = new ArrayList<>();
		if(odsEssenceLocationModel.primaryEssenceLocation != null &&
				!odsEssenceLocationModel.primaryEssenceLocation.isEmpty()) {
			result.addAll(odsEssenceLocationModel.primaryEssenceLocation);
		}
		if(odsLibraryEssenceLocationModel.primaryEssenceLocation != null &&
				!odsLibraryEssenceLocationModel.primaryEssenceLocation.isEmpty()) {
			result.addAll(odsLibraryEssenceLocationModel.primaryEssenceLocation);
		}
		return result;
	}

	/**
	 * ods関連（ODA Cartridge/Library）のproxyEssence一覧
	 * @return
	 */
	public List<String> getProxyEssenceArchivedToODA() {
		List<String> result = new ArrayList<>();
		if(odsEssenceLocationModel.proxyEssenceLocation != null &&
				!odsEssenceLocationModel.proxyEssenceLocation.isEmpty()) {
			result.addAll(odsEssenceLocationModel.proxyEssenceLocation);
		}
		if(odsLibraryEssenceLocationModel.proxyEssenceLocation != null &&
				!odsLibraryEssenceLocationModel.proxyEssenceLocation.isEmpty()) {
			result.addAll(odsLibraryEssenceLocationModel.proxyEssenceLocation);
		}
		return result;
	}

	/**
	 * ods 以外の Archive済みEssenceLocation一覧取得
	 * @return
	 */
	public List<String> getStorageArchived() {
		List<String> result = new ArrayList<>();
		if(archiveStorageEssenceLocationModel.primaryEssenceLocation != null &&
				!archiveStorageEssenceLocationModel.primaryEssenceLocation.isEmpty()) {
			result.addAll(archiveStorageEssenceLocationModel.primaryEssenceLocation);
		}
		if (archiveStorageEssenceLocationModel.proxyEssenceLocation != null &&
				!archiveStorageEssenceLocationModel.proxyEssenceLocation.isEmpty()) {
			result.addAll(archiveStorageEssenceLocationModel.proxyEssenceLocation);
		}
		return result;
	}

	/**
	 * Archive Storage 関連のprimary一覧を取得
	 * @return
	 */
	public List<String> getPrimaryEssenceArchivedToStorage() {
		List<String> result = new ArrayList<>();
		if(archiveStorageEssenceLocationModel.primaryEssenceLocation != null &&
				!archiveStorageEssenceLocationModel.primaryEssenceLocation.isEmpty()) {
			result.addAll(archiveStorageEssenceLocationModel.primaryEssenceLocation);
		}
		if(archiveStorageEssenceLocationModel.primaryEssenceLocation != null &&
				!archiveStorageEssenceLocationModel.primaryEssenceLocation.isEmpty()) {
			result.addAll(archiveStorageEssenceLocationModel.primaryEssenceLocation);
		}
		return result;
	}

	/**
	 * Archive Storage 関連のproxyEssence一覧
	 * @return
	 */
	public List<String> getProxyEssenceArchivedToStorage() {
		List<String> result = new ArrayList<>();
		if(archiveStorageEssenceLocationModel.proxyEssenceLocation != null &&
				!archiveStorageEssenceLocationModel.proxyEssenceLocation.isEmpty()) {
			result.addAll(archiveStorageEssenceLocationModel.proxyEssenceLocation);
		}
		if(archiveStorageEssenceLocationModel.proxyEssenceLocation != null &&
				!archiveStorageEssenceLocationModel.proxyEssenceLocation.isEmpty()) {
			result.addAll(archiveStorageEssenceLocationModel.proxyEssenceLocation);
		}
		return result;
	}
	
	/**
	 * placeType を区別しないprimaryEssence 一覧を返します。
	 * @return
	 */
	public List<String> getArchivedPrimaryEssences() {
		List<String> result = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(essenceLocationModel.primaryEssenceLocation)) {
			result.addAll(essenceLocationModel.primaryEssenceLocation);
		}
		return result;
	}
	
	/**
	 * placeType を区別しないproxyEssence 一覧を返します。
	 * @return
	 */
	public List<String> getArchivedProxyEssences() {
		List<String> result = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(essenceLocationModel.proxyEssenceLocation)) {
			result.addAll(essenceLocationModel.proxyEssenceLocation);
		}
		return result;
	}

	public boolean containODAArchived() {
		return !(getODAArchived().isEmpty());
	}

	public boolean containStorageArchived() {
		return !(getStorageArchived().isEmpty());
	}

	/**
	 * 引数として受け取ったlocationを適切なEssenceLocationに追加します。
	 *
	 * @param model AssetModel
	 * @param location EssenceLocationに追加するlocation
	 * @param essenceLocationModel locationの追加対象を決めるModel情報
	 */
	private void addEssenceLocation(JsonObject model, String location, EssenceLocationModel essenceLocationModel) {
		// primaryのEssenceはprimaryEssenceLocation
		if (model.getBoolean("primary")) {
			essenceLocationModel.primaryEssenceLocation.add(location);
		} else if (model.getBoolean("proxy")) {
			// proxyのEssenceはproxyEssenceLocation
			essenceLocationModel.proxyEssenceLocation.add(location);
		}
	}
}
