package com.sony.pro.mwa.activity.nvxutil.essence.converter;

public interface IEssenceLocationPathConverter {
    public String convert(String fromMwaPath);
    public String getLocationName();
}