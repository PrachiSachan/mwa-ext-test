package com.sony.pro.mwa.util.text;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.util.exceptions.ExportException;
import com.sony.pro.mwa.enumeration.StandardPerspective;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.storage.IPerspective;
import com.sony.pro.mwa.utils.UriUtils;
import com.sony.pro.mwa.util.exceptions.ExportException;
import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.activity.nvxutil.FileManagementUtils;
import com.sony.pro.mwa.activity.nvxutil.LogUtils;
import com.sony.pro.mwa.util.exceptions.NLEExportException;
import com.sony.pro.mwa.util.nvx.asset.models.exceptions.AssetModelsException;

/**
 * 文字の計算・変換などを行うクラス
 */
public class TextConverter {

	private static String[] prefixList = {
			"CON", "PRN", "AUX", "NUL", "CLOCK$",
			"COM0", "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9",
			"LPT0", "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9"
	};
	private static String[] symbolList = { "\r\n", "\n", "\\", "/", ":", "*", "?", "\"", "<", ">", "|", "\t" };
	private static String[] encodeSymbolList = { "%C2%A5r%C2%A5n", "%C2%A5n", "%C2%A5"};

	private static final int EXPORT_MAX_LENGTH = 259;
	private static final String PROXY_SUFFIX = "_proxy";

	private static MwaLogger logger = MwaLogger.getLogger(TextConverter.class);

	/**
	 * ファイル名の変換をする
	 *
	 * @param encodeFileName (拡張子なし)
	 * @param fileLength
	 * @param returnByEncode
	 * @return 記号等があった場合はreplaceした文字列 string
	 * @throws AssetModelsException
	 */
	public static String normalizedFileName(String encodeFileName, int fileLength, boolean returnByEncode) throws ExportException {
		if (StringUtils.isEmpty(encodeFileName)) {
			throw ExportException.supplyInvalidParameter("encodeFileName", encodeFileName);
		}
		if (fileLength <= 0) {
			throw ExportException.supplyInvalidParameter("fileLength", String.valueOf(fileLength));
		}

		// assetTitle に円マークが"%C2%A5"という特殊なエンコードをされてくる場合がある。
		// デコードすると、java 上で "\" 一文字となり、それをひっかける方法がない。
		// そのため、エンコードされた状態で円マークの変換を行う。
		String result = replaceEncodeSymbol(encodeFileName);

		// これより上ではエンコード文字への処理だったが、これ以降の処理はデコードした文字列を扱うためデコードする
		try {
			result = result.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
			result = URLDecoder.decode(result.replace("+", "%2B"), "UTF-8").replace("%2B", "+");
		} catch (UnsupportedEncodingException e) {
			throw ExportException.decodeFailure(result);
		}
		result = addPrefix(result);

		// 特殊なエンコードをされた文字列の変換は上記 replaceEncodeSymbol で実施したが、
		// 一般的な URL エンコードの記号はデコード後、下記 replaceSymbol で変換する
		result = replaceSymbol(result);
		result = result.trim();

		if (returnByEncode) {
			result = FileManagementUtils.encoder(result);
		}

		result = cutLength(result, fileLength);
		return result;
	}

	/**
	 * エンコードされた特定文字を変換する
	 * 変換対象) \r\n, \n, \ がエンコードされたもの
	 *
	 * @param str
	 * @return 記号等があった場合はreplaceした文字列 string
	 */
	public static String replaceEncodeSymbol(String str) {
		for (String symbol : encodeSymbolList) {
			if (str.contains(symbol)) {
				str = str.replace(symbol, "_");
			}
		}
		return str;
	}

	/**
	 * Windows 予約文字が先頭にあった場合、"_"をファイル名の先頭に付与する
	 *
	 * @param str
	 * @return Windows 予約文字があった場合はreplaceした文字列 string
	 */
	public static String addPrefix(String str) {
		for (String prefix : prefixList) {
			if (str.startsWith(prefix)) {
				str = String.format("_%s", str);
			}
		}
		return str;
	}

	/**
	 * 特定の記号があった場合、"_"に変換する
	 * 変換対象) ¥ / : * ? " < > | \n \r\n
	 *
	 * @param str
	 * @return 特定の記号があった場合はreplaceした文字列 string
	 */
	public static String replaceSymbol(String str) {
		for (String symbol : symbolList) {
			if (str.contains(symbol)) {
				str = str.replace(symbol, "_");
			}
		}
		return str;
	}

	/**
	 * 文字列を指定した文字列長にカットする
	 *
	 * @param str
	 * @param fileLength
	 * @return 文字列長が fileLength の文字数にカットされた文字列
	 */
	public static String cutLength(String str, int fileLength) {
		return str.length() > fileLength ? str.substring(0, fileLength) : str;
	}

	/**
	 * エクスポートにおけるファイル名の最大文字列長を算出する
	 *
	 * @param directoryPath(末尾がバックスラッシュのもの)
	 * @param extension
	 * @return ファイル名の最大文字列長
	 * @throws AssetModelsException
	 */
	public static int calcFileNameLength(String directoryPath, String extension) throws NLEExportException {
		int fileNameLength = EXPORT_MAX_LENGTH - (directoryPath.length() + extension.length());
		if (fileNameLength + extension.length() < 11 || fileNameLength < 1) {
			throw NLEExportException.pathLengthOverLimit("Export file name is more than 259 characters.");
		}
		return fileNameLength;
	}

	/**
	 * エクスポートにおけるプロキシファイル名の最大文字列長を算出する
	 *
	 * @param directoryPath(末尾がバックスラッシュのもの)
	 * @param extension
	 * @return ファイル名の最大文字列長
	 * @throws AssetModelsException
	 */
	public static int calcProxyNameLength(String directoryPath, String extension) throws NLEExportException {
		int fileNameLength = EXPORT_MAX_LENGTH - (directoryPath.length() + PROXY_SUFFIX.length() + extension.length());
		if (fileNameLength + PROXY_SUFFIX.length() + extension.length() < 11 || fileNameLength < 1) {
			throw NLEExportException.pathLengthOverLimit("Export file name is more than 259 characters.");
		}
		return fileNameLength;
	}

	/**
	 * Input のディレクトリパスがパスのデリミターで終わっていない場合、パスの最後にデリミタ―文字を付加する
	 *
	 * @param directoryPath
	 * @return デリミターで終わるディレクトリパス
	 */
	public static String addDelimiter(String directoryPath) {
		if (directoryPath.contains("\\") && !directoryPath.endsWith("\\")) {
			directoryPath = directoryPath + "\\";
		} else if (directoryPath.contains("/") && !directoryPath.endsWith("/")) {
			directoryPath = directoryPath + "/";
		}
		return directoryPath;
	}

	/**
	 * Input の値が UUID 形式であるか判定する
	 *
	 * @param input
	 * @return デリミターで終わるディレクトリパス
	 */
	public static Boolean isUUID(String input) {
		if (StringUtils.isEmpty(input)) {
			return false;
		}
		String reg = "^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}$";
		Pattern p = Pattern.compile(reg);
		Matcher m = p.matcher(input);
		return m.find();
	}

	/**
	 * Input のパスが IP アドレスを含むUNC 形式であるか判定する
	 *
	 * @param uri
	 * @return デリミターで終わるディレクトリパス
	 */
	public static Boolean validIp(String uri) {
		if (StringUtils.isEmpty(uri)) {
			return false;
		}
		String backSlashUNC = "^\\\\\\\\(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\\\.*";
		Pattern p = Pattern.compile(backSlashUNC);
		Matcher m = p.matcher(uri);
		if (m.find()) {
			return true;
		}
		String slashUNC = "^//(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])/.*";
		Pattern p2 = Pattern.compile(slashUNC);
		Matcher m2 = p2.matcher(uri);
		if (m2.find()) {
			return true;
		}
		return false;
	}

	/**
	 * Input のパスが Windows のディレクトリパス形式であるか判定する
	 *
	 * @param uri
	 * @return デリミターで終わるディレクトリパス
	 */
	public static Boolean isWindows(String uri) {
		if (StringUtils.isEmpty(uri)) {
			return false;
		}
		String backSlashUNC = "^[a-zA-Z]:\\\\";
		Pattern p = Pattern.compile(backSlashUNC);
		Matcher m = p.matcher(uri);
		if (m.find()) {
			return true;
		}
		String slashUNC = "^[a-zA-Z]:/";
		Pattern p2 = Pattern.compile(slashUNC);
		Matcher m2 = p2.matcher(uri);
		if (m2.find()) {
			return true;
		}
		return false;
	}

	/**
	 * Input のパスのファイルが既に存在している場合、Input のパスのファイル名をリネームします。
	 * 例) test.mxf -> test(1).mxf
	 *
	 * @param path
	 *
	 * @return リネームに成功した場合: リネームしたパス、リネームに失敗した場合: null
	 * @throws NLEExportException
	 * @throws IOException
	 */
	public static Path renameFile(Path path) throws NLEExportException {
		String parentPath = path.getParent().toString();
		String fileName = path.getFileName().toString();
		int extensionStartPosition = fileName.lastIndexOf(".");
		String extension = fileName.substring(extensionStartPosition);
		String fileNameWithoutExtension =  fileName.substring(0, extensionStartPosition);

		final int start = 1;
		final int limit = 99;
		for (int increment = start; increment <= limit; increment++) {
			Path newPath = Paths.get(parentPath, fileNameWithoutExtension + "(" + increment + ")" + extension);
			if (!Files.exists(newPath)) {
				LogUtils.log("New path to use when copying or linking :" + newPath.toString());
				return newPath;
			}
		}
		throw NLEExportException.renameFailure(path.toString());
	}

	/**
	 * Input のパスが UNC 形式であるか判定する。
	 * 上記実装のisUNC はIPアドレス形式のUNC Path を判定するためのものであり、
	 * こちらのメソッドは、単純にUNC 形式か判定するだけである。
	 *
	 * @param uri
	 * @return
	 */
	public static Boolean validUNC(String uri) {
		if (StringUtils.isEmpty(uri)) {
			return false;
		}
		IPerspective uriPerspective = UriUtils.checkPerspective(uri);
		if (StandardPerspective.UNC.equals(uriPerspective) || StandardPerspective.UNC_FORWARD_SLASH.equals(uriPerspective)) {
			return true;
		}
		return false;
	}

	/****************************************************************************
	 * 現在はpomのバージョン都合により、以下のメソッド群をこのクラスに実装している。
	 * pomのバージョンを2.13.0 → 2.14.0 に変更する時に、以下のメソッド群を
	 * mwa-storage-management のStorageManagerUtils に移動することを検討する。
	 * ただし、core側のUtilを参照することが前提となっていることについても合わせて検討する。
	 ****************************************************************************/
	/**
	 * ホスト名からIPアドレスを取得します
	 * @param hostName ホスト名
	 * @return IPアドレス
	 */
	public static String getIpAddress(String hostName) {
		String ipAddress = "";
		try {
			ipAddress = InetAddress.getByName(hostName).getHostAddress();
		} catch(Exception e) {
			throw new MwaError(MWARC.CONNECTION_HOST_NOT_FOUND, null, e.getMessage());
		}
		return ipAddress;
	}

	/**
	 * ホスト名を含むUNCパスをIPアドレス形式のUNCパスに変換します
	 * @param nativePath ホスト名を含むUNCパス
	 * @return 変換後のUNCパス
	 */
	public static String convertHostToIP(String nativePath) {
		final String UNC = "\\\\";
		final String UNC_FORWARD = "//";
		String SEPARATOR = "\\";
		final String FORWARD_SEPARATOR = "/";

		String hostName = "";
		int hostNameLastCharIndex = 0;

		IPerspective uriPerspective = UriUtils.checkPerspective(nativePath);
		if (StandardPerspective.UNC.equals(uriPerspective)) {
			hostNameLastCharIndex = nativePath.indexOf(SEPARATOR, UNC.length());
			hostName = nativePath.substring(UNC.length(), hostNameLastCharIndex);
		} else if (StandardPerspective.UNC_FORWARD_SLASH.equals(uriPerspective)) {
			hostNameLastCharIndex =nativePath.indexOf(FORWARD_SEPARATOR, UNC_FORWARD.length());
			hostName = nativePath.substring(UNC_FORWARD.length(), hostNameLastCharIndex);
		} else {
			// UNCパス以外はエラー
			throw new MwaError(MWARC.INVALID_INPUT_URI_FORMAT);
		}
		return nativePath.replace(hostName, getIpAddress(hostName));
	}

	/**
	 * Output として出すパスをデコードします。
	 *
	 * @param path
	 *
	 * @return 変換したパス
	 * @throws ExportException
	 */
	public static String decodeOutput(String path) throws ExportException {
		String result = path.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
		result = result.replaceAll("\\+", "%2B");
		try {
			result = URLDecoder.decode(result, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw ExportException.decodeFailure(result);
		}
		return result;
	}

	/**
	 * 文字列をデコードします。
	 *
	 * @param str
	 *
	 * @return 変換した文字列
	 * @throws UnsupportedEncodingException
	 */
	public static String decode(String str) throws UnsupportedEncodingException {
		try {
			str = URLDecoder.decode(str, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw e;
		}
		return str;
	}

	/**
	 * 受け取ったファイル名をエンコードします。
	 *
	 * @param fileName
	 * @return 変換したパス
	 */
	public static String encodeFileName(String fileName) {
		String result = null;
		try {
			result = UriUtils.percentEncode(fileName);
		} catch (UnsupportedEncodingException e) {
			throw new MwaError(MWARC.INVALID_INPUT);
		}
		result = result.replace("%2F", "/");
		result = result.replace("%3A", ":");
		result = result.replace("%3D", "=");
		result = result.replace("%3F", "?");
		return result;
	}

	/**
	 * 同名ファイルの存在確認とリネーム処理を行います。
	 * NVX-7157 対応。
	 * @param filePath
	 * @return
	 */
	public static String renameFilePath(String filePath) {
		File exportFile = new File(filePath);
		if (!exportFile.exists()) {
			// 同名ファイルが存在していなかったらそのまま返却する
			return filePath;
		}
		logger.info(String.format("filePath before rename : %s", filePath));
		Path renamedPath = null;
		try {
			renamedPath = TextConverter.renameFile(exportFile.toPath());
		} catch (NLEExportException e) {
			logger.error(e.getStackTrace());
			throw new MwaError(MWARC.SYSTEM_ERROR, null, e.getMessage());
		}
		if (renamedPath == null) {
			throw new MwaError(MWARC.INVALID_INPUT_FORMAT, null, "Failed to rename file.");
		}
		String renamedUri = renamedPath.toAbsolutePath().toString();
		logger.info(String.format("filePath after rename : %s", renamedUri));
		return renamedUri;
	}

	/**
	 * Input のUri がMwaUri であるか判定します。
	 * @param uri
	 * @return
	 */
	public static boolean isMwaUri(String uri) {
		if (StringUtils.isEmpty(uri)) {
			return false;
		}
		IPerspective uriPerspective = UriUtils.checkPerspective(uri);
		return StandardPerspective.MWA.equals(uriPerspective) ? true : false;
	}
}
