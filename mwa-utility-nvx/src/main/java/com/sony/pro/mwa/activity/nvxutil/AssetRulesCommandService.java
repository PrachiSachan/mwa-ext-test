package com.sony.pro.mwa.activity.nvxutil;

import java.util.List;

import org.springframework.util.StringUtils;

import com.sony.pro.mwa.activity.scheme.httpclient.HttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.httpclient.HttpResponseResult;
import com.sony.pro.mwa.activity.scheme.httpclient.IHttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.utils.EndpointProvider;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;

import io.vertx.core.json.JsonObject;

/**
 * NavigatorX における Asset Rules API の HTTP リクエストに対するサービスクラスです。
 */
public class AssetRulesCommandService extends CommandServiceBase {
	private static MwaLogger logger = MwaLogger.getLogger(AssetRulesCommandService.class);
	private final static String BASE_URI = "/nvx/api/v1/asset-rules";

	/**
	 * 引数付きコンストラクタを提供します。
	 *
	 * @param coreModules
	 */
	public AssetRulesCommandService(ICoreModules coreModules) {
		super(coreModules);
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public JsonObject getAssetRules() {
		return getAssetRules(null, null, null, null);
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public JsonObject getAssetRules(List<String> sort, List<String> filter, String offset, String limit) {
		// rule 取得
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), BASE_URI);
		String urlWithQuery = url;

		// TODO: 後々は引数をクエリパラメータ化する必要がある.
		// いまいまは何も渡さない実装にしておくので修正が必要。
		HttpResponseResult httpResponse = new HttpResponseResult();

		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.get(urlWithQuery, this.getHttpRequestProvider());
			logger.debug(httpResponse.getResponseBody());
		} catch (Exception e) {
			String errorMessage = "Failed to get asset-rules from navigatorx.";
			logger.error(errorMessage, e);
			throw new MwaError(MWARC.CONNECTION_FAILED, null, errorMessage);
		}

		JsonObject result = null;

		// TODO: レスポンスボディから VolumeCollection にデシリアライズする。
		if (!StringUtils.isEmpty(httpResponse.getResponseBody())) {
			result = new JsonObject(httpResponse.getResponseBody());
		}

		return result;
	}
}