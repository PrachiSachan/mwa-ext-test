package com.sony.pro.mwa.activity.nvxutil;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.activity.scheme.httpclient.HttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.httpclient.HttpResponseResult;
import com.sony.pro.mwa.activity.scheme.httpclient.IHttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.utils.EndpointProvider;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.scheduler.CronTriggerInfoCollection;
import com.sony.pro.mwa.model.scheduler.CronTriggerInfoModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;
import com.sony.pro.nvx.base.dao.jdbc.util.Helper;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * NavigatorX における Archive Volume API の HTTP リクエストに対するサービスクラスです。
 */
public class SchedulerCommandService extends CommandServiceBase {
	private static MwaLogger logger = MwaLogger.getLogger(SchedulerCommandService.class);
	private final static String BASE_URI = "mwa/api/v2/schedulers/cron";

	/**
	 * 引数付きコンストラクタを提供します。
	 *
	 * @param coreModules
	 */
	public SchedulerCommandService(ICoreModules coreModules) {
		super(coreModules);
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return schedulers の Collection
	 */
	public CronTriggerInfoCollection getSchedulers() {
		return getSchedulers(null, null, null, null, null);
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return schedulers の Collection
	 */
	public CronTriggerInfoCollection getSchedulers(List<String> sort, String search, List<String> filter, String offset, String limit) {
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), BASE_URI);
		String urlWithQuery = url;

		HttpResponseResult httpResponse = new HttpResponseResult();

		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.get(urlWithQuery, this.getHttpRequestProvider());
			logger.debug(httpResponse.getResponseBody());
		} catch (Exception e) {
			String message = "Internal communication error was occured.";
			logger.error(message, e);
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, message);
		}

		JsonObject response = new JsonObject();
		if (!StringUtils.isEmpty(httpResponse.getResponseBody())) {
			response = new JsonObject(httpResponse.getResponseBody());
		}

		CronTriggerInfoCollection result = deserialize(response);
		return result;
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return クライアント PC の schedulers の Collection のリスト
	 */
	public List<CronTriggerInfoCollection> getClientsSchedulers() {
		return getClientsSchedulers(null, null, null, null, null);
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return クライアント PC の schedulers の Collection のリスト
	 */
	public List<CronTriggerInfoCollection> getClientsSchedulers(List<String> sort, String search, List<String> filter, String offset, String limit) {
		List<String> urls = EndpointProvider.getURLsAsODSClientProvider(this.getCoreModules(), BASE_URI);
		List<CronTriggerInfoCollection> result = new ArrayList<>();

		if (!urls.isEmpty()) {
			for (String url : urls) {
				HttpResponseResult httpResponse = new HttpResponseResult();

				try {
					IHttpClientWrapper client = new HttpClientWrapper();
					httpResponse = client.get(url, this.getHttpRequestProvider());
					logger.debug(httpResponse.getResponseBody());
				} catch (Exception e) {
					String message = "Failed to get information from client PC. Please check the connection to client PC.";
					logger.error(message, e);
					throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, message);
				}

				JsonObject response = new JsonObject();
				if (!StringUtils.isEmpty(httpResponse.getResponseBody())) {
					response = new JsonObject(httpResponse.getResponseBody());
				}
				result.add(deserialize(response));
			}
		}
		return result;
	}

	/**
	 * CronTriggersInfoCollectionのjsonをオブジェクトにパースします。
	 *
	 * @param cronTriggerInfoJson
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public CronTriggerInfoCollection deserialize(JsonObject json) {
		
		List<Map<String, Object>> models = json.getJsonArray(Helper.CollectionFieldEnum.models.name()).getList();
		ArrayList<CronTriggerInfoModel> modelList = new ArrayList<CronTriggerInfoModel>();
		for (Map<String, Object> map : models) {
			CronTriggerInfoModel model = new CronTriggerInfoModel();
			try {
				BeanUtils.populate(model, map);
			} catch (IllegalAccessException e) {
				String message = "Failed to desirialize response body to cron trigger info collection.";
				logger.error(message, e);
				throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, message);
			} catch (InvocationTargetException e) {
				String message = "Failed to desirialize response body to cron trigger info collection.";
				logger.error(message, e);
				throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, message);
			}
			modelList.add(model);
		}

		CronTriggerInfoCollection result = new CronTriggerInfoCollection(modelList);
		return result;
	}
}