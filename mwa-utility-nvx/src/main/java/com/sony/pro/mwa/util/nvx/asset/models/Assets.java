package com.sony.pro.mwa.util.nvx.asset.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.sony.pro.mwa.util.nvx.asset.models.exceptions.AssetModelsException;

/**
 * Assetリスト
 * 
 * /nvx/api/v1/assetsが返却するJsonデータのルート要素「models」を保持し、model一覧、id一覧の取得、modelの検索処理等を実装します。
 */
public class Assets extends ArrayNodeBase {

	/***
	 * コンストラクタ
	 * @param models
	 * @throws AssetModelsException 
	 */
	public Assets(JsonNode models) throws AssetModelsException {
		super(models);
	}
	
	/**
	 * JSON文字列を指定するコンストラクタ
	 * @param jsonText　Assetリストに相当するJSON文字列
	 */
	public Assets(String jsonText) throws AssetModelsException {
		super(jsonText);
	}
	
	/***
	 * Assetのリストを取得する
	 * @return
	 */
	public List<Asset> get() throws AssetModelsException {
		
		JsonNode models = this.getModels();
		Iterator<JsonNode> iter = models.iterator();
		List<Asset> result = new ArrayList<Asset>();
		
		while (iter.hasNext()) {
			result.add(new Asset(iter.next()));
		}
		
		return result;
	}

	/**
	 * idで指定されたAssetを検索する
	 * @param id 取得したいAssetのId
	 * @return Idが一致したAsset
	 */
	public Asset get(String id) {
		throw new UnsupportedOperationException();
	}

	/**
	 * 全Assetのidリストを取得する
	 * @return modelsが保持している全AssetのIdリスト
	 */
	public List<String> getIds() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * models要素を取得する
	 * @return models要素
	 * @throws AssetModelsException models要素がない、またはArrayでない場合
	 */
	private JsonNode getModels() throws AssetModelsException {
		JsonNode models = this.getNodeValue(Constants.Models);
		
		if (models == null || !models.isArray()) {
			throw AssetModelsException.supplyJsonIsInvalid("models property not found.");
		}
		
		return models;
	}
}
