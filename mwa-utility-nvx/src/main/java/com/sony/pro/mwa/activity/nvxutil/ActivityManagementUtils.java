package com.sony.pro.mwa.activity.nvxutil;

import java.util.HashMap;
import java.util.Map;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.service.ICoreModules;

/**
 * NavigatorX における Activity に関連したユーティリティー機能を提供します。
 *
 * このクラスでは Activty Manager に対する create instance を簡単に呼べるようにすることを目的としている。
 * そのため、instance 生成に関わることしか定義しないこと。
 *
 * @author katou
 */
public class ActivityManagementUtils {
	private static MwaLogger logger = MwaLogger.getLogger(ActivityManagementUtils.class);

	public static OperationResult createGetCartridgeInfoTask(String parentInstanceId, String cartridgeIdList, String providerId, ICoreModules coreModule) {
		Map<String, Object> input = new HashMap<>();
		input.put("ParentInstanceId", parentInstanceId);
		input.put("ProviderId", providerId);
		input.put("serialIdList", cartridgeIdList);
		input.put("Volatile", "false");
		OperationResult operationResult = coreModule.getActivityManager().createInstance("com.sony.pro.mwa.activity.navigator.GetCartridgeInfoTask", "1", input);
		return operationResult;
	}

	public static OperationResult createClientGetCartridgeInfoTask(String parentInstanceId, String cartridgeIdList, String clientProviderId, ICoreModules coreModule) {
		Map<String, Object> input = new HashMap<>();
		input.put("ParentInstanceId", parentInstanceId);
		input.put("clientProviderId", clientProviderId);
		input.put("serialIdList", cartridgeIdList);
		input.put("Volatile", "false");
		OperationResult operationResult = coreModule.getActivityManager().createInstance("com.sony.pro.mwa.activity.navigator.ClientGetCartridgeInfoTask", "1", input);
		return operationResult;
	}

	public static OperationResult createClientFileTransferTask(
			String actionIfExists,
			String clientProviderId,
			String destinations,
			String directFlg,
			String mode,
			String ParentInstanceId,
			String rollback,
			String sources,
			ICoreModules coreModule) {
		Map<String, Object> input = new HashMap<>();
		if (actionIfExists != null) {
			input.put("actionIfExists", actionIfExists);
		}
		input.put("clientProviderId", clientProviderId);
		input.put("destinations", destinations);
		input.put("directFlg", directFlg);
		input.put("mode", mode);
		input.put("ParentInstanceId", ParentInstanceId);
		input.put("rollback", rollback);
		input.put("sources", sources);
		OperationResult operationResult = coreModule.getActivityManager().createInstance("com.sony.pro.mwa.activity.navigator.ClientFileTransferTask", "1", input);
		return operationResult;
	}

	public static OperationResult createClientFileTransferTask(
			String actionIfExists,
			String clientProviderId,
			String destinations,
			String directFlg,
			String mode,
			String ParentInstanceId,
			String rollback,
			String sources,
			String doesExport,
			String partialAssetRegistered,
			ICoreModules coreModule) {
		Map<String, Object> input = new HashMap<>();
		if (actionIfExists != null) {
			input.put("actionIfExists", actionIfExists);
		}
		input.put("clientProviderId", clientProviderId);
		input.put("destinations", destinations);
		input.put("directFlg", directFlg);
		input.put("mode", mode);
		input.put("ParentInstanceId", ParentInstanceId);
		input.put("rollback", rollback);
		input.put("sources", sources);
		input.put("doesExport", doesExport);
		input.put("partialAssetRegistered", partialAssetRegistered);
		OperationResult operationResult = coreModule.getActivityManager().createInstance("com.sony.pro.mwa.activity.navigator.ClientFileTransferTask", "1", input);
		return operationResult;
	}

	public static OperationResult createFileTransferTask(
			String actionIfExists,
			String destinations,
			String mode,
			String ParentInstanceId,
			String rollback,
			String sources,
			ICoreModules coreModule) {
		Map<String, Object> input = new HashMap<>();
		if (actionIfExists != null) {
			input.put("actionIfExists", actionIfExists);
		}
		input.put("destinations", destinations);
		input.put("mode", mode);
		input.put("ParentInstanceId", ParentInstanceId);
		input.put("rollback", rollback);
		input.put("sources", sources);
		OperationResult operationResult = coreModule.getActivityManager().createInstance("com.sony.pro.mwa.activity.nvx.assetcopy.TransferTask", "1", input);
		return operationResult;
	}

	public static OperationResult createFileTransferService(
			String actionIfExists,
			String clientProviderId,
			String destinations,
			String directFlg,
			String mode,
			String ParentInstanceId,
			String rollback,
			String sources,
			ICoreModules coreModule) {
		Map<String, Object> input = new HashMap<>();
		if (actionIfExists != null) {
			input.put("actionIfExists", actionIfExists);
		}
		input.put("clientProviderId", clientProviderId);
		input.put("destinations", destinations);
		input.put("directFlg", directFlg);
		input.put("mode", mode);
		input.put("ParentInstanceId", ParentInstanceId);
		input.put("rollback", rollback);
		input.put("sources", sources);
		OperationResult operationResult = coreModule.getActivityManager().createInstance("com.sony.pro.mwa.wf.transferService", "1", input);
		return operationResult;
	}
}