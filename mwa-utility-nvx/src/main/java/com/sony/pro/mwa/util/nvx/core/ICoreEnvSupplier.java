package com.sony.pro.mwa.util.nvx.core;

import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.service.ICoreModules;

/**
 * ICoreModulesを隠蔽し、MwaUriのパス生成処理を支援するためのインターフェース
 *
 * TODO:仮組。必要なI/Fを別途定義
 */
public interface ICoreEnvSupplier {

	/**
	 * UNC形式への変換を試みる。成功した場合はUNC形式で、失敗した場合は物理形式のパスが返却される
	 * @param mwaUri　変換元のMWA-URI
	 * @return 成功時：UNCパス、失敗時：物理パス
	 */
	public String convertToUNC(String mwaUri);
	/**
	 * 物理形式への変換を試みる。成功した場合はWINDOW形式で、失敗した場合は物理形式のパスが返却される
	 * @param mwaUri　変換元のMWA-URI
	 * @return 成功時：物理パス、失敗時：UNCパス
	 */
	public String convertToPhysical(String mwaUri);
	/**
	 * CoreModule を取得する
	 * ※非推奨：リファクタリングの暫定対処のため追加している
	 * @return
	 */
	@Deprecated
	public ICoreModules getCoreModules();
	/**
	 * ロケーションIdから、ロケーション名を取得する
	 * @param locationId
	 * @return
	 */
	public String getLocationName(String locationId);
	/**
	 * Primary のロケーション名を取得する
	 * @param volumeType
	 * @return
	 */
	public String getPrimaryLocationName(String volumeType);
	/**
	 * Activity Instance の Model を取得する
	 * @param activityInstanceId
	 * @return
	 */
	public ActivityInstanceModel getActivityInstanceModel(String activityInstanceId);
	/**
	 * 指定した Provider 名の ID を取得する
	 * @param providerName
	 * @return
	 */
	public String getProviderId(String providerName);
}
