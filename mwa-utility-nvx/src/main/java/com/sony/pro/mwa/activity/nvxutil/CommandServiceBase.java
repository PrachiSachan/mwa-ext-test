package com.sony.pro.mwa.activity.nvxutil;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import com.sony.pro.mwa.activity.scheme.httpclient.HttpRequestCSRFTokenProviderImpl;
import com.sony.pro.mwa.activity.scheme.httpclient.IHttpRequestConfigProvider;
import com.sony.pro.mwa.activity.scheme.utils.HTTPRequestHeaderProviderFactory;
import com.sony.pro.mwa.activity.scheme.utils.HTTPRequestHeaderSet;
import com.sony.pro.mwa.service.ICoreModules;

/**
 * NavigatorX における Web API の HTTP リクエストに対するサービスの抽象クラスです。
 * エンドポイントを取得する必要があるため、必ず CoreModule を設定する設計とします。
 */
public abstract class CommandServiceBase {
	private ICoreModules coreModules;

	public CommandServiceBase(ICoreModules coreModules) {
		this.coreModules = coreModules;
	}

	public ICoreModules getCoreModules() {
		return coreModules;
	}

	public IHttpRequestConfigProvider getHttpRequestProvider() {

		Header auth = new BasicHeader("Authorization", "Basic bXdhOk13YUFkbTFu");
		Header[] headers = HTTPRequestHeaderProviderFactory.get(HTTPRequestHeaderSet.ApplicationJSON).getHeaders();

		List<Header> buffer = new ArrayList<Header>();
		for (Header header: headers) {
			buffer.add(header);
		}
		buffer.add(auth);
		Header[] defaultHeaders = buffer.toArray(new Header[buffer.size()]);

		return new HttpRequestCSRFTokenProviderImpl(defaultHeaders);
	}

	// 引数なしの getHttpRequestProvider メソッドとほぼ処理が同じなので後々リファクタリングする。 ※ accessKey をAssetCommandServiceクラスで持つ実装にするか等、検討の余地がある。
	public IHttpRequestConfigProvider getHttpRequestProvider(String accessKey) {

		Header auth = new BasicHeader("Authorization", "Basic bXdhOk13YUFkbTFu");
		Header ak = new BasicHeader("accessKey", accessKey);
		Header[] headers = HTTPRequestHeaderProviderFactory.get(HTTPRequestHeaderSet.ApplicationJSON).getHeaders();

		List<Header> buffer = new ArrayList<Header>();
		for (Header header : headers) {
			buffer.add(header);
		}
		buffer.add(auth);
		buffer.add(ak);
		Header[] defaultHeaders = buffer.toArray(new Header[buffer.size()]);

		return new HttpRequestCSRFTokenProviderImpl(defaultHeaders);
	}
}
