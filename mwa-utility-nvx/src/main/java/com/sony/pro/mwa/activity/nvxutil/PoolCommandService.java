package com.sony.pro.mwa.activity.nvxutil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.sony.pro.mwa.activity.scheme.httpclient.HttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.httpclient.HttpResponseResult;
import com.sony.pro.mwa.activity.scheme.httpclient.IHttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.utils.EndpointProvider;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.service.ICoreModules;
import com.sony.pro.nvx.base.dao.jdbc.util.Helper;
import com.sony.pro.nvx.bl.archive.model.PoolModel;

import io.vertx.core.json.JsonObject;

/**
 * NavigatorX における Asset Rules API の HTTP リクエストに対するサービスクラスです。
 */
public class PoolCommandService extends CommandServiceBase {
	private static MwaLogger logger = MwaLogger.getLogger(PoolCommandService.class);
	private final static String BASE_URI = "/nvx/api/v1/pools";

	/**
	 * 引数付きコンストラクタを提供します。
	 *
	 * @param coreModules
	 */
	public PoolCommandService(ICoreModules coreModules) {
		super(coreModules);
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public JsonObject getPool() {
		return getPool(null, null, null, null);
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public JsonObject getPool(List<String> sort, List<String> filter, String offset, String limit) {
		// rule 取得
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), BASE_URI);
		String urlWithQuery = url;

		// TODO: 後々は引数をクエリパラメータ化する必要がある.
		// いまいまは何も渡さない実装にしておくので修正が必要。
		HttpResponseResult httpResponse = new HttpResponseResult();

		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.get(urlWithQuery, this.getHttpRequestProvider());
			logger.debug(httpResponse.getResponseBody());
		} catch (Exception e) {
			String errorMessage = "Failed to get pool from navigatorx.";
			logger.error(errorMessage, e);
		}

		JsonObject jsonModel = null;

		// TODO: レスポンスボディから VolumeCollection にデシリアライズする。
		if (!StringUtils.isEmpty(httpResponse.getResponseBody())) {
			jsonModel = new JsonObject(httpResponse.getResponseBody());
		}

		return jsonModel;
	}

	/**
	 * PoolCollectionのjsonをオブジェクトにパースします。
	 *
	 * @param jsonModel
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PoolModel> deserialize(String jsonModel) {

		JsonObject json = new JsonObject(jsonModel);

		List<Map<String, Object>> modelJsons = json.getJsonArray(Helper.CollectionFieldEnum.models.name()).getList();

		List<PoolModel> poolModelList = new ArrayList<>();
		for (Map<String, Object> modelMap : modelJsons) {

			JsonObject modelJson = new JsonObject(modelMap);

			PoolModel poolModel = new PoolModel(modelJson);
			poolModelList.add(poolModel);

		}

		return poolModelList;

	}
}