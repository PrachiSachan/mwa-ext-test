package com.sony.pro.mwa.activity.nvxutil.essence.validator;

import com.sony.pro.mwa.service.ICoreModules;

import io.vertx.core.json.JsonObject;

public interface IEssenceLocationValidator {
	public boolean validDuplicateArchive(JsonObject assetModel, ICoreModules coreModules, String archivePrimaryMwaPath, String archiveProxyMwaPath);
}
