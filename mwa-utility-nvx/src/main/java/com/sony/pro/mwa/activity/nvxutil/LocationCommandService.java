package com.sony.pro.mwa.activity.nvxutil;

import java.util.List;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.model.storage.LocationCollection;
import com.sony.pro.mwa.service.ICoreModules;

/**
 * NavigatorX における Asset Rules API の HTTP リクエストに対するサービスクラスです。
 */
public class LocationCommandService extends CommandServiceBase {
	private static MwaLogger logger = MwaLogger.getLogger(LocationCommandService.class);
	/**
	 * 引数付きコンストラクタを提供します。
	 *
	 * @param coreModules
	 */
	public LocationCommandService(ICoreModules coreModules) {
		super(coreModules);
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public LocationCollection getLocation() {
		return getLocation(null, null, null, null);
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public LocationCollection getLocation(List<String> sort, List<String> filter, String offset, String limit) {

		LocationCollection collection = this.getCoreModules().getStorageManager().getLocations(null, null, null, null);

		return collection;
	}
}