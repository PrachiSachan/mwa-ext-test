package com.sony.pro.mwa.util.exceptions;

/**
 * NLEExport に関する例外
 *
 * このクラスは、通常のExceptionから派生しています。このユーザーはこの例外をハンドリングし
 * 適宜MwaInstanceErrorを生成して例外を報告してください。
 *
 * TODO：Supplierも兼ねている。。。よろしくない。。。。
 */
public class NLEExportException extends Exception {

	public static final String INVALID_PARAMETER = "The value %s specified for %s is invalid.";
	public static final String FILE_LENGTH_OVER_LIMIT = "File path length is over 259 characters.[ %s ]";
	public static final String DECODE_FAILURE = "Failed to decode. Input value is [%s].";
	public static final String RENAME_FAILURE = "Failed to rename. Input value is [%s].";

	public NLEExportException() {}
	public NLEExportException(String message) { super(message); }
	public NLEExportException(Exception e) { super(e); }
	public NLEExportException(String message, Exception e) { super(message, e); }

	/**
	 * ファイル名の文字列長が制限を超えた場合にスローする例外
	 * @param reason 不正と判断した理由
	 * @return
	 */
	public static NLEExportException pathLengthOverLimit(String reason) {
		return new NLEExportException(String.format(FILE_LENGTH_OVER_LIMIT, reason));
	}

	/**
	 * パラメータ不正
	 * @param name
	 * @param value
	 * @return
	 */
	public static NLEExportException supplyInvalidParameter(String name, String value) {
		return new NLEExportException(String.format(INVALID_PARAMETER, value == null ? "null" : value, name));
	}

	/**
	 * デコードの失敗
	 * @param name
	 * @param value
	 * @return
	 */
	public static NLEExportException decodeFailure(String value) {
		return new NLEExportException(String.format(DECODE_FAILURE, value == null ? "null" : value));
	}

	/**
	 * リネームの失敗
	 * @param name
	 * @param value
	 * @return
	 */
	public static NLEExportException renameFailure(String value) {
		return new NLEExportException(String.format(RENAME_FAILURE, value == null ? "null" : value));
	}
}
