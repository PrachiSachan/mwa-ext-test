package com.sony.pro.mwa.util.exceptions;

/**
 * NLEExport に関する例外
 *
 * このクラスは、通常のExceptionから派生しています。このユーザーはこの例外をハンドリングし
 * 適宜MwaInstanceErrorを生成して例外を報告してください。
  */
public class SubclipException extends Exception {

	public static final String JOB_SELECT_FAILURE = "Failed to select subclip job.";

	public SubclipException() {}
	public SubclipException(String message) { super(message); }
	public SubclipException(Exception e) { super(e); }
	public SubclipException(String message, Exception e) { super(message, e); }

	/**
	 * デコードの失敗
	 * @return
	 */
	public static SubclipException jobSelectFailure() {
		return new SubclipException(JOB_SELECT_FAILURE);
	}
}
