package com.sony.pro.mwa.activity.nvxutil;

import java.util.ArrayList;
import java.util.List;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.model.scheduler.CronTriggerInfoCollection;
import com.sony.pro.mwa.model.scheduler.CronTriggerInfoModel;
import com.sony.pro.mwa.service.ICoreModules;

/**
 * NavigatorX における Asset に関連したユーティリティー機能を提供します。
 */
public class SchedulerUtils {
	private static MwaLogger logger = MwaLogger.getLogger(SchedulerUtils.class);

	public static CronTriggerInfoModel getPlaceVolumeCronSchedulers(ICoreModules core) {
		SchedulerCommandService schedulerCommandService = new SchedulerCommandService(core);
		CronTriggerInfoCollection schedulerCollection = schedulerCommandService.getSchedulers();
		CronTriggerInfoModel result = new CronTriggerInfoModel();

		if (schedulerCollection.getModels().isEmpty()) {
			// schedulerCollection が空の場合、空で返す
		} else {
			List<CronTriggerInfoModel> models = schedulerCollection.getModels();
			for (CronTriggerInfoModel model : models) {
				if (model.getEndpoint().contains("PlaceVolumeManagementTask")) {
					result = model;
				}
			}
		}

		return result;
	}

	public static List<CronTriggerInfoModel> getClientPlaceVolumeCronSchedulers(ICoreModules core) {
		SchedulerCommandService schedulerCommandService = new SchedulerCommandService(core);
		List<CronTriggerInfoCollection> schedulerCollectionList = schedulerCommandService.getClientsSchedulers();
		List<CronTriggerInfoModel> result = new ArrayList<>();

		if (schedulerCollectionList.isEmpty()) {
			logger.info("Client PC scheduler is not found.");
		} else {
			for (CronTriggerInfoCollection schedulerCollection : schedulerCollectionList) {
				List<CronTriggerInfoModel> models = schedulerCollection.getModels();
				for (CronTriggerInfoModel model : models) {
					if (model.getEndpoint().contains("ClientPlaceVolumeManagementTask")) {
						result.add(model);
					}
				}
			}
		}

		return result;
	}

}