package com.sony.pro.mwa.activity.nvxutil;

import java.util.List;

import org.springframework.util.StringUtils;

import com.sony.pro.mwa.activity.scheme.httpclient.HttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.httpclient.HttpResponseResult;
import com.sony.pro.mwa.activity.scheme.httpclient.IHttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.utils.EndpointProvider;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.service.ICoreModules;

import io.vertx.core.json.JsonObject;

/**
 * NavigatorX における Archive Place API の HTTP リクエストに対するサービスクラスです。
 */
public class ArchivePlaceCommandService extends CommandServiceBase {
	private static MwaLogger logger = MwaLogger.getLogger(ArchivePlaceCommandService.class);
	private final static String BASE_URI = "/nvx/api/v1/archive-places";

	/**
	 * 引数付きコンストラクタを提供します。
	 *
	 * @param coreModules
	 */
	public ArchivePlaceCommandService(ICoreModules coreModules) {
		super(coreModules);
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public JsonObject getArchivePlaces() {
		return getArchivePlaces(null, null, null, null);
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public JsonObject getArchivePlaces(List<String> sort, List<String> filter, String offset, String limit) {
		// rule 取得
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), BASE_URI);
		String urlWithQuery = url;

		// TODO: 後々は引数をクエリパラメータ化する必要がある.
		// いまいまは何も渡さない実装にしておくので修正が必要。
		HttpResponseResult httpResponse = new HttpResponseResult();

		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.get(urlWithQuery, this.getHttpRequestProvider());
			logger.debug(httpResponse.getResponseBody());
		} catch (Exception e) {
			String errorMessage = "Failed to get place from navigatorx.";
			logger.error(errorMessage, e);
		}

		JsonObject jsonModel = null;

		// TODO: レスポンスボディから VolumeCollection にデシリアライズする。
		if (!StringUtils.isEmpty(httpResponse.getResponseBody())) {
			jsonModel = new JsonObject(httpResponse.getResponseBody());
		}

		return jsonModel;
	}
}