package com.sony.pro.mwa.activity.nvxutil;

import com.sony.pro.mwa.activity.scheme.httpclient.HttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.httpclient.HttpResponseResult;
import com.sony.pro.mwa.activity.scheme.httpclient.IHttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.utils.EndpointProvider;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * NavigatorX における Asset API の HTTP リクエストに対するサービスクラスです。
 */
public class AssetCommandService extends CommandServiceBase {
	private static MwaLogger logger = MwaLogger.getLogger(AssetCommandService.class);
	// Base URI
	private final static String BASE_URI = "nvx/api/v1";
	// Asset URI
	private final static String GET_URI = "/asset/api/v1/single-assets";
	private final static String POST_URI = BASE_URI + "/bins/%s/assets";
	private final static String DELETE_URI = BASE_URI + "/bins/%s/assets?search=id.raw:%s";
	private final static String ESSENCE_COPY_URI = BASE_URI + "/assets/%s/essences?mode=COPY";
	// Storyboard URI
	private final static String STORYBOARD_URI = BASE_URI + "/storyboards";
	private final static String STORYBOARD_SPECIFIED_URI = STORYBOARD_URI + "/%s";

	/**
	 * 引数付きコンストラクタを提供します。
	 *
	 * @param coreModules
	 */
	public AssetCommandService(ICoreModules coreModules) {
		super(coreModules);
	}

	public JsonObject registerAssetModel(String accessKey, JsonArray assetModelJson, String binId) {
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), BASE_URI);
		String urlWithQuery = url + "/bins/" + binId + "/assets";
		logger.info("AssetCommandService_registerAssetModel_urlWithQuery: " + urlWithQuery);
		HttpResponseResult httpResponse = new HttpResponseResult();
		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			LogUtils.log("AssetCommandService_registerAssetModel_requestBody", assetModelJson.encodePrettily());
			httpResponse = client.post(urlWithQuery, assetModelJson.encodePrettily(), this.getHttpRequestProvider(accessKey));
			LogUtils.log("AssetCommandService_registerAssetModel_responseBody", httpResponse.getResponseBody());
		} catch (Exception e) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to register asset. Connection error occurred.");
		}
		if (httpResponse.getStatusCode() != 200) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to register asset. Response is incorrect from NVX server. binId = " + binId);
		}
		return new JsonObject(httpResponse.getResponseBody());
	}
	
	/**
	 * GET リクエストによるレスポンスを文字列として返却します。
	 * 
	 * @param accessKey
	 * @param assetId
	 * @return
	 */
	public String getAsset(String accessKey, String assetId) {
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), GET_URI);
		String urlWithQuery = url + "/" + assetId;
		logger.info("AssetCommandService_getAssetModel_urlWithQuery: " + urlWithQuery);
		// TODO: 後々は引数をクエリパラメータ化する必要がある.
		// いまいまは何も渡さない実装にしておくので修正が必要。
		HttpResponseResult httpResponse = new HttpResponseResult();
		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.get(urlWithQuery, this.getHttpRequestProvider(accessKey));
			LogUtils.log("AssetCommandService_getAssetModel_responseBody", httpResponse.getResponseBody());
		} catch (Exception e) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, null, "Failed to get asset model from NavigatorX. Connection error occurred.");
		}
		if (httpResponse.getStatusCode() != 200) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, null, "Asset model is not found on NavigatorX. assetId = " + assetId);
		}
		return httpResponse.getResponseBody();
	}

	// Workflow Editor対応中のため、下記のメソッドを対応する時間がなうから一旦残すが削除する予定
	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public JsonObject getAssetModel(String accessKey, String assetId) {
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), GET_URI);
		String urlWithQuery = url + "/" + assetId;
		logger.info("AssetCommandService_getAssetModel_urlWithQuery: " + urlWithQuery);
		// TODO: 後々は引数をクエリパラメータ化する必要がある.
		// いまいまは何も渡さない実装にしておくので修正が必要。
		HttpResponseResult httpResponse = new HttpResponseResult();
		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.get(urlWithQuery, this.getHttpRequestProvider(accessKey));
			LogUtils.log("AssetCommandService_getAssetModel_responseBody", httpResponse.getResponseBody());
		} catch (Exception e) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, null, "Failed to get asset model from NavigatorX. Connection error occurred.");
		}
		if (httpResponse.getStatusCode() != 200) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, null, "Asset model is not found on NavigatorX. assetId = " + assetId);
		}
		return new JsonObject(httpResponse.getResponseBody());
	}

	public JsonObject postAssetModel(String accessKey, String destinationBinId, String requestBody) {
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), POST_URI);
		String urlWithQuery = String.format(url, destinationBinId) + "?mode=SHORTCUT";
		logger.info("AssetCommandService_postAssetModel_urlWithQuery: " + urlWithQuery);
		// TODO: 後々は引数をクエリパラメータ化する必要がある.
		// いまいまは何も渡さない実装にしておくので修正が必要。
		HttpResponseResult httpResponse = new HttpResponseResult();
		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			LogUtils.log("AssetCommandService_postAssetModel_requestBody", requestBody);
			httpResponse = client.post(urlWithQuery, requestBody, this.getHttpRequestProvider(accessKey));
			LogUtils.log("AssetCommandService_postAssetModel_responseBody", httpResponse.getResponseBody());
		} catch (Exception e) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to post asset. Connection error occurred.");
		}
		if (httpResponse.getStatusCode() != 200) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to post asset. Response is incorrect from NVX server. destinationBinId = " + destinationBinId);
		}
		return new JsonObject(httpResponse.getResponseBody());
	}

	public JsonObject deleteAssetModel(String accessKey, String assetId, String binId) {
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), DELETE_URI);
		String urlWithQuery = String.format(url, binId, "%22" + assetId + "%22");
		logger.info("AssetCommandService_deleteAssetModel_urlWithQuery: " + urlWithQuery);
		// TODO: 後々は引数をクエリパラメータ化する必要がある.
		// いまいまは何も渡さない実装にしておくので修正が必要。
		HttpResponseResult httpResponse = new HttpResponseResult();
		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.delete(urlWithQuery, "", this.getHttpRequestProvider(accessKey));
			LogUtils.log("AssetCommandService_deleteAssetModel_responseBody", httpResponse.getResponseBody());
		} catch (Exception e) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to delete asset. Connection error occurred.");
		}
		if (httpResponse.getStatusCode() != 200) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to delete asset. Response is incorrect from NVX server. assetId = " + assetId);
		}
		return new JsonObject(httpResponse.getResponseBody());
	}

	public JsonObject updateAssetModel(JsonObject assetModelJson) {
		return null;
	}

	public JsonObject getStoryboard(String accessKey, String collectionId) {
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), STORYBOARD_SPECIFIED_URI);
		String urlWithQuery = String.format(url, collectionId);
		logger.info("AssetCommandService_getStoryboard_urlWithQuery: " + urlWithQuery);
		HttpResponseResult httpResponse = new HttpResponseResult();
		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.get(urlWithQuery, this.getHttpRequestProvider(accessKey));
			LogUtils.log("AssetCommandService_getStoryboard_responseBody", httpResponse.getResponseBody());
		} catch (Exception e) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to get storyboard. Connection error occurred.");
		}
		if (httpResponse.getStatusCode() != 200) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to get storyboard. Response is incorrect from NVX server.");
		}
		return new JsonObject(httpResponse.getResponseBody());
	}

	public JsonObject patchStoryboard(String accessKey, JsonObject requestBody, String collectionId) {
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), STORYBOARD_SPECIFIED_URI);
		String urlWithQuery = String.format(url, collectionId);
		logger.info("AssetCommandService_getStoryboard_urlWithQuery: " + urlWithQuery);
		HttpResponseResult httpResponse = new HttpResponseResult();
		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			LogUtils.log("AssetCommandService_patchStoryboard_requestBody", requestBody.encodePrettily());
			httpResponse = client.patch(urlWithQuery, requestBody.encodePrettily(), this.getHttpRequestProvider(accessKey));
			LogUtils.log("AssetCommandService_getStoryboard_responseBody", httpResponse.getResponseBody());
		} catch (Exception e) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to patch storyboard. Connection error occurred.");
		}
		if (httpResponse.getStatusCode() != 200) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to patch storyboard. Response is incorrect from NVX server.");
		}
		return new JsonObject(httpResponse.getResponseBody());
	}

	public JsonObject copyEssence(String accessKey, JsonArray requestBody, String assetId) {
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), ESSENCE_COPY_URI);
		String urlWithQuery = String.format(url, assetId);
		logger.info("AssetCommandService_copyEssence_urlWithQuery: " + urlWithQuery);
		HttpResponseResult httpResponse = new HttpResponseResult();
		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			LogUtils.log("AssetCommandService_copyEssence_requestBody", requestBody.encodePrettily());
			httpResponse = client.post(urlWithQuery, requestBody.encodePrettily(), this.getHttpRequestProvider(accessKey));
			LogUtils.log("AssetCommandService_copyEssence_responseBody", httpResponse.getResponseBody());
		} catch (Exception e) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to copy essence. Connection error occurred.");
		}
		if (httpResponse.getStatusCode() != 200) {
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, "Failed to copy essence. Response is incorrect from NVX server. assetId = " + assetId);
		}
		return new JsonObject(httpResponse.getResponseBody());
	}
}