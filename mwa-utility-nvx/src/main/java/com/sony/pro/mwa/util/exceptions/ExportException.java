package com.sony.pro.mwa.util.exceptions;

/**
 * NLEExport に関する例外
 *
 * このクラスは、通常のExceptionから派生しています。このユーザーはこの例外をハンドリングし
 * 適宜MwaInstanceErrorを生成して例外を報告してください。
 *
 * TODO：Supplierも兼ねている。。。よろしくない。。。。
 */
public class ExportException extends Exception {

	public static final String INVALID_PARAMETER = "The value %s specified for %s is invalid.";
	public static final String DECODE_FAILURE = "Failed to decode. Input value is [%s].";

	public ExportException() {}
	public ExportException(String message) { super(message); }
	public ExportException(Exception e) { super(e); }
	public ExportException(String message, Exception e) { super(message, e); }

	/**
	 * パラメータ不正
	 * @param name
	 * @param value
	 * @return
	 */
	public static ExportException supplyInvalidParameter(String name, String value) {
		return new ExportException(String.format(INVALID_PARAMETER, value == null ? "null" : value, name));
	}

	/**
	 * デコードの失敗
	 * @param value
	 * @return
	 */
	public static ExportException decodeFailure(String value) {
		return new ExportException(String.format(DECODE_FAILURE, value == null ? "null" : value));
	}
}
