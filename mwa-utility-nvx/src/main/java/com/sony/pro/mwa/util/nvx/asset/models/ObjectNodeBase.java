package com.sony.pro.mwa.util.nvx.asset.models;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sony.pro.mwa.util.nvx.asset.models.exceptions.AssetModelsException;

/**
 * Object 型のJsonNode を定義する基底クラス
 * 
 * @author Kousuke Hosaka
 *
 */
public class ObjectNodeBase {
	protected ObjectMapper mapper = new ObjectMapper();
	protected JsonNode jsonNode = null;

	//★ こちらですが、一旦廃止させてください。
	// models下のクラスで扱うノードの全てがObjectNodeではないため、これがあると利用箇所が限定されてしまいます。
	// ObjectとArrayでクラスを分けるなどの工夫が必要かも知れません。
	// protected ObjectNode objectjNode = null;
	
	/**
	 * Assetのように、idを指定してもmodels配列として来てしまうようなケースでは
	 * コンストラクタにJsonNodeを渡すだけでは処理がたりない。
	 * 
	 * 派生クラスの方で、基底コンストラクタを呼ばなくでもコンストラクタを記述することが出来
	 * 然るべきノードを検索してjsonNodeを差し替えられるようにしておきたいので、protectedでデフォルトコンストラクタを切ります。
	 */
	protected ObjectNodeBase() {
		this.jsonNode = mapper.createObjectNode();
		// this.jsonNode = this.objectjNode;
		// this.jsonNode = JsonNodeFactory.instance.objectNode();
		// this.objectjNode = (ObjectNode) this.jsonNode;
	}

	/**
	 * このクラスで処理するJsonNodeを差し替える
	 * @param node
	 * @throws AssetModelsException 
	 */
	protected void setNode(JsonNode node) throws AssetModelsException {
		if (!node.isObject()) {
			throw AssetModelsException.supplyJsonIsInvalid("node is Object.");
		}
		this.jsonNode = node;
		// this.objectjNode = (ObjectNode) node;
	}
	
	/**
	 * このクラスで処理するJsonNodeを差し替える
	 * @param jsonText
	 */
	protected void setNode(String jsonText) throws AssetModelsException {
		try {
			ObjectMapper mapper = new ObjectMapper();
			this.setNode(mapper.readValue(jsonText, JsonNode.class));
		} catch (Exception e) {
			throw AssetModelsException.supplyJsonParseOrMappingFailed(jsonText, e);
		}		
	}
	
	/**
	 * JSON文字列を指定するコンストラクタ
	 * @param jsonText Assetデータに相当するJSON文字列
	 * @throws AssetModelsException パースまたはマッピングエラーが発生した場合にスローされる
	 */
	public ObjectNodeBase(String jsonText) throws AssetModelsException {
		this.setNode(jsonText);
	}
	
	/**
	 * コンストラクタ
	 * @param jsonNode
	 */
	public ObjectNodeBase (JsonNode jsonNode) {
		this.jsonNode = jsonNode;
		// this.objectjNode = (ObjectNode) this.jsonNode;
	}
	
	/**
	 * 指定のKey からJsonNodeでValue を返却します。
	 * @param key
	 * @return
	 */
	public JsonNode getNodeValue(String key) {
		JsonNode nodeValue = this.jsonNode.get(key);
		// TODO : nullチェック
		// TODO : requiredチェック
		return nodeValue;
	}
	
	/**
	 * keyで指定されたノードを取得する。getNodeValueと異なり、孫階層も合わせて検索する
	 * @param key 取得したいノードのキー名
	 * @return 取得したいノード
	 */
	public JsonNode find(String key) {
		return this.jsonNode.findPath(key);
	}
	
	/**
	 * 文字列型のValue をJsonNode に書き込みます。
	 * @param key
	 * @param value
	 */
	public void setNodeValue(String key, String value) {
		// this.objectjNode.put(key, value);
		((ObjectNode) this.jsonNode).put(key, value);
	}
	
	/**
	 * int 型のValue をJsonNode に書き込みます。
	 * @param key
	 * @param value
	 */
	public void setNodeValue(String key, int value) {
		// this.objectjNode.put(key, value);
		((ObjectNode) this.jsonNode).put(key, value);
	}
	
	/**
	 * long 型のValue をJsonNode に書き込みます。
	 * @param key
	 * @param value
	 */
	public void setNodeValue(String key, long value) {
		// this.objectjNode.put(key, value);
		((ObjectNode) this.jsonNode).put(key, value);
	}
	
	/**
	 * double 型のValue をJsonNode　に書き込みます。
	 * @param key
	 * @param value
	 */
	public void setNodeValue(String key, double value) {
		// this.objectjNode.put(key, value);
		((ObjectNode) this.jsonNode).put(key, value);
	}
	
	/**
	 * boolean 型のValue をJsonNode に書き込みます。
	 * @param key
	 * @param value
	 */
	public void setNodeValue(String key, boolean value) {
		// this.objectjNode.put(key, value);
		((ObjectNode) this.jsonNode).put(key, value);
	}
	
	/**
	 * JsonNode 型のValue をJsonNode　に書き込みます。
	 * @param key
	 * @param nodeValue
	 */
	public void setNodeValue(String key, JsonNode nodeValue) {
		// this.objectjNode.set(key, nodeValue);
		((ObjectNode) this.jsonNode).set(key, nodeValue);
	}
	
	public void setObjectNode(String rootKey, Object object) throws AssetModelsException {
		JsonNode node = this.convertObjectToJsonNode(object);
		((ObjectNode) this.jsonNode).set(rootKey, node);
	}
	
	/**
	 * 複数のvalue を持つJsonNode をList<String> に変換します。
	 * @param nodeValues
	 * @return
	 * @throws JsonProcessingException 
	 */
	public List<String> convertJsonNodeToListString(JsonNode nodeValues) {
		List<String> valueList = new ArrayList<>();
		if (nodeValues != null) {
			for (JsonNode value : nodeValues) {
				valueList.add(value.asText());
			}
		}
		return valueList;
	}
	
	/**
	 * 複数のvalue を持つJsonNode をList<Integer> に変換します。
	 * @param nodeValues
	 * @return
	 */
	public List<Integer> convertJsonNodeToListInteger(JsonNode nodeValues) {
		List<Integer> valueList = new ArrayList<>();
		if (nodeValues != null) {
			for (JsonNode value : nodeValues) {
				valueList.add(value.asInt());
			}
		}
		return valueList;
	}
	
	/**
	 * List<?> をJsonNode に変換します。
	 * @param valueList
	 * @return
	 * @throws AssetModelsException 
	 */
	public JsonNode convertListToJsonNode(List<?> valueList) throws AssetModelsException {
		ObjectMapper objMapper = new ObjectMapper();
		JsonNode nodeValues = null;
		try {
			String listString = objMapper.writeValueAsString(valueList);
			nodeValues = objMapper.readTree(listString);	
		} catch (Exception e) {
			throw AssetModelsException.supplyJsonIsInvalid("Failed to mapping JsonNode.");
		}
        return nodeValues;	 
	}
	
	/**
	 * Object をJsonNode に変換します。
	 * @param object
	 * @return
	 * @throws AssetModelsException
	 */
	public JsonNode convertObjectToJsonNode(Object object) throws AssetModelsException {
		JsonNode nodeValue = null;
		try {
			String objectStr = mapper.writeValueAsString(object);
			// System.out.println(objectStr);
			nodeValue = mapper.readTree(objectStr);	
		} catch (Exception e) {
			e.printStackTrace();
			throw AssetModelsException.supplyJsonIsInvalid("Failed to mapping JsonNode.");
		}
		return nodeValue;
	}
}
