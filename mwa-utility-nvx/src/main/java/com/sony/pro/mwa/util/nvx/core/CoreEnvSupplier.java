package com.sony.pro.mwa.util.nvx.core;

import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.model.provider.ActivityProviderCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.util.enumeration.VolumeTypeEnum;
import org.apache.commons.lang.StringUtils;

import com.sony.pro.mwa.activity.nvxutil.EssenceLocationHelper;
import com.sony.pro.mwa.activity.nvxutil.EssenceLocationHelper.PrimaryVolumeResponse;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;

public class CoreEnvSupplier implements ICoreEnvSupplier {

	public static String[] UNC_FIRST = new String[] {"UNC", "UNC_FORWARD_SLASH","WINDOWS", "WINDOWS_FORWARD_SLASH"};
	public static String[] PHYSICAL_FIRST = new String[] {"WINDOWS", "WINDOWS_FORWARD_SLASH", "UNC", "UNC_FORWARD_SLASH"};

	private ICoreModules coreModule;

	public CoreEnvSupplier(ICoreModules coreModule) {
		this.coreModule = coreModule;
	}

	@Override
	public String convertToUNC(String mwaUri) {
		return this.coreModule.getUriResolver().asNativePath(mwaUri, UNC_FIRST).getFirstPath();
	}
	@Override
	public String convertToPhysical(String mwaUri) {
		return this.coreModule.getUriResolver().asNativePath(mwaUri, PHYSICAL_FIRST).getFirstPath();
	}
	@Override
	@Deprecated
	public ICoreModules getCoreModules() {
		return coreModule;
	}
	@Override
	public String getLocationName(String locationId) {
		return this.coreModule.getStorageManager().getLocation(locationId).getName();
	}
	@Override
	public String getPrimaryLocationName(String volumeType) {
		PrimaryVolumeResponse volumeInfo = getVolumeInfo();
		String locationName = null;
		if (volumeType.equals(VolumeTypeEnum.MASTER.toString()) && !StringUtils.isEmpty(volumeInfo.masterVolumeName)) {
			locationName = volumeInfo.masterVolumeName;
		} else if (volumeType.equals(VolumeTypeEnum.PROXY.toString()) && !StringUtils.isEmpty(volumeInfo.proxyVolumeName)) {
			locationName = volumeInfo.proxyVolumeName;
		} else {
			throw new MwaError(MWARC.OPERATION_FAILED, null, "Failed to get location name.");
		}
		return locationName;
	}
	private PrimaryVolumeResponse getVolumeInfo() {
		PrimaryVolumeResponse volumeInfo = EssenceLocationHelper.getPrimaryVolumeInfo(this.coreModule);
		if (volumeInfo == null) {
			throw new MwaError(MWARC.OPERATION_FAILED, null, "Failed to get volume model.");
		}
		return volumeInfo;
	}
	@Override
	public ActivityInstanceModel getActivityInstanceModel(String activityInstanceId) {
		ActivityInstanceModel activityInstanceModel = this.coreModule.getActivityManager().getInstance(activityInstanceId);
		if (activityInstanceModel == null) {
			throw new MwaError(MWARC.INVALID_INPUT_RESOURCE_NOT_FOUND);
		}
		return activityInstanceModel;
	}
	@Override
	public String getProviderId(String providerName) {
		String providerId = null;
		ActivityProviderCollection providerCollection = coreModule.getActivityProviderManager().getProviders(null, null, null, null);
		for (ActivityProviderModel providerModel : providerCollection.getModels()) {
			if (providerModel.getName().equals(providerName)) {
				providerId = providerModel.getId();
				break;
			}
		}
		return providerId;
	}
}
