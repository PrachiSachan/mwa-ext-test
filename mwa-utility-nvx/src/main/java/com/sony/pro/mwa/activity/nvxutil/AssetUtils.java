package com.sony.pro.mwa.activity.nvxutil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * NavigatorX における Asset に関連したユーティリティー機能を提供します。
 */
public class AssetUtils {

	private static MwaLogger logger = MwaLogger.getLogger(AssetUtils.class);

	private static final String JSON_ARRAY_FORMAT = "[{\"id\": \"%s\"}]";
	
	public static JsonObject registerAssetModelJson(JsonArray assetModelJson, String binId, ICoreModules core) {
		String accessKey = getAccessKey(core);
		AssetCommandService assetCommandService = new AssetCommandService(core);
		JsonObject response = assetCommandService.registerAssetModel(accessKey, assetModelJson, binId);
		return response.getJsonArray("models").getJsonObject(0);
	}

	public static String getAsset(String assetId, ICoreModules core) {
		String accessKey = getAccessKey(core);
		AssetCommandService assetCommandService = new AssetCommandService(core);
		return assetCommandService.getAsset(accessKey, assetId);
	}
	
	// Workflow Editor対応中のため、下記のメソッドを対応する時間がなうから一旦残すが削除する予定
	public static JsonObject getAssetModelJson(String assetId, ICoreModules core) {
		String accessKey = getAccessKey(core);
		AssetCommandService assetCommandService = new AssetCommandService(core);
		return assetCommandService.getAssetModel(accessKey, assetId);
	}

	public static void deleteAssetModel(String assetId, String binId, ICoreModules core) {
		String accessKey = getAccessKey(core);
		AssetCommandService assetCommandService = new AssetCommandService(core);
		assetCommandService.deleteAssetModel(accessKey, assetId, binId);
	}

	public static List<String> getCartrigdeIdsFromAssetModel(String assetId, ICoreModules core) {
		String accessKey = getAccessKey(core);

		// ※リファクタリング対象、JsonObjectではなくAssetModelで取れるようにする
		AssetCommandService assetCommand = new AssetCommandService(core);
		JsonObject assetModelJson = assetCommand.getAssetModel(accessKey, assetId);
		if (assetModelJson == null) {
			return null;
		}

		JsonArray essenceModelsJsonArray = assetModelJson.getJsonArray("essences");
		List<JsonObject> essenceModels = new ArrayList<JsonObject>();
		Iterator<Object> eitr = essenceModelsJsonArray.iterator();
		while (eitr.hasNext()) {
			essenceModels.add((JsonObject) eitr.next());
		}

		List<String> cartridgeIdList = new ArrayList<>();
		for (JsonObject essenceModel : essenceModels) {
			if (essenceModel.getBoolean("primary")) {
				String location = (essenceModel.getString("location"));
				LogUtils.log("AssetUtils_getCartrigdeIdsFromAssetModel_location", location);
				String cartridgeId = EssenceLocationHelper.getCartridgeId(location, true);
				if (StringUtils.isEmpty(cartridgeId)) {
					continue;
				}
				cartridgeIdList.add(cartridgeId);
			}
		}

		return cartridgeIdList;
	}

	public static String getAssetType(JsonObject assetModel) {
		// assetModelにassetInfoがない場合のチェック
		if (!(assetModel.containsKey("assetInfo"))) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, null, "assetInfo is Empty");
		// assetInfoにtypeがない場合、またはnull, 空の場合のチェック
		} else if (!(assetModel.getJsonObject("assetInfo").containsKey("type"))
				|| StringUtils.isEmpty(assetModel.getJsonObject("assetInfo").getString("type"))) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, null, "assetType is null or empty");
		}
		return assetModel.getJsonObject("assetInfo").getString("type");
	}

	private static String getAccessKey(ICoreModules core) {
		UserCommandService userCommandService = new UserCommandService(core);
		return userCommandService.loginToAmsServer();
	}

	public static List<String> getLocationPaths(JsonObject assetModel) {
		List<String> locationPathList = new ArrayList<String>();
		for (int i = 0; i < assetModel.getJsonArray("essences").size(); i++) {
			String location = assetModel.getJsonArray("essences").getJsonObject(i).getString("location");
			if (StringUtils.isEmpty(location)) {
				continue;
			}
			locationPathList.add(location);
		}
		for (int i = 0; i < assetModel.getJsonArray("events").size(); i++) {
			String imageLocation = assetModel.getJsonArray("events").getJsonObject(i).getString("imageLocation");
			if (StringUtils.isEmpty(imageLocation) || !EssenceLocationHelper.isMwaLocationPath(imageLocation)) {
				continue;
			}
			locationPathList.add(imageLocation);
		}
		LogUtils.log("AssetUtils_getLocationPaths_locationPathList", locationPathList);
		return locationPathList;
	}

	public static List<String> getOnlineLocationPaths(JsonObject assetModel) {
		List<String> locationPathList = new ArrayList<String>();
		for (int i = 0; i < assetModel.getJsonArray("essences").size(); i++) {
			if ("offline".equalsIgnoreCase(assetModel.getJsonArray("essences").getJsonObject(i).getString("status"))) {
				continue;
			}
			String location = assetModel.getJsonArray("essences").getJsonObject(i).getString("location");
			if (StringUtils.isEmpty(location)) {
				continue;
			}
			locationPathList.add(location);
		}
		for (int i = 0; i < assetModel.getJsonArray("events").size(); i++) {
			String imageLocation = assetModel.getJsonArray("events").getJsonObject(i).getString("imageLocation");
			if (StringUtils.isEmpty(imageLocation) || !EssenceLocationHelper.isMwaLocationPath(imageLocation)) {
				continue;
			}
			locationPathList.add(imageLocation);
		}
		LogUtils.log("AssetUtils_getOnlineLocationPaths_locationPathList", locationPathList);
		return locationPathList;
	}

	public static List<String> getOnlineLocationPathsWithoutSubtitle(JsonObject assetModel) {
		List<String> locationPathList = new ArrayList<String>();
		for (int i = 0; i < assetModel.getJsonArray("essences").size(); i++) {
			if ("offline".equalsIgnoreCase(assetModel.getJsonArray("essences").getJsonObject(i).getString("status"))
					|| "SUBTITLE".equals(assetModel.getJsonArray("essences").getJsonObject(i).getString("type"))) {
				continue;
			}
			String location = assetModel.getJsonArray("essences").getJsonObject(i).getString("location");
			if (StringUtils.isEmpty(location)) {
				continue;
			}
			locationPathList.add(location);
		}
		for (int i = 0; i < assetModel.getJsonArray("events").size(); i++) {
			String imageLocation = assetModel.getJsonArray("events").getJsonObject(i).getString("imageLocation");
			if (StringUtils.isEmpty(imageLocation) || !EssenceLocationHelper.isMwaLocationPath(imageLocation)) {
				continue;
			}
			locationPathList.add(imageLocation);
		}
		LogUtils.log("AssetUtils_getOnlineLocationPaths_locationPathList", locationPathList);
		return locationPathList;
	}

	public static Map<String, String> getLocationWithMasterProxyFlag(JsonObject assetModel) {
		Map<String, String> result = new HashMap<>();
		for (int i = 0; i < assetModel.getJsonArray("essences").size(); i++) {
			JsonObject essence = assetModel.getJsonArray("essences").getJsonObject(i);
			if ("offline".equalsIgnoreCase(essence.getString("status"))) {
				continue;
			}
			String type = essence.getString("type");
			String location = essence.getString("location");
			if (StringUtils.isEmpty(location)) {
				continue;
			}
			if (essence.getBoolean("proxy") && ("VIDEO".equals(type) || "AUDIO_VIDEO".equals(type))){
				result.put(location, "PROXY");
			} else {
				result.put(location, "MASTER");
			}
		}
		for (int i = 0; i < assetModel.getJsonArray("events").size(); i++) {
			String imageLocation = assetModel.getJsonArray("events").getJsonObject(i).getString("imageLocation");
			if (StringUtils.isEmpty(imageLocation) || !EssenceLocationHelper.isMwaLocationPath(imageLocation)) {
				continue;
			}
			result.put(imageLocation, "MASTER");
		}
		LogUtils.logForString("AssetUtils_getLocationWithMasterProxyFlag_result", result);
		return result;
	}

	public static String getAssetId(JsonObject assetModel) {
		return assetModel.getString("associateId");
	}

	public static JsonObject getStoryboard(String collectionId, ICoreModules core) {
		String accessKey = getAccessKey(core);
		AssetCommandService assetCommandService = new AssetCommandService(core);
		JsonObject storyboard = assetCommandService.getStoryboard(accessKey, collectionId);
		return storyboard.getJsonArray("models").getJsonObject(0);
	}

	public static JsonObject patchStoryboard(JsonObject requestBody, String collectionId, ICoreModules core) {
		String accessKey = getAccessKey(core);
		AssetCommandService assetCommandService = new AssetCommandService(core);
		return assetCommandService.patchStoryboard(accessKey, requestBody, collectionId);
	}

	public static JsonObject postAssetModel(String destinationBinId, String assetId, ICoreModules core) {
		String accessKey = getAccessKey(core);
		AssetCommandService assetCommandService = new AssetCommandService(core);
		return assetCommandService.postAssetModel(accessKey, destinationBinId, String.format(JSON_ARRAY_FORMAT, assetId));
	}

	public static String getStatus(JsonObject assetModel) {
		// assetModelにassetInfoがない場合のチェック
		if (!(assetModel.containsKey("assetInfo"))) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, null, "assetInfo is Empty");
			// assetInfoにstatusがない場合、またはnull, 空の場合のチェック
			} else if (!(assetModel.getJsonObject("assetInfo").containsKey("status"))
					|| StringUtils.isEmpty(assetModel.getJsonObject("assetInfo").getString("status"))) {
			throw new MwaInstanceError(MWARC.INVALID_INPUT, null, "Status is null or empty");
		}
		return assetModel.getJsonObject("assetInfo").getString("status");
	}

	public static JsonObject copyEssence(JsonArray requestBody, String assetId, ICoreModules core) {
		String accessKey = getAccessKey(core);
		AssetCommandService assetCommandService = new AssetCommandService(core);
		return assetCommandService.copyEssence(accessKey, requestBody, assetId);
	}

}