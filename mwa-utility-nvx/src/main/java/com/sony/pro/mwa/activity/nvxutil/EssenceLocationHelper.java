package com.sony.pro.mwa.activity.nvxutil;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.sony.pro.mwa.util.exceptions.MwaUriException;
import com.sony.pro.mwa.util.uri.MwaUri;
import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;
import com.sony.pro.mwa.utils.UriUtils;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * NavigatorX における Essence 内の Location(MWA Path) に関連するユーティリティー機能を提供します。
 */
public class EssenceLocationHelper {
	private static MwaLogger logger = MwaLogger.getLogger(EssenceUtils.class);

	/**
	 * 拡張子付で MP4 プロキシファイル名の Suffix 文字列を定義します。
	 */
	public static final String PROXY_FILE_SUFFIX_WITH_EXTENSION = "_proxy.mp4";

	/**
	 * 拡張子付で SRT 用 VTT プロキシファイル名の Suffix 文字列を定義します。
	 */
	public static final String SRT_FILE_SUFFIX_WITH_VTT_EXTENSION = "_convert.vtt";

	private static final String CARTRIDGE_ID_FORMAT = "?cartridgeId=";

	private static final String DRIVE_LETTER_FORMAT = "&amp;drive=";

	// -------------------------------------------------------------
	// MWA Location Path に対するユーティリティ―メソッド群を提供します。
	// -------------------------------------------------------------

	/**
	 * 指定された MWA パスをもとに、LocationName 部分を置換した文字列を返却します。
	 *
	 * @param sourceMwaLocationPath 置換前のロケーションパス (MWA パス）
	 * @param newLocationName 置換したい Location Name
	 * @return
	 */
	public static String replaceLocationName(String sourceMwaLocationPath, String newLocationName) {
		String locationName = getLocationName(sourceMwaLocationPath);
		LogUtils.log("EssenceLocationHelper_replaceLocationName_locationName", locationName);
		String result = sourceMwaLocationPath.replaceFirst(locationName, newLocationName);
		return result;
	}

	/**
	 * 指定された MWA パスをもとに、LocationName 部分を返却します。
	 *
	 * @param sourceMwaLocationPath 探索するロケーションパス (MWA パス）
	 * @return
	 */
	public static String getLocationName(String sourceMwaLocationPath) {
		final String MWA_PATH_PREFIX = "mwa://";
		int locationLastCharIndex = sourceMwaLocationPath.indexOf("/", MWA_PATH_PREFIX.length());
		String locationName = sourceMwaLocationPath.substring(MWA_PATH_PREFIX.length(), locationLastCharIndex);
		LogUtils.log("EssenceLocationHelper_getLocationName_locationName", locationName);
		return locationName;
	}

	/**
	 * 指定された MWA パスをもとに、fileName 部分を返却します。
	 *
	 * @param sourceMwaLocationPath
	 *            探索するロケーションパス (MWA パス）
	 * @return
	 */
	public static String getFileName(String sourceMwaLocationPath) {
		LogUtils.log("EssenceLocationHelper_getFileName_sourceMwaLocationPath", sourceMwaLocationPath);
		String fileName = null;
		int fileNameStartCharIndex = sourceMwaLocationPath.lastIndexOf("/");
		if (sourceMwaLocationPath.contains("?")) {
			int fileNameLastCharIndex = sourceMwaLocationPath.lastIndexOf("?");
			fileName = sourceMwaLocationPath.substring(fileNameStartCharIndex + 1, fileNameLastCharIndex);
		} else {
			fileName = sourceMwaLocationPath.substring(fileNameStartCharIndex + 1, sourceMwaLocationPath.length());
		}

		return fileName;
	}

	/**
	 * 指定された MWA パスをもとに、assetId 部分を返却します。
	 *
	 * @param sourceMwaLocationPath
	 *            探索するロケーションパス (MWA パス）
	 * @return
	 */
	public static String getAssetId(String sourceMwaLocationPath) {
		String assetId = null;
		int assetIdLastCharIndex = sourceMwaLocationPath.lastIndexOf("/");
		String fileDirectoryPath = sourceMwaLocationPath.substring(0, assetIdLastCharIndex);
		LogUtils.log("EssenceLocationHelper_getAssetId_fileDirectoryPath", fileDirectoryPath);

		if (fileDirectoryPath.endsWith("/")) {
			fileDirectoryPath = fileDirectoryPath.substring(0, fileDirectoryPath.length() - 1);
		}

		int assetIdStartCharIndex = fileDirectoryPath.lastIndexOf("/") + 1;
		assetId = sourceMwaLocationPath.substring(assetIdStartCharIndex, assetIdLastCharIndex);

		return assetId;
	}

	/**
	 * 指定された MWA パスをもとに、MXF ファイル名部分を VTT ファイル名に変換した文字列を返却します。
	 *
	 * ファイル名を元に SUFFIX を付けたいときしか使わないこと
	 *
	 * @param srtMwaPath
	 * @return
	 */
	public static String changeFileNameToConvertVTT(String srtMwaPath) {
		return srtMwaPath.substring(0, srtMwaPath.lastIndexOf('.')) + SRT_FILE_SUFFIX_WITH_VTT_EXTENSION;
	}

	/**
	 * 指定された MWA パスをもとに、MXF ファイル名部分を MP4 プロキシファイル名に変換した文字列を返却します。
	 *
	 * ファイル名を元に SUFFIX を付けたいときしか使わないこと
	 *
	 * @param mxfMwaPath
	 * @return
	 */
	public static String changeFileNameToProxyMP4(String mxfMwaPath) {
		return mxfMwaPath.substring(0, mxfMwaPath.lastIndexOf('.')) + PROXY_FILE_SUFFIX_WITH_EXTENSION;
	}

	/**
	 * UUID(EssenceId) を AssetId と FileName の間に挿入します。
	 * ex) "mwa://【location】/【assetId】/【fileName】"  ->  "mwa://【location】/【assetId】/【essenceId】/【fileName】"
	 *
	 * @param mwaFilePath
	 * @return
	 */
	public static String insertEssenceIdBetweenAssetIdAndFileName(String mwaFilePath, String essenceId) {
		int directoryLastSlashIndex = mwaFilePath.lastIndexOf("/");
		LogUtils.log("EssenceLocationHelper_insertEssenceIdBetweenAssetIdAndFileName_mwaFilePath", mwaFilePath);
		String directoryPath = mwaFilePath.substring(0, directoryLastSlashIndex);
		LogUtils.log("EssenceLocationHelper_insertEssenceIdBetweenAssetIdAndFileName_directoryPath", directoryPath);

		int fileNameStartIndex = directoryLastSlashIndex + 1;
		String fileName = mwaFilePath.substring(fileNameStartIndex);

		String result = String.format("%s/%s/%s", directoryPath, essenceId, fileName);
		LogUtils.log("EssenceLocationHelper_insertEssenceIdBetweenAssetIdAndFileName_result", result);

		return result;
	}

	/**
	 * MXF ファイル用に VTT ファイルパスを作成する
	 *
	 * @param locationName
	 * @param assetId
	 * @param essenceId
	 * @return
	 */
	public static String formatTimecodeVTTMwaPathForMXF(String locationName, String assetId, String essenceId) {
		String result = String.format("mwa://%s/%s/%s/timecode.vtt", locationName, assetId, essenceId);
		LogUtils.log("EssenceLocationHelper_formatTimecodeVTTMwaPathForMXF_result", result);
		return result;
	}

	/**
	 * MXF ファイル用に VTT ファイルパスを作成する
	 *
	 * @param locationName
	 * @param assetId
	 * @param essenceId
	 * @return
	 */
	public static String formatThumbnailMwaPathForMXF(String locationName, String assetId, String essenceId) {
		String result = String.format("mwa://%s/%s/%s/image.jpg", locationName, assetId, essenceId);
		LogUtils.log("EssenceLocationHelper_formatThumbnailMwaPathForMXF_result", result);
		return result;
	}

	/**
	 * MXF ファイル用にパスを作成する
	 *
	 * @param locationName
	 * @param assetId
	 * @param title
	 * @return
	 */
	public static String formatMXFMwaPath(String locationName, String assetId, String title) {
		String result = String.format("mwa://%s/%s/%s.mxf", locationName, assetId, title);
		LogUtils.log("EssenceLocationHelper_formatMXFMwaPath_result", result);
		return result;
	}

	/**
	 * AssetID 直下にファイル名をおいて、MWA ファイルパスを作成する
	 *
	 * @param locationName
	 * @param assetId
	 * @param fileName
	 * @return
	 */
	public static String formatMwaPathUnderAssetId(String locationName, String assetId, String fileName) {
		String result = String.format("mwa://%s/%s/%s", locationName, assetId, fileName);
		LogUtils.log("EssenceLocationHelper_formatMwaPathUnderAssetId_result", result);
		return result;
	}

	/**
	 *
	 *
	 * @param locationPath
	 * @return ODA/ODA_LIBARARY であれば cartridgeId を返す。それ以外で取得できない場合には空文字列を返却する。
	 */
	public static String getCartridgeId(String locationPath, boolean decode) {
		String cartridgeId = "";

		if (locationPath.contains("cartridgeId")){
			cartridgeId = locationPath.substring(locationPath.lastIndexOf("cartridgeId=") + "cartridgeId=".length(), locationPath.length());
		}

		if (decode){
			try {
				cartridgeId = UriUtils.percentDecode(cartridgeId);
			} catch (UnsupportedEncodingException e) {
				throw new MwaInstanceError(MWARC.SYSTEM_ERROR, "", e.getMessage());
			}
		}

		return cartridgeId;
	}

	/**
	 * filePath に ODA Cartridge のドライブレターを付加する。
	 *
	 * @param filePath
	 * @param driveLetter
	 * @return ODA Cartridge のドライブレターを付加された filePath を返却する。
	 */
	public static String addDriveLetterToRetrieveSourceFilePath(String filePath, String driveLetter) {
		LogUtils.log("EssenceLocationHelper_addDriveLetterToRetrieveSourceFilePath_return", String.format("%s%s%s", filePath, "&amp;drive=", driveLetter));
		return String.format("%s%s%s", filePath, "&amp;drive=", driveLetter);
	}

	/**
	 * Location Path に含まれるカートリッジIDを第二引数のカートリッジIDに置換する。
	 *
	 * @param locationPath
	 * @param newCartridgeId
	 *            置換後のカートリッジID
	 * @return カートリッジIDが置換された Location Path を返却する。locationPath
	 *         にカートリッジIDがない場合は引数の locationPath のまま返却する。
	 */
	public static String replaceCartridgeIdOfLocationPath(String locationPath, String newCartridgeId) {
		String oldCartridgeId = getCartridgeId(locationPath, false);
		String resultLocationPath = null;

		// TODO newCartridgeId がエンコードされていても、いなくても問題ないような実装にする。(残：下記コメントアウトを外し、動作確認)
		// if (newCartridgeId.contains("%20")){
		//	newCartridgeId = newCartridgeId.replace("%20", " ");
		// }

		if (StringUtils.isEmpty(oldCartridgeId)) {
			resultLocationPath = locationPath;
			logger.info("Failed to replace cartridge ID. Location Path does not have cartridge ID.");
		} else {
			resultLocationPath = locationPath.replace(oldCartridgeId, newCartridgeId);
		}

		LogUtils.log("EssenceLocationHelper_replaceCartridgeIdOfLocationPath_resultLocationPath", resultLocationPath);
		return resultLocationPath;
	}

	// -------------------------------------------------------------
	// MWA Location Name に対するユーティリティ―メソッド群を提供します。
	// -------------------------------------------------------------

	public static class PrimaryVolumeResponse {
		public String masterVolumeName;
		public String proxyVolumeName;
	}

	/**
	 * プライマリとして使用するボリュームの情報を取得します。
	 *
	 * 取得できなかった場合には null を返却します。
	 *
	 * @param core
	 * @return
	 */
	public static PrimaryVolumeResponse getPrimaryVolumeInfo(ICoreModules core){

		// NavigatorX に対して、どのボリュームを使ったらよいか確認する必要がある。
		// そのために Rule/Volume に対する GET リクエストを発行する

		AssetRulesCommandService ruleCommand = new AssetRulesCommandService(core);
		JsonObject rules = ruleCommand.getAssetRules();
		if (rules == null){
			return null;
		}

		AssetVolumesCommandService volumeCommand = new AssetVolumesCommandService(core);
		JsonObject volumes = volumeCommand.getAssetVolumes();
		if (volumes == null){
			return null;
		}

		String primaryMasterVolumeId = "";
		String primaryProxyVolumeId = "";

		JsonArray ruleModelsJsonArray = rules.getJsonArray("models");
		List<JsonObject> ruleModels = new ArrayList<JsonObject>();
		Iterator<Object> itr = ruleModelsJsonArray.iterator();
		while (itr.hasNext()) {
			ruleModels.add((JsonObject) itr.next());
		}

		for (JsonObject rule : ruleModels){
			String type = rule.getString("type");
			if (type != null && type.equals("ESSENCE_STORED_ONLINE_DISK_PRIORITY")) { // TODO: /*RuleTypeEnum.ESSENCE_STORED_ONLINE_DISK_PRIORITY.getJsonObjectKey()*/
				JsonObject detail = rule.getJsonObject("essenceStoredOnlineDiskPriorityInfo"); // TODO:
				if (detail != null) {
					primaryMasterVolumeId = detail.getString("primaryMasterVolumeId");  // TODO: Enum 値でリファクタリングする
					primaryProxyVolumeId = detail.getString("primaryProxyVolumeId");    // TODO: Enum 値でリファクタリングする
					break;
				}
			}
		}

		PrimaryVolumeResponse response = new PrimaryVolumeResponse();

		JsonArray volumeModelsJsonArray = volumes.getJsonArray("models");
		List<JsonObject> volumeModels = new ArrayList<JsonObject>();
		Iterator<Object> vitr = volumeModelsJsonArray.iterator();
		while (vitr.hasNext()) {
			volumeModels.add((JsonObject) vitr.next());
		}

		JsonObject proxyVolume = findVolumeFromModels(primaryProxyVolumeId, volumeModels);
		if (proxyVolume != null) {
			response.proxyVolumeName = proxyVolume.getString("name");
		}

		JsonObject masterVolume = findVolumeFromModels(primaryMasterVolumeId, volumeModels);
		if (masterVolume != null) {
			response.masterVolumeName = masterVolume.getString("name");
		}

		logger.info(String.format("Get Primary Volume Info master: %s, proxy: %s", response.masterVolumeName, response.proxyVolumeName));
		return response;

	}

	/**
	 * VolumeModel の集合体から VolumeId と一致する Volume を探します。
	 *
	 * @param volumeId
	 * @param models
	 * @return
	 */
	private static JsonObject findVolumeFromModels(String volumeId, List<JsonObject> models) {
		for (JsonObject volume : models) {
			if (volume.getString("id").equals(volumeId)) {
				return volume;
			}
		}

		// not found case.
		return null;
	}

	/**
	 * ODA用のエッセンスロケーションパスを生成します。
	 * @param srcUrl
	 * @param cartridgeId
	 * @return
	 */
	public static String createODAEssenceLocation(String srcUrl, String cartridgeId) {
		StringBuilder ODAPathBuilder = new StringBuilder();
		ODAPathBuilder.append(srcUrl);
		ODAPathBuilder.append(CARTRIDGE_ID_FORMAT);
		ODAPathBuilder.append(cartridgeId);
		LogUtils.log("EssenceLocationHelper_createODAEssenceLocation_return", ODAPathBuilder.toString());
		return ODAPathBuilder.toString();
	}

	/**
	 * filePath に ODA Cartridge のドライブレターを付加する。
	 *
	 * @param essenceLocationPath
	 * @param driveLetter
	 * @return ODA Cartridge のドライブレターを付加された filePath を返却する。
	 */
	public static String addDriveLetterToEssenceLocationPath(String essenceLocationPath, String driveLetter) {
		StringBuilder essenceLocationBuilder = new StringBuilder();
		essenceLocationBuilder.append(essenceLocationPath);
		essenceLocationBuilder.append(DRIVE_LETTER_FORMAT);
		essenceLocationBuilder.append(driveLetter);
		LogUtils.log("EssenceLocationHelper_addDriveLetterToEssenceLocationPath_return", essenceLocationBuilder.toString());
		return essenceLocationBuilder.toString();
	}

	/**
	 * Essence/EventのlocationをMaster/Proxy情報で上書きする。
	 *
	 * @param assetModel
	 * @param assetId
	 * @param core
	 * @return Essence/Eventのlocationが上書きされたAssetModel
	 */
	public static JsonObject overwriteLocationPath(JsonObject assetModel, String assetId, ICoreModules core) {
		JsonArray essenceList = assetModel.getJsonArray("essences");
		if (essenceList == null) {
			return assetModel;
		}
		for (int i = 0; i < essenceList.size(); i++) {
			JsonObject essence = essenceList.getJsonObject(i);
			boolean isProxy = false;
			boolean isVttOrImage = false;
			String essenceType = essence.getString("type");
			if (essence.getBoolean("proxy") && ("VIDEO".equals(essenceType) || "AUDIO_VIDEO".equals(essenceType))) {
				isProxy = true;
			} else if (essence.getBoolean("proxy")) {
				isVttOrImage  = true;
			}
			String location = essence.getString("location");
			String fileName = new File(location).getName();
			if (isProxy) {
				location = EssenceUtils.getAnyProxyLocationPath(fileName, assetId, core);
			} else if (isVttOrImage) {
				location = EssenceUtils.getAnyVttOrImageLocationPath(location, assetModel.getString("id"), assetId, core);
			} else {
				location = EssenceUtils.getAnyMasterLocationPath(fileName, assetId, core);
			}

			/**
			 * 下記はCBS 向けの暫定対応です(2018/07/27(Fri) 時点)。
			 * AssetImporter からSUBTITLE をImport した場合にEssenceModel のStatus というKey が入っておらず、
			 * NullPointer が発生します。
			 * 松井さんから、Status が無かった場合には一律でTrue に倒してくださいと伺いましたので、下記の対応を行いました。
			 * 後にAssetImporter 側を修正するそうです。
			 */
			boolean isOnline = false;
			if(StringUtils.isEmpty(essence.getString("status"))) {
				isOnline = true;
			} else {
				isOnline = "Online".equalsIgnoreCase(essence.getString("status")) ? true : false;
			}
			// statusがOnlineの場合はlocationを更新する
			if (isOnline) {
				essence.put("location", location);
				// "retrieveLocation"keyが存在し、statusがOfflineかつprimaryの場合はretrieveLocationを更新する
			} else if (essence.getBoolean("primary") && essence.containsKey("retrieveLocation")) {
				essence.put("retrieveLocation", location);
			}
		}
		JsonArray eventList = assetModel.getJsonArray("events");
		for (int j = 0; j < eventList.size(); j++) {
			JsonObject event = eventList.getJsonObject(j);
			String eventId = event.getString("id");
			String imageLocation = event.getString("imageLocation");
			if (StringUtils.isEmpty(eventId) || StringUtils.isEmpty(imageLocation)) {
				continue;
			}
			logger.info("Image location. " + imageLocation);
			if (!isMwaLocationPath(imageLocation)) {
				event.put("imageLocation", imageLocation);
			} else {
				String locationName = getMasterLocationName(core);
				try {
					MwaUri mwaUri = new MwaUri(imageLocation);
					mwaUri.setLocation(locationName);
					mwaUri.setAssetId(assetId);
					mwaUri.setEssenceId(eventId);
					event.put("imageLocation", mwaUri.get());
				} catch (MwaUriException e) {
					throw new MwaInstanceError(MWARC.INVALID_INPUT, null, e.getMessage());
				}
			}
		}
		return assetModel;
	}

	/**
	 * PrimaryVolume のLocationName を取得します
	 * @param coreModules
	 * @return
	 */
	private static String getMasterLocationName(ICoreModules coreModules) {
		PrimaryVolumeResponse volumeInfo = EssenceLocationHelper.getPrimaryVolumeInfo(coreModules);

		String locationName = null;
		if (volumeInfo != null && !org.apache.commons.lang.StringUtils.isEmpty(volumeInfo.masterVolumeName)) {
			locationName = volumeInfo.masterVolumeName;
		} else {
			String message = "Failed to get location name for any Master.";
			throw new MwaInstanceError(MWARC.OPERATION_FAILED, null, message);
		}
		return locationName;
	}
	/**
	 * EssenceのStatus情報を真偽値で取得する
	 * @param essence
	 * @return　Onlineの場合はtrue, Offlineの場合はfalse
	 */
	public static boolean isOnlineStatus(JsonObject essence) {
		return essence.getString("status").equals("Online") ? true : false;
	}

	/**
	 * path がMwaLocationPath か判定します
	 * @param path
	 * @return　Onlineの場合はtrue, Offlineの場合はfalse
	 */
	public static boolean isMwaLocationPath(String path) {
		return path.startsWith("mwa://");
	}

	public static List<String> getSubtitleIds(JsonObject assetModel) {
		List<String> subtitleIds = new ArrayList<>();
		JsonArray essenceList = assetModel.getJsonArray("essences");
		if (essenceList == null) {
			return subtitleIds;
		}
		for (int i = 0; i < essenceList.size(); i++) {
			JsonObject essence = essenceList.getJsonObject(i);
			String essenceType = essence.getString("type");
			if (!essenceType.equals("SUBTITLE")) {
				continue;
			}
			logger.debug("Add -> "+ essence.getString("sourceEssenceId"));
			subtitleIds.add(essence.getString("sourceEssenceId"));
		}
		return subtitleIds;
	}

	public static JsonObject removeSubtitles(JsonObject assetModel) {
		JsonArray essenceList = assetModel.getJsonArray("essences");
		if (essenceList == null) {
			return assetModel;
		}
		List<Integer> removeNumber = new ArrayList<>();
		for (int i = 0; i < essenceList.size(); i++) {
			JsonObject essence = essenceList.getJsonObject(i);
			String essenceType = essence.getString("type");
			if (essenceType.equals("SUBTITLE")) {
				removeNumber.add(i);
			}
		}
		if (removeNumber != null || !removeNumber.isEmpty()) {
			Collections.sort(removeNumber, Collections.reverseOrder());
			for (int number : removeNumber) {
				logger.debug("Remove -> " + essenceList.getJsonObject(number));
				essenceList.remove(number);
			}
		}

		return assetModel.put("essences", essenceList);
	}
}
