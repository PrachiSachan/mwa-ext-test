package com.sony.pro.mwa.util.uri;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.util.exceptions.MwaUriException;

/**
 * MWA-URIの変換処理を集約するクラス。
 * 以下のいずれかの書式で指定されるURIのLocation, AssetId, EssenceId, FileNameの置換処理を行う。
 *
 * パターン１：mwa://{location}/{assetId}/{fileName}
 * パターン２：mwa://{location}/{assetId}/{essenceId}/{fileName}
 *
 * また、ICoreEnvSupplierに依存して、現在Navigator-X側に登録されている物理、UNCのRootパス、Location名の置換処理も合わせて行う
 *
 */
public class MwaUri {

	/**
	 * MWA-URIであることを示すスキーマ
	 * TODO　あちこちで定義されているはず。極力ここに集約していきたい。
	 */
	public static String MWA_URI_SCHEME = "mwa://";
	public static String MWA_URI_SCHEME_NAME = "mwa";

	// UUID文字列検査用の正規表現
	private static final String UUID_FORMAT = "[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}";

	private String location = null;
	private String assetId = null;
	private List<String> essenceLocation = new ArrayList<String>();
	private String resourceName = null;
	private Map<String, String> queryMap = new HashMap<String, String>();

	/**
	 * URI の各要素の連結
	 * @return 結合した文字列(MWA URI)
	 */
	private String concat() {
		StringBuilder result = new StringBuilder(MWA_URI_SCHEME);

		if (this.location != null && !this.location.isEmpty()) {
			result.append(this.location);
		}

		if (this.assetId != null && !this.assetId.isEmpty()) {
			result.append("/");
			result.append(this.assetId);
		}

		for (String essence : this.essenceLocation) {
			if (StringUtils.isEmpty(essence)) {
				continue;
			}
			result.append("/");
			result.append(essence);
		}

		if (this.resourceName != null && !this.resourceName.isEmpty()) {
			result.append("/");
			result.append(this.resourceName);
		}

		if (hasQuery()) {
			result.append("?");
			result = appendQuery(result);
		}

		return result.toString();
	}

	/**
	 * Location名を取得する
	 * @return Location名
	 */
	public String getLocation() {
		return this.location;
	}
	/**
	 * AssetIdを取得する
	 * @return AssetId
	 */
	public String getAssetId() {
		return this.assetId;
	}
	/***
	 * EssenceIdを取得する
	 * @return EssenceId
	 */
	public List<String> getEssenceId() {
		return this.essenceLocation;
	}

	/***
	 * EssenceLocationを文字列で取得する。先頭は"/"から開始され、末尾は"/"を付与しない。
	 * @return
	 */
	public String getEssenceLocationPath() {
		StringBuffer result = new StringBuffer();
		for (String essenceId : this.essenceLocation) {
			result.append("/");
			result.append(essenceId);
		}
		String path = result.toString();
		return path.isEmpty() ? null : path;
	}

	/**
	 * URIのパス部分を取得する
	 * @param withResource パスにリソース名を含めるかどうか
	 * @return
	 */
	public String getPath(boolean withResource) {
		StringBuffer result = new StringBuffer();

		if (this.assetId != null) {
			result.append(this.assetId);
		}

		if (this.essenceLocation != null && this.essenceLocation.size() > 0) {
			result.append(this.getEssenceLocationPath());
		}

		if (withResource && this.resourceName != null) {
			result.append("/");
			result.append(this.resourceName);
		}
		return result.toString();
	}

	/**
	 * リソース名を取得する
	 * @return リソース名
	 */
	public String getResourceName() {
		return this.resourceName;
	}
	/**
	 * リソース名の拡張子を取得する("."ドットを含む)
	 * @return リソース名
	 */
	// TODO test: null or empty
	public String getResourceExtension() throws MwaUriException {
		int index = this.resourceName.lastIndexOf('.');
		if (index == -1) {
			throw MwaUriException.supplyInvalidUriFormat(this.resourceName);
		}
		return this.resourceName.substring(index);
	}
	/**
	 * リソース名を取得する
	 * @return リソース名
	 */
	public Map<String, String> getQueryMap() {
		return this.queryMap;
	}
	/**
	 * EssenceIdを保持しているかチェックする
	 * @return essenceIdを保持している場合はtrue
	 */
	public boolean hasEssenceId() {
		return !this.essenceLocation.isEmpty();
	}
	/**
	 * ResourceIdを保持しているかチェックする
	 * @return
	 */
	public boolean hasResource() {
		// 「nullかどうか」をもって、持っている持っていないを判断します。空文字は想定しません（setで空文字を詰めさせない）
		return (this.resourceName != null);
	}
	/**
	 * Queryを保持しているかチェックする
	 * @return
	 */
	public boolean hasQuery() {
		return !this.queryMap.isEmpty();
	}
	/***
	 * コンストラクタ
	 */
	public MwaUri() {}

	/***
	 * コンストラクタ
	 * @param baseURI ベースとなるMWA-URI
	 */
	public MwaUri(String baseURI) throws MwaUriException {
		this.setURI(baseURI);
	}

	/**
	 * コンストラクタ
	 * @param locationId
	 * @param resourceName
	 */
	public MwaUri(String locationId, String resourceName) throws MwaUriException {
		this.set(locationId, resourceName);
	}

	/**
	 * コンストラクタ
	 * @param locationId
	 * @param assetId
	 * @param resourceName
	 */
	public MwaUri(String locationId, String assetId, String resourceName) throws MwaUriException {
		this.set(locationId, assetId, resourceName);
	}

	/***
	 * コンストラクタ
	 * @param locationId
	 * @param assetId
	 * @param essenceId
	 * @param resourceName
	 */
	public MwaUri(String locationId, String assetId, String essenceId, String resourceName) throws MwaUriException {
		this.set(locationId, assetId, essenceId, resourceName);
	}

	/***
	 * コンストラクタ
	 * @param locationId
	 * @param assetId
	 * @param essenceId
	 * @param resourceName
	 */
	public MwaUri(String locationId, String assetId, String essenceId, String resourceName, Map<String, String> queryMap) throws MwaUriException {
		this.set(locationId, assetId, essenceId, resourceName, queryMap);
	}

	/***
	 * URIを設定する
	 * @param baseURI
	 */
	public void setURI(String baseURI) throws MwaUriException {

		if (baseURI == null || baseURI.isEmpty()) {
			throw MwaUriException.supplyBaseIsEmpty();
		}

		try {
			URI uri = new URI(baseURI);

			String scheme = uri.getScheme();
			if (scheme == null || !scheme.equals(MwaUri.MWA_URI_SCHEME_NAME)) {
				throw MwaUriException.supplyInvalidUriFormat(baseURI);
			}

			this.location = uri.getAuthority();

			String query = uri.getRawQuery();

			if (query != null) {
				this.splitQuery(query);
			}

			// assetId, essenceLocation, resourceNameを読み取る
			// AssetImporterを経由したAssetに含まれるロケーションURIのように、UUIDを含まず、かつ階層数が可変のケースがあるため
			// 先頭をAssetId, 末尾をResourceName, 中間層はEssenceLocationとみなしてしまう
			//String path = uri.getRawPath();
			String path = uri.getRawPath();
			// uri.getPath()の先頭に"/"が付与されているので取り除く
			path = path.substring(1);

			if (path != null && !path.isEmpty()) {
				// 再修正
				// Authorityの次がリソース名の場合に、assetIdに不正な値（リソース名）が格納されてしまう。
				String[] elements = path.split("/");
				int ubound = elements.length - 1;

				this.resourceName = elements[ubound];

				for (int n = 0; n < ubound; n++) {
					switch (n) {
						case 0:
							this.assetId = elements[n];
							break;
						default:
							this.essenceLocation.add(elements[n]);
							break;
					}
				}
			}
		} catch (URISyntaxException uriExp) {
			throw MwaUriException.supplySyntaxErrorOccured(uriExp, baseURI);
		}
	}

	/**
	 * MWA-URI形式で返却する
	 * @return
	 */
	public String get() {
		return concat();
	}

	/**
	 * ロケーションを差し替える
	 * @param newLocation
	 * @return
	 */
	public String setLocation(String newLocation) throws MwaUriException {
		// 本来なら、仕様に基づく正規表現チェックであるべきだが。。。。
		if (newLocation == null || newLocation.isEmpty()) {
			throw MwaUriException.supplyInvalidParameter("location", newLocation);
		}
		this.location = newLocation;
		return concat();

	}

	/**
	 * アセットIDを差し替える
	 * @param newAssetId
	 * @return
	 */
	public String setAssetId(String newAssetId) throws MwaUriException {
		this.assetId = newAssetId;
		return concat();
	}

	/**
	 * エッセンスIDを差し替える。
	 * @param newEssenceId
	 * @return
	 */
	public String setEssenceId(String newEssenceId) throws MwaUriException {
		this.essenceLocation.clear();
		if (newEssenceId != null && !newEssenceId.isEmpty()) {
			this.essenceLocation.add(newEssenceId);
		}
		return concat();
	}

	/**
	 * エッセンスIDを差し替える。
	 * @param essenceId
	 * @return
	 */
	public String appendEssenceId(String essenceId) throws MwaUriException {
		if (essenceId == null) {
			throw MwaUriException.supplyInvalidParameter("essenceId", essenceId);
		}
		this.essenceLocation.add(essenceId);
		return concat();
	}

	/**
	 * エッセンスIDを取り除く
	 * @return
	 */
	public String removeEssenceId() {
		this.essenceLocation.clear();
		return concat();
	}

	/**
	 * Query を取り除きます。
	 * @return
	 */
	public String removeQuery() {
		this.queryMap.clear();
		return concat();
	}

	/***
	 * リソース名を設定する。
	 * @param newResourceName
	 * @return リソース名設定後のURI文字列
	 */
	public String setResourceName(String newResourceName) throws MwaUriException {
		if (newResourceName == null || newResourceName.isEmpty()) {
			throw MwaUriException.supplyInvalidParameter("resourceName", newResourceName);
		}
		this.resourceName = newResourceName;
//		try {
//			this.resourceName = URLEncoder.encode(newResourceName, "UTF-8");
//		} catch (UnsupportedEncodingException ue) {
//			throw MwaUriException.supplyInvalidUriFormat(newResourceName);
//		}
		return concat();
	}

	/***
	 * クエリを設定する。
	 * @param queryMap
	 * @return リソース名設定後のURI文字列
	 */
	public String setQueryMap(Map<String, String> queryMap) throws MwaUriException {
		if (queryMap == null || queryMap.isEmpty()) {
			throw MwaUriException.supplyInvalidParameter("queryMap", queryMap.toString());
		}
		this.queryMap = queryMap;
		return concat();
	}


	/**
	 * ロケーション名、アセットId、リソース名を設定する
	 * @param location
	 * @param resourceName
	 * @return 設定後のURI文字列
	 */
	public String set(String location, String resourceName) throws MwaUriException {
		this.setLocation(location);
		this.setResourceName(resourceName);
		return concat();
	}

	/**
	 * ロケーション名、アセットId、リソース名を設定する
	 * @param location
	 * @param assetId
	 * @param resourceName
	 * @return 設定後のURI文字列
	 */
	public String set(String location, String assetId, String resourceName) throws MwaUriException {
		this.setLocation(location);
		this.setAssetId(assetId);
		this.setResourceName(resourceName);
		return concat();
	}

	/**
	 * ロケーション名、アセットId、エッセンスId, リソース名を設定する
	 * @param location
	 * @param assetId
	 * @param essenceId
	 * @param resourceName
	 * @return 設定後のURI文字列
	 */
	public String set(String location, String assetId, String essenceId, String resourceName) throws MwaUriException {
		this.setLocation(location);
		this.setAssetId(assetId);
		this.setEssenceId(essenceId);
		this.setResourceName(resourceName);
		return concat();
	}

	/**
	 * ロケーション名、アセットId、エッセンスId, リソース名, クエリを設定する
	 * @param location
	 * @param assetId
	 * @param essenceId
	 * @param resourceName
	 * @return 設定後のURI文字列
	 */
	public String set(String location, String assetId, String essenceId, String resourceName, Map<String, String> queryMap) throws MwaUriException {
		this.setLocation(location);
		this.setAssetId(assetId);
		this.setEssenceId(essenceId);
		this.setResourceName(resourceName);
		this.setQueryMap(queryMap);
		return concat();
	}

	/**
	 * MwaUri形式かどうかチェックして、真ならそのインスタンスを返す
	 * @param src
	 * @return
	 */
	public static MwaUri getInstance(String src) {

		MwaUri result = null;

		if (src != null && src.startsWith(MwaUri.MWA_URI_SCHEME)) {
			try {
				result = new MwaUri(src);
			} catch (Exception e) {
				// ...nullを返すので握りつぶす。
			}
		}
		return result;
	}

	/**
	 * locationName が引数で指定したものと一致するか判定する
	 * @param locationName
	 * @return
	 */
	public boolean isLocation(String locationName) {
		boolean result = false;

		if (getLocation().equals(locationName)) {
			result = true;
		}
		return result;
	}

	/**
	 * URL に含まれるクエリをMap形式で保持します
	 *
	 * @param queryParams
	 */
	private void splitQuery(String queryParams) throws MwaUriException {
		String[] queryArray = queryParams.split("&");
		for(String query : queryArray) {
			String[] q = query.split("=");
			if (q.length < 2) {
				throw MwaUriException.supplyInvalidUriFormat(queryParams);
			}
			this.setQuery(q[0], q[1]);
		}
	}

	/**
	 * StringBuilder にクエリを付与して返却します
	 *
	 * @param stringBuilder
	 *
	 * @return クエリをアペンドした StringBuilder
	 */
	private StringBuilder appendQuery(StringBuilder stringBuilder) {
		for (String key : queryMap.keySet()) {
			if (!stringBuilder.toString().endsWith("?")){
				stringBuilder.append("&");
			}
			stringBuilder.append(key + "=" + queryMap.get(key));
		}
		return stringBuilder;
	}

	/**
	 * 指定したキーと値を追加します
	 * 既に同名キーが存在している場合は値を上書きします
	 *
	 * @param key
	 * @param value
	 * @throws MwaUriException
	 */
	public String setQuery(String key, String value) throws MwaUriException {
		this.queryMap.put(key, value);
		return concat();
	}


	// TODO テスト
	/**
	 * 指定したキーの値を取得します
	 *
	 * @param key
	 */
	public String getValue(String key) {
		return this.queryMap.get(key);
	}
}
