package com.sony.pro.mwa.util.nvx.asset.models;

/**
 * JSON処理で必要になる文字列定数をまとめたクラス。基本的にはノード名のみ。。？
 * 
 * ※package内privateです。スコープを変える必要がある、という場合はご連絡ください（基本、ないはず）
 * 
 */
public class Constants {

	/**
	 * インスタンス化しない
	 */
	private Constants() {}

	public static final String Models = "models";
	// 各要素で共通ですが、あまりに汎用的なのでここに定義
	public static final String Id = "id";
	
	/***
	 * models内のオブジェクトの要素名
	 */
	public static class Asset {
		private Asset() {}
		
		public static final String _Index = "_index";
		public static final String _Type = "_type";
		public static final String _Id = "_id";
		public static final String _Score = "_score";
		public static final String _Source = "_source";
		
		public static final String Events = "events";
		public static final String Essences = "essences";
		public static final String AssetInfo = "assetInfo";
		
		public static final String Id = "id";
		public static final String RevisionId = "revisionId";
		public static final String RevisionNote = "revisionNote";
		public static final String Parents = "parents";
		public static final String ShortcutParentIds = "shortcutParentIds";
		public static final String ThumbnailEventId = "thumbnailEventId";
		public static final String MetadataSchemaId = "metadataSchemaId";
		public static final String Operator = "operator";
		public static final String Readable = "readable";
		public static final String Writable = "writable";
		public static final String RelatedReadable = "relatedReadable";
		public static final String LimitedReadable = "limitedReadable";
		public static final String ImportTime = "importTime";
		public static final String UpdateTime = "updateTime";
		public static final String Activities = "activities";
		public static final String Tags = "tags";
		public static final String BinInfo = "binInfo";
		public static final String CollectionId = "collectionId";
		public static final String WorkgroupId = "workgroupId";
		public static final String ClusterName = "clusterName";
		public static final String LockUser = "lockUser";
		public static final String Create = "create";
		public static final String Update = "update";
		public static final String Owner = "owner";
		public static final String Location = "location";
		public static final String Trashed = "trashed";
		public static final String LockExpiretime = "lockExpiretime";
		public static final String Type = "type";
		public static final String RelatedWritable = "relatedWritable";
		public static final String Hidden = "hidden";
		public static final String Locked = "locked";
	}
	
	/**
	 * assetInfoの要素名
	 */
	public static class AssetInfo {
		private AssetInfo() {}
		
		public static final String OdsBarcodeIds = "odsBarcodeIds";
		public static final String Note = "note";
		public static final String Rating = "rating";
		public static final String Length = "length";
		public static final String Title = "title";
		public static final String Type = "type";
		public static final String Numerator = "numerator";
		public static final String Denominator = "denominator";
		public static final String Duration = "duration";
		public static final String Archived = "archived";
		public static final String FrameRate = "frameRate";
		public static final String StartTimecode = "startTimecode";
		public static final String CustomMetadata = "customMetadata";
		public static final String CopyOdsBarcodeIds = "copyOdsBarcodeIds";
		public static final String StartTimecode_msec = "startTimecode_msec";
		public static final String OdsCartridgeIds = "odsCartridgeIds";
		public static final String CopyOdsCartridgeIds = "copyOdsCartridgeIds";
		public static final String DropFrame = "dropFrame";
		public static final String Status = "status";
	}

	/**
	 * events内のオブジェクトの要素名
	 */
	public static class Event {
		private Event() {}
		
		public static final String Duration = "duration";
		public static final String CustomMetadata ="customMetadata";
		public static final String Type ="type";
		public static final String Title ="title";
		public static final String Frame ="frame";
		public static final String ImageLocation ="imageLocation";
		public static final String Note ="note";
	}
	
	/**
	 * Essenceの要素名
	 */
	public static class Essence {
		private Essence() {}
		
		public static final String Extension = "extension";
		public static final String VideoInfo = "videoInfo";
		public static final String Format = "format";
		public static final String Type = "type";
		public static final String Proxy = "proxy";
		public static final String FrameCount = "frameCount";
		public static final String Size = "size";
		public static final String AudioInfo = "audioInfo";
		public static final String Name = "name";
		public static final String Location = "location";
		public static final String FormatCommercial = "formatCommercial";
		public static final String Primary = "primary";
		public static final String Status = "status";
		
		//TODO Video/AudioInfoなど、必要に応じて追加して下さい
	}	
}
