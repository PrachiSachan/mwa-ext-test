package com.sony.pro.mwa.activity.nvxutil;

import java.util.Arrays;

import com.sony.pro.mwa.activity.scheme.httpclient.HttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.httpclient.HttpResponseResult;
import com.sony.pro.mwa.activity.scheme.httpclient.IHttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.utils.EndpointProvider;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.enumeration.FilterOperatorEnum;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.model.provider.ActivityProviderCollection;
import com.sony.pro.mwa.model.provider.ActivityProviderModel;
import com.sony.pro.mwa.model.provider.ServiceEndpointModel;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;

import io.vertx.core.json.JsonObject;

/**
 * NavigatorX における User API の HTTP リクエストに対するサービスクラスです。
 */
public class UserCommandService extends CommandServiceBase {
	private static MwaLogger logger = MwaLogger.getLogger(UserCommandService.class);
	private final static String BASE_URI = "user/api/v1/login";

	/**
	 *
	 * @param coreModules
	 * @return
	 */
	public UserCommandService(ICoreModules coreModules) {
		super(coreModules);
	}

	/**
	 * AMSサーバにログインしてアクセスキーを取得します。
	 *
	 * @return accessKey
	 */
	public String loginToAmsServer() {
		// 既存実装を組み合わせている状態のため後々リファクタリングする。

		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), BASE_URI);

		// Provider名"NVX_LOCAL"のProviderから、AMSサーバへのログイン情報を取得する。
		String user = null;
		String pass = null;

		ActivityProviderCollection col = this.getCoreModules().getActivityProviderManager().getProviders(null, Arrays.asList("name" + FilterOperatorEnum.EQUAL.toSymbol() + "NVX_LOCAL"), null, null);

		if (col.getCount() == 1) {
			ActivityProviderModel model = col.first();

			if (model.getEndpointList().size() == 1) {
				ServiceEndpointModel serviceEndpointModel = model.getEndpointList().get(0);
				user = serviceEndpointModel.getUser();
				pass = serviceEndpointModel.getPassword();
			} else {
				throw new MwaInstanceError(MWARC.INVALID_INPUT_PROVIDER_NOT_FOUND, null, null);
			}
		} else {
			throw new MwaInstanceError(MWARC.INVALID_INPUT_PROVIDER_NOT_FOUND, null, null);
		}

		HttpResponseResult httpResponse = new HttpResponseResult();

		JsonObject requestBody = new JsonObject();
		requestBody.put("username", user);
		requestBody.put("password", pass);

		String accessKey = null;
		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.post(url, requestBody.toString(), this.getHttpRequestProvider());

			// Internal username and accessKey should be concealed
			JsonObject httpResponseObject = new JsonObject(httpResponse.getResponseBody());

			accessKey = (httpResponseObject.getString("accessKey") != null) ? httpResponseObject.getString("accessKey") : null;
		} catch (Exception e) {
			String message = "login failed.";
			logger.error(message, e);
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, message);
		}
		return accessKey;
	}
}