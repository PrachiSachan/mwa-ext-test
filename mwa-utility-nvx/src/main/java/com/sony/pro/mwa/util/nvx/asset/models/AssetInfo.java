package com.sony.pro.mwa.util.nvx.asset.models;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.sony.pro.mwa.util.nvx.asset.models.exceptions.AssetModelsException;

/**
 * Define the properties of AssetInfo.
 * 
 * @author Kousuke Hosaka
 *
 */
public class AssetInfo extends ObjectNodeBase {
	
	/**
	 * デフォルトコンストラクタ
	 */
	public AssetInfo() {}
	
	/**
	 * コンストラクタ
	 * AssetInfo に該当するJsonNode を渡して、
	 * このクラスで処理するJsonNode のインスタンスを生成します。
	 * @param jsonNode
	 */
	public AssetInfo(JsonNode jsonNode) {
		super(jsonNode);
	}
	
	/**
	 * Title を取得します。
	 * @return
	 */
	public String getTitle() {
		return this.getNodeValue(Constants.AssetInfo.Title).asText();
	}
	
	/**
	 * Title を書き換えます。
	 * @param title
	 */
	public void setTitle(String title) {
		this.setNodeValue(Constants.AssetInfo.Title, title);
	}
	
	/**
	 * Rating を取得します。
	 * @return
	 * @throws AssetModelsException 
	 */
	public int getRating() throws AssetModelsException {
		if (this.getNodeValue(Constants.AssetInfo.Rating).isNull()) {
			// TODO : Null の時はNull を返したいが、現状は例外を投げる
			throw AssetModelsException.supplyJsonIsInvalid("Rating value of AssetInfo is Null.");
		}
		return this.getNodeValue(Constants.AssetInfo.Rating).asInt();
	}
	
	/**
	 * Rating を書き換えます。
	 * @param rating
	 */
	public void setRating(int rating) {
		this.setNodeValue(Constants.AssetInfo.Rating, rating);
	}
	
	/**
	 * Note を取得します。
	 * @return
	 */
	public String getNote() {
		return this.getNodeValue(Constants.AssetInfo.Note).asText();
	}
	
	/**
	 * Note を書き換えます。
	 * @param note
	 */
	public void setNote(String note) {
		this.setNodeValue(Constants.AssetInfo.Note, note);
	}
	
	/**
	 * Status を取得します。
	 * @return
	 */
	public String getStatus() {
		return this.getNodeValue(Constants.AssetInfo.Status).asText();
	}
	
	/**
	 * Status を書き換えます。
	 * @param status
	 */
	public void setStatus(String status) {
		this.setNodeValue(Constants.AssetInfo.Status, status);
	}
	
	/**
	 * Archive フラグを取得します。
	 * @return
	 */
	public boolean getArchived() {
		return this.getNodeValue(Constants.AssetInfo.Archived).asBoolean();
	}
	
	/**
	 * Archive フラグを書き換えます。
	 * @param archived
	 */
	public void setArchived(boolean archived) {
		this.setNodeValue(Constants.AssetInfo.Archived, archived);
	}
	
	/**
	 * Type を取得します。
	 * @return
	 */
	public String getType() {
		return this.getNodeValue(Constants.AssetInfo.Type).asText();
	}
	
	/**
	 * Type を書き換えます。
	 * @param type
	 */
	public void setType(String type) {
		this.setNodeValue(Constants.AssetInfo.Type, type);
	}
	
	/**
	 * StartTimecode を取得します。 
	 * @return
	 */
	public String getStartTimecode() {
		return this.getNodeValue(Constants.AssetInfo.StartTimecode).asText();
	}
	
	/**
	 * StartTimecode を書き換えます。
	 * @param startTimecode
	 */
	public void setStartTimecode(String startTimecode) {
		this.setNodeValue(Constants.AssetInfo.StartTimecode, startTimecode);
	}
	
	/**
	 * StartTimecode_msec を取得します。
	 * @return
	 */
	public long getStartTimecode_msec() {
		return this.getNodeValue(Constants.AssetInfo.StartTimecode_msec).asLong();
	}
	
	/**
	 * StartTimecode_msec を書き換えます。
	 * @param startTimecode_msec
	 */
	public void setStartTimecode_msec(long startTimecode_msec) {
		this.setNodeValue(Constants.AssetInfo.StartTimecode_msec, startTimecode_msec);
	}
	
	/**
	 * Duration を取得します。
	 * @return
	 */
	public long getDuration() {
		return this.getNodeValue(Constants.AssetInfo.Duration).asLong();
	}
	
	/**
	 * Duration を書き換えます。
	 * @param duration
	 */
	public void setDuration(long duration) {
		this.setNodeValue(Constants.AssetInfo.Duration, duration);
	}
	
	/**
	 * Length を取得します。
	 * @return
	 */
	public long getLength() {
		return this.getNodeValue(Constants.AssetInfo.Length).asLong();
	}
	
	/**
	 * Length を書き換えます。
	 * @param length
	 */
	public void setLength(long length) {
		this.setNodeValue(Constants.AssetInfo.Length, length);
	}
	
	/**
	 * Numerator を取得します。
	 * @return
	 */
	public long getNumerator() {
		return this.getNodeValue(Constants.AssetInfo.Numerator).asLong();
	}
	
	/**
	 * Numerator を書き換えます。
	 * @param numerator
	 */
	public void setNumerator(long numerator) {
		this.setNodeValue(Constants.AssetInfo.Numerator, numerator);
	}
	
	/**
	 * Denominator を取得します。
	 * @return
	 */
	public long getDenominator() {
		return getNodeValue(Constants.AssetInfo.Denominator).asLong();
	}
	
	/**
	 * Denominator を書き換えます。
	 * @param denominator
	 */
	public void setDenominator(long denominator) {
		this.setNodeValue(Constants.AssetInfo.Denominator, denominator);
	}
	
	/**
	 * FrameRate を取得します。
	 * @return
	 */
	public double getFrameRate() {
		return getNodeValue(Constants.AssetInfo.FrameRate).asDouble();
	}
	
	/**
	 * FrameRate を書き換えます。
	 * @param frameRate
	 */
	public void setFrameRate(double frameRate) {
		this.setNodeValue(Constants.AssetInfo.FrameRate, frameRate);
	}
	
	/**
	 * DropFrame の有無を取得します。
	 * @return
	 */
	public boolean getDropFrame() {
		return this.getNodeValue(Constants.AssetInfo.DropFrame).asBoolean();
	}
	
	/**
	 * DropFrame の有無を書き換えます。
	 * @param dropFrame
	 */
	public void setDropFrame(boolean dropFrame) {
		this.setNodeValue(Constants.AssetInfo.DropFrame, dropFrame);
	}
	
	/**
	 * OdsCartridgeIds を取得します。
	 * @return
	 */
	// TODO : ODSカートリッジID はList で返して問題ないか確認する
	public List<String> getOdsCartridgeIds() {
		return this.convertJsonNodeToListString(this.getNodeValue(Constants.AssetInfo.OdsCartridgeIds));
	}
	
	/**
	 * OdsCartridgeIds を書き換えます。
	 * @param valueList
	 * @throws AssetModelsException 
	 */
	public void setOdsCartridgeIds(List<?> valueList) throws AssetModelsException {
		this.setNodeValue(Constants.AssetInfo.OdsCartridgeIds, this.convertListToJsonNode(valueList));
	}
	
	/**
	 * OdsBarcodeIds を取得します。
	 * @return
	 */
	public List<String> getOdsBarcodeIds() {
		return this.convertJsonNodeToListString(this.getNodeValue(Constants.AssetInfo.OdsBarcodeIds));
	}
	
	/**
	 * OdsBarcodeIds を書き換えます。
	 * @param valueList
	 * @throws AssetModelsException 
	 */
	public void setOdsBarcodeIds(List<?> valueList) throws AssetModelsException {
		this.setNodeValue(Constants.AssetInfo.OdsBarcodeIds, this.convertListToJsonNode(valueList));
	}
	
	/**
	 * CopyOdsCartridgeIds を取得します。
	 * @return
	 */
	public List<String> getCopyOdsCartridgeIds() {
		return this.convertJsonNodeToListString(this.getNodeValue(Constants.AssetInfo.CopyOdsCartridgeIds));
	}
	
	/**
	 * CopyOdsCartridgeIds を書き換えます。
	 * @param valueList
	 * @throws AssetModelsException 
	 */
	public void setCopyOdsCartridgeIds(List<?> valueList) throws AssetModelsException {
		this.setNodeValue(Constants.AssetInfo.CopyOdsCartridgeIds, this.convertListToJsonNode(valueList));
	}
	
	/**
	 * OdsBarcodeIds を取得します。
	 * @return
	 */
	public List<String> getCopyBarcodeIds() {
		return this.convertJsonNodeToListString(this.getNodeValue(Constants.AssetInfo.CopyOdsBarcodeIds));
	}
	
	/**
	 * OdsBarcodeIds を書き換えます。
	 * @param valueList
	 * @throws AssetModelsException 
	 */
	public void setCopyBarcodeIds(List<?> valueList) throws AssetModelsException {
		this.setNodeValue(Constants.AssetInfo.CopyOdsBarcodeIds, this.convertListToJsonNode(valueList));
	}
	
	/**
	 * CustomMetadata を取得します。
	 * @return
	 */
	// CustomMeta は型が不明なため、現状JsonNode で実装
	public JsonNode getCustomMetadata() {
		return this.getNodeValue(Constants.AssetInfo.CustomMetadata);
	}
	
	/**
	 * CustomMetadata を書き換えます。
	 * @param customMetadata
	 */
	public void setCustomMetadata(JsonNode customMetadata) {
		this.setNodeValue(Constants.AssetInfo.CustomMetadata, customMetadata);
	}
	

}
