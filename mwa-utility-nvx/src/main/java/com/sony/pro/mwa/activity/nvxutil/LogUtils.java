package com.sony.pro.mwa.activity.nvxutil;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sony.pro.mwa.common.log.MwaLogger;

public class LogUtils {

	private static MwaLogger logger = MwaLogger.getLogger(LogUtils.class);

	private static Boolean isEnable = false;

	public static void log(String messege) {
		if (isEnable) {
			logger.info(messege);
		}
	}   
	
	public static void log(Map<String, Object> map) {
		
		Set<String> keys = map.keySet();
		
		StringBuffer buffer = new StringBuffer();
		
		for (String key : keys) {
			Object value = map.get(key);
			buffer.append(String.format("%s : %s\r\n", key, value == null ? "null" : value.toString()));
		}
		LogUtils.log(buffer.toString());
	}
	
	public static void logForString(Map<String, String> map) {
		
		Set<String> keys = map.keySet();
		
		StringBuffer buffer = new StringBuffer();
		
		for (String key : keys) {
			Object value = map.get(key);
			buffer.append(String.format("%s : %s\r\n", key, value == null ? "null" : value.toString()));
		}
		LogUtils.log(buffer.toString());
	}

	public static void log(List<String> list) {
		
		StringBuffer buffer = new StringBuffer();
		int pos = 0;

		for (String item : list) {
			buffer.append(String.format("[%d] : %s\r\n", pos++, item));
		}
		LogUtils.log(buffer.toString());
	}
	
	public static void logForPath(List<Path> list) {
		
		StringBuffer buffer = new StringBuffer();
		int pos = 0;
		
		for (Path item : list) {
			buffer.append(String.format("[%d] : %s\r\n", pos++, item));
		}
		LogUtils.log(buffer.toString());
	}

	public static void log(String prefix, Map<String, Object> map) {
		LogUtils.log(prefix);
		LogUtils.log(map);
	}
	
	public static void logForString(String prefix, Map<String, String> map) {
		LogUtils.log(prefix);
		LogUtils.logForString(map);
	}
	
	public static void log(String prefix, List<String> list) {
		LogUtils.log(prefix);
		LogUtils.log(list);
	}
	
	public static void logForPath(String prefix, List<Path> list) {
		LogUtils.log(prefix);
		LogUtils.logForPath(list);
	}
	
	public static void log(String prefix, String message) {
		LogUtils.log(prefix);
		LogUtils.log(message);
	}
}
