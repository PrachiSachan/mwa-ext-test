package com.sony.pro.mwa.util.nvx.asset.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.sony.pro.mwa.util.exceptions.MwaUriException;
import com.sony.pro.mwa.util.nvx.asset.models.exceptions.AssetModelsException;
import com.sony.pro.mwa.util.uri.MwaUri;

/**
 * Define the properties of event.
 * 
 * @author Kousuke Hosaka
 *
 */
public class Event extends ObjectNodeBase {
	
	/**
	 * デフォルトコンストラクタ
	 */
	public Event() {}
	
	/**
	 * コンストラクタ
	 * @param event
	 */
	public Event(JsonNode event) {
		super(event);
	}

	/**
	 * Event ID を取得します
	 * @return EventId
	 */
	public String getId() throws AssetModelsException {
		JsonNode id = this.getNodeValue(Constants.Id);
		if (id == null || !id.isTextual()) {
			throw AssetModelsException.supplyJsonIsInvalid("Event id's type is invalid.");
		}
		return id.asText();
	}
	
	/**
	 * Event ID を書き換えます。
	 * @param eventId
	 */
	public void setId(String eventId) {
		this.setNodeValue(Constants.Id, eventId);
	}
	
	/**
	 * Event Title を取得します。
	 * @return
	 */
	public String getTitle() {
		return this.getNodeValue(Constants.Event.Title).asText();
	}
	
	/**
	 * Event Title を書き換えます。
	 * @param title
	 */
	public void setTitle(String eventTitle) {
		this.setNodeValue(Constants.Event.Title, eventTitle);
	}
	
	/**
	 * Event Type を取得します。
	 * @return
	 */
	public String getType() {
		return this.getNodeValue(Constants.Event.Type).asText();
	}
	
	/**
	 * Event Type を書き換えます。
	 * @param eventType
	 */
	public void setType(String eventType) {
		this.setNodeValue(Constants.Event.Type, eventType);
	}
	
	/**
	 * Event Note を取得します。
	 * @return
	 */
	public String getNote() {
		// audio_video_sample.json には、Key/Value ともに存在しないが、
		// 下記仕様には存在するため、作成した。省略可のプロパティ なのかも。
		// https://www.tool.sony.biz/confluence/display/NVX/Asset+Model+1.2
		return this.getNodeValue(Constants.Event.Note).asText();
	}
	
	/**
	 * Event Note を書き換えます。
	 * @param eventNote
	 */
	public void setNote(String eventNote) {
		this.setNodeValue(Constants.Event.Note, eventNote);
	}
	
	/**
	 * Event Frame を取得します。
	 * @return
	 */
	public long getFrame() {
		return this.getNodeValue(Constants.Event.Frame).asLong();
	}
	
	/**
	 * Event Frame を書き換えます。
	 * @param eventFrame
	 */
	public void setFrame(long eventFrame) {
		this.setNodeValue(Constants.Event.Frame, eventFrame);
	}
	
	/**
	 * Event Duration を取得します。
	 * @return
	 */
	public long getDuration() {
		return this.getNodeValue(Constants.Event.Duration).asLong();
	}
	
	/**
	 * Event Duration を書き換えます。
	 * @param eventDuration
	 */
	public void setDuration(long eventDuration) {
		this.setNodeValue(Constants.Event.Duration, eventDuration);
	}
	
	/**
	 * Image Location を取得します。
	 * @return
	 */
	public String getImageLocation()  {
		return this.getNodeValue(Constants.Event.ImageLocation).asText();
	}
	
	/**
	 * Image Location を書き換えます。
	 * @param mwaUri
	 */
	public void setImageLocation(MwaUri mwaUri) {
		this.setNodeValue(Constants.Event.ImageLocation, mwaUri.get());
	}
	
	/**
	 * Image Location を書き換えます。
	 * @param imageLocation
	 */
	public void setImageLocation(String imageLocation) {
		this.setNodeValue(Constants.Event.ImageLocation, imageLocation);
	}
	
	/**
	 * Event CustomMetadata を取得します。
	 * @return
	 */
	public JsonNode getCustomMetadata() {
		// CustomMeta は型が不明なため、現状JsonNode で実装
		return this.getNodeValue(Constants.Event.CustomMetadata);
	}
	
	/**
	 * Event CustomMetadata を書き換えます。
	 * @param customMetadata
	 */
	public void setCustomMetadata(JsonNode eventCustomMetadata) {
		this.setNodeValue(Constants.Event.CustomMetadata, eventCustomMetadata);
	}
}
