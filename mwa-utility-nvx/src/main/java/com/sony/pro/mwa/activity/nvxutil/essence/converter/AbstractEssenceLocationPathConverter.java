package com.sony.pro.mwa.activity.nvxutil.essence.converter;

import com.sony.pro.mwa.activity.nvxutil.EssenceLocationHelper;

public abstract class AbstractEssenceLocationPathConverter implements IEssenceLocationPathConverter {
    public String convert(String fromMwaPath) {
        return EssenceLocationHelper.replaceLocationName(fromMwaPath, this.getLocationName());
     }

     public abstract String getLocationName(); // 抽象メソッド
}