package com.sony.pro.mwa.util.enumeration;

public enum LocationNameEnum {
	DEFAULT("DEFAULT"),
	ODA("ODA"),
	ODA_LIBRARY("ODA_LIBRARY"),
	PFD("PFD");

	private String name;

	private LocationNameEnum(String name) {
		this.name = name;
	}

	public static LocationNameEnum getLocationName(String name) {
		LocationNameEnum[] locationNameArray = LocationNameEnum.values();

        for (LocationNameEnum locationName : locationNameArray) {
            if (locationName.name.toString().equals(name)) {
                return locationName;
            }
        }
        return LocationNameEnum.DEFAULT;
    }
}
