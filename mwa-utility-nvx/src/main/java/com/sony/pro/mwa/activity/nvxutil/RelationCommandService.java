package com.sony.pro.mwa.activity.nvxutil;

import java.util.List;

import org.springframework.util.StringUtils;

import com.sony.pro.mwa.activity.scheme.httpclient.HttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.httpclient.HttpResponseResult;
import com.sony.pro.mwa.activity.scheme.httpclient.IHttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.utils.EndpointProvider;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;

import io.vertx.core.json.JsonObject;

public class RelationCommandService extends CommandServiceBase {

	private static MwaLogger logger = MwaLogger.getLogger(RelationCommandService.class);
	private final static String GET_BASE_URI = "nvx/api/v1/assets/%s/relations";
	private final static String POST_BASE_URI = "nvx/api/v1/assets/%s/relations";

	/**
	 * 引数付きコンストラクタを提供します。
	 *
	 * @param coreModules
	 */
	public RelationCommandService(ICoreModules coreModules) {
		super(coreModules);
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public JsonObject getRelations(String assetId) {
		return getRelations(null, null, null, null, assetId);
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public JsonObject getRelations(List<String> sort, List<String> filter, String offset, String limit, String assetId) {
		// rule 取得
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), POST_BASE_URI);
		String urlWithQuery = String.format(url, assetId);
		logger.info("url = " + urlWithQuery);

		// TODO: 後々は引数をクエリパラメータ化する必要がある.
		// いまいまは何も渡さない実装にしておくので修正が必要。
		HttpResponseResult httpResponse = new HttpResponseResult();

		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.get(urlWithQuery, this.getHttpRequestProvider());
			LogUtils.log("RelationCommandService_getRelations_responseBody", httpResponse.getResponseBody());
		} catch (Exception e) {
			String errorMessage = "Failed to get relations from navigatorx.";
			logger.error(errorMessage, e);
		}

		JsonObject jsonModel = null;

		// TODO: レスポンスボディから VolumeCollection にデシリアライズする。
		if (!StringUtils.isEmpty(httpResponse.getResponseBody())) {
			jsonModel = new JsonObject(httpResponse.getResponseBody());
		}

		return jsonModel;
	}

	public JsonObject postRelations(String assetId, String requestBody) {
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), POST_BASE_URI);
		String urlWithQuery = String.format(url, assetId);
		logger.info("url = " + urlWithQuery);
		HttpResponseResult httpResponse = new HttpResponseResult();
		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			LogUtils.log("RelationCommandService_postRelations_requestBody", requestBody);
			httpResponse = client.post(urlWithQuery, requestBody, this.getHttpRequestProvider());
			LogUtils.log("RelationCommandService_postRelations_responseBody", httpResponse.getResponseBody());
		} catch (Exception e) {
			String errorMessage = "Failed to patch archive-volume from navigatorx. volumeId = " + assetId;
			logger.error(errorMessage, e);
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, errorMessage);
		}
		if (!StringUtils.isEmpty(httpResponse.getResponseBody())) {
			return new JsonObject(httpResponse.getResponseBody());
		} return null;
	}
}
