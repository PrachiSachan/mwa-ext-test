package com.sony.pro.mwa.util.nvx.asset.models.filters;

import com.sony.pro.mwa.util.nvx.asset.models.Essence;

/**
 * EssencesクラスでEssenceを検索する際のフィルタ
 */
public interface EssenceFilter {
	/**
	 * フィルタ条件。
	 * @param essence チェック対象のフィルタ。
	 * @return 指定されたessenceを結果に含めてよい場合はtrue, 含めない場合はfalseを返す。
	 */
	public boolean accept(Essence essence);
}
