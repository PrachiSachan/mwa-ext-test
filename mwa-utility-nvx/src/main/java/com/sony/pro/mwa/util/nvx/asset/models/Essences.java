package com.sony.pro.mwa.util.nvx.asset.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.sony.pro.mwa.util.exceptions.MwaUriException;
import com.sony.pro.mwa.util.nvx.asset.models.exceptions.AssetModelsException;
import com.sony.pro.mwa.util.nvx.asset.models.filters.EssenceFilter;
import com.sony.pro.mwa.util.uri.MwaUri;

/**
 * あるAssetに含まれるEssenceのリスト
 */
public class Essences extends ArrayNodeBase {

	/**
	 * コンストラクタ
	 * @param essences
	 * @throws AssetModelsException 
	 */
	public Essences(JsonNode essences) throws AssetModelsException {
		super(essences);
	}

	/**
	 * Essenceリストを取得する
	 * @return
	 */
	public List<Essence> get() {
		Iterator<JsonNode> iter = this.jsonNode.iterator();
		List<Essence> result = new ArrayList<Essence>();
		
		while (iter.hasNext()) {
			result.add(new Essence(iter.next()));
		}
		
		return result;
	}

	/**
	 * idで指定されたEssenceを取得する
	 * @param id
	 * @return
	 */
	public Essence get(String id) {
		Essence essence = null;
		Iterator<Essence> iterator = this.get().iterator();
		try {
			while (iterator.hasNext()) {
				if (id.equals(iterator.next().getId())) {
					essence = iterator.next();
					break;
				}
			}
		} catch (Exception e) {
			throw new UnsupportedOperationException();
		}
		return essence;
	}

	/**
	 * 全EssenceのId一覧を取得する
	 * @return
	 */
	public List<String> getIds() throws AssetModelsException {
		// これもfindでうまく集められないかしら。。。ひとまずベタな処理で。
		Iterator<JsonNode> iter = this.jsonNode.iterator();
		List<String> result = new ArrayList<String>();
		
		while (iter.hasNext()) {
			Essence essence = new Essence(iter.next());
			result.add(essence.getId());
		}
		
		return result;
	}
	
	/**
	 * 全エッセンスのLocationパス一覧を取得する
	 * @return　全エッセンスのlocation
	 */
	public List<MwaUri> getLocations() throws AssetModelsException, MwaUriException {
		Iterator<JsonNode> iter = this.jsonNode.iterator();
		List<MwaUri> result = new ArrayList<MwaUri>();
		
		while (iter.hasNext()) {
			Essence essence = new Essence(iter.next());
			result.add(essence.getLocation());
		}
		return result;
	}

	/**
	 * フィルタ条件に合致したEssenceのlocationパス一覧を取得する
	 * @param filter
	 * @return
	 * @throws AssetModelsException
	 * @throws MwaUriException
	 */
	public List<MwaUri> getLocations(EssenceFilter filter) throws AssetModelsException, MwaUriException {
		Iterator<JsonNode> iter = this.jsonNode.iterator();
		List<MwaUri> result = new ArrayList<MwaUri>();
		
		while (iter.hasNext()) {
			Essence essence = new Essence(iter.next());
			
			if (!filter.accept(essence)) {
				continue;
			}
			result.add(essence.getLocation());
		}
		return result;
	}
}
