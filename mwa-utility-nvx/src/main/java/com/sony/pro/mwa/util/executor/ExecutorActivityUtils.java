package com.sony.pro.mwa.util.executor;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;
import com.sony.pro.mwa.util.nvx.core.ICoreEnvSupplier;
import org.apache.commons.lang3.StringUtils;

/**
 * ExecutorActivity のユーティリティ機能を提供します
 *
 * @author Kousuke Hosaka
 */
public class ExecutorActivityUtils {
    private static MwaLogger logger = MwaLogger.getLogger(ExecutorActivityUtils.class);

    /**
     * operationResult から ActivityInstanceModel の ID を取得します
     * @param operationResult
     * @return
     */
    public static String getValue(String key, OperationResult operationResult) {
//        if (!operationResult.containsKey("ActivityInstanceId") ||
        if (!operationResult.containsKey(key) ||
                operationResult.get(key) == null) {
            throw new MwaError(MWARC.OPERATION_FAILED_PROCESS_NOT_FOUND);
        }
        String id = (String) operationResult.get(key);
        if (StringUtils.isEmpty(id)) {
            throw new MwaError(MWARC.OPERATION_FAILED_PROCESS_NOT_FOUND);
        }
        return id;
    }

    /**
     * 実行中のActivity がTerminated 状態になるまで待ち合わせします
     * @param activityInstanceModel
     * @return
     */
    public static boolean isTerminatedStatus(ActivityInstanceModel activityInstanceModel) {
        logger.info(String.format("Start waiting for the end of the task. ActivityInstanceId : %s", activityInstanceModel.getId()));
        if (!activityInstanceModel.getStatus().isTerminated()) {
            return false;
        }
        logger.info(String.format("Waiting for the end of the task is completed. ActivityInstanceId : %s", activityInstanceModel.getId()));
        return true;
    }

    /**
     * 実行中のActivity がTerminated 状態になるまで待ち合わせします
     * @return
     */
    public static ActivityInstanceModel waitTerminating(String instanceId, ICoreEnvSupplier coreEnvSupplier) {
        ActivityInstanceModel activityInstanceModel = coreEnvSupplier.getActivityInstanceModel(instanceId);
        logger.info(String.format("Start waiting for the end of the task. ActivityInstanceId : %s", activityInstanceModel.getId()));
        while (!activityInstanceModel.getStatus().isTerminated()) {
            activityInstanceModel = coreEnvSupplier.getActivityInstanceModel(instanceId);
        }
        logger.info(String.format("Waiting for the end of the task is completed. ActivityInstanceId : %s", activityInstanceModel.getId()));
        return activityInstanceModel;
    }
}