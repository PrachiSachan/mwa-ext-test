package com.sony.pro.mwa.util.nvx.asset.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.sony.pro.mwa.util.exceptions.MwaUriException;
import com.sony.pro.mwa.util.nvx.asset.models.exceptions.AssetModelsException;
import com.sony.pro.mwa.util.uri.MwaUri;

/**
 * Asset.essencesに含まれる１エッセンスを処理するクラス
 */
public class Essence extends ObjectNodeBase {

	/**
	 * コンストラクタ
	 * @param essence
	 */
	public Essence(JsonNode essence) {
		super(essence);
	}
	
	/**
	 * IDを取得する
	 * @return エッセンスId
	 */
	public String getId() throws AssetModelsException {
		JsonNode id = this.getNodeValue(Constants.Id);

		if (id == null) 
			return null;
		if (!id.isTextual()) {
			throw AssetModelsException.supplyJsonIsInvalid("Essense id's type is invalid.");
		}
		
		return id.asText();
	}
	
	/**
	 * primaryプロパティを取得する。
	 * 
	 * @return
	 * @throws AssetModelsException
	 */
	public boolean isPrimary() throws AssetModelsException {
		return this.getNodeValue(Constants.Essence.Primary).asBoolean();
	}
	
	/**
	 * proxyプロパティの値を取得する。
	 * 
	 * TODO：型チェック保留中。"true"とtrueの話題があった気がするが。。。
	 * @return
	 */
	public boolean isProxy() throws AssetModelsException {
		return this.getNodeValue(Constants.Essence.Proxy).asBoolean();
	}
	
	/**
	 * statusプロパティの値を取得する。
	 * 
	 * @return
	 * @throws AssetModelsException
	 */
	public String getStatus() throws AssetModelsException {
		JsonNode status = this.getNodeValue(Constants.Essence.Status);
		if (status == null) 
			return null;
		if (!status.isTextual()) {
			throw AssetModelsException.supplyJsonIsInvalid("Essense status's type is invalid.");
		}
		return status.asText();
	}
	
	/**
	 * typeプロパティの値を取得する。
	 * 
	 * @return
	 * @throws AssetModelsException
	 */
	public String getType() throws AssetModelsException {
		JsonNode type = this.getNodeValue(Constants.Essence.Type);

		if (type == null) 
			return null;
		if (!type.isTextual()) {
			throw AssetModelsException.supplyJsonIsInvalid("Essense assetType's type is invalid.");
		}
		return type.asText();
	}
	
	/**
	 * locationパスを取得する
	 * @return
	 * @throws AssetModelsException
	 */
	public MwaUri getLocation() throws AssetModelsException, MwaUriException {
		JsonNode location = this.getNodeValue(Constants.Essence.Location);
		
		if (location == null) 
			return null;
		if (!location.isTextual()) {
			throw AssetModelsException.supplyJsonIsInvalid("Essense location's type is invalid.");
		}
		
		return new MwaUri(location.asText());
	}
}
