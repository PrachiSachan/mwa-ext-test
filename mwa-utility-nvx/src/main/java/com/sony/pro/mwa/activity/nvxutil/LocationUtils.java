package com.sony.pro.mwa.activity.nvxutil;

import java.util.List;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.model.storage.LocationCollection;
import com.sony.pro.mwa.model.storage.LocationModel;
import com.sony.pro.mwa.service.ICoreModules;

/**
 * NavigatorX における Volume に関連したユーティリティー機能を提供します。
 */
public class LocationUtils {
	private static MwaLogger logger = MwaLogger.getLogger(LocationUtils.class);
	/**
	 * ロケーション名に紐付く Location ID の取得
	 *
	 * @param locationName ロケーション名
	 * @param core CoreModule
	 * @return 取得した locationModel のId
	 */
	public static String getLocationId(String locationName, ICoreModules core) {
		LocationCommandService command = new LocationCommandService(core);
		LocationCollection collection = command.getLocation();
		List<LocationModel> list = (List<LocationModel>) collection.getModels();// ロケーションからすべてのロケーションモデルを取得
		String locationId = null;

		for (LocationModel model : list) {
			if (locationName.equals(model.getName())) {
				locationId = model.getId();
			}
		}

		return locationId;
	}
}