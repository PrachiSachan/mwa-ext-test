package com.sony.pro.mwa.activity.nvxutil;

import io.vertx.core.json.JsonObject;

import java.util.List;

import org.springframework.util.StringUtils;
import com.sony.pro.mwa.activity.scheme.httpclient.HttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.httpclient.HttpResponseResult;
import com.sony.pro.mwa.activity.scheme.httpclient.IHttpClientWrapper;
import com.sony.pro.mwa.activity.scheme.utils.EndpointProvider;
import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.service.ICoreModules;

public class ActivityProviderCommandService extends CommandServiceBase {
	private static MwaLogger logger = MwaLogger.getLogger(ActivityProviderCommandService.class);
	private final static String BASE_URI = "/mwa/api/v2/activity-providers";

	/**
	 * 引数付きコンストラクタを提供します。
	 *
	 * @param coreModules
	 */
	public ActivityProviderCommandService(ICoreModules coreModules) {
		super(coreModules);
	}
	
	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public JsonObject getActivityProvider() {
		return getActivityProvider(null, null, null, null);
	}

	/**
	 * GET リクエストによるレスポンスをデータモデルにて取得します。
	 *
	 * @return
	 */
	public JsonObject getActivityProvider(List<String> sort, List<String> filter, String offset, String limit) {
		// activity-providers 取得
		String url = EndpointProvider.getURLAsNvxProvider(this.getCoreModules(), BASE_URI);
		String urlWithQuery = url;

		// TODO: 後々は引数をクエリパラメータ化する必要がある.
		// いまいまは何も渡さない実装にしておくので修正が必要。
		HttpResponseResult httpResponse = new HttpResponseResult();

		try {
			IHttpClientWrapper client = new HttpClientWrapper();
			httpResponse = client.get(urlWithQuery, this.getHttpRequestProvider());
			logger.debug(httpResponse.getResponseBody());
		} catch (Exception e) {
			String errorMessage = "Failed to get activity-providers from navigatorx.";
			logger.error(errorMessage, e);
		}

		JsonObject jsonModel = null;

		// TODO: レスポンスボディから ActivityProviderCollection にデシリアライズする。
		if (!StringUtils.isEmpty(httpResponse.getResponseBody())) {
			jsonModel = new JsonObject(httpResponse.getResponseBody());
		}

		return jsonModel;
	}

}
