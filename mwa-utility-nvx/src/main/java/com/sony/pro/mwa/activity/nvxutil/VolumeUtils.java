package com.sony.pro.mwa.activity.nvxutil;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sony.pro.mwa.exception.MwaError;
import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.util.executor.ExecutorActivityUtils;
import com.sony.pro.mwa.util.nvx.core.CoreEnvSupplier;
import com.sony.pro.mwa.util.nvx.core.ICoreEnvSupplier;
import com.sony.pro.mwa.util.text.TextConverter;
import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.parameter.OperationResult;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.service.ICoreModules;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * NavigatorX における Volume に関連したユーティリティー機能を提供します。
 */
public class VolumeUtils {
	private static MwaLogger logger = MwaLogger.getLogger(VolumeUtils.class);
	// private static final MessageHolder messageHolder = MessageHolder.getInstance();

	public static List<String> getOnlineCartridgeNames(ICoreModules core) {
		ArchiveVolumesCommandService volumeCommand = new ArchiveVolumesCommandService(core);

		// ※リファクタリング対象、JsonObjectではなくVolumeModelで取れるようにする
		JsonObject volumes = volumeCommand.getArchiveVolumes();
		if (volumes == null) {
			return null;
		}

		JsonArray volumeModelsJsonArray = volumes.getJsonArray("models");
		List<JsonObject> volumeModels = new ArrayList<JsonObject>();
		Iterator<Object> vitr = volumeModelsJsonArray.iterator();
		while (vitr.hasNext()) {
			volumeModels.add((JsonObject) vitr.next());
		}

		List<String> onlineCartridgeNames = new ArrayList<>();

		for (JsonObject volume : volumeModels) {
			if (StringUtils.isNotEmpty(volume.getString("onlinePlaceId")) && StringUtils.isNotEmpty(volume.getString("serial"))) {
				onlineCartridgeNames.add(volume.getString("serial"));
			}
		}

		return onlineCartridgeNames;
	}

	/**
	 * Serialに紐づくVolumeModelのId取得
	 *
	 * @param serial カートリッジのシリアルナンバー
	 * @param core CoreModule
	 * @return 取得したVolumeModelのId
	 */
	public static String getVolumeIdBySerial(String serial, ICoreModules core) {
		if (serial.contains("%20")){
			serial = serial.replace("%20", " ");
		}
		ArchiveVolumesCommandService volumeCommand = new ArchiveVolumesCommandService(core);
		List<String> filters = new ArrayList<String>();
		filters.add("serial==" + serial);
		JsonObject volumeModels = volumeCommand.getArchiveVolumes(null, null, filters, null, null);
		if (volumeModels != null) {
			for (Object volumeObject: volumeModels.getJsonArray("models")) {
				JsonObject volumeJson = (JsonObject) volumeObject;
				if (serial.equals(volumeJson.getString("serial"))) {
					return volumeJson.getString("id");
				}
			}
		}
		return null;
	}

    /**
     * 引数としてカートリッジシリアルIDを元にして、Volume Model にある Provider ID を取得します。
     *
     * 戻り値：null の場合、シリアルが一致しないとき、および、シリアルが一致しているけど Provider ID が存在しない時
     * 戻り値：Provider ID の場合、シリアルが一致（%20または空白）のどちらにも対応している
     *
     * @param serial
     * @param core
     * @return
     */
    public static String getProviderIdByCartridgeSerial(String serial, ICoreModules core) {
        try {
            serial = TextConverter.decode(serial);
        } catch (UnsupportedEncodingException e) {
            throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, "Failed to decode serial : ."+ serial);
        }

        ArchiveVolumesCommandService volumeCommand = new ArchiveVolumesCommandService(core);
        JsonObject volumes = volumeCommand.getArchiveVolumes();
        if (volumes == null) {
            return null;
        }
        
        String placeId = StringUtils.EMPTY;
        for (Object volumeObject : volumes.getJsonArray("models")) {
            JsonObject volume = (JsonObject) volumeObject;
            if (serial.equals(volume.getString("serial"))) {
                placeId = volume.getString("onlinePlaceId");
                if (StringUtils.isEmpty(placeId)) {
                    placeId = volume.getString("offlinePlaceId");
                }
                break;
            }
        }
        if (StringUtils.isEmpty(placeId)) {
            return null;
        }

        ArchivePlaceCommandService placeCommand = new ArchivePlaceCommandService(core);
        JsonObject places = placeCommand.getArchivePlaces();
        if (places == null) {
            return null;
        }
        
        String providerId = null;
        for (Object placeObject : places.getJsonArray("models")) {
            JsonObject place = (JsonObject) placeObject;
            if (placeId.equals(place.getString("id"))) {
                providerId = place.getString("activityProviderId");
                break;
            }
        }
        // providerId がない場合もあるので例外を出さずに終了
        return providerId;
    }

	/**
	 * Cartridgeのステータスが"NORMAL"(使用可能)のときのみ、ProviderIdを返却するメソッド
	 * 2018.2.0対応用メソッドなのでいずれ削除します。使わないでください。
	 *
	 * @param cartridgeSerial
	 * @param core
	 * @return
	 */
	public static String getProviderIdByAvailableCartridgeSerial(String cartridgeSerial, ICoreModules core) {
		ArchiveVolumesCommandService volumeCommand = new ArchiveVolumesCommandService(core);

		// ※リファクタリング対象、JsonObjectではなくVolumeModelで取れるようにする
		JsonObject volumes = volumeCommand.getArchiveVolumes();
		if (volumes == null) {
			return null;
		}

		JsonArray volumeModelsJsonArray = volumes.getJsonArray("models");
		List<JsonObject> volumeModels = new ArrayList<JsonObject>();
		Iterator<Object> vitr = volumeModelsJsonArray.iterator();
		while (vitr.hasNext()) {
			volumeModels.add((JsonObject) vitr.next());
		}

		String providerId = null;

		if (cartridgeSerial.contains("%20")){
			cartridgeSerial = cartridgeSerial.replace("%20", " ");
		}

		for (JsonObject volume : volumeModels) {
			if (cartridgeSerial.equals(volume.getString("serial")) && "NORMAL".equals(volume.getJsonObject("odsCartridgeInfo").getString("status"))) {
				providerId = volume.getString("providerId");
			}
		}

		// providerId がない場合もあるので例外を出さずに終了
		return providerId;
	}

    /**
     * PART サービスからカートリッジ情報を取得したいときに、サーバー側と ODA クライアント側で両方見て取得を試みます。
     * もし取得できなかった場合には　null を返却します。
     *
     * @param parentInstanceId
     * @param cartridgeId
     * @param providerId
     * @param coreModule
     * @return
     */
    public static String getCartridgeInfoXMLFromGetCartridgeInfoTask(String parentInstanceId, String cartridgeId, String providerId, ICoreModules coreModule) {
        final String CARTRID_INFO_XML = "cartridInfoXml";
        String result = null;
        String cartridgeIdList = FileManagementUtils.serializeMwaListString(cartridgeId);

        logger.info("will go looking at server cartidge. ID:" + cartridgeId);
        OperationResult operationResult = null;
        try {
            operationResult = ActivityManagementUtils.createGetCartridgeInfoTask(parentInstanceId, cartridgeIdList, providerId, coreModule);
        } catch (MwaError e) {
            switch ((MWARC)e.getMWARC()) {
                case INTEGRATION_ERROR :
                    logger.warn(e.getMessage());
                    break;
                case DEVICE_ERROR:
                    throw new MwaInstanceError(MWARC.DEVICE_ERROR, e);
                default :
                    throw new MwaInstanceError(MWARC.CONNECTION_FAILED);
            }
        }

        /***************************************** ここから追加 **********************************************/
        ActivityInstanceModel activityInstanceModel = null;
        // TODO いずれは引数の coreModule を削除して CoreEnvSupplier のみ受け取るようにする
        ICoreEnvSupplier coreEnvSupplier = new CoreEnvSupplier(coreModule);
        activityInstanceModel = ExecutorActivityUtils.waitTerminating(ExecutorActivityUtils.getValue("ActivityInstanceId", operationResult), coreEnvSupplier);
        if (ExecutorActivityUtils.isTerminatedStatus(activityInstanceModel)) {
            /**
             * 基底クラスをSyncTask からExecutorTask に変更したため、子Activity の終了を待つ
             * Cancel の場合でも、子Activity のメインスレッドは最後まで走り切るので、
             * getStatus().isCancel() はハンドリングせずに通常通り処理を実行する
             */
            if (activityInstanceModel.getStatus().isError()) {
                throw new MwaError(MWARC.OPERATION_FAILED_RESOURCE_INCOMPATIBLE);
            }
            if (!activityInstanceModel.getOutputDetails().containsKey(CARTRID_INFO_XML)) {
                // もし存在していなかったら、ODA Client 側に問い合わせにいく必要がある。
                // 今まで通りのサーバーだけのケースであれば、Provider ID は存在しない。
                String odaClientProviderId = VolumeUtils.getProviderIdByCartridgeSerial(cartridgeId, coreModule);
                logger.info(String.format("### oda client provider id : %s", odaClientProviderId));
                if (StringUtils.isNotEmpty(odaClientProviderId)) {
                    logger.info("will go looking at client cartridge. ID:" + cartridgeId);
                    try {
                        operationResult = ActivityManagementUtils.createClientGetCartridgeInfoTask(parentInstanceId, cartridgeIdList, odaClientProviderId, coreModule);
                    }  catch (MwaError e) {
                        switch ((MWARC)e.getMWARC()) {
                            case INTEGRATION_ERROR :
                                logger.warn(e.getMessage());
                                break;
                            case DEVICE_ERROR:
                                throw new MwaInstanceError(MWARC.DEVICE_ERROR, e);
                            default :
                                throw new MwaInstanceError(MWARC.CONNECTION_FAILED);
                        }
                    }
                    activityInstanceModel = ExecutorActivityUtils.waitTerminating(ExecutorActivityUtils.getValue("ActivityInstanceId", operationResult), coreEnvSupplier);
                    if (ExecutorActivityUtils.isTerminatedStatus(activityInstanceModel)) {
                        if (activityInstanceModel.getStatus().isError()) {
                            throw new MwaInstanceError(MWARC.OPERATION_FAILED_RESOURCE_INCOMPATIBLE, "",activityInstanceModel.getStatusDetails().getDeviceResponseDetails());
                        }
                    }
                }
            }
            result = (String) activityInstanceModel.getOutputDetails().get(CARTRID_INFO_XML);
        }
        /***************************************** ここまで **********************************************/
        return result;
    }

	/**
	 * VolumeModelの更新処理(PATCH)
	 *
	 * @param updateVolumeId 更新されるVolumeのID
	 * @param requestBodyJson 更新するためのJson
	 * @param core CoreModule
	 * @return 更新したVolumeModelのJsonObject
	 */
	public static JsonObject patchVolume(String updateVolumeId, JsonObject requestBodyJson, ICoreModules core) {
		ArchiveVolumesCommandService volumeCommand = new ArchiveVolumesCommandService(core);
		return volumeCommand.patchArchiveVolumes(updateVolumeId, requestBodyJson.encodePrettily());
	}

	/**
	 * VolumeModelの更新処理(PATCH)
	 *
	 * ※リファクタリング対象
	 * プールID の更新が Internal が付いたベースパスだとできないため、
	 * Internal が付いてないベースパスで更新するメソッドを用意した
	 *
	 * @param updateVolumeId 更新されるVolumeのID
	 * @param requestBodyJson 更新するためのJson
	 * @param core CoreModule
	 * @return 更新したVolumeModelのJsonObject
	 */
	public static JsonObject patchVolumeNotInternal(String updateVolumeId, JsonObject requestBodyJson, ICoreModules core) {
		ArchiveVolumesCommandService volumeCommand = new ArchiveVolumesCommandService(core);
		return volumeCommand.patchArchiveVolumesNotInternal(updateVolumeId, requestBodyJson.encodePrettily());
	}

	/**
	 * URLエンコードされたスペースを含む文字列に対して、スペース文字のデコードをします
	 *
	 * @param arg 文字列
	 * @return スペース文字をデコードした文字列
	 */
	public static String decodeSpace(String arg) {
		if (arg.contains("%20")) {
			arg = arg.replace("%20", " ");
		}
		return arg;
	}

	/**
	 * 入力パラメータ poolId と一致する NAS用のArchiveVolumeModel を取得します。
	 * @param poolId
	 * @return
	 */
	public static JsonObject getVolumeModel(String poolId, ICoreModules core) {
		JsonObject result = null;
		JsonArray volumeModels = getVolumeModels(core);
		if (volumeModels == null) {
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, null);
		}
		for (int i = 0; i < volumeModels.size(); i++) {
			JsonObject volume = volumeModels.getJsonObject(i);
			if (volume != null && volume.containsKey("poolId") && poolId.equals(volume.getString("poolId"))) {
				result = volume;
				break;
			}
		}
		return result;
	}

	/**
	 * ArchiveVolumeCollectionの取得を行います
	 * @return
	 */
	public static JsonArray getVolumeModels(ICoreModules core) {
		ArchiveVolumesCommandService archiveVolumesCommandService = new ArchiveVolumesCommandService(core);
		JsonObject archiveVolumeCollection = new JsonObject();
		String errMsg = "Internal communication error was occured. Please contact the system administrator.";
		try {
			archiveVolumeCollection = archiveVolumesCommandService.getArchiveVolumes();
			if (archiveVolumeCollection == null || !archiveVolumeCollection.containsKey("models")) {
				logger.error("Failed to get information on Archive Volume.");
				throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, errMsg);
			}
		} catch (Exception e) {
			logger.error("An error occurred. Failed to get information on Archive Volume.", e);
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, errMsg);
		}

		JsonArray volumeModelsArray = new JsonArray();
		if (!archiveVolumeCollection.isEmpty()) {
			volumeModelsArray = archiveVolumeCollection.getJsonArray("models");
		}
		return volumeModelsArray;
	}
}