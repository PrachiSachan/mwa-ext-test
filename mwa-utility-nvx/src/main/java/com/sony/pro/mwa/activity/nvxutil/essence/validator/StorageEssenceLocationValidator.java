package com.sony.pro.mwa.activity.nvxutil.essence.validator;

import java.util.List;

import org.apache.commons.lang3.StringUtils;



public class StorageEssenceLocationValidator extends AbstractEssenceLocationValidator {

	/**
	 * primary のエッセンスロケーションパスが一致しているかどうかを返します。
	 * @param targetEssencePath
	 * @param primaryEssenceLocationPaths
	 * @return
	 */
	@Override
	protected boolean validPrimaryEssence(String targetEssencePath, List<String> primaryEssenceLocationPaths) {
		if (StringUtils.isNotEmpty(targetEssencePath)) {
			for (String essenceLocation : primaryEssenceLocationPaths) {
				if (targetEssencePath.equals(essenceLocation)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * proxy のエッセンスロケーションパスが一致しているかどうかを返します。
	 * @param targetEssencePath
	 * @param proxyEssenceLocationPaths
	 * @return
	 */
	@Override
	protected boolean validProxyEssence(String targetEssencePath, List<String> proxyEssenceLocationPaths) {
		// 現状はprimary とproxy で同一処理を実装しておく。
		// 今後、proxy で独自のvalidate が必要になった場合は、このメソッドを修正する必要があります。
		return this.validPrimaryEssence(targetEssencePath, proxyEssenceLocationPaths);
	}
}
