package com.sony.pro.mwa.activity.nvxutil.essence.filter;

import java.util.ArrayList;
import java.util.List;

public class EssenceLocationModel {
	List<String> primaryEssenceLocation;
	List<String> proxyEssenceLocation;
	
	public EssenceLocationModel () {
		primaryEssenceLocation = new ArrayList<>();
		proxyEssenceLocation = new ArrayList<>();
	}
}
