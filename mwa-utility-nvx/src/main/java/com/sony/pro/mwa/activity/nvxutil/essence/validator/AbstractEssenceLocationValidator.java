package com.sony.pro.mwa.activity.nvxutil.essence.validator;

import java.util.List;

import com.sony.pro.mwa.activity.nvxutil.essence.filter.EssenceLocationFilter;
import com.sony.pro.mwa.service.ICoreModules;
import com.sony.pro.nvx.enumeration.EssenceStatusEnum;

import io.vertx.core.json.JsonObject;

abstract class AbstractEssenceLocationValidator implements IEssenceLocationValidator {
	
	/**
	 * アセットがアーカイブ可能かどうかを判定します。
	 */
	public boolean validDuplicateArchive(JsonObject assetModel, ICoreModules coreModules, String archivePrimaryMwaPath, String archiveProxyMwaPath) {
		boolean isDuplicatePrimary = false;
		boolean isDuplicateProxy = false;
		Boolean isArchived = assetModel.getJsonObject("assetInfo").getBoolean("archived");
		String assetStatus = assetModel.getJsonObject("assetInfo").getString("status");
		EssenceLocationFilter essenceLocationFilter = new EssenceLocationFilter(assetModel, coreModules);
				
		isDuplicatePrimary = validPrimaryEssence(archivePrimaryMwaPath, essenceLocationFilter.getArchivedPrimaryEssences());
		isDuplicateProxy = validProxyEssence(archiveProxyMwaPath, essenceLocationFilter.getArchivedProxyEssences());
		
		if (isArchived && EssenceStatusEnum.ONLINE.toAlias().equalsIgnoreCase(assetStatus) && (isDuplicatePrimary || isDuplicateProxy)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 抽象メソッド
	 */
	protected abstract boolean validPrimaryEssence(String targetEssencePath, List<String> primaryEssenceLocationPaths);
	
	/**
	 * 抽象メソッド
	 */
	protected abstract boolean validProxyEssence(String targetEssencePath, List<String> proxyEssenceLocationPaths);
}
