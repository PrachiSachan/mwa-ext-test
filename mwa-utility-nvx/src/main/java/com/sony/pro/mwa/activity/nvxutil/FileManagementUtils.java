package com.sony.pro.mwa.activity.nvxutil;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sony.pro.mwa.exception.MwaError;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import com.sony.pro.mwa.common.log.MwaLogger;
import com.sony.pro.mwa.exception.MwaInstanceError;
import com.sony.pro.mwa.rc.MWARC;
import com.sony.pro.mwa.util.exceptions.NLEExportException;
import com.sony.pro.mwa.util.text.TextConverter;

/**
 * NavigatorX におけるファイル操作に関連したユーティリティー機能を提供します。
 */
public class FileManagementUtils {

	private static MwaLogger logger = MwaLogger.getLogger(FileManagementUtils.class);

	private static String[] prefixList = {
			"CON", "PRN", "AUX", "NUL", "CLOCK$",
			"COM0", "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9",
			"LPT0", "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9"
			};
	private static String[] symbolList = { "\r\n", "\n", "\\", "/", ":", "*", "?", "\"", "<", ">", "|", "\t" };

	/**
	 * 任意の複数ファイルを指定のディレクトリにコピーします。(既に存在していれば上書きする）
	 *
	 * @param sourceFilePathList コピー元になるファイルパス（複数）
	 * @param destinationDirPath 出力先ディレクトリパス
	 * @return コピー先の複数ファイルパスリスト
	 * @throws IOException
	 */
	public static List<Path> copyFilesAsOverwrite(List<Path> sourceFilePathList, Path destinationDirPath) throws IOException {
		List<Path> copiedFilePathList = new ArrayList<>();
		for (Path sourceFilePath : sourceFilePathList) {
			Path copiedFilePath = destinationDirPath.resolve(sourceFilePath.getFileName());
			Files.copy(sourceFilePath, copiedFilePath, StandardCopyOption.REPLACE_EXISTING);
			copiedFilePathList.add(copiedFilePath);
		}
		LogUtils.logForPath("FileManagementUtils_copyFilesAsOverwrite_copiedFilePathList", copiedFilePathList);
		return copiedFilePathList;
	}

	/***
	 *
	 * 任意の複数ファイルを指定のディレクトリにコピーします。(既に存在していればリネームをする）
	 *
	 * @param sourceFilePathList
	 * @param destinationDirPath
	 * @return
	 * @throws IOException
	 */
	public static List<Path> copyFilesAsRename(List<Path> sourceFilePathList, Path destinationDirPath) throws IOException {
		List<Path> copiedFilePathList = reserveFilePathListForRename(sourceFilePathList, destinationDirPath);

		// ファイルパスのリネームのために予約が済んでからコピーする。
		for (int index = 0; index < sourceFilePathList.size(); index ++) {
			Files.copy(sourceFilePathList.get(index), copiedFilePathList.get(index), StandardCopyOption.REPLACE_EXISTING);
		}

		LogUtils.logForPath("FileManagementUtils_copyFilesAsRename_copiedFilePathList", copiedFilePathList);
		return copiedFilePathList;
	}

	/**
	 * 任意の複数ファイルを指定のディレクトリにハードリンクファイルを作成します。(既に存在していれば上書きする）
	 *
	 * @param sourceFilePathList コピー元になるファイルパス（複数）
	 * @param destinationDirPath 出力先ディレクトリパス
	 * @return リンク先の複数ファイルパスリスト
	 * @throws IOException
	 */
	public static List<Path> hardLinkAsOverwrite(List<Path> sourceFilePathList, Path destinationDirPath) throws IOException {
		List<Path> linkedFilePathList = new ArrayList<>();
		for (Path sourceFilePath : sourceFilePathList) {
			Path linkedFilePath = destinationDirPath.resolve(sourceFilePath.getFileName());
			if (linkedFilePath.toFile().exists()) {
				linkedFilePath.toFile().delete();
			}
			Files.createLink(linkedFilePath, sourceFilePath);
			linkedFilePathList.add(linkedFilePath);
		}
		LogUtils.logForPath("FileManagementUtils_hardLinkAsOverwrite_linkedFilePathList", linkedFilePathList);
		return linkedFilePathList;
	}

	/**
	 *
	 * 任意の複数ファイルを指定のディレクトリにハードリンクファイルを作成します。(転送対象(sourceFilePathListに含まれるパス)が重複する場合はリネームする）
	 *
	 * @param sourceFilePathList
	 * @param destinationDirPath
	 * @return
	 * @throws IOException
	 */
	public static List<Path> hardLinkAsRename(List<Path> sourceFilePathList, Path destinationDirPath) throws IOException {
		List<Path> linkedFilePathList = reserveFilePathListForRename(sourceFilePathList, destinationDirPath);
		for (int index = 0; index < sourceFilePathList.size(); index++) {
			Path convertedSrcPath = sourceFilePathList.get(index);
			Path convertedDestPath = linkedFilePathList.get(index);
			try {
				if (!TextConverter.validIp(convertedSrcPath.toString())
						&& !TextConverter.isWindows(convertedSrcPath.toString())) {
					convertedSrcPath = Paths.get(TextConverter.convertHostToIP(convertedSrcPath.toString()));
				}
				if (!TextConverter.validIp(convertedDestPath.toString())
						&& !TextConverter.isWindows(convertedDestPath.toString())) {
					convertedDestPath = Paths.get(TextConverter.convertHostToIP(convertedDestPath.toString()));
				}
			} catch (MwaError e) {
				throw new MwaInstanceError(e.getMWARC(), e.getDeviceResponseCode(),
						"Failed to convert UNC path containing hostname to UNC path containing IP Address.");
			}
			Files.createLink(convertedDestPath, convertedSrcPath);
		}
		LogUtils.logForPath("FileManagementUtils_hardLinkAsRename_linkedFilePathList", linkedFilePathList);
		return linkedFilePathList;
	}

	/**
	 * ファイルのハードリンクを作成します。転送先に同じ名前のファイルがある場合はリネームします。
	 *
	 * @param srcPath
	 * @param destPath
	 *
	 * @throws IOException
	 */
	public static Path hardLink(Path srcPath, Path destPath) throws IOException {

		/**************************************************************************
		 * 以下の処理では。ホスト名を含むUNCパスをIPアドレス形式のUNCパスに変換します。
		 * また、本メソッドが返すべき値に変更を加えたくないため、ハードリンク処理用と返却値用に
		 * destPath, convertedDestPath という変数を用意しています。
		 **************************************************************************/
		Path convertedSrcPath = srcPath;
		Path convertedDestPath = destPath;
		try {
			if (!TextConverter.validIp(convertedSrcPath.toString())
					&& !TextConverter.isWindows(convertedSrcPath.toString())) {
				convertedSrcPath = Paths.get(TextConverter.convertHostToIP(convertedSrcPath.toString()));
			}
			if (!TextConverter.validIp(convertedDestPath.toString())
					&& !TextConverter.isWindows(convertedDestPath.toString())) {
				convertedDestPath = Paths.get(TextConverter.convertHostToIP(convertedDestPath.toString()));
			}
		} catch (MwaError e) {
			throw new MwaInstanceError(e.getMWARC(), e.getDeviceResponseCode(),
					"Failed to convert UNC path containing hostname to UNC path containing IP Address.");
		}

		LogUtils.log("linkFile start.");
		LogUtils.log("linkFile: " + convertedSrcPath + " -> " + convertedDestPath);
		// srcPath存在チェック
		if (!Files.exists(convertedSrcPath)) {
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, "Source file does not exist.");
		}

		// 上書き対象がある場合
		if (Files.exists(convertedDestPath)) {
			LogUtils.log("File has already existed. Start to rename file. target -> " + convertedDestPath);
			try {
				// ハードリンク処理用パス
				convertedDestPath = TextConverter.renameFile(convertedDestPath);
				// メソッド出力用パス
				destPath = TextConverter.renameFile(destPath);
			} catch (NLEExportException e) {
				throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, e.getMessage());
			}
			LogUtils.log("Renamed. target -> " + convertedDestPath);
		}

		if (convertedDestPath == null || StringUtils.isEmpty(convertedDestPath.toString())) {
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, "Failed to rename destination file name.");
		}

		Files.createLink(convertedDestPath, convertedSrcPath);
		LogUtils.log("linkFile end.");
		return destPath;
	}

	/**
	 * 指定された文字列のリストを MWA 側で定義される <list><string>xxx</string></list> 形式からデシリアライズします。
	 *
	 * @param string 指定された文字列のリスト
	 * @return デシリアライズされた文字列
	 */
	public static String deserializeMwaListString(String string) {
		if (string.contains("<list>")){
			string = string.replace("<list>", "");
		}
		if (string.contains("<string>")){
			string = string.replace("<string>", "");
		}
		if (string.contains("</string>")){
			string = string.replace("</string>", "");
		}
		if (string.contains("</list>")){
			string = string.replace("</list>", "");
		}
		logger.info("Deserialized string:" + string);
		return string;
	}

	/**
	 * 指定された文字列のリストを MWA 側で定義される <list><string>xxx</string></list> 形式からデシリアライズします。
	 *
	 * @param string 指定された文字列のリスト
	 * @return デシリアライズされた文字列
	 */
	public static List<String> deserializeMwaListToList(String string) {
		if (string.contains("<list>")) {
			string = string.replace("<list>", "");
		}
		if (string.contains("</list>")) {
			string = string.replace("</list>", "");
		}
		if (string.contains("<string>")) {
			string = string.replace("<string>", "");
		}
		if (string.contains("</string>")) {
			string = string.replace("</string>", ",");
		}
		List<String> result = string.isEmpty() ? new ArrayList() : Arrays.asList(string.split(",", 0));
		LogUtils.log("FileManagementUtils_deserializeMwaListToList_result", result);
		return result;
	}

	/**
	 * 指定された文字列のリストを MWA 側で定義される <list><string>xxx</string></list> 形式にシリアライズします。
	 *
	 * @param stringList 指定された文字列のリスト
	 * @return シリアライズされた文字列
	 */
	public static String serializeMwaListString(List<String> stringList) {
		StringBuilder sb = new StringBuilder();
		sb.append("<list>");
		for (String str : stringList) {
			sb.append("<string>");
			sb.append(StringEscapeUtils.escapeXml(str));
			sb.append("</string>");
		}
		sb.append("</list>");
		logger.info("Serialize string list:" + sb.toString());
		return sb.toString();
	}

	/**
	 * 指定された文字列を MWA 側で定義される <list><string>xxx</string></list> 形式にシリアライズします。
	 *
	 * @param stringList 指定された文字列
	 * @return シリアライズされた文字列
	 */
	public static String serializeMwaListString(String str) {
		StringBuilder sb = new StringBuilder();
		sb.append("<list>");
		sb.append("<string>");
		sb.append(str);
		sb.append("</string>");
		sb.append("</list>");
		LogUtils.log("FileManagementUtils_serializeMwaListString_sb", sb.toString());
		return sb.toString();
	}

	/***
	 * export 予定の　File Path List に export したい File Path を登録する
	 *
	 * @param sourceFilePathList exportしたいFolrPathのList
	 * @param destinationDirPath export先のディレクトリパス
	 * @return exportするfile pathのlist
	 */
	public static List<Path> reserveFilePathListForRename(List<Path> sourceFilePathList, Path destinationDirPath) {
		List<Path> filePathList = new ArrayList<>();
		for (Path sourceFilePath : sourceFilePathList) {
			Path destinationFilePath = destinationDirPath.resolve(sourceFilePath.getFileName());
			destinationFilePath = reserveFilePathForRename(destinationFilePath, filePathList);
			if (destinationFilePath == null) {
				logger.warn(String.format("A file already exists and could not be changed to a new Path. [Path: %s]", sourceFilePath.toString()));
				final String errMsg = "Number of rename attempts of the copy destination reached the upper limit.";
				logger.error(errMsg);
				throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, errMsg);
			} else {
				filePathList.add(destinationFilePath);
			}
		}
		LogUtils.logForPath("FileManagementUtils_reserveFilePathListForRename_filePathList", filePathList);
		return filePathList;
	}

	/***
	 *
	 * exportしたいfilePath と 既にexportするために予約されているfilaPathList を比較し、
	 * 既にListに含まれている場合はexportしたいfileの名前を変更します
	 *
	 * @param filePath exportしたいfile Path
	 * @param reserveFilePathList 予約済みのfilePath
	 * @return 予約に追加されるfilePath
	 */
	public static Path reserveFilePathForRename(Path filePath, List<Path> reserveFilePathList) {

		LogUtils.log("FileManagementUtils_reserveFilePathForRename_filePath", filePath.toString());

		if (!reserveFilePathList.contains(filePath)) {
			// If the file with the same name does not exist yet, I will not do anything.
			return filePath;
		}

		// Retrieve extension and name from file name
		logger.info(String.format("The same file already exists in the specified Path. Start Rename of file name. [filePath : %s]", filePath));
		String parentPath = filePath.getParent().toString();
		String fileName = filePath.getFileName().toString();
		int extensionStartPosition = fileName.lastIndexOf(".");
		String extension = fileName.substring(extensionStartPosition);
		String noExtensionfile =  fileName.substring(0, extensionStartPosition);

		final int start = 1;
		final int limit = 99;
		for (int increment = start; increment <= limit; increment++) {
			Path newPath = Paths.get(parentPath, noExtensionfile + "(" + increment + ")" + extension);
			if (!reserveFilePathList.contains(newPath)) {
				// If the file name does not exist, the path is returned as the result of rename.
				logger.info("New path to use when copying or linking :" + newPath.toString());
				return newPath;
			}
		}
		return null;
	}

	/**
	 * ファイル名で使用できない文字を変換する
	 *
	 * @param fileName
	 * @return 記号等があった場合はreplaceした文字列 string
	 */
	public static String convertToFileName(String fileName) {
		if (StringUtils.isEmpty(fileName)) {
			String errorMsg = "fileName is Empty!";
			logger.error(errorMsg);
			throw new MwaInstanceError(MWARC.INVALID_INPUT, null, errorMsg);
		}

		String result = fileName;

		for (String prefix : prefixList) {
			// 接頭辞が含まれるか判別
			if (result.startsWith(prefix)) {
				// 文字列の頭に"_"を追加
				result = ("_" + result);
			}
		}

		for (String symbol : symbolList) {
			// 記号があるか判別
			if (result.contains(symbol)) {
				// 記号を"_"に変換
				result = result.replace(symbol, "_");
			}
		}

		// 先頭文字 末尾のトリム
		result = result.trim();

		return result;
	}

	/**
	 * Archiveの転送元ファイル名をファイルサイズをチェックする。
	 *
	 * @param mwaurl
	 *            ArchiveのSrcファイル名
	 * @return true:チェックOK false：チェックNG
	 */
	public static boolean checkArchiveFileNameLength(String mwaurl) {
		if (StringUtils.isEmpty(mwaurl)) {
			return true;
		}

		/**
		 * Archiveの転送元ファイル名をデコードする
		 */
		String decodedFileName = decodeArchiveFileName(mwaurl);

		// 1.最大文字数チェック
		if (decodedFileName.length() > 127) {
			return false;
		}
		return true;
	}

	/**
	 * ファイル名をデコードする。
	 *
	 * @param fileName
	 *            ArchiveのSrcファイル名
	 * @return decodedFileName デコードしたファイル名
	 */
	public static String decode(String fileName) {

		// decode
		String decodedFileName = null;
		try {
			decodedFileName = URLDecoder.decode(fileName.replace("+", "%2B"), "UTF-8").replace("%2B", "+");
		} catch (UnsupportedEncodingException e) {
			String errorMsg = "FileName is Decode Error!";
			logger.error(errorMsg);
			logger.error(e);
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, errorMsg);
		}
		return decodedFileName;
	}

	/**
	 * Archiveの転送元ファイル名をデコードする。
	 *
	 * @param fileName
	 *            ArchiveのSrcファイル名
	 * @return decodedFileName デコードしたファイル名
	 */
	private static String decodeArchiveFileName(String mwaurl) {
		LogUtils.log("FileManagementUtils_decodeArchiveFileName_mwaurl", mwaurl);
		String fileName = mwaurl.substring(mwaurl.lastIndexOf("/") + 1);

		// decode
		String decodedFileName = null;
		try {
			decodedFileName = URLDecoder.decode(fileName.replace("+", "%2B"), "UTF-8").replace("%2B", "+");
		} catch (UnsupportedEncodingException e) {
			String errorMsg = "FileName is Decode Error!";
			logger.error(errorMsg);
			logger.error(e);
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, errorMsg);
		}
		return decodedFileName;
	}

	/**
	 * Archiveの転送元ファイル名をデコードする。
	 *
	 * @param fileName
	 *            ArchiveのSrcファイル名
	 * @return decodedFileName デコードしたファイル名
	 */
	public static String decodeFileNameUNC(String uncFilePath) {

		if (!(uncFilePath.startsWith("\\") || uncFilePath.startsWith("/"))) {
			return uncFilePath;
		}
		LogUtils.log("FileManagementUtils_decodeFileNameUNC_uncFilePath", uncFilePath);

		String fileName = null;
		// directoryPath の最終文字はデリミタを含む
		String directoryPath = null;
		if (uncFilePath.contains("\\")) {
			fileName = uncFilePath.substring(uncFilePath.lastIndexOf("\\") + 1);
			directoryPath = uncFilePath.substring(0, uncFilePath.lastIndexOf("\\") + 1);
		} else if (uncFilePath.contains("/")) {
			fileName = uncFilePath.substring(uncFilePath.lastIndexOf("/") + 1);
			directoryPath = uncFilePath.substring(0, uncFilePath.lastIndexOf("/") + 1);
		}

		fileName = fileName.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
		fileName = fileName.replaceAll("\\+", "%2B");

		LogUtils.log("FileManagementUtils_decodeFileNameUNC_replacedFileName", fileName);

		// decode
		String decodedFileName = null;
		try {
			decodedFileName = URLDecoder.decode(fileName.replace("+", "%2B"), "UTF-8").replace("%2B", "+");
		} catch (UnsupportedEncodingException e) {
			String errorMsg = "FileName is Decode Error!";
			logger.error(errorMsg);
			logger.error(e);
			throw new MwaInstanceError(MWARC.CONNECTION_FAILED, null, errorMsg);
		}
		LogUtils.log("FileManagementUtils_decodeFileNameUNC_result", String.format("%s%s", directoryPath, decodedFileName));
		return String.format("%s%s", directoryPath, decodedFileName);
	}

	/**
	 * Archiveの転送元ファイル名を127字に変換する。
	 *
	 * @param mwaurl
	 *            ArchiveのSrcファイル名
	 * @return result コンバート後のArchiveのSrcファイル名
	 */
	public static String convertArchiveFileName(String mwaurl) {
		LogUtils.log("FileManagementUtils_convertArchiveFileName_mwaurl", mwaurl);
		String result = null;
		String urlPrefix =  mwaurl.substring(0, mwaurl.lastIndexOf("/") +1);
		String fileName = decodeArchiveFileName(mwaurl);
		String extensionSign = ".";
		String proxyExtension = "_proxy.mp4";

		// 「_proxy.mp4」を含むファイル名の文字数調整
		if (fileName.contains(proxyExtension)) {
			int extensionIndex = fileName.indexOf(proxyExtension);
			String extensionString = fileName.substring(extensionIndex);

			// 文字数を全長127文字にする
			while (fileName.length() > 127) {
				fileName = cutFileNameChar(fileName, extensionString);
			}
		}

		// 「.」を含むファイル名の文字数調整
		else if (fileName.contains(extensionSign)) {
			int extensionIndex = fileName.indexOf(extensionSign);
			String extensionString = fileName.substring(extensionIndex);

			// 文字数を全長127文字にする
			while (fileName.length() > 127) {
				fileName = cutFileNameChar(fileName, extensionString);
			}
		}
		fileName = encoder(fileName);
		result = urlPrefix + fileName;
		LogUtils.log("FileManagementUtils_convertArchiveFileName_result", result);
		return result;
	}

	/**
	 * Archiveの転送元ファイル名を127字に変換する。
	 *
	 * 2018/09/07 追記
	 * Client にMoveArchive したAsset のPartialExport を実行すると、
	 * 一時ファイルのpath がファイル名のみエンコードされた状態となってしまうため、
	 * ファイル名のエンコードを実施しない同一メソッドを作成します。
	 *
	 * @param uncFilePath
	 *            ArchiveのSrcファイル名
	 * @return result コンバート後のArchiveのSrcファイル名
	 */
	public static String convertArchiveFileNameUNC(String uncFilePath) {

		// UNC パスではない場合はそのまま返す
		if (!(uncFilePath.startsWith("\\") || uncFilePath.startsWith("/"))) {
			return uncFilePath;
		}
		LogUtils.log("FileManagementUtils_convertArchiveFileNameUNC_uncFilePath", uncFilePath);

		String result = null;
		String urlPrefix =  uncFilePath.substring(0, uncFilePath.lastIndexOf("\\") +1);
		String fileName = uncFilePath.substring(uncFilePath.lastIndexOf("\\") + 1);
		String extensionSign = ".";
		String proxyExtension = "_proxy.mp4";

		// 「_proxy.mp4」を含むファイル名の文字数調整
		if (fileName.contains(proxyExtension)) {
			int extensionIndex = fileName.indexOf(proxyExtension);
			String extensionString = fileName.substring(extensionIndex);

			// 文字数を全長127文字にする
			while (fileName.length() > 127) {
				fileName = cutFileNameChar(fileName, extensionString);
			}
		}

		// 「.」を含むファイル名の文字数調整
		else if (fileName.contains(extensionSign)) {
			int extensionIndex = fileName.indexOf(extensionSign);
			String extensionString = fileName.substring(extensionIndex);

			// 文字数を全長127文字にする
			while (fileName.length() > 127) {
				fileName = cutFileNameChar(fileName, extensionString);
			}
		}

		fileName = encoder(fileName);
		result = urlPrefix + fileName;
		return result;
	}

	/**
	 * 上記convertArchiveFileNameUNC()はsourcePathだけでなくdestPathでも使われている。
	 *  2018.2.0Patchリリースで影響範囲を絞るため、下記の暫定メソッドを用意する。
	 *  ロジックの修正は2018.3.0ではできないため、2018.3.1以降でメソッドの結合を行う＞保坂さん、お願いします。
	 * @param uncFilePath
	 * @return
	 */
	public static String convertArchiveFileNameUNCForSourcePath(String uncFilePath) {

		// UNC パスではない場合はそのまま返す
		if (!(uncFilePath.startsWith("\\") || uncFilePath.startsWith("/"))) {
			return uncFilePath;
		}
		LogUtils.log("FileManagementUtils_convertArchiveFileNameUNC_uncFilePath", uncFilePath);

		String result = null;
		String urlPrefix =  uncFilePath.substring(0, uncFilePath.lastIndexOf("\\") +1);
		String fileName = uncFilePath.substring(uncFilePath.lastIndexOf("\\") + 1);
		String extensionSign = ".";
		String proxyExtension = "_proxy.mp4";

		// 「_proxy.mp4」を含むファイル名の文字数調整
		if (fileName.contains(proxyExtension)) {
			int extensionIndex = fileName.indexOf(proxyExtension);
			String extensionString = fileName.substring(extensionIndex);
		}

		// 「.」を含むファイル名の文字数調整
		else if (fileName.contains(extensionSign)) {
			int extensionIndex = fileName.indexOf(extensionSign);
			String extensionString = fileName.substring(extensionIndex);
		}

		fileName = encoder(fileName);
		result = urlPrefix + fileName;
		return result;
	}

	/**
	 * 転送元ファイル名を127字に変換する。
	 *
	 * 2018/09/06 - 2018/09/07
	 * Client にMoveArchive したAsset がPartialExport 出来ない不具合が設計内で発生しました。
	 * 原因は、1つ上に作成されているconvertArchiveFileNameUNC を使用した時に、
	 * エンコードされてないディレクトリパス + エンコードされたファイル名 の形式となってしまう事が原因でした。
	 * com.sony.pro.mwa.activity.nvx.redistribute.subclip.SubclipBase のcreateClientTemporaryFile で
	 * convertArchiveFileNameUNC を使用して一時ファイルの作成を行っている箇所があり、生成されたpath のケアを
	 * 行っていないため、エラーとなっていた。
	 * そもそもとして、メソッドが返却するpath にエンコードされたものと、エンコードされてないものが混在している事自体が
	 * 処理としておかしいので、暫定対応として、ファイル名のエンコードを実施しない同一処理メソッドを作成する。
	 * このメソッドは2018/09/07 時点でcom.sony.pro.mwa.activity.nvx.redistribute.subclip.SubclipBase しか
	 * 使用していません。
	 *
	 *
	 * @param uncFilePath
	 *            ArchiveのSrcファイル名
	 * @return result コンバート後のArchiveのSrcファイル名
	 */
	public static String convertFileNameUNC(String uncFilePath) {

		// UNC パスではない場合はそのまま返す
		if (!(uncFilePath.startsWith("\\") || uncFilePath.startsWith("/"))) {
			return uncFilePath;
		}
		LogUtils.log("FileManagementUtils_convertArchiveFileNameUNC_uncFilePath", uncFilePath);

		String result = null;
		String urlPrefix =  uncFilePath.substring(0, uncFilePath.lastIndexOf("\\") +1);
		String fileName = uncFilePath.substring(uncFilePath.lastIndexOf("\\") + 1);
		String extensionSign = ".";
		String proxyExtension = "_proxy.mp4";

		// 「_proxy.mp4」を含むファイル名の文字数調整
		if (fileName.contains(proxyExtension)) {
			int extensionIndex = fileName.indexOf(proxyExtension);
			String extensionString = fileName.substring(extensionIndex);

			// 文字数を全長127文字にする
			while (fileName.length() > 127) {
				fileName = cutFileNameChar(fileName, extensionString);
			}
		}

		// 「.」を含むファイル名の文字数調整
		else if (fileName.contains(extensionSign)) {
			int extensionIndex = fileName.indexOf(extensionSign);
			String extensionString = fileName.substring(extensionIndex);

			// 文字数を全長127文字にする
			while (fileName.length() > 127) {
				fileName = cutFileNameChar(fileName, extensionString);
			}
		}
		result = urlPrefix + fileName;
		return result;
	}

	/**
	 * Archiveの転送元ファイル名から文字を削減する。
	 *
	 * @param fileName
	 *            ArchiveのSrcファイル名
	 * @param extensionString
	 *            ArchiveのSrcファイルの拡張子
	 * @return decodedFileName デコードしたファイル名
	 */
	private static String cutFileNameChar(String fileName, String extensionString) {
		// 拡張子部分を取り除く
		String fileNameWithoutExtension = fileName.replace(extensionString, "");

		char[] fileNameCharArray = fileNameWithoutExtension.toCharArray();
		int charArrayLength = fileNameCharArray.length;

			// 文字数を一字減らす
			if (checkSurrogatePair(fileNameCharArray, charArrayLength)) {
				fileNameWithoutExtension = fileNameWithoutExtension.substring(0, fileNameWithoutExtension.length() - 2);
			} else {
				fileNameWithoutExtension = fileNameWithoutExtension.substring(0, fileNameWithoutExtension.length() - 1);
			}

		// 拡張子をもとに戻す
		fileName = fileNameWithoutExtension + extensionString;
		return fileName;
	}

	/**
	 * 文字列をエンコードする。
	 *
	 * エンコードの方式はURLEncode.encod(str, "UTF-8")に準拠し、
	 * エンコードされない文字"*"、"-"、" "をそれぞれ"%2a"、"%2d"、"%20" にreplaceする。
	 *
	 * @param str
	 *            対象の文字列
	 * @return エンコードされた文字列
	 */
	public static String encoder(String str) {
		try {
			str = URLEncoder.encode(str, "UTF-8");
			str = str.replace("*", "%2a");
			str = str.replace("-", "%2d");
			str = str.replace("+", "%20");
		} catch (UnsupportedEncodingException e) {
			throw new MwaInstanceError(MWARC.INTEGRATION_ERROR);
		}
		LogUtils.log("FileManagementUtils_encoder_encodeString", str);
		return str;
	}

	/**
	 * サロゲートペアの判定をする。
	 *
	 * @param fileNameCharArray
	 *            ArchiveのSrcファイル名
	 * @param charArrayLength
	 *            ArchiveのSrcファイル名の文字数
	 * @return true:サロゲートペア false:その他
	 */
	public static boolean checkSurrogatePair(char[] fileNameCharArray, int charArrayLength) {
		if (Character.isSurrogatePair(fileNameCharArray[charArrayLength - 2], fileNameCharArray[charArrayLength -1])) {
			return true;
		}
		return false;
	}

	/**
	 * 指定パスの存在確認を行います。
	 * @return
	 */
	public static boolean canTransfer(String path) {
		boolean canTransfer = false;
		File file = new File(path);
		LogUtils.log("FileManagementUtils_canTransfer_file", file.toString());
		try {
			if (file.exists()) {
				canTransfer = true;
			}
		} catch (Exception e) {
			String errMsg = "Destination status is not online.";
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, errMsg);
		}
		return canTransfer;
	}

	/**
	 * 指定したパスの空き容量を確認し、ファイルサイズとの比較を行います。
	 * @param path
	 * @param size
	 * @return
	 */
	public static boolean hasFreeSpace(String path, String size) {
		boolean hasFreeSpace = false;
		File file = new File(path);
		try {
			long freeSpace = file.getFreeSpace();
			long fileSize = Long.parseLong(size);
			logger.info(String.format("storage free space. %s byte", freeSpace));
			logger.info(String.format("transfer file size. %s byte", fileSize));
			if (fileSize <= freeSpace) {
				hasFreeSpace = true;
			}
		} catch (Exception e) {
			String errMsg = "Destination status is not online.";
			throw new MwaInstanceError(MWARC.SYSTEM_ERROR, null, errMsg);
		}
		return hasFreeSpace;
	}

	/**
	 * 指定された MWA パスをもとに、fileName 部分を返却します。
	 *
	 * @param sourceUNCPath
	 *            探索するロケーションパス (MWA パス）
	 * @return
	 */
	public static String getFileName(String sourceUNCPath) {
		String fileName = null;
		LogUtils.log("FileManagementUtils_getFileName_sourceUNCPath", sourceUNCPath);
		int fileNameStartCharIndex = sourceUNCPath.lastIndexOf("/");
		fileName = sourceUNCPath.substring(fileNameStartCharIndex + 1, sourceUNCPath.length());
		return fileName;
	}

	/**
	 * 指定されたパスの、アセットID 部分を変更して返却します。
	 *
	 * @param sourcePath
	 *            変換したいアセットIDを含むパス
	 * @param assetId
	 *            パスに含まれているアセットID
	 * @param newAssetId
	 *            変換したいアセットID
	 * @return
	 */
	public static String changePathAssetId(String sourcePath, String assetId, String newAssetId) {
		String prefixPath = sourcePath.substring(0, sourcePath.indexOf(assetId));
		String suffixPath = sourcePath.substring(sourcePath.indexOf(assetId) + assetId.length());

		String result = String.format("%s%s%s", prefixPath, newAssetId, suffixPath);
		LogUtils.log("FileManagementUtils_changePathAssetId_result", result);

		return result;
	}
}
