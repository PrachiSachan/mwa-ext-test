package com.sony.pro.mwa.util.nvx.asset.models;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.util.nvx.asset.models.exceptions.AssetModelsException;

/**
 * あるAssetに含まれるEventの一覧
 */
public class Events extends ArrayNodeBase {
	
	/**
	 * コンストラクタ
	 * @param events
	 * @throws AssetModelsException 
	 */
	public Events(JsonNode events) throws AssetModelsException {
		super(events);
	}

	/**
	 * Eventリストを取得する
	 * @return
	 */
	public List<Event> getEventList() {
		Iterator<JsonNode> iterator = this.jsonNode.iterator();
		List<Event> result = new ArrayList<Event>();
		while (iterator.hasNext()) {
			result.add(new Event(iterator.next()));
		}
		return result;
	}

	/**
	 * idで指定されたEventを取得する
	 * @param id
	 * @return
	 */
	public Event getEvent(String id) {
		Event event = null;
		for (Event eventModel : this.getEventList()) {
			try {
				if (id.equals(eventModel.getId())) {
					event = eventModel;
				}
			} catch (AssetModelsException e) {
				e.printStackTrace();
			}
		}
		/*
		Iterator<Event> iterator = this.getEventList().iterator();
		try {
			while (iterator.hasNext()) {
				String eventId = iterator.next().getEventId();
				System.out.println(eventId);
				if (id.equals(eventId)) {
					event = iterator.next();
					break;
				}
			}
		} catch (Exception e) {
			throw new UnsupportedOperationException();
		}
		*/
		return event;
	}

	/**
	 * 全Eventのid一覧を取得する
	 * @return
	 * @throws AssetModelsException 
	 */
	public List<String> getIds() throws AssetModelsException {
		Iterator<JsonNode> iterator = this.jsonNode.iterator();
		List<String> result = new ArrayList<String>();
		while (iterator.hasNext()) {
			Event event = new Event(iterator.next());
			result.add(event.getId());
		}
		return result;
	}
	
	public void setEvent(Event event) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		String str = mapper.writeValueAsString(event);
		JsonNode node = mapper.readTree(str);
		
		this.setObjectNode(node);
	}
}
