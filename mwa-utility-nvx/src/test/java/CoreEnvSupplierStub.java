import com.sony.pro.mwa.model.activity.ActivityInstanceModel;
import com.sony.pro.mwa.service.ICoreModules;
import com.sony.pro.mwa.util.nvx.core.ICoreEnvSupplier;

public class CoreEnvSupplierStub implements ICoreEnvSupplier {

	// テスト用のスタブなんで、、、まいっか
	public String unc = "";
	public String physical = "";
	public String location = "DEFAULT";

	@Override
	public String convertToUNC(String mwaUri) {
		return this.unc;
	}
	@Override
	public String convertToPhysical(String mwaUri) {
		return this.physical;
	}
	@Override
	public String getLocationName(String locationId) {
		return this.location;
	}
	@Override
	public ICoreModules getCoreModules() {
		// TODO スタブ実装
		return null;
	}
	@Override
	public String getPrimaryLocationName(String volumeType) {
		return this.location;
	}
	@Override
	public ActivityInstanceModel getActivityInstanceModel(String activityInstanceId) {
		return null;
	}
	@Override
	public String getProviderId(String providerName) {
		return null;
	}
}
