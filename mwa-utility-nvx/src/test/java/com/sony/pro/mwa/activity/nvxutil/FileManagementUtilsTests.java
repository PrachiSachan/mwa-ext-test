package com.sony.pro.mwa.activity.nvxutil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.testng.Assert;

public class FileManagementUtilsTests {
	@Test
	public void test_copyFilesAsOverwrite() throws IOException {
		/*
		List<Path> sourceFilePathList = new ArrayList<>();
		//sourceFilePathList.add(Paths.get("\\\\43.16.194.80\\volumes\\TEST\\C0001_01.mxf"));
		//sourceFilePathList.add(Paths.get("\\\\43.16.194.80\\volumes\\TEST\\A034C004_160216RF.MXF.mp4"));
		//sourceFilePathList.add(Paths.get("\\\\43.16.194.80\\volumes\\TEST\\IMX 30 NTSC 720x486 8ch 16 bit 48 kHz from Premiere.mxf"));

		//sourceFilePathList.add(Paths.get("\\\\43.25.0.30\\mwa\\Temp\\katou\\C0001_01.mxf"));
		//sourceFilePathList.add(Paths.get("\\\\43.25.0.30\\mwa\\Temp\\katou\\A034C004_160216RF.MXF.mp4"));
		//sourceFilePathList.add(Paths.get("\\\\43.25.0.30\\mwa\\Temp\\katou\\IMX 30 NTSC 720x486 8ch 16 bit 48 kHz from Premiere.mxf"));

		Path destinationDirPath = Paths.get("\\\\43.25.0.30\\mwa\\Temp\\katou\\export");

		//List<Path> actualList = FileManagementUtils.copyFilesAsOverwrite(sourceFilePathList, destinationDirPath);
		List<Path> actualList = FileManagementUtils.hardLinkAsOverwrite(sourceFilePathList, destinationDirPath);
		System.out.print(actualList);*/
	}

	/**
	 * 指定のPathにすでにファイルがあるケース
	 * 期待値はファイル名に(1)が付与されたPathになる
	 */
	@Test
	public void test_reserveFilePathForRename_000() {
		String pathString = "C:\\volumes\\test\\stitchTest1_20170913152234\\C0001_11.mxf";
		Path filePath = Paths.get(pathString);
		List<Path> reserveFilePathList = new ArrayList<>();
		reserveFilePathList.add(filePath);
		Path actualPath = FileManagementUtils.reserveFilePathForRename(filePath, reserveFilePathList);
		Path expectedPath = Paths.get("C:\\volumes\\test\\stitchTest1_20170913152234\\C0001_11(1).mxf");
		System.out.println(actualPath.toString());
		Assert.assertEquals(actualPath.toString(), expectedPath.toString());
	}

	/**
	 * 指定のPathにファイルがないケース
	 * 期待値はそのままのPathが返される
	 */
	@Test
	public void test_reserveFilePathForRename_001() {
		String pathString = "C:\\volumes\\test\\stitchTest1_20170913152234\\C0001_11.mxf";
		Path filePath = Paths.get(pathString);
		List<Path> reserveFilePathList = new ArrayList<>();
		Path actualPath = FileManagementUtils.reserveFilePathForRename(filePath, reserveFilePathList);
		Path expectedPath = Paths.get("C:\\volumes\\test\\stitchTest1_20170913152234\\C0001_11.mxf");
		System.out.println(actualPath.toString());
		Assert.assertEquals(actualPath.toString(), expectedPath.toString());
	}

	/**
	 * 指定のPathにファイルがあるかつ、Rename後の(1)を付与したファイルもあるケース
	 * 期待値はファイル名に(2)が付与される
	 */
	@Test
	public void test_reserveFilePathForRename_002() {
		String pathString = "C:\\volumes\\test\\stitchTest1_20170913152234\\C0001_11.mxf";
		Path filePath = Paths.get(pathString);
		List<Path> reserveFilePathList = createRenamePath(pathString, 2);
		Path actualPath = FileManagementUtils.reserveFilePathForRename(filePath, reserveFilePathList);
		Path expectedPath = Paths.get("C:\\volumes\\test\\stitchTest1_20170913152234\\C0001_11(2).mxf");
		System.out.println(actualPath.toString());
		Assert.assertEquals(actualPath.toString(), expectedPath.toString());
	}

	/***
	 * PathのRename上限を超えるケース
	 */
	@Test
	public void test_reserveFilePathForRename_003() {
		String pathString = "C:\\volumes\\test\\stitchTest1_20170913152234\\C0001_11.mxf";
		Path filePath = Paths.get(pathString);
		List<Path> reserveFilePathList = createRenamePath(pathString, 100);
		Path actualPath = FileManagementUtils.reserveFilePathForRename(filePath, reserveFilePathList);
		//System.out.println(actualPath.toString());
		Assert.assertNull(actualPath);

	}

	/***
	 * ファイル名のコンバート ファイルパスがUNCパス(デリミタが/)の場合
	 */
	@Test
	public void test_covertFileName_004() {
		String path = "//43.16.194.195/volumes/d987126e-1530-4258-b1a7-0103d61e2735/1%21%23%24%25%26%27%28%29%2B%20%2d.%20%20%40%5B%5D%5E_%60%7B%7D%7E.MXF";
		String actualPath = FileManagementUtils.decodeFileNameUNC(path);
		String expectPath = "//43.16.194.195/volumes/d987126e-1530-4258-b1a7-0103d61e2735/1!#$%&'()+ -.  @[]^_`{}~.MXF";
		System.out.println(actualPath.toString());
		Assert.assertEquals(actualPath, expectPath);
	}

	/***
	 * ファイル名のコンバート ファイルパスがUNCパス(デリミタが\)の場合
	 */
	@Test
	public void test_covertFileName_005() {
		String path = "\\\\43.16.194.195\\volumes\\d987126e-1530-4258-b1a7-0103d61e2735\\1%21%23%24%25%26%27%28%29%2B%20%2d.%20%20%40%5B%5D%5E_%60%7B%7D%7E.MXF";
		String actualPath = FileManagementUtils.decodeFileNameUNC(path);
		String expectPath = "\\\\43.16.194.195\\volumes\\d987126e-1530-4258-b1a7-0103d61e2735\\1!#$%&'()+ -.  @[]^_`{}~.MXF";
		System.out.println(actualPath.toString());
		Assert.assertEquals(actualPath, expectPath);
	}

	/***
	 * ファイル名のアセットIDのコンバート ファイルパスがUNCパス(デリミタが\)の場合
	 */
	@Test
	public void test_changeFilePathAssetId_001() {
		String path = "\\\\43.16.194.195\\volumes\\d987126e-1530-4258-b1a7-0103d61e2735\\test.MXF";
		String assetId = "d987126e-1530-4258-b1a7-0103d61e2735";
		String newAssetId = "e987126e-1530-4258-b1a7-0103d61e2735";
		String actualPath = FileManagementUtils.changePathAssetId(path, assetId, newAssetId);
		String expectPath = "\\\\43.16.194.195\\volumes\\e987126e-1530-4258-b1a7-0103d61e2735\\test.MXF";
		System.out.println("test_changeFilePathAssetId_001 actualPath:" + actualPath.toString());
		System.out.println("test_changeFilePathAssetId_001 expectPath:" + expectPath.toString());

		Assert.assertEquals(actualPath, expectPath);
	}

	/***
	 * ファイル名のアセットIDのコンバート ファイルパスがUNCパス(デリミタが\)の場合
	 */
	@Test
	public void test_changeFilePathAssetId_002() {
		String path = "\\\\43.16.194.195\\volumes\\d987126e-1530-4258-b1a7-0103d61e2735\\test\\test.MXF";
		String assetId = "d987126e-1530-4258-b1a7-0103d61e2735";
		String newAssetId = "e987126e-1530-4258-b1a7-0103d61e2735";
		String actualPath = FileManagementUtils.changePathAssetId(path, assetId, newAssetId);
		String expectPath = "\\\\43.16.194.195\\volumes\\e987126e-1530-4258-b1a7-0103d61e2735\\test\\test.MXF";
		System.out.println("test_changeFilePathAssetId_001 actualPath:" + actualPath.toString());
		System.out.println("test_changeFilePathAssetId_001 expectPath:" + expectPath.toString());

		Assert.assertEquals(actualPath, expectPath);
	}

//	/***
//	 * ファイル名のアセットIDのコンバート ファイルパスがUNCパス(デリミタが\)の場合
//	 */
//	@Test
//	public void test_deserializeMwaListToList_001() {
//		String path = "<list><string>\\\\43.16.194.78\\volumes\\57d2c733-f55c-4bae-bae7-fff97d175d68\\test_01.mxf</string><string>\\\\43.16.194.78\\volumes\\57d2c733-f55c-4bae-bae7-fff97d175d68\\test_02.mxf</string></list>";
//		List<String> actualPath = FileManagementUtils.deserializeMwaListToList(path);
//		List<String> expectPath = new ArrayList<>();
//		expectPath.add("\\\\43.16.194.78\\volumes\\57d2c733-f55c-4bae-bae7-fff97d175d68\\test_01.mxf");
//		expectPath.add("\\\\43.16.194.78\\volumes\\57d2c733-f55c-4bae-bae7-fff97d175d68\\test_02.mxf");
//		System.out.println("test_changeFilePathAssetId_001 actualPath:" + actualPath.toString());
//		System.out.println("test_changeFilePathAssetId_001 expectPath:" + expectPath.toString());
//
//		Assert.assertEquals(actualPath, expectPath);
//	}
//
//	@Test
//	public void test_deserializeMwaListToList_002() {
//		String path = "<list><string>\\\\43.16.194.78\\volumes\\57d2c733-f55c-4bae-bae7-fff97d175d68\\test_01.mxf</string><string>\\\\43.16.194.78\\volumes\\57d2c733-f55c-4bae-bae7-fff97d175d68\\test_02.mxf</string></list>";
//
//		List<String> resultList = null;
//
//		do {
//			resultList = FileManagementUtils.deserializeMwaListToList(path);
//			path = convert(path);
//
//		} while (!resultList.isEmpty());
//	}
//
//	public static String convert(String path) {
//
//		List<String> srcUrlList = FileManagementUtils.deserializeMwaListToList(path);
//
//		String srcUrl = srcUrlList.get(0);
//		List<String> srcUrlsBeforeList = FileManagementUtils.deserializeMwaListToList(path);
//		srcUrlsBeforeList.remove(srcUrl);
//
//		// OutputSequence
//		return FileManagementUtils.serializeMwaListString(srcUrlsBeforeList);
//	}

	@Test
	public void hardLink_destFileDoesNotExit_001() {
		try {
			tempFolderA.newFile("test_01.MXF");
		} catch (IOException e) {
			Assert.fail();
		}
		Path destPath = Paths.get(tempFolderB.getRoot().getAbsolutePath() + "\\test_01.MXF");
		Path srcPath = Paths.get(tempFolderA.getRoot().getPath() + "\\test_01.MXF");
		System.out.println(destPath.toString());
		System.out.println(srcPath.toString());

		Path result = null;
		try {
			result = FileManagementUtils.hardLink(srcPath, destPath);
		} catch (IOException e) {
			Assert.fail();
		}
		Path expect = Paths.get(tempFolderB.getRoot().getAbsolutePath() + "\\test_01.MXF");
		System.out.println(expect.toString());
		Assert.assertEquals(result.toString(), expect.toString());
		Assert.assertTrue(Files.exists(result));
	}

	@Test
	public void hardLink_destFileExits_001() {
		try {
			tempFolderA.newFile("test_01.MXF");
		} catch (IOException e) {
			Assert.fail();
		}
		Path destPath = Paths.get(tempFolderA.getRoot().getAbsolutePath() + "\\test_01.MXF");
		Path srcPath = Paths.get(tempFolderA.getRoot().getPath() + "\\test_01.MXF");

		System.out.println(destPath.toString());
		System.out.println(srcPath.toString());

		Path result = null;
		try {
			result = FileManagementUtils.hardLink(srcPath, destPath);
		} catch (IOException e) {
			Assert.fail();
		}
		Path expect = Paths.get(tempFolderA.getRoot().getAbsolutePath() + "\\test_01(1).MXF");
		System.out.println(expect.toString());
		Assert.assertEquals(result.toString(), expect.toString());
		Assert.assertTrue(Files.exists(result));
	}

	@Test
	public void hardLink_destFileExits_002() {
		try {
			tempFolderA.newFile("test_01.MXF");
			tempFolderA.newFile("test_01(1).MXF");
		} catch (IOException e) {
			Assert.fail();
		}
		Path destPath = Paths.get(tempFolderA.getRoot().getAbsolutePath() + "\\test_01.MXF");
		Path srcPath = Paths.get(tempFolderA.getRoot().getPath() + "\\test_01.MXF");

		System.out.println(destPath.toString());
		System.out.println(srcPath.toString());

		Path result = null;
		try {
			result = FileManagementUtils.hardLink(srcPath, destPath);
		} catch (IOException e) {
			Assert.fail();
		}
		Path expect = Paths.get(tempFolderA.getRoot().getAbsolutePath() + "\\test_01(2).MXF");
		System.out.println(expect.toString());
		Assert.assertEquals(result.toString(), expect.toString());
		Assert.assertTrue(Files.exists(result));
	}

	@Rule
    public TemporaryFolder tempFolderA = new TemporaryFolder();
	@Rule
    public TemporaryFolder tempFolderB = new TemporaryFolder();

	/**
	 *
	 * basePathに対してcreateCount数分、Renameを行ったPathを作成します.
	 * createCountが1の場合、basePathのみ、
	 * createCountが2の場合、basePathとbasePathのファイル名に(1)を付与したPathが返却されます
	 *
	 * @param basePath
	 * @param createCount
	 * @return
	 */
	private List<Path> createRenamePath(String basePathString, int createCount) {
		List<Path> pathList = new ArrayList<>();
		Path basePath = Paths.get(basePathString);
		if (createCount <= 0) {
			return null;
		} else if (createCount == 1){
			pathList.add(basePath);
			return pathList;
		} else {
			pathList.add(basePath);

			String parentPath = basePath.getParent().toString();
			String fileName = basePath.getFileName().toString();
			int extensionStartPosition = fileName.lastIndexOf(".");
			String extension = fileName.substring(extensionStartPosition);
			String noExtensionfile =  fileName.substring(0, extensionStartPosition);

			final int start = 1;
			for (int count = start; count < createCount; count++) {
				Path renamePath = Paths.get(parentPath, noExtensionfile + "(" + count + ")" + extension);
				pathList.add(renamePath);
			}
		}

		return pathList;
	}
}
