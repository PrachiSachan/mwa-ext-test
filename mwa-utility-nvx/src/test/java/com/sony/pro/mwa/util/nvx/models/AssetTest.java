package com.sony.pro.mwa.util.nvx.models;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.sony.pro.mwa.util.ResourceUtil;
import com.sony.pro.mwa.util.exceptions.MwaUriException;
import com.sony.pro.mwa.util.nvx.asset.models.Asset;
import com.sony.pro.mwa.util.nvx.asset.models.AssetInfo;
import com.sony.pro.mwa.util.nvx.asset.models.Constants;
import com.sony.pro.mwa.util.nvx.asset.models.Event;
import com.sony.pro.mwa.util.nvx.asset.models.Events;
import com.sony.pro.mwa.util.nvx.asset.models.exceptions.AssetModelsException;
import com.sony.pro.mwa.util.uri.MwaUri;

/***
 * com.sony.pro.mwa.util.nvx.asset.models.Assetのテスト
 */
public class AssetTest {

	private ObjectMapper mapper = new ObjectMapper();
	private ResourceUtil resUtils = new ResourceUtil();
	
	private JsonNode assetRootNode = null;
	private Asset asset = null;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		assetRootNode = mapper.readValue(resUtils.getJsonTextResource("audio_video_sample.json").toString(), JsonNode.class);
		asset = new Asset(assetRootNode.findPath(Constants.Asset._Source));
	}

	@Test
	public void modelsの親オブジェクトを指定されても正しく動作できること() throws Exception {
		// Arrange
		String jsonTest = resUtils.getJsonTextResource("4asset_test_asset_body.json").toString();
		// Action
		Asset asset = new Asset(jsonTest);
		// Assert
		String id = asset.getAssetId();
		Assert.assertEquals("3bca8f99-0554-4c1a-8e70-2ade827bf2ab", id);
	}	
	
	@Test
	public void modelsの配列要素から始まるJSONデータを指定されても正しく動作できること() throws Exception {
		// Arrange
		String jsonTest = resUtils.getJsonTextResource("audio_video_sample.json").toString();
		// Action
		Asset asset = new Asset(jsonTest);
		// Assert
		String id = asset.getAssetId();
		Assert.assertEquals("3bca8f99-0554-4c1a-8e70-2ade827bf2ab", id);
	}

	/**
	 * 
	 * @throws Exception
	 */
	@Test
	public void エラーが発生したAssetJsonの読み込みテスト() throws Exception {
		// Arrange
		String jsonTest = resUtils.getJsonTextResource("errord_asset_single_asset_api_result20180330.json").toString();
		// Action
		Asset asset = new Asset(jsonTest);
		// Assert
		String id = asset.getAssetId();
	}
	
	@Test
	public void AssetInfoの新規作成テスト001() throws AssetModelsException {
		
		List<String> odsCartridgeIds = new ArrayList<>();
		odsCartridgeIds.add("001 111111111");
		odsCartridgeIds.add("001 222222222");
		odsCartridgeIds.add("001 333333333");
		
		List<String> odsBarcodeIds = new ArrayList<>();
		odsBarcodeIds.add("Barcode 001");
		odsBarcodeIds.add("Barcode 002");
		odsBarcodeIds.add("Barcode 003");

		List<String> copyOdsCartridgeIds = new ArrayList<>();
		copyOdsCartridgeIds.add("COPY 001 111111111");
		copyOdsCartridgeIds.add("COPY 001 222222222");
		copyOdsCartridgeIds.add("COPY 001 333333333");
		
		List<String> copyOdsBarcodeIds = new ArrayList<>();
		copyOdsBarcodeIds.add("COPY Barcode 001");
		copyOdsBarcodeIds.add("COPY Barcode 002");
		copyOdsBarcodeIds.add("COPY Barcode 003");
		
		AssetInfo assetInfo = new AssetInfo();
		assetInfo.setTitle("AssetInfo の新規作成テスト");
		assetInfo.setNote("AssetInfo が正常に作成出来ているか確認するためのModelです。");
		assetInfo.setRating(0);
		assetInfo.setLength(123456789);
		assetInfo.setType("AUDIO_VIDEO");
		assetInfo.setNumerator(60000);
		assetInfo.setDenominator(1000);
		assetInfo.setFrameRate(60.0);
		assetInfo.setDropFrame(false);
		assetInfo.setDuration(987654321);
		assetInfo.setArchived(false);
		assetInfo.setStartTimecode("036071");
		assetInfo.setStartTimecode_msec(6000000);
		assetInfo.setStatus("Online");
		assetInfo.setOdsCartridgeIds(odsCartridgeIds);
		assetInfo.setOdsBarcodeIds(odsBarcodeIds);
		assetInfo.setCopyOdsCartridgeIds(copyOdsCartridgeIds);
		assetInfo.setCopyBarcodeIds(copyOdsBarcodeIds);
		assetInfo.setCustomMetadata(JsonNodeFactory.instance.objectNode());
		
		Asset asset = new Asset();		
		asset.setAssetInfo(assetInfo);
		
		Assert.assertThat(asset.getAssetInfo(), is(notNullValue()));
		Assert.assertThat(asset.getAssetInfo().getTitle(), is("AssetInfo の新規作成テスト"));
		Assert.assertThat(asset.getAssetInfo().getFrameRate(), is(60.0));
		Assert.assertEquals(asset.getAssetInfo().getStartTimecode_msec(), 6000000);
		Assert.assertThat(asset.getAssetInfo().getArchived(), is(false));
		Assert.assertThat(asset.getAssetInfo().getOdsCartridgeIds(), hasItems("001 111111111", "001 222222222", "001 333333333"));
		
		System.out.println(asset.getAssetInfo().getTitle());

	}
	
	@Test
	public void EventModelの新規作成テスト001() throws AssetModelsException {
		Event event = new Event();
		event.setId("b368ee4d-4237-4db1-b0d0-cedf5c746254");
		event.setTitle("Event の新規作成テスト");
		event.setType("KEY_FRAME");
		event.setNote("Event が正常に作成出来ているか確認するためのModelです。");
		event.setFrame(1000);
		event.setDuration(6140);
		
		MwaUri mwaUri = new MwaUri();
		try {
			mwaUri.set("TEST001", "3bca8f99-0554-4c1a-8e70-2ade827bf2ab", "image.svg");
		} catch (MwaUriException e) {
			e.printStackTrace();
		}
		event.setImageLocation(mwaUri.get());
		event.setCustomMetadata(JsonNodeFactory.instance.objectNode());
		
		List<Event> eventList = new ArrayList<>();
		eventList.add(event);
		Asset asset = new Asset();
		asset.setEvents(eventList);
		
		Assert.assertThat(asset.getEvents().getEventList(), is(notNullValue()));
		Events events = asset.getEvents();
		Assert.assertThat(events, is(notNullValue()));

		Event ev = events.getEventList().get(0);
		Assert.assertThat(ev.getId(), is("b368ee4d-4237-4db1-b0d0-cedf5c746254"));
		Assert.assertThat(ev.getTitle(), is("Event の新規作成テスト"));
		Assert.assertThat(ev.getType(), is("KEY_FRAME"));
		Assert.assertThat(ev.getNote(), is("Event が正常に作成出来ているか確認するためのModelです。"));
		Assert.assertEquals(ev.getFrame(), 1000);
		Assert.assertEquals(ev.getDuration(), 6140);
		Assert.assertThat(ev.getImageLocation(), is(mwaUri.get()));
	}
	
	@Test
	public void EventModelの新規追加テスト001() throws AssetModelsException {
		Event event = new Event();
		event.setId("7c76fed1-b5e8-4443-a40f-803862677fc2");
		event.setTitle("Event の新規追加テスト");
		event.setType("KEY_FRAME");
		event.setNote("既存のEventList に新規Event が正常に作成出来ているか確認するためのModelです。");
		event.setFrame(2000);
		event.setDuration(5391);
		
		MwaUri mwaUri = new MwaUri();
		try {
			mwaUri.set("TEST001", "3bca8f99-0554-4c1a-8e70-2ade827bf2ab", "testImage.svg");
		} catch (MwaUriException e) {
			e.printStackTrace();
		}
		event.setImageLocation(mwaUri.get());
		event.setCustomMetadata(JsonNodeFactory.instance.objectNode());
		
		Events events = new Events(assetRootNode.findPath(Constants.Asset.Events));
		List<Event> eventList = events.getEventList();
		eventList.add(event);
		Asset asset = new Asset();
		asset.setEvents(eventList);
		
		for (Event eve : asset.getEvents().getEventList()) {
			Assert.assertThat(eve.getId(), is(notNullValue()));
			Assert.assertThat(eve.getTitle(), is(notNullValue()));
			Assert.assertThat(eve.getType(), is(notNullValue()));
			Assert.assertThat(eve.getNote(), is(notNullValue()));
			Assert.assertThat(eve.getFrame(), is(notNullValue()));
			Assert.assertThat(eve.getDuration(), is(notNullValue()));
			Assert.assertThat(eve.getImageLocation(), is(notNullValue()));
			Assert.assertThat(eve.getCustomMetadata(), is(notNullValue()));
		}
	}
	
	@Test
	public void EventModelの新規追加テスト002() throws AssetModelsException {

		Event event = new Event();
		event.setId("8e289d90-9042-4d2e-8ae1-f47e8fe791f7");
		event.setTitle("既存Asset へのEvent の新規追加テスト");
		event.setType("KEY_FRAME");
		event.setNote("既存のAsset に新規Event を追加したEventList が正常に作成出来ているか確認するためのModelです。");
		event.setFrame(2000);
		event.setDuration(5391);
		
		MwaUri mwaUri = new MwaUri();
		try {
			mwaUri.set("TEST002", "3bca8f99-0554-4c1a-8e70-2ade827bf2ab", "testImage.png");
		} catch (MwaUriException e) {
			e.printStackTrace();
		}
		event.setImageLocation(mwaUri.get());
		event.setCustomMetadata(JsonNodeFactory.instance.objectNode());
		
		Events events = new Events(assetRootNode.findPath(Constants.Asset.Events));
		List<Event> eventList = events.getEventList();
		eventList.add(event);
		asset.setEvents(eventList);
		
		for (Event eve : asset.getEvents().getEventList()) {
			Assert.assertThat(eve.getId(), is(notNullValue()));
			Assert.assertThat(eve.getTitle(), is(notNullValue()));
			Assert.assertThat(eve.getType(), is(notNullValue()));
			Assert.assertThat(eve.getNote(), is(notNullValue()));
			Assert.assertThat(eve.getFrame(), is(notNullValue()));
			Assert.assertThat(eve.getDuration(), is(notNullValue()));
			Assert.assertThat(eve.getImageLocation(), is(notNullValue()));
			Assert.assertThat(eve.getCustomMetadata(), is(notNullValue()));
		}
	}
}

