package com.sony.pro.mwa.util.nvx.models;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.util.ResourceUtil;
import com.sony.pro.mwa.util.nvx.asset.models.Constants;
import com.sony.pro.mwa.util.nvx.asset.models.Event;
import com.sony.pro.mwa.util.nvx.asset.models.Events;

/**
 * Unit test of event model.
 * 
 * @author Kousuke Hosaka
 *
 */
public class EventTest {
	private ObjectMapper mapper = new ObjectMapper();
	private ResourceUtil resUtils = new ResourceUtil();
	
	private JsonNode assetRootNode = null;
	private Events events = null;
	private Event event = null;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		assetRootNode = mapper.readValue(resUtils.getJsonTextResource("audio_video_sample.json").toString(), JsonNode.class);
		events = new Events(assetRootNode.findPath(Constants.Asset.Events));
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void Eventの取得テスト001() throws Exception {
		Assert.assertThat(events.getEventList(), is(notNullValue()));
		event = events.getEvent("55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4");
		Assert.assertThat(event, is(notNullValue()));
	}
	
	@Test
	public void EventIdの取得テスト() throws Exception {
		event = events.getEvent("55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4");
		Assert.assertThat(event.getId(), is(notNullValue()));
		System.out.println(event.getId());
	}
	
	@Test
	public void EventIdの更新テスト() throws Exception {
		event = events.getEvent("55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4");
		Assert.assertThat(event.getId(), is(notNullValue()));
		String newEventId = "85b382c5-a3b2-4d34-b5cc-08a0a3160f95";
		event.setId(newEventId);
		
		Event newEvent = events.getEvent("85b382c5-a3b2-4d34-b5cc-08a0a3160f95");
		Assert.assertThat(newEvent, is(notNullValue()));
	}
	
	@Test
	public void EventTitleの取得テスト() throws Exception {
		event = events.getEvent("55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4");
		Assert.assertThat(event.getId(), is(notNullValue()));
		System.out.println(event.getId());
	}
	
	@Test
	public void EventTitleの更新テスト() throws Exception {
		event = events.getEvent("55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4");
		Assert.assertThat(event.getId(), is(notNullValue()));
		String newEventId = "85b382c5-a3b2-4d34-b5cc-08a0a3160f95";
		event.setId(newEventId);
		
		Event newEvent = events.getEvent("85b382c5-a3b2-4d34-b5cc-08a0a3160f95");
		Assert.assertThat(newEvent, is(notNullValue()));
	}
	
	@Test
	public void EventのmwaUriの取得テスト() throws Exception {
		event = events.getEvent("55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4");
		Assert.assertThat(event, is(notNullValue()));
		//String mwaUriStr = event.getImageLocation().get();
		//System.out.println(mwaUriStr);
	}
}
