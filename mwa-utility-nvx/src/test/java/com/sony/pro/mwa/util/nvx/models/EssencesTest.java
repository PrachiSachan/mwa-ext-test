package com.sony.pro.mwa.util.nvx.models;

import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.util.ResourceUtil;
import com.sony.pro.mwa.util.nvx.asset.models.Asset;
import com.sony.pro.mwa.util.nvx.asset.models.Assets;
import com.sony.pro.mwa.util.nvx.asset.models.Constants;
import com.sony.pro.mwa.util.nvx.asset.models.Essence;
import com.sony.pro.mwa.util.nvx.asset.models.Essences;
import com.sony.pro.mwa.util.nvx.asset.models.filters.EssenceFilter;
import com.sony.pro.mwa.util.uri.MwaUri;

/**
 * com.sony.pro.mwa.util.nvx.asset.models.Essencesのテスト
 */
public class EssencesTest {

	private ObjectMapper mapper = new ObjectMapper();
	private ResourceUtil resUtils = new ResourceUtil();
	
	private JsonNode assetRootNode = null;
	private Essences essences = null;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		assetRootNode = mapper.readValue(resUtils.getJsonTextResource("audio_video_sample.json").toString(), JsonNode.class);
		essences = new Essences(assetRootNode.findPath(Constants.Asset.Essences));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void EssenceFilterのテスト001() throws Exception {
		System.out.println("EssenceFilterのテスト001()----------------------------------");
		// Arrange
		String jsonText = resUtils.getJsonTextResource("4essences_test.json").toString();
		
		// Action
		Assets assets = new Assets(jsonText);
		List<Asset> assetList = assets.get();
		
		// Assert
		for (Asset asset : assetList) {
			System.out.println(asset.getAssetId());
			
			Essences essences = asset.getEssences();

			List<MwaUri> uris = essences.getLocations(new EssenceFilter() {
				// proxy=falseのものだけ返す
				public boolean accept(Essence essence) {
					try {
						return essence.isProxy() == false;
					} catch (Exception e) {
						// ログに出すとか。。。？
						return false;
					}
				}
			});
			
			for (MwaUri uri : uris) {
				System.out.println("\t" + uri.get());
			}
		}
	}
	
	@Test
	public void EssenceFilterのテスト002() throws Exception {
		System.out.println("EssenceFilterのテスト002()---------------------");
		// Arrange
		String jsonText = resUtils.getJsonTextResource("4essences_test.json").toString();
		
		// Action
		Assets assets = new Assets(jsonText);
		List<Asset> assetList = assets.get();
		
		// Assert
		for (Asset asset : assetList) {
			System.out.println(asset.getAssetId());
			
			Essences essences = asset.getEssences();

			List<MwaUri> uris = essences.getLocations(new EssenceFilter() {
				// proxy=falseのものだけ返す
				public boolean accept(Essence essence) {
					try {
						String location = essence.getLocation().getLocation();
						return !"ODA".equals(location) && !"ODA_LIBRARY".equals(location);
					} catch (Exception e) {
						// ログに出すとか。。。？
						return false;
					}
				}
			});
			
			for (MwaUri uri : uris) {
				System.out.println("\t" + uri.get());
			}
		}
	}
	@Test
	public void 単体のAssetデータを指定する場合の使用例001() throws Exception {
		System.out.println("単体のAssetデータを指定する場合の使用例001()--------------------------");
		// Arrange
		String jsonText = resUtils.getJsonTextResource("4asset_test_multi_essence.json").toString();
		// Action
		Asset asset = new Asset(jsonText);
		
		// Assert
		Essences essences = asset.getEssences();

		List<MwaUri> uris = essences.getLocations(new EssenceFilter() {
			// proxy=falseのものだけ返す
			public boolean accept(Essence essence) {
				try {
					String location = essence.getLocation().getLocation();
					return !"ODA".equals(location) && !"ODA_LIBRARY".equals(location);
				} catch (Exception e) {
					// ログに出すとか。。。？
					return false;
				}
			}
			});
			
		for (MwaUri uri : uris) {
			System.out.println(uri.get());
		}
	}	

	@Test
	public void 単体のAssetデータを指定する場合の使用例002() throws Exception {
		System.out.println("単体のAssetデータを指定する場合の使用例002()-----------------------------");
		// Arrange
		String jsonText = resUtils.getJsonTextResource("4asset_test_multi_essence.json").toString();
		// Action
		Asset asset = new Asset(jsonText);
		
		// Assert
		Essences essences = asset.getEssences();

		List<MwaUri> uris = essences.getLocations();
			
		for (MwaUri uri : uris) {
			System.out.println(uri.get());
		}
	}	
	
	@Test
	public void EssencesのType取得() throws Exception {
		List<Essence> essenceList = essences.get();
		for (Essence essence : essenceList) {
			System.out.println(essence.getType());
		}
		System.out.println("===================================");
		JsonNode nodeValues = null;
		try {
			String listString = mapper.writeValueAsString(essenceList);
			System.out.println(listString);
			nodeValues = mapper.readTree(listString);	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
}
