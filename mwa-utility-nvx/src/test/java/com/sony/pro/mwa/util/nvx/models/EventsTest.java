package com.sony.pro.mwa.util.nvx.models;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.util.ResourceUtil;
import com.sony.pro.mwa.util.nvx.asset.models.Asset;
import com.sony.pro.mwa.util.nvx.asset.models.Constants;
import com.sony.pro.mwa.util.nvx.asset.models.Event;
import com.sony.pro.mwa.util.nvx.asset.models.Events;

public class EventsTest {
	private ObjectMapper mapper = new ObjectMapper();
	private ResourceUtil resUtils = new ResourceUtil();
	
	private JsonNode assetRootNode = null;
	private Asset asset = null;
	private Events events = null;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		assetRootNode = mapper.readValue(resUtils.getJsonTextResource("audio_video_sample.json").toString(), JsonNode.class);
		asset = new Asset(assetRootNode.findPath(Constants.Asset._Source));
		events = new Events(assetRootNode.findPath(Constants.Asset.Events));
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void Eventsの取得テスト001() throws Exception {
		List<Event> eventList = events.getEventList();
		Assert.assertThat(eventList, is(notNullValue()));
		for (Event event : eventList) {
			System.out.println(event.getId());
			//System.out.println(event.getImageLocation().get());
		}
	}
	
	@Test
	public void Eventsの取得テスト002() throws Exception {
		String eventId = "55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4";
		List<Event> eventList = events.getEventList();
		Assert.assertThat(eventList, is(notNullValue()));
		for (Event event : eventList) {
			if (eventId.equals(event.getId())) {
				System.out.println(event.getId());
				System.out.println(event.getTitle());
			}
		}
	}
	
	@Test
	public void Eventの追加テスト001() throws Exception {
		List<Event> eventList = events.getEventList();
		Event event = new Event();
		event.setId("00000000-1111-2222-3333-444444444444");
		event.setTitle("EventModel の追加テスト");
		event.setType("KEY_FRAME");
		event.setImageLocation("mwa://test/xxxxxxxxxxxxxxxxx/0000000000000000000000000/image.jpg");
		event.setFrame(0);
		event.setDuration(1);
		eventList.add(event);
		
		for (Event e : eventList) {
			System.out.println(e.getId());
			System.out.println(e.getTitle());
			System.out.println(e.getType());
			System.out.println(e.getTitle());
			System.out.println(e.getImageLocation());
			System.out.println(e.getFrame());
			System.out.println(e.getDuration());
			System.out.println("==============================================================");
		}
	}
}
