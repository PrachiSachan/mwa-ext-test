package com.sony.pro.mwa.util.text;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.sony.pro.mwa.util.exceptions.ExportException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.testng.Assert;

import com.sony.pro.mwa.util.exceptions.NLEExportException;
import com.sony.pro.mwa.util.nvx.asset.models.exceptions.AssetModelsException;

/**
 * 文字の計算・変換などを行うクラス
 */
public class TextConverterTest {

	@Test
	public void convertFileNameWithDecode_PrefixConvert_001() throws ExportException {
		String baseName = "CLOCK$_test";
		String actual = TextConverter.normalizedFileName(baseName, 250, false);
		String expect = "_CLOCK$_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void convertFileNameWithDecode_PrefixConvert_002() throws ExportException {
		String baseName = "\ntest";
		String actual = TextConverter.normalizedFileName(baseName, 250, false);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void convertFileNameWithDecode_PrefixConvert_ResultEncodeTrue_001() throws ExportException {
		String baseName = "CLOCK$_test";
		String actual = TextConverter.normalizedFileName(baseName, 250, true);
		String expect = "_CLOCK%24_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void convertFileNameWithDecode_EncodedPrefixConvert_001() throws ExportException {
		String baseName = "CLOCK%24_test";
		String actual = TextConverter.normalizedFileName(baseName, 250, false);
		String expect = "_CLOCK$_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void convertFileNameWithDecode_EncodedPrefixConvert_002() throws ExportException {
		String baseName = "%C2%A5r%C2%A5ntest";
		String actual = TextConverter.normalizedFileName(baseName, 250, false);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void convertFileNameWithDecode_EncodedPrefixConvert_003() throws ExportException {
		String baseName = "%C2%A5ntest";
		String actual = TextConverter.normalizedFileName(baseName, 250, false);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void convertFileNameWithDecode_EncodedPrefixConvert_004() throws ExportException {
		String baseName = "%C2%A5r%C2%A5ntest";
		String actual = TextConverter.normalizedFileName(baseName, 250, false);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void convertFileNameWithDecode_baseNameIsNull() throws ExportException {
		String baseName = null;
		try {
			String actual = TextConverter.normalizedFileName(baseName, 250, false);
			Assert.fail();
		} catch (ExportException e) {
			Assert.assertEquals(e.getMessage(), "The value null specified for encodeFileName is invalid.");
		}
	}

	@Test
	public void convertFileNameWithDecode_baseNameIsEmpty() {
		String baseName = "";
		try {
			String actual = TextConverter.normalizedFileName(baseName, 250, false);
			Assert.fail();
		} catch (ExportException e) {
			Assert.assertEquals(e.getMessage(), "The value  specified for encodeFileName is invalid.");
		}
	}

	@Test
	public void convertFileNameWithDecode_fileLengthIs0() {
		String baseName = "test";
		try {
			String actual = TextConverter.normalizedFileName(baseName, 0, false);
			Assert.fail();
		} catch (ExportException e) {
			Assert.assertEquals(e.getMessage(), "The value 0 specified for fileLength is invalid.");
		}
	}

	@Test
	public void convertFileNameWithDecode_fileLengthIsMinus() {
		String baseName = "test";
		try {
			String actual = TextConverter.normalizedFileName(baseName, -1, false);
			Assert.fail();
		} catch (ExportException e) {
			Assert.assertEquals(e.getMessage(), "The value -1 specified for fileLength is invalid.");
		}
	}

	@Test
	public void replaceEncodeSymbol_001() {
		String baseName = "%C2%A5r%C2%A5ntest";
		String actual = TextConverter.replaceEncodeSymbol(baseName);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void replaceEncodeSymbol_002() {
		String baseName = "%C2%A5ntest";
		String actual = TextConverter.replaceEncodeSymbol(baseName);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void replaceEncodeSymbol_003() {
		String baseName = "%C2%A5test";
		String actual = TextConverter.replaceEncodeSymbol(baseName);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void replaceSymbol_001() {
		String baseName = "\r\ntest";
		String actual = TextConverter.replaceSymbol(baseName);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void replaceSymbol_002() {
		String baseName = "\ntest";
		String actual = TextConverter.replaceSymbol(baseName);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void replaceSymbol_003() {
		String baseName = "\\test";
		String actual = TextConverter.replaceSymbol(baseName);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void replaceSymbol_004() {
		String baseName = "/test";
		String actual = TextConverter.replaceSymbol(baseName);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void replaceSymbol_005() {
		String baseName = ":test";
		String actual = TextConverter.replaceSymbol(baseName);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void replaceSymbol_006() {
		String baseName = "*test";
		String actual = TextConverter.replaceSymbol(baseName);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void replaceSymbol_007() {
		String baseName = "\"test";
		String actual = TextConverter.replaceSymbol(baseName);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void replaceSymbol_008() {
		String baseName = "<test";
		String actual = TextConverter.replaceSymbol(baseName);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void replaceSymbol_009() {
		String baseName = ">test";
		String actual = TextConverter.replaceSymbol(baseName);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void replaceSymbol_010() {
		String baseName = "|test";
		String actual = TextConverter.replaceSymbol(baseName);
		String expect = "_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_001() {
		String baseName = "CON_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_CON_test";
		Assert.assertEquals(expect, actual);
	}


	@Test
	public void addPrefix_002() {
		String baseName = "PRN_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_PRN_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_003() {
		String baseName = "AUX_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_AUX_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_004() {
		String baseName = "NUL_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_NUL_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_005() {
		String baseName = "CLOCK$_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_CLOCK$_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_006() {
		String baseName = "COM0_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_COM0_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_007() {
		String baseName = "COM1_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_COM1_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_008() {
		String baseName = "COM2_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_COM2_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_009() {
		String baseName = "COM3_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_COM3_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_010() {
		String baseName = "COM4_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_COM4_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_011() {
		String baseName = "COM5_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_COM5_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_012() {
		String baseName = "COM6_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_COM6_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_013() {
		String baseName = "COM7_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_COM7_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_014() {
		String baseName = "COM8_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_COM8_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_015() {
		String baseName = "COM9_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_COM9_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_016() {
		String baseName = "LPT0_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_LPT0_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_017() {
		String baseName = "LPT1_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_LPT1_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_018() {
		String baseName = "LPT2_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_LPT2_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_019() {
		String baseName = "LPT3_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_LPT3_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_020() {
		String baseName = "LPT4_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_LPT4_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_021() {
		String baseName = "LPT5_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_LPT5_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_022() {
		String baseName = "LPT6_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_LPT6_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_023() {
		String baseName = "LPT7_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_LPT7_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_024() {
		String baseName = "LPT8_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_LPT8_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void addPrefix_025() {
		String baseName = "LPT9_test";
		String actual = TextConverter.addPrefix(baseName);
		String expect = "_LPT9_test";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void cutFileNameLength_underMaxLength() throws NLEExportException {
		String baseName = "0123";
		String actual = TextConverter.cutLength(baseName, 5);
		String expect = "0123";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void cutFileNameLength_equalMaxLength() throws NLEExportException {
		String baseName = "01234";
		String actual = TextConverter.cutLength(baseName, 5);
		String expect = "01234";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void cutFileNameLength_overMaxLength() throws NLEExportException {
		String baseName = "012345";
		String actual = TextConverter.cutLength(baseName, 5);
		String expect = "01234";
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void calcMasterMaxLength_filePathUnderMaxLength() throws NLEExportException  {
		String directoryPath = "C:\\012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012\\";
		String extension = ".mxf";
		int actual = TextConverter.calcFileNameLength(directoryPath, extension);
		int expect = 8;
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void calcMasterMaxLength_filePathEqualMaxLength() throws NLEExportException  {
		String directoryPath = "C:\\0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123\\";
		String extension = ".mxf";
		int actual = TextConverter.calcFileNameLength(directoryPath, extension);
		int expect = 7;
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void calcMasterMaxLength_filePathOverMaxLength() {
		String directoryPath = "C:\\01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234\\";
		String extension = ".mxf";
		int actual = 0;
		try {
			actual = TextConverter.calcFileNameLength(directoryPath, extension);
			Assert.fail();
		} catch (NLEExportException e) {
			System.out.println(e.getMessage());
			Assert.assertEquals(e.getMessage(), "File path length is over 259 characters.[ Export file name is more than 259 characters. ]");
		}
	}

	@Test
	public void calcProxyMaxLength_filePathUnderMaxLength() throws NLEExportException  {
		String directoryPath = "C:\\012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012\\";
		String extension = ".mxf";
		int actual = TextConverter.calcProxyNameLength(directoryPath, extension);
		int expect = 2;
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void calcProxyMaxLength_filePathEqualMaxLength() throws NLEExportException  {
		String directoryPath = "C:\\0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123\\";
		String extension = ".mxf";
		int actual = TextConverter.calcProxyNameLength(directoryPath, extension);
		int expect = 1;
		Assert.assertEquals(expect, actual);
	}

	@Test
	public void calcProxyMaxLength_filePathOverMaxLength() {
		String directoryPath = "C:\\01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234\\";
		String extension = ".mxf";
		int actual = 0;
		try {
			actual = TextConverter.calcProxyNameLength(directoryPath, extension);
			Assert.fail();
		} catch (NLEExportException e) {
			System.out.println(e.getMessage());
			Assert.assertEquals(e.getMessage(), "File path length is over 259 characters.[ Export file name is more than 259 characters. ]");
		}
	}

	@Test
	public void addDelimiter_DirectoryPathEndsWithBackSlash() {
		String directoryPath = "\\\\11.11.111.11\\directory\\";
		String actual = TextConverter.addDelimiter(directoryPath);
		String expect = "\\\\11.11.111.11\\directory\\";
		Assert.assertEquals(actual, expect);
	}

	@Test
	public void addDelimiter_DirectoryPathDoesNotEndWithBackSlash() {
		String directoryPath = "\\\\11.11.111.11\\directory";
		String actual = TextConverter.addDelimiter(directoryPath);
		String expect = "\\\\11.11.111.11\\directory\\";
		Assert.assertEquals(actual, expect);
	}

	@Test
	public void addDelimiter_DirectoryPathEndsWithSlash() {
		String directoryPath = "//11.11.111.11/directory/";
		String actual = TextConverter.addDelimiter(directoryPath);
		String expect = "//11.11.111.11/directory/";
		Assert.assertEquals(actual, expect);
	}

	@Test
	public void addDelimiter_DirectoryPathDoesNotEndWithSlash() {
		String directoryPath = "//11.11.111.11/directory";
		String actual = TextConverter.addDelimiter(directoryPath);
		String expect = "//11.11.111.11/directory/";
		Assert.assertEquals(actual, expect);
	}

	@Test
	public void isUUID_ID() {
		boolean result = TextConverter.isUUID("edf7179e-7d18-401d-b219-29b669c5c101");
		System.out.println(result);
		Assert.assertTrue(result);
	}

	@Test
	public void isUUID_UUIDInFilePath() {
		boolean result = TextConverter.isUUID("\\\\43.16.194.78\\volumes\\fdf7179e-7d18-401d-b219-29b669c5c101");
		System.out.println(result);
		Assert.assertFalse(result);
	}

	@Test
	public void isUUID_InputIsNull() {
		boolean result = TextConverter.isUUID(null);
		Assert.assertFalse(result);
	}

	@Test
	public void isUUID_InputIsSpace() {
		boolean result = TextConverter.isUUID("");
		Assert.assertFalse(result);
	}

	@Test
	public void isUNC_BackSlashCase() {
		boolean result = TextConverter.validIp("\\\\43.16.194.78\\volumes\\fdf7179e-7d18-401d-b219-29b669c5c101");
		System.out.println(result);
		Assert.assertTrue(result);
	}

	@Test
	public void isUNC_SlashCase() {
		boolean result = TextConverter.validIp("//43.16.194.78/volumes/fdf7179e-7d18-401d-b219-29b669c5c101");
		System.out.println(result);
		Assert.assertTrue(result);
	}

	@Test
	public void isUNC_IPAdrressIsInvalidFormat() {
		boolean result = TextConverter.validIp("\\\\43.16.194.78.00\\volumes\\fdf7179e-7d18-401d-b219-29b669c5c101");
		System.out.println(result);
		Assert.assertFalse(result);
	}

	@Test
	public void isUNC_WindowsPath() {
		boolean result = TextConverter.validIp("C:/volumes/fdf7179e-7d18-401d-b219-29b669c5c101");
		System.out.println(result);
		Assert.assertFalse(result);
	}

	@Test
	public void isUNC_InputIsNull() {
		boolean result = TextConverter.validIp(null);
		Assert.assertFalse(result);
	}

	@Test
	public void isUNC_InputIsSpace() {
		boolean result = TextConverter.validIp("");
		Assert.assertFalse(result);
	}

	@Test
	public void isWindows_BackSlashCase() {
		boolean result = TextConverter.isWindows("C:\\volumes\\fdf7179e-7d18-401d-b219-29b669c5c101");
		System.out.println(result);
		Assert.assertTrue(result);
	}

	@Test
	public void isWindows_SlashCase() {
		boolean result = TextConverter.isWindows("C:/volumes/fdf7179e-7d18-401d-b219-29b669c5c101");
		System.out.println(result);
		Assert.assertTrue(result);
	}

	@Test
	public void isWindows_DriveLetterIsLowerCaseLetter() {
		boolean result = TextConverter.isWindows("c:/volumes/fdf7179e-7d18-401d-b219-29b669c5c101");
		System.out.println(result);
		Assert.assertTrue(result);
	}

	@Test
	public void isWindows_DriveLetterOnly() {
		boolean result = TextConverter.isWindows("D:/");
		System.out.println(result);
		Assert.assertTrue(result);
	}

	@Test
	public void isWindows_UNCPath() {
		boolean result = TextConverter.isWindows("//43.16.194.78/volumes/fdf7179e-7d18-401d-b219-29b669c5c101");
		System.out.println(result);
		Assert.assertFalse(result);
	}

	@Test
	public void isWindows_InputIsNull() {
		boolean result = TextConverter.isWindows(null);
		Assert.assertFalse(result);
	}

	@Test
	public void isWindows_InputIsSpace() {
		boolean result = TextConverter.isWindows("");
		Assert.assertFalse(result);
	}

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	@Test
	public void renameFile_001() throws NLEExportException {
		try {
			tempFolder.newFile("A.MXF");
		} catch (IOException e) {
			Assert.fail();
		}
		Path path = Paths.get(tempFolder.getRoot().getAbsolutePath() + "\\A.MXF");
		String result = TextConverter.renameFile(path).toString();
		String expect = tempFolder.getRoot().getAbsolutePath() + "\\A(1).MXF";
		Assert.assertEquals(result, expect);
	}

	@Test
	public void renameFile_002() throws NLEExportException {
		try {
			tempFolder.newFile("A.MXF");
			tempFolder.newFile("A(1).MXF");
		} catch (IOException e) {
			Assert.fail();
		}
		Path path = Paths.get(tempFolder.getRoot().getAbsolutePath() + "\\A.MXF");
		String result = TextConverter.renameFile(path).toString();
		String expect = tempFolder.getRoot().getAbsolutePath() + "\\A(2).MXF";
		Assert.assertEquals(result, expect);
	}

	@Test
	public void decodeOutput_001() throws UnsupportedEncodingException {
		String result = TextConverter.decode("1%21%23%24%25%26%27%28%29%2B%20-.%20%20%40%5B%5D%5E_%60%7B%7D%7E");
		String expect = "1!#$%&'()+ -.  @[]^_`{}~";
		Assert.assertEquals(result, expect);
	}

	@Test
	public void decodeOutput_002() throws UnsupportedEncodingException {
		String result = TextConverter.decode("test_01.MXF");
		String expect = "test_01.MXF";
		Assert.assertEquals(result, expect);
	}
}
