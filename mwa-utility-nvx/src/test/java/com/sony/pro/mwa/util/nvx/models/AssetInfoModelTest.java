package com.sony.pro.mwa.util.nvx.models;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.util.ResourceUtil;
import com.sony.pro.mwa.util.nvx.asset.models.AssetInfo;
import com.sony.pro.mwa.util.nvx.asset.models.Constants;
import com.sony.pro.mwa.util.nvx.asset.models.exceptions.AssetModelsException;


/**
 * Unit test of asset info model.
 * 
 * @author Kousuke Hosaka
 *
 */
public class AssetInfoModelTest {

	private ObjectMapper mapper = new ObjectMapper();
	private ResourceUtil resUtils = new ResourceUtil();
	
	private JsonNode assetRootNode = null;
	private AssetInfo assetInfo = null;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		assetRootNode = mapper.readValue(resUtils.getJsonTextResource("audio_video_sample.json").toString(), JsonNode.class);		
		assetInfo = new AssetInfo(assetRootNode.findPath(Constants.Asset.AssetInfo));
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void title取得のテスト() throws Exception {
		String title = assetInfo.getTitle();
		Assert.assertThat(title, is(notNullValue()));
		System.out.println("title : " + title);
	}
	
	@Test
	public void title更新のテスト() throws Exception {
		String title = assetInfo.getTitle();
		System.out.println("更新前 : " + title);
		
		String updatedTitle = "Updated Title";
		assetInfo.setTitle(updatedTitle);
		Assert.assertThat(assetInfo.getTitle(), is(updatedTitle));
		System.out.println("更新後 : " + assetInfo.getTitle());
	}
	
	@Test
	public void rating取得のテスト() throws Exception {
		Assert.assertThat(assetInfo.getRating(), is(instanceOf(Integer.class)));
		int rating = assetInfo.getRating();
		Assert.assertThat(rating, is(notNullValue()));
		System.out.println("rating : " + rating);
	}
	
	@Test
	public void rating更新のテスト() throws Exception {
		int rating = assetInfo.getRating();
		System.out.println("更新前 : " + rating);
		
		int updatedRating = 5;
		assetInfo.setRating(updatedRating);
		Assert.assertThat(assetInfo.getRating(), is(updatedRating));
		System.out.println("更新後 : " + assetInfo.getRating());
	}
	
	@Test
	public void note取得のテスト() throws Exception {
		String note = assetInfo.getNote();
		Assert.assertThat(note, is(notNullValue()));
		System.out.println("note : " + note);
	}
	
	@Test
	public void note更新のテスト() throws Exception {
		String note = assetInfo.getNote();
		System.out.println("更新前 : " + note);
		
		String updatedNote = "\nテスト用のノートです。\n更新後の場合のみ表示されます。";
		assetInfo.setNote(updatedNote);
		Assert.assertThat(assetInfo.getNote(), is(updatedNote));
		System.out.println("更新後 : " + assetInfo.getNote());
	}
	
	@Test
	public void status取得のテスト() throws Exception {
		String status = assetInfo.getStatus();
		Assert.assertThat(status, is(notNullValue()));
		System.out.println("status : " + status);
	}
	
	@Test
	public void status更新のテスト() throws Exception {
		String status = assetInfo.getStatus();
		System.out.println("更新前 : " + status);
		
		String updatedStatus = "Offline";
		assetInfo.setStatus(updatedStatus);
		Assert.assertThat(assetInfo.getStatus(), is(updatedStatus));
		System.out.println("更新後 : " + assetInfo.getStatus());
	}
	
	@Test
	public void archived取得のテスト() throws Exception {
		Assert.assertThat(assetInfo.getArchived(), is(instanceOf(Boolean.class)));
		boolean archived = assetInfo.getArchived();
		Assert.assertThat(archived, is(notNullValue()));
		System.out.println("archived : " + archived);
	}
	
	@Test
	public void archived更新のテスト() throws Exception {
		boolean archived = assetInfo.getArchived();
		System.out.println("更新前 : " + archived);
		
		boolean updatedArchived = true;
		assetInfo.setArchived(updatedArchived);
		Assert.assertThat(assetInfo.getArchived(), is(updatedArchived));
		System.out.println("更新後 : " + assetInfo.getArchived());
	}
	
	@Test
	public void type取得のテスト() throws Exception {
		String type = assetInfo.getType();
		Assert.assertThat(type, is(notNullValue()));
		System.out.println("type : " + type);
	}
	
	@Test
	public void type更新のテスト() throws Exception {
		String type = assetInfo.getType();
		System.out.println("更新前 : " + type);
		
		String updatedType = "VIDEO";
		assetInfo.setType(updatedType);
		Assert.assertThat(assetInfo.getType(), is(updatedType));
		System.out.println("更新後 : " + assetInfo.getType());
	}
	
	@Test
	public void startTimecode取得のテスト() throws Exception {
		Assert.assertThat(assetInfo.getStartTimecode(), is(instanceOf(String.class)));
		String startTimecode = assetInfo.getStartTimecode();
		Assert.assertThat(startTimecode, is(notNullValue()));
		System.out.println("startTimecode : " + startTimecode);
	}
	
	@Test
	public void startTimecode更新のテスト() throws Exception {
		String startTimecode = assetInfo.getStartTimecode();
		System.out.println("更新前 : " + startTimecode);
		
		String updatedStartTimecode = "99999999";
		assetInfo.setStartTimecode(updatedStartTimecode);
		Assert.assertThat(assetInfo.getStartTimecode(), is(updatedStartTimecode));
		System.out.println("更新後 : " + assetInfo.getStartTimecode());
	}
	
	@Test
	public void startTimecode_msec取得のテスト() throws Exception {
		Assert.assertThat(assetInfo.getStartTimecode_msec(), is(instanceOf(Long.class)));
		long startTimecode_msec = assetInfo.getStartTimecode_msec();
		Assert.assertThat(startTimecode_msec, is(notNullValue()));
		System.out.println("startTimecode : " + startTimecode_msec);
	}
	
	@Test
	public void startTimecode_msec更新のテスト() throws Exception {
		long startTimecode_msec = assetInfo.getStartTimecode_msec();
		System.out.println("更新前 : " + startTimecode_msec);
		
		long updatedStartTimecode_msec = 5000000;
		assetInfo.setStartTimecode_msec(updatedStartTimecode_msec);
		Assert.assertThat(assetInfo.getStartTimecode_msec(), is(updatedStartTimecode_msec));
		System.out.println("更新後 : " + assetInfo.getStartTimecode_msec());
	}
	
	@Test
	public void duration取得のテスト() throws Exception {
		Assert.assertThat(assetInfo.getDuration(), is(instanceOf(Long.class)));
		long duration = assetInfo.getDuration();
		Assert.assertThat(duration, is(notNullValue()));
		System.out.println("duration : " + duration);
	}
	
	@Test
	public void duration更新のテスト() throws Exception {
		long duration = assetInfo.getDuration();
		System.out.println("更新前 : " + duration);
		
		long updatedDuration = 40;
		assetInfo.setDuration(updatedDuration);
		Assert.assertThat(assetInfo.getDuration(), is(updatedDuration));
		System.out.println("更新後 : " + assetInfo.getDuration());
	}
	
	@Test
	public void length取得のテスト() throws Exception {
		Assert.assertThat(assetInfo.getLength(), is(instanceOf(Long.class)));
		long length = assetInfo.getLength();
		Assert.assertThat(length, is(notNullValue()));
		System.out.println("length : " + length);
	}
	
	@Test
	public void length更新のテスト() throws Exception {
		long length = assetInfo.getLength();
		System.out.println("更新前 : " + length);
		
		long updatedLength = 3000;
		assetInfo.setLength(updatedLength);
		Assert.assertThat(assetInfo.getLength(), is(updatedLength));
		System.out.println("更新後 : " + assetInfo.getLength());
	}
	
	@Test
	public void numerator取得のテスト() throws Exception {
		Assert.assertThat(assetInfo.getNumerator(), is(instanceOf(Long.class)));
		long numerator = assetInfo.getNumerator();
		Assert.assertThat(numerator, is(notNullValue()));
		System.out.println("numerator : " + numerator);
	}
	
	@Test
	public void numerator更新のテスト() throws Exception {
		long numerator = assetInfo.getNumerator();
		System.out.println("更新前 : " + numerator);
		
		long updatedNumerator = 60000;
		assetInfo.setNumerator(updatedNumerator);
		Assert.assertThat(assetInfo.getNumerator(), is(updatedNumerator));
		System.out.println("更新後 : " + assetInfo.getNumerator());
	}
	
	@Test
	public void denominator取得のテスト() throws Exception {
		Assert.assertThat(assetInfo.getDenominator(), is(instanceOf(Long.class)));
		long denominator = assetInfo.getDenominator();
		Assert.assertThat(denominator, is(notNullValue()));
		System.out.println("denominator : " + denominator);
	}
	
	@Test
	public void denominator更新のテスト() throws Exception {
		long denominator = assetInfo.getDenominator();
		System.out.println("更新前 : " + denominator);
		
		long updatedDenominator = 1000;
		assetInfo.setDenominator(updatedDenominator);
		Assert.assertThat(assetInfo.getDenominator(), is(updatedDenominator));
		System.out.println("更新後 : " + assetInfo.getDenominator());
	}
	
	@Test
	public void frameRate取得のテスト() throws Exception {
		Assert.assertThat(assetInfo.getFrameRate(), is(instanceOf(Double.class)));
		double frameRate = assetInfo.getFrameRate();
		Assert.assertThat(frameRate, is(notNullValue()));
		System.out.println("frameRate : " + frameRate);
	}
	
	@Test
	public void frameRate更新のテスト() throws Exception {
		double frameRate = assetInfo.getFrameRate();
		System.out.println("更新前 : " + frameRate);
		
		double updatedFrameRate = 60.00;
		assetInfo.setFrameRate(updatedFrameRate);
		// closeTo で比較したいが、pom に依存関係書かないとダメっぽいので保留
		// Assert.assertThat(assetInfo.getFrameRate(), is(closeTo(updatedFrameRate, 0.0));
		Assert.assertEquals(updatedFrameRate, assetInfo.getFrameRate(), 0.0);
		System.out.println("更新後 : " + assetInfo.getFrameRate());
	}
	
	@Test
	public void dropFrame取得のテスト() throws Exception {
		Assert.assertThat(assetInfo.getDropFrame(), is(instanceOf(Boolean.class)));
		boolean dropFrame = assetInfo.getDropFrame();
		Assert.assertThat(dropFrame, is(notNullValue()));
		System.out.println("dropFrame : " + dropFrame);
	}
	
	@Test
	public void dropFrame更新のテスト() throws Exception {
		boolean dropFrame = assetInfo.getDropFrame();
		System.out.println("更新前 : " + dropFrame);
		
		boolean updatedDropFrame = false;
		assetInfo.setDropFrame(updatedDropFrame);
		Assert.assertThat(assetInfo.getDropFrame(), is(updatedDropFrame));
		System.out.println("更新後 : " + assetInfo.getDropFrame());
	}
	
	@Test
	public void odsCardridgeIds取得更新のテスト() throws Exception {
		Assert.assertThat(assetInfo.getOdsCartridgeIds(), is(instanceOf(List.class)));
		// odsCardridgeIds の取得
		List<String> odsCardridgeIds = assetInfo.getOdsCartridgeIds();
		Assert.assertThat(assetInfo.getOdsCartridgeIds(), is(notNullValue()));
		
		System.out.println("odsCardridgeIds : ↓");
		for (String cartridgeId : odsCardridgeIds) {
			System.out.println(cartridgeId);
		}

		// 更新テスト用フラグ、確認したい場合はtrue
		boolean isUpdate = true;
		
		if (isUpdate) {
			// 更新用リストの作成
			List<String> dummyList = new ArrayList();
			dummyList.add("002 0123456789");
			dummyList.add("003 0123456789");
			dummyList.add("004 0123456789");
			
	        assetInfo.setOdsCartridgeIds(dummyList);
	        Assert.assertThat(assetInfo.getOdsCartridgeIds(), hasItems("002 0123456789", "003 0123456789", "004 0123456789"));
	        for (String cartridgeId : assetInfo.getOdsCartridgeIds()) {
	        	System.out.println(cartridgeId);
	        }
	        
		}
	}
	
	@Test
	public void odsBarcodeIds取得更新のテスト() throws Exception {
		Assert.assertThat(assetInfo.getOdsBarcodeIds(), is(instanceOf(List.class)));
		// odsBarcodeIds の取得
		List<String> odsBarcodeIds = assetInfo.getOdsBarcodeIds();
		Assert.assertThat(odsBarcodeIds, is(notNullValue()));
		
		System.out.println("odsBarcodeIds : ↓");
		for (String barcodeId : odsBarcodeIds) {
			System.out.println(barcodeId);
		}
		
		// 更新テスト用フラグ、確認したい場合はtrue
		boolean isUpdate = true;
		
		if (isUpdate) {
			// 更新用リストの作成
			List<String> dummyList = new ArrayList();
			dummyList.add("00000000000000");
			dummyList.add("99999999999999");
			dummyList.add("11111111111111");
	        	        
	        assetInfo.setOdsBarcodeIds(dummyList);
	        
	        Assert.assertThat(assetInfo.getOdsBarcodeIds(), hasItems("00000000000000", "99999999999999", "11111111111111"));
	        for (String barcodeId : assetInfo.getOdsBarcodeIds()) {
	        	System.out.println(barcodeId);
	        }
		}
	}
	
	@Test
	public void copyOdsCartridgeIds取得更新のテスト() throws Exception {
		Assert.assertThat(assetInfo.getCopyOdsCartridgeIds(), is(instanceOf(List.class)));
		// copyOdsCardridgeIds の取得
		List<String> copyOdsCardridgeIds = assetInfo.getCopyOdsCartridgeIds();
		Assert.assertThat(copyOdsCardridgeIds, is(notNullValue()));
		
		System.out.println("copyOdsCardridgeIds : ↓");
		for (String copyOdsCartridgeId : copyOdsCardridgeIds) {
			System.out.println(copyOdsCartridgeId);
		}
		
		// 更新テスト用フラグ、確認したい場合はtrue
		boolean isUpdate = true;
		
		if (isUpdate) {		
			// 更新用リストの作成
			List<String> dummyList = new ArrayList();
			dummyList.add("999 9999999999");
			dummyList.add("111 1111111111");
			
	        assetInfo.setCopyOdsCartridgeIds(dummyList);
	        
	        Assert.assertThat(assetInfo.getCopyOdsCartridgeIds(), hasItems("999 9999999999", "111 1111111111"));
			for (String copyOdsCartridgeId : assetInfo.getCopyOdsCartridgeIds()) {
				System.out.println(copyOdsCartridgeId);
			}
		}
	}
	
	@Test
	public void copyOdsBarcodeIds取得更新のテスト() throws Exception {
		Assert.assertThat(assetInfo.getCopyBarcodeIds(), is(instanceOf(List.class)));
		// copyOdsBarcodeIds の取得
		List<String> copyOdsBarcodeIds = assetInfo.getCopyBarcodeIds();
		Assert.assertThat(copyOdsBarcodeIds, is(notNullValue()));
		
		System.out.println("copyOdsBarcodeIds : ↓");
		for (String copyOdsBarcodeId : copyOdsBarcodeIds) {
			System.out.println(copyOdsBarcodeId);
		}
		
		// 更新テスト用フラグ、確認したい場合はtrue
		boolean isUpdate = true;
		
		if (isUpdate) {		
			// 更新用リストの作成
			List<String> dummyList = new ArrayList();
			dummyList.add("000 00000000000");
			
			assetInfo.setCopyBarcodeIds(dummyList);
			
			Assert.assertThat(assetInfo.getCopyBarcodeIds(), hasItems("000 00000000000"));
			for (String copyOdsBarcodeId : assetInfo.getCopyBarcodeIds()) {
				System.out.println(copyOdsBarcodeId);
			}
		}
	}
	
	@Test
	public void customMetadata取得更新のテスト() throws Exception {
		Assert.assertThat(assetInfo.getCustomMetadata(), is(instanceOf(JsonNode.class)));
		// customMetadata の取得
		JsonNode customMetadata = assetInfo.getCustomMetadata();		
		Assert.assertThat(customMetadata, is(notNullValue()));
		
		// TODO : customMetadata の更新		
	}
	
	@Test
	public void jsonNodeからlistに変換() {
		try {
			JsonNode stub = mapper.readValue(
					resUtils.getJsonTextResource("audio_video_sample.json").toString(), 
					JsonNode.class);
			
			// Root の取得
			JsonNode assetInfo = stub.findPath(Constants.Asset.AssetInfo);
			JsonNode nodeValues = assetInfo.get(Constants.AssetInfo.OdsCartridgeIds);
			Assert.assertThat(nodeValues, is(notNullValue()));
			
			List<String> valueList = new ArrayList<>();
			if (nodeValues != null) {
				for (JsonNode value : nodeValues) {
					System.out.println(value.asText());
					valueList.add(value.asText());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void listからjsonNodeに変換() throws AssetModelsException {
		List<String> valueList = new ArrayList<>();
		valueList.add("001 TEST_001");
		valueList.add("001 TEST_002");
		valueList.add("001 TEST_003");
		valueList.add("001 TEST_004");
		valueList.add("001 TEST_005");
		
		assetInfo.setOdsBarcodeIds(valueList);
		try {
			String assetinfo = mapper.writeValueAsString(assetInfo);
			System.out.println(assetinfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
