package com.sony.pro.mwa.util.uri;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.drools.core.command.assertion.AssertEquals;
import org.junit.Test;

import com.sony.pro.mwa.util.exceptions.MwaUriException;

import junit.framework.Assert;

/**
 * MwaUriのテスト
 */
public class MwaUriTest {

	@Test
	public void URI構文が不正な文字列を指定した場合は例外となる() throws Exception {

		try {
			//## Arrange
			String str = "mwa://Assets/AAA/NFL_20160207_SUPER050_CBS_19h28m15s02_CAM 26 Near Low Left EZ_CAR28 RUN.mxf";
			//## Action
			MwaUri uri = new MwaUri(str);

		} catch (MwaUriException me) {
			me.printStackTrace();
		}
		try {
			//## Arrange
			String str = "mwa://Assets/AAA/NFL.mxf";
			//## Action
			MwaUri uri = new MwaUri(str);

		} catch (MwaUriException me) {
			me.printStackTrace();
		}
		try {
			//## Arrange
			String str = "mwa//Assets/AAA/NFL.mxf";
			//## Action
			MwaUri uri = new MwaUri(str);

		} catch (MwaUriException me) {
			me.printStackTrace();
		}
	}

	@Test
	public void AssetImporter経由でアップロードされたアセットのIDを処理するケース_001() throws Exception {

		//## Arrange
		String str = "mwa://AUTHORITY/image.jpg?cartridgeId=01?111111EEEEDDDD";
		//## Action
		MwaUri uri = new MwaUri(str);

		Assert.assertEquals(uri.getLocation(), "AUTHORITY");
		Assert.assertEquals(uri.getAssetId(), null);
		Assert.assertEquals(uri.getResourceName(), "image.jpg");
	}


	@Test
	public void AssetImporter経由でアップロードされたアセットのIDを処理するケース_002() throws Exception {

		//## Arrange
		String str = "mwa://AUTHORITY/a/b/c/image.jpg?cartridgeId=01_111111EEEEDDDD";
		//## Action
		MwaUri uri = new MwaUri(str);

		Assert.assertEquals(uri.getLocation(), "AUTHORITY");
		Assert.assertEquals(uri.getEssenceLocationPath(), "/b/c");
		Assert.assertEquals(uri.getAssetId(), "a");
		Assert.assertEquals(uri.getResourceName(), "image.jpg");
	}

	@Test
	public void EssenceIdなしのURI文字列からMwaUriを生成する() throws Exception {
		//## Arrange
		String str = "mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4/image.jpg";
		//## Action
		MwaUri uri = new MwaUri(str);
		//## Assert
		Assert.assertEquals(str, uri.get());
	}

	@Test
	public void 個々の値を指定してMwaUriを生成する_1() throws Exception {
		//## Arrange/Action
		MwaUri uri = new MwaUri("DEFAULT", "3bca8f99-0554-4c1a-8e70-2ade827bf2ab", "image.jpg");

		//## Assert
		Assert.assertEquals("mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/image.jpg", uri.get());
	}

	@Test
	public void 個々の値を指定してMwaUriを生成する_2() throws Exception {
		//## Arrange/Action
		MwaUri uri = new MwaUri("DEFAULT", "3bca8f99-0554-4c1a-8e70-2ade827bf2ab", "55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4", "image.jpg");

		//## Assert
		Assert.assertEquals("mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4/image.jpg", uri.get());
	}

	@Test
	public void 個々の値を指定してMwaUriを生成する_3() throws Exception {
		//## Arrange/Action
		MwaUri uri = new MwaUri("DEFAULT", "image.jpg");

		//## Assert
		Assert.assertEquals("mwa://DEFAULT/image.jpg", uri.get());
	}

	@Test
	public void 個々の値を指定してMwaUriを生成する_4() throws Exception {
		//## Arrange/Action
		Map<String, String> query = new HashMap<String, String>();
		query.put("cartridgeId", "001%2051AA1016PE10072");
		MwaUri uri = new MwaUri("DEFAULT", "3bca8f99-0554-4c1a-8e70-2ade827bf2ab", "55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4", "image.jpg", query);

		//## Assert
		Assert.assertEquals("mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4/image.jpg?cartridgeId=001%2051AA1016PE10072", uri.get());
	}

	@Test
	public void 個々の値を指定してMwaUriを生成する_5() throws Exception {
		//## Arrange/Action
		Map<String, String> query = new HashMap<String, String>();
		query.put("drive", "C");
		query.put("cartridgeId", "001%2051AA1016PE10072");
		MwaUri uri = new MwaUri("DEFAULT", "3bca8f99-0554-4c1a-8e70-2ade827bf2ab", "55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4", "image.jpg", query);

		//## Assert
		Assert.assertEquals("mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4/image.jpg?drive=C&cartridgeId=001%2051AA1016PE10072", uri.get());
	}

	@Test
	public void URIを生成後に各項目を正しく取得できる_1() throws Exception {
		//## Arrange
		String str = "mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4/image.jpg";
		//## Action
		MwaUri uri = new MwaUri(str);
		//## Assert
		Assert.assertEquals("DEFAULT", uri.getLocation());
		Assert.assertEquals("3bca8f99-0554-4c1a-8e70-2ade827bf2ab", uri.getAssetId());
		Assert.assertEquals("/55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4", uri.getEssenceLocationPath());
		Assert.assertEquals("image.jpg", uri.getResourceName());
		Assert.assertTrue(uri.hasEssenceId());
		Assert.assertTrue(uri.hasResource());
	}

	@Test
	public void URIを生成後に各項目を正しく取得できる_2() throws Exception {
		//## Arrange
		String str = "mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/image.jpg";
		//## Action
		MwaUri uri = new MwaUri(str);
		//## Assert
		Assert.assertEquals("DEFAULT", uri.getLocation());
		Assert.assertEquals("3bca8f99-0554-4c1a-8e70-2ade827bf2ab", uri.getAssetId());
		Assert.assertEquals("image.jpg", uri.getResourceName());
		Assert.assertFalse(uri.hasEssenceId());
		Assert.assertTrue(uri.hasResource());
	}

	@Test
	public void URIを生成後に各項目を正しく取得できる_3() throws Exception {
		//## Arrange
		String str = "mwa://DEFAULT/image.jpg";
		//## Action
		MwaUri uri = new MwaUri(str);
		//## Assert
		Assert.assertEquals("DEFAULT", uri.getLocation());
		Assert.assertEquals("image.jpg", uri.getResourceName());
		Assert.assertTrue(uri.hasResource());
	}

	@Test
	public void URIを生成後に各項目を正しく取得できる_4() throws Exception {
		//## Arrange
		String str = "mwa://ODA/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/image.jpg?cartridgeId=001%2051AA1016PE10072";
		//## Action
		MwaUri uri = new MwaUri(str);
		//## Assert
		Assert.assertEquals("ODA", uri.getLocation());
		Assert.assertEquals("3bca8f99-0554-4c1a-8e70-2ade827bf2ab", uri.getAssetId());
		Assert.assertEquals("image.jpg", uri.getResourceName());
		Assert.assertEquals("001%2051AA1016PE10072", uri.getValue("cartridgeId"));
		Assert.assertTrue(uri.hasResource());
	}

	@Test
	public void URIを生成後に各項目を正しく取得できる_5() throws Exception {
		//## Arrange
		String str = "mwa://ODA/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/image.jpg?cartridgeId=001%2051AA1016PE10072&drive=G";
		//## Action
		MwaUri uri = new MwaUri(str);
		//## Assert
		Assert.assertEquals("ODA", uri.getLocation());
		Assert.assertEquals("3bca8f99-0554-4c1a-8e70-2ade827bf2ab", uri.getAssetId());
		Assert.assertEquals("image.jpg", uri.getResourceName());
		Assert.assertEquals("001%2051AA1016PE10072", uri.getValue("cartridgeId"));
		Assert.assertEquals("G", uri.getValue("drive"));
		Assert.assertTrue(uri.hasResource());
	}

	@Test
	public void EssenceId付でURIを生成したあとEssenceIdを除去できる() throws Exception {
		//## Arrange
		String str = "mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4/image.jpg";
		MwaUri uri = new MwaUri(str);
		//## Action
		String removed = uri.removeEssenceId();

		//## Assert
		Assert.assertEquals("mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/image.jpg", removed);
		Assert.assertFalse(uri.hasEssenceId());
	}

	@Test
	public void クエリ付でURIを生成したあとクエリを変更できる() throws Exception {

		//## Arrange
		String str = "mwa://ODA/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/image.jpg?cartridgeId=001%2051AA1016PE10072";
		MwaUri uri = new MwaUri(str);
		//## Action
		String changed = uri.setQuery("cartridgeId", "001%2051AA1016PE10099");

		//## Assert
		Assert.assertEquals("mwa://ODA/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/image.jpg?cartridgeId=001%2051AA1016PE10099", changed);
		Assert.assertTrue(uri.hasQuery());
	}

	@Test (expected = MwaUriException.class)
	public void 指定されたURI文字列が不正な場合はMwaUriExceptionがスローされる_1() throws Exception {
		//## Arrange / Action
		MwaUri error = new MwaUri("ssssssssss");
	}

	@Test
	public void スキーマがMWAであれば例外はスローされない() throws Exception {
		//## Arrange / Action
		MwaUri error = new MwaUri("mwa://1/2/3/4/5/5");
	}

	@Test (expected = MwaUriException.class)
	public void コンストラクタ引数nullに対しては例外を投げる() throws Exception {
		//## Arrange / Action
		MwaUri error = new MwaUri(null);
	}

	@Test
	public void 指定されたURI文字列のがUUIDを含んでいなくても正しく処理される() throws Exception {
		//## Arrange / Action
		MwaUri error = new MwaUri("mwa://DEFAULT/AAA/BBB/resource.jpg");
	}

	@Test
	public void UUIDに似た文字列を含む場合もmwa形式パスの書式が正しければ許容する_1() throws Exception {
		//## Arrange / Action
		// AssetIdの2項目が3桁
		MwaUri error = new MwaUri("mwa://DEFAULT/3bca8f99-054-4c1a-8e70-2ade827bf2ab/55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4/image.jpg");
	}

	@Test
	public void UUIDに似た文字列を含む場合もmwa形式パスの書式が正しければ許容する_2() throws Exception {
		//## Arrange / Action
		// AssetIdの2項目に16進数以外のアルファベットが混入
		MwaUri error = new MwaUri("mwa://DEFAULT/3bca8f99-054G-4c1a-8e70-2ade827bf2ab/55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4/image.jpg");
	}

	@Test (expected = MwaUriException.class)
	public void 指定されたURI文字列のクエリが不正な場合はMwaUriExceptionがスローされる_1() throws Exception {
		//## Arrange / Action
		MwaUri error = new MwaUri("mwa://ODA/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/image.jpg?cartridgeId=");
	}

	@Test (expected = MwaUriException.class)
	public void 指定されたURI文字列のクエリが不正な場合はMwaUriExceptionがスローされる_2() throws Exception {
		//## Arrange / Action
		MwaUri error = new MwaUri("mwa://ODA/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/image.jpg?cartridgeId");
	}

	@Test (expected = MwaUriException.class)
	public void 指定されたURI文字列のクエリが不正な場合はMwaUriExceptionがスローされる_3() throws Exception {
		//## Arrange / Action
		MwaUri error = new MwaUri("mwa://ODA/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/image.jpg?");
	}

	@Test
	public void UUIDは大文字でも小文字でも許容する() throws Exception {
		//## Arrange
		String upper = "mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4/image.jpg";
		String lower = "mwa://DEFAULT/3BCA8F99-0554-4C1A-8E70-2ADE827BF2AB/55EF50C9-30F5-4BBE-9C9F-CD4085F0D1A4/IMAGE.JPG";
		//## Action　/ Assert
		MwaUri u = new MwaUri(upper);
		MwaUri l = new MwaUri(lower);
	}

	@Test
	public void 検証関数のテスト() throws Exception {
		// Arrange
		String upper = "mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4/image.jpg";

		MwaUri result = MwaUri.getInstance(upper);

		Assert.assertNotNull(result);
	}

	@Test
	public void URIのロケーション名がODAであることを判定する() throws Exception {
		// Arrange
		MwaUri uri = new MwaUri("mwa://ODA/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/image.jpg");
		Assert.assertTrue(uri.isLocation("ODA"));
	}

	@Test
	public void URIのロケーション名がODAではないことを判定する() throws Exception {
		// Arrange
		MwaUri uri = new MwaUri("mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/image.jpg");
		Assert.assertFalse(uri.isLocation("ODA"));
	}

	@Test
	public void URIのロケーション名がODALibraryであることを判定する() throws Exception {
		// Arrange
		MwaUri uri = new MwaUri("mwa://ODA_LIBRARY/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/image.jpg");
		Assert.assertTrue(uri.isLocation("ODA_LIBRARY"));
	}

	@Test
	public void URIのロケーション名がODALibraryではないことを判定する() throws Exception {
		// Arrange
		MwaUri uri = new MwaUri("mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/image.jpg");
		Assert.assertFalse(uri.isLocation("ODA_LIBRARY"));
	}
	@Test
	public void 各パートが正しく取得できる() throws Exception {
		//## Arrange / Action
		MwaUri uri = new MwaUri("mwa://AssetCopy/1/2/3/4/5/aaa.mxf?cartridge=test");

		Assert.assertTrue("AssetCopy".equals(uri.getLocation()));
		Assert.assertTrue("1".equals(uri.getAssetId()));
		Assert.assertTrue("/2/3/4/5".equals(uri.getEssenceLocationPath()));

		List<String> essense = uri.getEssenceId();

		Assert.assertTrue("2".equals(essense.get(0)));
		Assert.assertTrue("3".equals(essense.get(1)));
		Assert.assertTrue("4".equals(essense.get(2)));
		Assert.assertTrue("5".equals(essense.get(3)));

		Assert.assertTrue("aaa.mxf".equals(uri.getResourceName()));
		Assert.assertTrue(".mxf".equals(uri.getResourceExtension()));
		Assert.assertTrue("test".equals(uri.getQueryMap().get("cartridge")));

	}
	@Test
	public void EssenseIdを正しく置き換えられること() throws Exception {
		// Arrange
		String upper = "mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/55ef50c9-30f5-4bbe-9c9f-cd4085f0d1a4/image.jpg";
		MwaUri result = MwaUri.getInstance(upper);

		result.setEssenceId("A");

		Assert.assertEquals("mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/A/image.jpg", result.get());
	}

	@Test
	public void EssenseIdを正しく置き換えられること_2() throws Exception {
		// Arrange
		String upper = "mwa://DEFAULT/1/2/3/4/image.jpg";
		MwaUri result = MwaUri.getInstance(upper);

		result.setEssenceId("A");

		Assert.assertEquals("mwa://DEFAULT/1/A/image.jpg", result.get());
	}
	@Test
	public void EssenseIdを持たないURIを正しく判定できていること() throws Exception {
		// Arrange
		String upper = "mwa://DEFAULT/3bca8f99-0554-4c1a-8e70-2ade827bf2ab/image.jpg";
		MwaUri result = MwaUri.getInstance(upper);

		Assert.assertFalse(result.hasEssenceId());
		Assert.assertEquals(result.getEssenceId().size(), 0);
		Assert.assertEquals(result.getEssenceLocationPath(), null);
	}

}
