package com.sony.pro.mwa.util.text;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class BpmnReaderTest {
    @Test
    public void findBpmnByActivityName() throws Exception {
        String targetString = "";
        File directory = new File("C:\\ProgramData\\Sony\\Media Backbone\\NavigatorX\\services\\mwa\\bpmns");
        File[] files = directory.listFiles();

        for (File file: files) {
            StringBuffer buffer = new StringBuffer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsoluteFile())));
            String line;
            int count = 0;
            while ((line = reader.readLine()) != null) {
                if (line.indexOf(targetString) != -1) {
                    count++;
                }
                buffer.append(line);
            }
            if (count != 0) {
                System.out.println(file.getName() + "(" + count + ")");
            }
        }
    }
}
