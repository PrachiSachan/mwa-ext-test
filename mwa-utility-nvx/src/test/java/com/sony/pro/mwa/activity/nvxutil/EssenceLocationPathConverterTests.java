package com.sony.pro.mwa.activity.nvxutil;

import com.sony.pro.mwa.activity.nvxutil.essence.converter.ODAEssenceLocationPathConverter;
import com.sony.pro.mwa.activity.nvxutil.essence.converter.ODALibraryEssenceLocationPathConverter;
import org.junit.Test;
import org.testng.Assert;

public class EssenceLocationPathConverterTests {

	/**
	 * MwaPathのロケーション名が指定のロケーション名に変換される
	 */
	@Test
	public void test_convertToODA_000() {
		ODAEssenceLocationPathConverter converter = new ODAEssenceLocationPathConverter();
		String mwaPath = "mwa://DEFAULT/dc78b40f-faa8-4d3c-a86e-d0809cfaa47f/test.mxf";
		String actualLocationName = converter.convert(mwaPath);
		String expectedLocationName = "mwa://ODA/dc78b40f-faa8-4d3c-a86e-d0809cfaa47f/test.mxf";
		Assert.assertEquals(actualLocationName, expectedLocationName);
	}

	/**
	 * MwaPathのロケーション名が指定のロケーション名に変換される
	 */
	@Test
	public void test_convertToODALibrary_000() {
		ODALibraryEssenceLocationPathConverter converter = new ODALibraryEssenceLocationPathConverter();
		String mwaPath = "mwa://DEFAULT/dc78b40f-faa8-4d3c-a86e-d0809cfaa47f/test.mxf";
		String actualLocationName = converter.convert(mwaPath);
		String expectedLocationName = "mwa://ODA_LIBRARY/dc78b40f-faa8-4d3c-a86e-d0809cfaa47f/test.mxf";
		Assert.assertEquals(actualLocationName, expectedLocationName);
	}
}
