package com.sony.pro.mwa.activity.nvxutil;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.testng.Assert;

import io.vertx.core.json.JsonObject;

public class AssetUtilsTests {

	/**
	 * mwa パスにロケーションがあるケース
	 * 期待値は mwa パスにロケーション名が取得できること
	 */
	@Test
	public void test_getOnlineLocationPaths_000() {
		String assetModelStr = "{\"hidden\":false,\"update\":\"2018-02-22T09:36:51.794084Z\",\"type\":\"GENERIC\",\"operator\":\"mwa\",\"clusterName\":\"Library\",\"revisionNote\":\"\",\"create\":\"2018-02-22T09:34:00.225929Z\",\"id\":\"4d3a6643-94f6-4117-b910-22221d1802c1\",\"collectionId\":\"\",\"assetInfo\":{\"odsBarcodeIds\":[],\"note\":null,\"rating\":null,\"length\":2269,\"title\":\"test_03\",\"type\":\"AUDIO_VIDEO\",\"numerator\":30000,\"denominator\":1001,\"duration\":68,\"archived\":false,\"frameRate\":29.97,\"startTimecode\":\"66105301\",\"customMetadata\":{},\"copyOdsBarcodeIds\":[],\"startTimecode_msec\":6790867,\"odsCartridgeIds\":[],\"copyOdsCartridgeIds\":[],\"dropFrame\":true,\"status\":\"Online\"},\"events\":[{\"duration\":1,\"customMetadata\":{},\"id\":\"ac764aba-2254-4650-b0e8-94bf16cc05d4\",\"type\":\"KEY_FRAME\",\"title\":\"Autogeneratethumbnail\",\"frame\":0,\"imageLocation\":\"mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/ac764aba-2254-4650-b0e8-94bf16cc05d4/image.jpg\"}],\"owner\":\"mwa\",\"essences\":[{\"extension\":\"MXF\",\"videoInfo\":{\"frameRate\":29.97,\"codec\":\"MPEG-2V\",\"codecProfile\":\"4:2:2@High\",\"bitRate\":50000000,\"bitDepth\":8,\"width\":1920,\"scanType\":\"Interlaced\",\"dropFrame\":true,\"numerator\":30000,\"denominator\":1001,\"height\":1080},\"format\":\"MXF\",\"type\":\"AUDIO_VIDEO\",\"proxy\":false,\"frameCount\":90,\"size\":22689340,\"audioInfo\":{\"channelCount\":8,\"channelInfos\":[{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":1,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":2,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":3,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":4,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":5,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":6,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":7,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":8,\"bitDepth\":24,\"samplingRate\":48000}]},\"name\":\"test_03\",\"location\":\"mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/test_03.MXF\",\"id\":\"a0f86ea2-9019-4033-8ae4-c1308b62021c\",\"formatCommercial\":\"SonyXDCAMHD422\",\"primary\":true,\"status\":\"Online\"},{\"proxy\":true,\"extension\":\"vtt\",\"format\":\"WebVTT\",\"name\":\"timecode\",\"location\":\"mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/70d388f0-cf9b-4d2d-aed4-39ef5c30acff/timecode.vtt\",\"id\":\"70d388f0-cf9b-4d2d-aed4-39ef5c30acff\",\"formatCommercial\":\"WebVTT\",\"type\":\"TIMECODE\",\"primary\":false,\"status\":\"Online\"},{\"proxy\":true,\"size\":666657,\"videoInfo\":{\"frameRate\":29.97,\"dropFrame\":true,\"numerator\":30000,\"denominator\":1001},\"name\":\"test_03_proxy\",\"location\":\"mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/test_03_proxy.mp4\",\"id\":\"1dea52b1-add0-4949-b3e9-aa605ae96d27\",\"type\":\"AUDIO_VIDEO\",\"primary\":false,\"status\":\"Online\"}],\"shortcutParentIds\":[],\"updateTime\":1519292211793,\"tags\":[],\"revisionId\":1,\"thumbnailEventId\":\"ac764aba-2254-4650-b0e8-94bf16cc05d4\",\"activities\":[{\"customMetadata\":{},\"id\":\"82bc693f-41e7-4ac4-8504-0ffc5e0e1a5a\"}],\"location\":{},\"metadataSchemaId\":\"generic\",\"importTime\":1519292040209,\"parents\":[\"fd0dd80b-6968-47b9-8898-7b9e8c7abec7\"]}";
		JsonObject assetModel = new JsonObject(assetModelStr);
		List<String> locationPaths = AssetUtils.getOnlineLocationPaths(assetModel);
		List<String> expectedLocationPaths = new ArrayList<>();
		expectedLocationPaths.add("mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/test_03.MXF");
		expectedLocationPaths.add("mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/70d388f0-cf9b-4d2d-aed4-39ef5c30acff/timecode.vtt");
		expectedLocationPaths.add("mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/test_03_proxy.mp4");
		expectedLocationPaths.add("mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/ac764aba-2254-4650-b0e8-94bf16cc05d4/image.jpg");

		Assert.assertEquals(locationPaths, expectedLocationPaths);
	}

	/**
	 * mwa パスにロケーションがあるケース
	 * 期待値は mwa パスにロケーション名が取得できること
	 */
	@Test
	public void test_getOnlineLocationPaths_001() {
		String assetModelStr = "{\"hidden\":false,\"update\":\"2018-02-22T09:36:51.794084Z\",\"type\":\"GENERIC\",\"operator\":\"mwa\",\"clusterName\":\"Library\",\"revisionNote\":\"\",\"create\":\"2018-02-22T09:34:00.225929Z\",\"id\":\"4d3a6643-94f6-4117-b910-22221d1802c1\",\"collectionId\":\"\",\"assetInfo\":{\"odsBarcodeIds\":[],\"note\":null,\"rating\":null,\"length\":2269,\"title\":\"test_03\",\"type\":\"AUDIO_VIDEO\",\"numerator\":30000,\"denominator\":1001,\"duration\":68,\"archived\":false,\"frameRate\":29.97,\"startTimecode\":\"66105301\",\"customMetadata\":{},\"copyOdsBarcodeIds\":[],\"startTimecode_msec\":6790867,\"odsCartridgeIds\":[],\"copyOdsCartridgeIds\":[],\"dropFrame\":true,\"status\":\"Online\"},\"events\":[{\"duration\":1,\"customMetadata\":{},\"id\":\"ac764aba-2254-4650-b0e8-94bf16cc05d4\",\"type\":\"KEY_FRAME\",\"title\":\"Autogeneratethumbnail\",\"frame\":0,\"imageLocation\":\"mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/ac764aba-2254-4650-b0e8-94bf16cc05d4/image.jpg\"}],\"owner\":\"mwa\",\"essences\":[{\"extension\":\"MXF\",\"videoInfo\":{\"frameRate\":29.97,\"codec\":\"MPEG-2V\",\"codecProfile\":\"4:2:2@High\",\"bitRate\":50000000,\"bitDepth\":8,\"width\":1920,\"scanType\":\"Interlaced\",\"dropFrame\":true,\"numerator\":30000,\"denominator\":1001,\"height\":1080},\"format\":\"MXF\",\"type\":\"AUDIO_VIDEO\",\"proxy\":false,\"frameCount\":90,\"size\":22689340,\"audioInfo\":{\"channelCount\":8,\"channelInfos\":[{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":1,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":2,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":3,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":4,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":5,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":6,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":7,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":8,\"bitDepth\":24,\"samplingRate\":48000}]},\"name\":\"test_03\",\"location\":\"mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/test_03.MXF\",\"id\":\"a0f86ea2-9019-4033-8ae4-c1308b62021c\",\"formatCommercial\":\"SonyXDCAMHD422\",\"primary\":true,\"status\":\"Offline\"},{\"proxy\":true,\"extension\":\"vtt\",\"format\":\"WebVTT\",\"name\":\"timecode\",\"location\":\"mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/70d388f0-cf9b-4d2d-aed4-39ef5c30acff/timecode.vtt\",\"id\":\"70d388f0-cf9b-4d2d-aed4-39ef5c30acff\",\"formatCommercial\":\"WebVTT\",\"type\":\"TIMECODE\",\"primary\":false,\"status\":\"Online\"},{\"proxy\":true,\"size\":666657,\"videoInfo\":{\"frameRate\":29.97,\"dropFrame\":true,\"numerator\":30000,\"denominator\":1001},\"name\":\"test_03_proxy\",\"location\":\"mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/test_03_proxy.mp4\",\"id\":\"1dea52b1-add0-4949-b3e9-aa605ae96d27\",\"type\":\"AUDIO_VIDEO\",\"primary\":false,\"status\":\"Online\"}],\"shortcutParentIds\":[],\"updateTime\":1519292211793,\"tags\":[],\"revisionId\":1,\"thumbnailEventId\":\"ac764aba-2254-4650-b0e8-94bf16cc05d4\",\"activities\":[{\"customMetadata\":{},\"id\":\"82bc693f-41e7-4ac4-8504-0ffc5e0e1a5a\"}],\"location\":{},\"metadataSchemaId\":\"generic\",\"importTime\":1519292040209,\"parents\":[\"fd0dd80b-6968-47b9-8898-7b9e8c7abec7\"]}";
		JsonObject assetModel = new JsonObject(assetModelStr);
		List<String> locationPaths = AssetUtils.getOnlineLocationPaths(assetModel);
		List<String> expectedLocationPaths = new ArrayList<>();
		expectedLocationPaths.add("mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/70d388f0-cf9b-4d2d-aed4-39ef5c30acff/timecode.vtt");
		expectedLocationPaths.add("mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/test_03_proxy.mp4");
		expectedLocationPaths.add("mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/ac764aba-2254-4650-b0e8-94bf16cc05d4/image.jpg");

		Assert.assertEquals(locationPaths, expectedLocationPaths);
	}

	/**
	 * mwa パスにロケーションがあるケース
	 * 期待値は mwa パスにロケーション名が取得できること
	 */
	@Test
	public void test_getOnlineLocationPaths_002() {
		JsonObject assetModel = new JsonObject("{\"hidden\":false,\"update\":\"2018-02-22T09:36:51.794084Z\",\"type\":\"GENERIC\",\"operator\":\"mwa\",\"clusterName\":\"Library\",\"revisionNote\":\"\",\"create\":\"2018-02-22T09:34:00.225929Z\",\"id\":\"4d3a6643-94f6-4117-b910-22221d1802c1\",\"collectionId\":\"\",\"assetInfo\":{\"odsBarcodeIds\":[],\"note\":null,\"rating\":null,\"length\":2269,\"title\":\"test_03\",\"type\":\"AUDIO_VIDEO\",\"numerator\":30000,\"denominator\":1001,\"duration\":68,\"archived\":false,\"frameRate\":29.97,\"startTimecode\":\"66105301\",\"customMetadata\":{},\"copyOdsBarcodeIds\":[],\"startTimecode_msec\":6790867,\"odsCartridgeIds\":[],\"copyOdsCartridgeIds\":[],\"dropFrame\":true,\"status\":\"Online\"},\"events\":[{\"duration\":1,\"customMetadata\":{},\"id\":\"ac764aba-2254-4650-b0e8-94bf16cc05d4\",\"type\":\"KEY_FRAME\",\"title\":\"Autogeneratethumbnail\",\"frame\":0,\"imageLocation\":\"mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/ac764aba-2254-4650-b0e8-94bf16cc05d4/image.jpg\"}],\"owner\":\"mwa\",\"essences\":[{\"primary\":true,\"proxy\":false,\"location\":\"mwa://DEFAULT/2ebd8b99-f723-4174-8e01-0d1d4341ec1f/test_01.MXF\",\"id\":\"f00970bf-22d2-4d45-a277-7f04e44f8ba7\",\"type\":\"AUDIO_VIDEO\",\"format\":\"MXF\",\"formatCommercial\":\"SonyXDCAMHD422\",\"name\":\"test_01\",\"extension\":\"MXF\",\"size\":22689340,\"videoInfo\":{\"frameRate\":29.97,\"numerator\":30000,\"denominator\":1001,\"dropFrame\":true,\"codec\":\"MPEG-2V\",\"codecProfile\":\"4:2:2@High\",\"bitRate\":50000000,\"bitDepth\":8,\"width\":1920,\"height\":1080,\"scanType\":\"Interlaced\"},\"audioInfo\":{\"channelCount\":8,\"channelInfos\":[{\"channelNumber\":1,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000},{\"channelNumber\":2,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000},{\"channelNumber\":3,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000},{\"channelNumber\":4,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000},{\"channelNumber\":5,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000},{\"channelNumber\":6,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000},{\"channelNumber\":7,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000},{\"channelNumber\":8,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000}]},\"frameCount\":90,\"status\":\"Online\"},{\"id\":\"2f79dd6b-c36b-4db3-ba6a-2ddbf4232162\",\"type\":\"TIMECODE\",\"format\":\"WebVTT\",\"formatCommercial\":\"WebVTT\",\"primary\":false,\"proxy\":true,\"name\":\"timecode\",\"extension\":\"vtt\",\"location\":\"mwa://DEFAULT/2ebd8b99-f723-4174-8e01-0d1d4341ec1f/2f79dd6b-c36b-4db3-ba6a-2ddbf4232162/timecode.vtt\",\"status\":\"Online\"},{\"id\":\"6a33fe2f-cbfc-4d3d-87f0-cabea1b0b272\",\"type\":\"AUDIO_VIDEO\",\"primary\":false,\"proxy\":true,\"status\":\"Online\",\"name\":\"test_01_proxy\",\"location\":\"mwa://DEFAULT/2ebd8b99-f723-4174-8e01-0d1d4341ec1f/test_01_proxy.mp4\",\"size\":666657,\"videoInfo\":{\"frameRate\":29.97,\"numerator\":30000,\"denominator\":1001,\"dropFrame\":true}},{\"proxy\":true,\"extension\":\"vtt\",\"size\":1316,\"videoInfo\":{},\"format\":\"WebVTT\",\"name\":\"vsshort-de\",\"location\":\"mwa://DEFAULT/2ebd8b99-f723-4174-8e01-0d1d4341ec1f/c377c89e-b82e-4285-aa74-27a13db207d4/vsshort-de_convert.vtt\",\"id\":\"c377c89e-b82e-4285-aa74-27a13db207d4\",\"formatCommercial\":\"WebVTT\",\"type\":\"SUBTITLE\",\"primary\":false,\"status\":\"Online\",\"sourceEssenceId\":\"d46330d3-3d45-4ff7-b1f5-847686ad9f81\",\"attachment\":\"Wogehstdusofr�hhin?SpeicherkeineNachrichten\"}],\"location\":{},\"metadataSchemaId\":\"generic\",\"importTime\":1519292040209,\"parents\":[\"fd0dd80b-6968-47b9-8898-7b9e8c7abec7\"]}");
		List<String> locationPaths = AssetUtils.getOnlineLocationPaths(assetModel);
		List<String> expectedLocationPaths = new ArrayList<>();
		expectedLocationPaths.add("mwa://DEFAULT/2ebd8b99-f723-4174-8e01-0d1d4341ec1f/test_01.MXF");
		expectedLocationPaths.add("mwa://DEFAULT/2ebd8b99-f723-4174-8e01-0d1d4341ec1f/2f79dd6b-c36b-4db3-ba6a-2ddbf4232162/timecode.vtt");
		expectedLocationPaths.add("mwa://DEFAULT/2ebd8b99-f723-4174-8e01-0d1d4341ec1f/test_01_proxy.mp4");
		expectedLocationPaths.add("mwa://DEFAULT/2ebd8b99-f723-4174-8e01-0d1d4341ec1f/c377c89e-b82e-4285-aa74-27a13db207d4/vsshort-de_convert.vtt");
		expectedLocationPaths.add("mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/ac764aba-2254-4650-b0e8-94bf16cc05d4/image.jpg");

		Assert.assertEquals(locationPaths, expectedLocationPaths);
	}

	/**
	 * mwa パスにロケーションがあるケース
	 * 期待値は mwa パスにロケーション名が取得できること
	 */
	@Test
	public void test_getOnlineLocationPathsWithoutSubtitle_001() {
		JsonObject assetModel = new JsonObject("{\"hidden\":false,\"update\":\"2018-02-22T09:36:51.794084Z\",\"type\":\"GENERIC\",\"operator\":\"mwa\",\"clusterName\":\"Library\",\"revisionNote\":\"\",\"create\":\"2018-02-22T09:34:00.225929Z\",\"id\":\"4d3a6643-94f6-4117-b910-22221d1802c1\",\"collectionId\":\"\",\"assetInfo\":{\"odsBarcodeIds\":[],\"note\":null,\"rating\":null,\"length\":2269,\"title\":\"test_03\",\"type\":\"AUDIO_VIDEO\",\"numerator\":30000,\"denominator\":1001,\"duration\":68,\"archived\":false,\"frameRate\":29.97,\"startTimecode\":\"66105301\",\"customMetadata\":{},\"copyOdsBarcodeIds\":[],\"startTimecode_msec\":6790867,\"odsCartridgeIds\":[],\"copyOdsCartridgeIds\":[],\"dropFrame\":true,\"status\":\"Online\"},\"events\":[{\"duration\":1,\"customMetadata\":{},\"id\":\"ac764aba-2254-4650-b0e8-94bf16cc05d4\",\"type\":\"KEY_FRAME\",\"title\":\"Autogeneratethumbnail\",\"frame\":0,\"imageLocation\":\"mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/ac764aba-2254-4650-b0e8-94bf16cc05d4/image.jpg\"}],\"owner\":\"mwa\",\"essences\":[{\"primary\":true,\"proxy\":false,\"location\":\"mwa://DEFAULT/2ebd8b99-f723-4174-8e01-0d1d4341ec1f/test_01.MXF\",\"id\":\"f00970bf-22d2-4d45-a277-7f04e44f8ba7\",\"type\":\"AUDIO_VIDEO\",\"format\":\"MXF\",\"formatCommercial\":\"SonyXDCAMHD422\",\"name\":\"test_01\",\"extension\":\"MXF\",\"size\":22689340,\"videoInfo\":{\"frameRate\":29.97,\"numerator\":30000,\"denominator\":1001,\"dropFrame\":true,\"codec\":\"MPEG-2V\",\"codecProfile\":\"4:2:2@High\",\"bitRate\":50000000,\"bitDepth\":8,\"width\":1920,\"height\":1080,\"scanType\":\"Interlaced\"},\"audioInfo\":{\"channelCount\":8,\"channelInfos\":[{\"channelNumber\":1,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000},{\"channelNumber\":2,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000},{\"channelNumber\":3,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000},{\"channelNumber\":4,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000},{\"channelNumber\":5,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000},{\"channelNumber\":6,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000},{\"channelNumber\":7,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000},{\"channelNumber\":8,\"codec\":\"PCM\",\"samplingRate\":48000,\"bitDepth\":24,\"bitRate\":1152000}]},\"frameCount\":90,\"status\":\"Online\"},{\"id\":\"2f79dd6b-c36b-4db3-ba6a-2ddbf4232162\",\"type\":\"TIMECODE\",\"format\":\"WebVTT\",\"formatCommercial\":\"WebVTT\",\"primary\":false,\"proxy\":true,\"name\":\"timecode\",\"extension\":\"vtt\",\"location\":\"mwa://DEFAULT/2ebd8b99-f723-4174-8e01-0d1d4341ec1f/2f79dd6b-c36b-4db3-ba6a-2ddbf4232162/timecode.vtt\",\"status\":\"Online\"},{\"id\":\"6a33fe2f-cbfc-4d3d-87f0-cabea1b0b272\",\"type\":\"AUDIO_VIDEO\",\"primary\":false,\"proxy\":true,\"status\":\"Online\",\"name\":\"test_01_proxy\",\"location\":\"mwa://DEFAULT/2ebd8b99-f723-4174-8e01-0d1d4341ec1f/test_01_proxy.mp4\",\"size\":666657,\"videoInfo\":{\"frameRate\":29.97,\"numerator\":30000,\"denominator\":1001,\"dropFrame\":true}},{\"proxy\":true,\"extension\":\"vtt\",\"size\":1316,\"videoInfo\":{},\"format\":\"WebVTT\",\"name\":\"vsshort-de\",\"location\":\"mwa://DEFAULT/2ebd8b99-f723-4174-8e01-0d1d4341ec1f/c377c89e-b82e-4285-aa74-27a13db207d4/vsshort-de_convert.vtt\",\"id\":\"c377c89e-b82e-4285-aa74-27a13db207d4\",\"formatCommercial\":\"WebVTT\",\"type\":\"SUBTITLE\",\"primary\":false,\"status\":\"Online\",\"sourceEssenceId\":\"d46330d3-3d45-4ff7-b1f5-847686ad9f81\",\"attachment\":\"Wogehstdusofr�hhin?SpeicherkeineNachrichten\"}],\"location\":{},\"metadataSchemaId\":\"generic\",\"importTime\":1519292040209,\"parents\":[\"fd0dd80b-6968-47b9-8898-7b9e8c7abec7\"]}");
		List<String> locationPaths = AssetUtils.getOnlineLocationPathsWithoutSubtitle(assetModel);
		List<String> expectedLocationPaths = new ArrayList<>();
		expectedLocationPaths.add("mwa://DEFAULT/2ebd8b99-f723-4174-8e01-0d1d4341ec1f/test_01.MXF");
		expectedLocationPaths.add("mwa://DEFAULT/2ebd8b99-f723-4174-8e01-0d1d4341ec1f/2f79dd6b-c36b-4db3-ba6a-2ddbf4232162/timecode.vtt");
		expectedLocationPaths.add("mwa://DEFAULT/2ebd8b99-f723-4174-8e01-0d1d4341ec1f/test_01_proxy.mp4");
		expectedLocationPaths.add("mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/ac764aba-2254-4650-b0e8-94bf16cc05d4/image.jpg");

		Assert.assertEquals(locationPaths, expectedLocationPaths);
	}

	/**
	 * mwa パスにロケーションがあるケース
	 * 期待値は mwa パスにロケーション名が取得できること
	 */
	@Test
	public void test_getLocationWithMasterProxyFlag_000() {
		String assetModelStr = "{\"hidden\":false,\"update\":\"2018-02-22T09:36:51.794084Z\",\"type\":\"GENERIC\",\"operator\":\"mwa\",\"clusterName\":\"Library\",\"revisionNote\":\"\",\"create\":\"2018-02-22T09:34:00.225929Z\",\"id\":\"4d3a6643-94f6-4117-b910-22221d1802c1\",\"collectionId\":\"\",\"assetInfo\":{\"odsBarcodeIds\":[],\"note\":null,\"rating\":null,\"length\":2269,\"title\":\"test_03\",\"type\":\"AUDIO_VIDEO\",\"numerator\":30000,\"denominator\":1001,\"duration\":68,\"archived\":false,\"frameRate\":29.97,\"startTimecode\":\"66105301\",\"customMetadata\":{},\"copyOdsBarcodeIds\":[],\"startTimecode_msec\":6790867,\"odsCartridgeIds\":[],\"copyOdsCartridgeIds\":[],\"dropFrame\":true,\"status\":\"Online\"},\"events\":[{\"duration\":1,\"customMetadata\":{},\"id\":\"ac764aba-2254-4650-b0e8-94bf16cc05d4\",\"type\":\"KEY_FRAME\",\"title\":\"Autogeneratethumbnail\",\"frame\":0,\"imageLocation\":\"mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/ac764aba-2254-4650-b0e8-94bf16cc05d4/image.jpg\"}],\"owner\":\"mwa\",\"essences\":[{\"extension\":\"MXF\",\"videoInfo\":{\"frameRate\":29.97,\"codec\":\"MPEG-2V\",\"codecProfile\":\"4:2:2@High\",\"bitRate\":50000000,\"bitDepth\":8,\"width\":1920,\"scanType\":\"Interlaced\",\"dropFrame\":true,\"numerator\":30000,\"denominator\":1001,\"height\":1080},\"format\":\"MXF\",\"type\":\"AUDIO_VIDEO\",\"proxy\":false,\"frameCount\":90,\"size\":22689340,\"audioInfo\":{\"channelCount\":8,\"channelInfos\":[{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":1,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":2,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":3,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":4,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":5,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":6,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":7,\"bitDepth\":24,\"samplingRate\":48000},{\"codec\":\"PCM\",\"bitRate\":1152000,\"channelNumber\":8,\"bitDepth\":24,\"samplingRate\":48000}]},\"name\":\"test_03\",\"location\":\"mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/test_03.MXF\",\"id\":\"a0f86ea2-9019-4033-8ae4-c1308b62021c\",\"formatCommercial\":\"SonyXDCAMHD422\",\"primary\":true,\"status\":\"Online\"},{\"proxy\":true,\"extension\":\"vtt\",\"format\":\"WebVTT\",\"name\":\"timecode\",\"location\":\"mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/70d388f0-cf9b-4d2d-aed4-39ef5c30acff/timecode.vtt\",\"id\":\"70d388f0-cf9b-4d2d-aed4-39ef5c30acff\",\"formatCommercial\":\"WebVTT\",\"type\":\"TIMECODE\",\"primary\":false,\"status\":\"Online\"},{\"proxy\":true,\"size\":666657,\"videoInfo\":{\"frameRate\":29.97,\"dropFrame\":true,\"numerator\":30000,\"denominator\":1001},\"name\":\"test_03_proxy\",\"location\":\"mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/test_03_proxy.mp4\",\"id\":\"1dea52b1-add0-4949-b3e9-aa605ae96d27\",\"type\":\"AUDIO_VIDEO\",\"primary\":false,\"status\":\"Online\"}],\"shortcutParentIds\":[],\"updateTime\":1519292211793,\"tags\":[],\"revisionId\":1,\"thumbnailEventId\":\"ac764aba-2254-4650-b0e8-94bf16cc05d4\",\"activities\":[{\"customMetadata\":{},\"id\":\"82bc693f-41e7-4ac4-8504-0ffc5e0e1a5a\"}],\"location\":{},\"metadataSchemaId\":\"generic\",\"importTime\":1519292040209,\"parents\":[\"fd0dd80b-6968-47b9-8898-7b9e8c7abec7\"]}";
		JsonObject assetModel = new JsonObject(assetModelStr);
		Map<String, String> locationPaths = AssetUtils.getLocationWithMasterProxyFlag(assetModel);
		Assert.assertEquals(locationPaths.get("mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/test_03.MXF"), "MASTER");
		Assert.assertEquals(locationPaths.get("mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/70d388f0-cf9b-4d2d-aed4-39ef5c30acff/timecode.vtt"), "MASTER");
		Assert.assertEquals(locationPaths.get("mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/test_03_proxy.mp4"), "PROXY");
		Assert.assertEquals(locationPaths.get("mwa://DEFAULT/4d3a6643-94f6-4117-b910-22221d1802c1/ac764aba-2254-4650-b0e8-94bf16cc05d4/image.jpg"), "MASTER");
	}

	/**
	 *
	 * basePathに対してcreateCount数分、Renameを行ったPathを作成します.
	 * createCountが1の場合、basePathのみ、
	 * createCountが2の場合、basePathとbasePathのファイル名に(1)を付与したPathが返却されます
	 *
	 * @param basePath
	 * @param createCount
	 * @return
	 */
	private List<Path> createRenamePath(String basePathString, int createCount) {
		List<Path> pathList = new ArrayList<>();
		Path basePath = Paths.get(basePathString);
		if (createCount <= 0) {
			return null;
		} else if (createCount == 1){
			pathList.add(basePath);
			return pathList;
		} else {
			pathList.add(basePath);

			String parentPath = basePath.getParent().toString();
			String fileName = basePath.getFileName().toString();
			int extensionStartPosition = fileName.lastIndexOf(".");
			String extension = fileName.substring(extensionStartPosition);
			String noExtensionfile =  fileName.substring(0, extensionStartPosition);

			final int start = 1;
			for (int count = start; count < createCount; count++) {
				Path renamePath = Paths.get(parentPath, noExtensionfile + "(" + count + ")" + extension);
				pathList.add(renamePath);
			}
		}

		return pathList;
	}

}
