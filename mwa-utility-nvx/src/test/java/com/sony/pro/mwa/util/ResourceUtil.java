package com.sony.pro.mwa.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * JUnitテスト実装時に必要なリソースの取得処理などはここに集約してください。
 */
public class ResourceUtil {

	/***
	 * filenameで指定されたJSONファイルのテキストを取得する。
	 * 
	 * ※TODO削除予定コメント
	 * おかしな話ですが、test/resourceに含めてしまうと、jarのトップにファイルが配置されてしまい困るので
	 * あくまでも「テスト用」ということで、このクラスと同階層に含めることにしました。
	 * なんかいい方法ないかしら。。。。
	 * 
	 * @param filename
	 * @return
	 * @throws Exception
	 */
	public StringBuffer getJsonTextResource(String filename) throws Exception {
		BufferedReader reader = null;
		StringBuffer buffer = new StringBuffer();
		
		try {
			// 出来ればやめたいパス生成
			String pkgName = ResourceUtil.class.getPackage().getName();
			Path absPath = Paths.get(
					new File("").getAbsoluteFile().toString(),
					"target\\test-classes",
					pkgName.replace(".", "\\"),
					filename);			

			// 1.8にしたいですね。。
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(absPath.toFile())));
			
			String line;
		
			while ((line = reader.readLine()) != null) {
				buffer.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			reader.close();
		}
		return buffer;
	}
}
