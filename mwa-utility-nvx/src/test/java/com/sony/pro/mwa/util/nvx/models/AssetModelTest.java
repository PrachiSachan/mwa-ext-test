package com.sony.pro.mwa.util.nvx.models;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.util.ResourceUtil;

/***
 * AssetModelの単体テスト
 */
public class AssetModelTest {

	private ResourceUtil resUtils = new ResourceUtil();
	
	@Test
	public void audio_video_sampleを正しく読み込めること() throws Exception {
		StringBuffer buffer = resUtils.getJsonTextResource("audio_video_sample.json");
		System.out.println(buffer.toString());
	}
	@Test
	public void image_sampleを正しく読み込めること() throws Exception {
		StringBuffer buffer = resUtils.getJsonTextResource("image_sample.json");
		System.out.println(buffer.toString());
	}
	@Test
	public void subtitle_sampleを正しく読み込めること() throws Exception {
		StringBuffer buffer = resUtils.getJsonTextResource("subtitle_sample.json");
		System.out.println(buffer.toString());
	}
	
	@Test
	public void AssetJsonデータのプロパティを検索するためのサンプル() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		try {
			// JsonNodeにマッピングすることが出来るので、専用のPOJOは用意せず、これを活用しましょう。
			JsonNode stub = mapper.readValue(
					resUtils.getJsonTextResource("audio_video_sample.json").toString(), 
					JsonNode.class);

			// 属性の値も一致させて、、というわけにはいかないようですが、多少は楽が出来そうです（一意の名前でないとトラブルがあるようですが）。
			// JsonPointerも使えそう。。？
			// これなら、single-assetsでもassetsでも、どちらでも対応できそうですね（_source以外は）
			// JsonNodeが提供する機能は、少し深堀していきましょう。vertxのパーサより楽そうです。
			JsonNode _source = stub.findPath("_source");
			JsonNode assetInfo = stub.findPath("assetInfo");
			JsonNode events = stub.findPath("events");
			JsonNode essences = stub.findPath("essences");
			
			
			System.out.println("Wao");

		} catch (Exception e) {
			
		}
	}
	
}
