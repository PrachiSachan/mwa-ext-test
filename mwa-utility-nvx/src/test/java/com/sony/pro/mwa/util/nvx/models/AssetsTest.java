package com.sony.pro.mwa.util.nvx.models;

import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sony.pro.mwa.util.ResourceUtil;
import com.sony.pro.mwa.util.nvx.asset.models.Asset;
import com.sony.pro.mwa.util.nvx.asset.models.Assets;
import com.sony.pro.mwa.util.nvx.asset.models.Essences;
import com.sony.pro.mwa.util.uri.MwaUri;

/**
 * com.sony.pro.mwa.util.nvx.asset.models.Assetsのテスト
 */
public class AssetsTest {

	private ObjectMapper mapper = new ObjectMapper();
	private ResourceUtil resUtils = new ResourceUtil();

	@Test
	public void Assetsオブジェクトを正しく生成できる() throws Exception {
		// Arrange
		JsonNode stub = this.mapper.readValue(
				resUtils.getJsonTextResource("assets_models_sample.json").toString(), 
				JsonNode.class);
		
		// Action
		Assets assets = new Assets(stub);
		List<Asset> assetList = assets.get();
		
		// Assert
		for (Asset asset : assetList) {
			System.out.println(asset.getAssetId());
		}
	}
	
	@Test
	public void 個々のassetからEssencesを正しく取得できる() throws Exception {
		// Arrange
		JsonNode stub = this.mapper.readValue(
				resUtils.getJsonTextResource("assets_models_sample.json").toString(), 
				JsonNode.class);
		
		// Action
		Assets assets = new Assets(stub);
		List<Asset> assetList = assets.get();
		
		// Assert
		for (Asset asset : assetList) {
			System.out.println(asset.getAssetId());
			
			Essences essences = asset.getEssences();

			List<MwaUri> uris = essences.getLocations();
			
			for (MwaUri uri : uris) {
				System.out.println("\t" + uri.get());
			}
		}
	}
}
